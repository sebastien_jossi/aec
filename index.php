<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- JQuery CDN -->
        <script
        src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <!-- Bootstrap CDN -->
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"
              integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7"
              crossorigin="anonymous">
        <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"
            integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS"
        crossorigin="anonymous"></script>
        <link rel="stylesheet" href="html/CSS/style.css">
        <title>Page de login</title>
    </head>
    <body>
        <header>
            <nav class="navbar navbar-default col-xs-12">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1"
                        aria-expanded="false" style="padding-bottom: 5px;">
                        <span class="sr-only">Barre de navigation</span>
                        <span class="glyphicon glyphicon-menu-hamburger"></span>
                    </button>
                    <a class="navbar-brand" href="#" style="padding-top: 5px;">
                        <img class="img-responsive" id="logoEE" name="logoEE" alt="Logo_EE" src="html/img/LogoAecTexte.png"
                            style="padding-left: 5px;" />
                    </a>
                </div>
                <div>
                    <div class="text-right" style="float: right;">
                        <a class="navbar-brand" href="/index.php" style="padding-top: 5px;">
                            <span class="navbar-brand"  style="padding-left: 5px;">École d'informatique</span>
                            <img class="img-responsive" id="logoEE" name="logoEE" alt="Logo_EE" src="html/img/logoEntrepriseEcole.png"
                                style="padding-top: 5px;" />
                        </a>
                    </div>
                </div>
            </nav>
        </header>
        <section class="container">
            <section class="row">
                <h1 style="margin-bottom: 40px;" class="text-center">Bienvenue sur l'application Suivi des Apprentis !</h1>
                <section class="colxs-12 col-md-8" id="pInfo">
                    <p>Cette application a été conçue pour permettre aux apprentis de voir les compétences à acquérir dans leur formation et a également pour but d'améliorer la formation des années à venir.</p>
                    <p>Pour utiliser l'application "Suivi des apprentis", il faut être connecté à son compte eduge.ch. Pour se connectez, cliquez sur le bouton "Connexion", une popup s'ouvrira.</p>
                </section>
            </section>
        </section>

        <!-- Pour la présentation (à supprimer après)-->
        <section class="container">
            <section class="row text-center">
                <!--<button type="button" class="btn btn-primary" id="floran.stck@eduge.ch">Élève</button>
                <button class="btn btn-primary" id="ronaldo.lrrpnt@eduge.ch">Direction</button>
                <button class="btn btn-primary" id="romain.ssr@eduge.ch">Administrateur</button>-->
                <?php if (isset($_SESSION['email']) && !empty($_SESSION['email'])): ?>
                    <button class="btn btn-primary" onclick="googleLogout()">Déconnexion google</button>
                <?php else: ?>
                    <section id="google" class="g-signin2" data-theme="dark"></section>
                <?php endif; ?>
            </section>
            <h4 id="info" style="color:red; text-align:center; width:100%;"></h4>
        </section>

    </body>
    <script  src="./js/eelauth.js"></script>
    <script  src="./js/utilities.js"></script>
    <script   defer src="https://apis.google.com/js/api.js" onload="this.onload = function () {};EELAuth.EELClientInitialize(updateSigninStatus)" onreadystatechange="if (this.readyState === 'complete') this.onload()"></script>
    <script  src="https://apis.google.com/js/platform.js" defer></script>
    <script>
        let waitTime = 0;
        var attempt;
        $(document).ready(function() {

            $("#google").click(handleAuthClick);
            
            EELAuth.EELClientInitialize(updateSigninStatus);
            // On n'a pas le status signé avant que la librairie 
            // oauth soit initialisé
            // On va appeler de manière récurrente, toutes les secondes
            // pour valider les status
            
            
            attempt = setInterval(ValidateSignedIn, 1000);          
        });

        /**
         * Valider le status de l'utilisateur connecté
         * @remark Si on est connecté, alors on va récupérer
         *         les infos de la personne connectée
         * @return void
         */
        function ValidateSignedIn()
        {
            console.log(EELAuth.isSignedIn());
            if (EELAuth.isSignedIn())
                EELAuth.getUserInfo(onReceiveUserInfo);
            waitTime++;
            if (waitTime > 10)
                clearInterval(attempt);
        }

        function googleLogout()
        {
            //updateSigninStatus(EELAuth.isSignedIn());
            window.location = "./php/logout.php";
        }
        function updateSigninStatus(isSignedIn)
        {
            console.log(isSignedIn);
            if (isSignedIn) {
                EELAuth.getUserInfo(onReceiveUserInfo);
            } else {
                EELAuth.signIn();
            }
        }
        function handleAuthClick(event)
        {
            updateSigninStatus(EELAuth.isSignedIn());
        }
        /*
        function createAccountFromGoogle(email, info)
        {
            $.ajax({
                method: 'POST',
                url: './php/addperson.php',
                data: {
                    'email': email,
                    'firstName': info.firstname,
                    'lastName': info.lastname
                },
                dataType: 'json',
                success: function (data) {
                    switch (data.ReturnCode) {
                        case 0: // tout bon
                            document.location.href = data.Message;
                            break;
                        case 1: // erreur param
                            break;
                        case 2: // erreur id
                            break;
                        case 3: // erreur encodage json
                            break;
                        case 4:
                            document.location.href = data.Message;
                            break;
                    }
                },
                error: function (e) {
                    console.log("Erreur dans la requete d'ajout de compt google dans la base x", e);
                }
            });
        }*/
        function onReceiveUserInfo(info)
        {
            // Email de l'utilisateur
            var email = info.email;

            // Image de profil de l'utilisateur
            var image = info.image;

            // Si l'utilisateur viens de Google
            var fromGoogle = info.fromGoogle;

            // Ici vous pouvez ajouter du code ajax afin de récupérer des informations
            // Et de le comparer à votre base de données
            $.ajax({
                method: 'POST',
                url: './php/getperson.php',
                data: {
                    'email': email,
                    'firstName': info.firstname,
                    'lastName': info.lastname,
                    'idRole': 1
                },
                dataType: 'json',
                success: function (data) {
                    var msg = "";
                    switch (data.ReturnCode) {
                        case 0: // tout bon
                            document.location.href = data.Message;
                            break;
                        case 1: // erreur param
                            msg = "erreur param";
                            break;
                        case 2: // erreur id
                            msg = "erreur id";
                            break;
                        case 3: // erreur encodage json
                            msg = "erreur encodage JSON";
                            break;
                        case 4: // erreur compte invalidé
                            msg = data.Message;
                            break;
                    }
                    $("#info").html(msg);
                },
                error: function (jqXHR) {
                    console.log(jqXHR);
                }
            });
        }

        // Pour la présentation (à supprimer après)
        $(".btn").click(function ()
        {
            var email = $(this).attr("id");
            // Ici vous pouvez ajouter du code ajax afin de récupérer des informations
            // Et de le comparer à votre base de données
            $.ajax({
                method: 'POST',
                url: './php/getperson.php',
                data: {'email': email},
                dataType: 'JSON',
                success: function (data) {
                    switch (data.ReturnCode) {
                        case 0: // tout bon
                            document.location.href = data.Message;
                            break;
                        case 1: // erreur param
                            break;
                        case 2: // erreur id
                            break;
                        case 3: // erreur encodage json
                            break;
                    }
                },
                error: function (jqXHR) {
                    var msg = "";
                    switch (jqXHR.status) {
                        case 404:
                            msg = "page not found. 404";
                            break;
                        case 200:
                            msg = "problem avec json. 200";
                            break;
                    }
                    $("#info").html(msg);
                }
            });
        });

    </script>
</html>