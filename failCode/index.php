<?php
$Path = explode('/',$_SERVER['SCRIPT_NAME'])[1];
if($Path!="aec")
  $Path=  "https://".$_SERVER['HTTP_HOST'] ."/" ;
else
  $Path= "http://".$_SERVER['HTTP_HOST'] ."/" ."aec/". "";
?>
<!DOCTYPE html>
<html lang="en" >

<head>
  <meta charset="UTF-8">
  <title>Erreur 404</title>
  <link href='https://fonts.googleapis.com/css?family=Monoton' rel='stylesheet' type='text/css'>
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
      <link rel="stylesheet" href="<?php echo $Path ?>failCode/css/style.css">
      <script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<!-- Bootstrap CDN -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- CSS EE -->
<link rel="stylesheet" href="<?php echo  $Path ?>html/CSS/style.css"/>
<link rel="stylesheet" href="<?php echo $Path  ?>html/CSS/print.css" media="print"/>
</head>

<body>

  <header>
    <nav class="navbar navbar-default">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1" aria-expanded="false" style="padding-bottom: 5px;">
            <span class="sr-only">Barre de navigation</span> 
            <span class="glyphicon glyphicon-menu-hamburger"></span>
          </button>
          <a class="navbar-brand" href="index.php" style="padding-top: 5px;">
            <img class="img-responsive" id="logoEE" name="logoEE" alt="Logo_EE" src="<?php echo  $Path  ?>html/img/LogoAecTexte.png"/>
          </a>			
        </div>
        <div>
				<div class="text-right" style="float: right;">
					<a class="navbar-brand" href="/index.php" style="padding-top: 5px;">
						<span class="navbar-brand"  style="padding-left: 5px;">École d'informatique</span>
						<img class="img-responsive" id="logoEE" name="logoEE" alt="Logo_EE" src="<?php echo $Path ?>html/img/logoEntrepriseEcole.png" style="padding-top: 5px;"/>
					</a>
				</div>
			</div>
        <div class="collapse navbar-collapse" id="navbar1">	
          <div class="container">
            <ul class="nav navbar-nav container text-center" style="line-height: 52px;">
              <li><a href="<?php echo  $Path  ?>html/aPropos.php"><span class="glyphicon glyphicon-question-sign"></span> À Propos</a></li>	
              <li><a href="<?php echo  $Path  ?>php/logout.php"><span class="glyphicon glyphicon-log-out"></span> Déconnexion</a></li>
            </ul>  
          </div>  	
        </div>
      </div>
    </nav>
</header>

  <div class="board">
  <p id="error">
    erreur
  </p>
  <p id="code">
    404
  </p>
  <p id="goBack" onclick="history.go(-1);">
    retour
  </p>
</div>
<script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
<script  src="<?php echo  $Path  ?>failCode/js/index.js"></script>
</body>

</html>
