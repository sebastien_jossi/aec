<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
require_once '../../php/security.php';
security();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- JQuery CDN -->
<script	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<!-- Bootstrap CDN -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- CSS EE -->
<link rel="stylesheet" href="../CSS/style.css"/>
<link rel="stylesheet" href="../CSS/print.css" media="print"/>
<title>Impression Modulable</title>
</head>
<body>
<?php include_once '../navigation.php'; ?>

<section class="col-lg-2 noprint"></section>
	<section class="col-xs-12 col-lg-8 bodyZone" id="printZone">
		<h1 class="noprint">Aperçu avant impression</h1>
		<section class="container-fluid table-responsive removeRefresh" style="padding-left: 0px; padding-right: 1px;" id="tableData">
			<table class="table table-bordered table-vcenter nocut" id="skillsTable">
				<thead>
					<tr id="domainsCodes">
						<th rowspan="2" id="thSkill">Domaine de compétence</th>
					</tr>
					<tr id="domainsDescriptions">

					</tr>
				</thead>
				<tbody id="bodySkill">
					<tr id="row0">
						<th id="thSkill" rowspan="1000">Compétences opérationelles</th>
					</tr>
				</tbody>
			</table>
		</section>
	</section>
	<section class="container-fluid noprint" id="buttonZone">
		<section class="col-lg-2"></section>
		<section class="col-xs-6 col-lg-4">
			<button class="btn btn-danger" id="btnCancel"><span class="glyphicon glyphicon-remove-sign"></span> Annuler</button>
		</section>
		<section class="col-xs-6 col-lg-4">
			<button class="btn btn-success pull-right" id="btnPrint"><span class="glyphicon glyphicon-print"></span> Imprimer</button>
		</section>
	</section>
</body>
<script src="../../js/libraryDirection.js"></script>
<script src="../../js/displayTableHome.js"></script>
<script src="../../js/colorSystem.js"></script>
<script src="../../js/utilities.js"></script>
<script>
/*$(document).ready(function()
{
	// Récupère les variables de l'URL
	var guidanceId = getUrlVar("guidanceid");
	var yearId = getUrlVar("yearid");
	var degreeId = getUrlVar("degreeid");
	var classroomId = getUrlVar("classroomid");
	var stateId = getUrlVar("stateid");

	completeArray(guidanceId, yearId, degreeId, classroomId, stateId);

}); // end of ready

/* Modifie les liens en ajoutant des données supplémentaires et en créant des évenement click
 * @param degreeId L'id  du degré
 * @param classroomId L'id  de la classe
 * @param yearId L'id  de l'année
 *
 */
/*function modifyURLRedirectionCase(degreeId = -1, classroomId = -1, yearId = -1)
{
	$('#bodySkill > tr').find('td').each(function(tdcase, td)
	{
		var a = $(td).find("a");
		$(a).attr('href', $(td).attr('id'));
		$(a).addClass('avgPlace');

		var link = $(a);
		link.click(function(event)
		{
			event.preventDefault();
			var fakeid = $('#bodySkill > tr > td#' + $(td).attr('id') + "> a").find('b:first').text();
			displayDataSkill($(td).attr('id'), fakeid);
		});
	});
}

/* Affiche les donnés de la compétence en desous du tableau
 * @param linkid L'id de la case du tableau
 *
 */
/*function displayDataSkill(linkid, fakeid)
{
	// Savoir dans quel case le membre de la direction a cliqué
	var domainid = linkid.split("-")[0];
	var skillid = linkid.split("-")[1];

	var guidanceid = getUrlVar("guidanceid");
	var yearid = getUrlVar("yearid");
	var degreeid = getUrlVar("degreeid");
	var classroomid = getUrlVar("classroomid");
	var historic = getUrlVar("historic") == 'false' ? false : true;

	getInfoSkill(guidanceid, domainid, skillid , yearid, degreeid, classroomid, fakeid, true, historic);

}

/* Ajoute un bouton de suppresion sur le tableau de chaque élève et affiche une message box pour demander comfirmation à l'utilisateur
 *
 */
/*function addBtnRemoveInTableStudent()
{
	$('.ArrStudent').each(function(index)
	{
		var sectionid = $(this).attr("id").split("-")[1];
		$(this).addClass('nocut');
		var btn = $('<button class="btn btn-danger btnRemoveDiv noprint btnArrStudent" id="btn/studentZone-'  + sectionid + '">');
		btn.append('<span class="glyphicon glyphicon-remove"></span></button>');
		$(this).prepend(btn);

		btn.click(function(event)
		{
			msgBox("si vous supprimez le tableau, vous devez recharger la page pour l'afficher de nouveau !");
			$("#msgBox").modal('toggle');
			$('#OK').click(function(){
				var zone = $(btn).attr('id').split("/")[1];
				$("#" + zone).remove();

				removeMsgBox();
			});
			$('#Cancel').click(function(){
				removeMsgBox();
			});
		});
	});
}*/

var studentid = <?php echo $_SESSION['id'].";";?>

$(document).ready(function()
{

	$('.ColorTd').hover(function() {
		$('.'+$(this).prop("class").split(" ")[0]).not(':first').addClass('bordered');

		$('#skillsTable').find('tbody:last td:not(.bordered)').css('opacity', '0.7');
	}, function() {
		$('.'+$(this).prop("class").split(" ")[0]).not(':first').removeClass('bordered');
		$('#skillsTable').find('tbody:last td:not(.bordered)').css('opacity', '1');
	});

	/** Récupérer les données de l'utilisateur **/
	var guidanceid = <?php echo $_SESSION['guidance'].";";?>


	/* Appels ajax pour la page */
	get_data('../../php/loadallhierarchybyguidance.php', getDataArray, {'guidanceId' : guidanceid}, true);
	function getDataArray(dataArray)
	{
		displayDomainAndSkills(dataArray, "EvaluationCompetence.php");
		get_data('../../php/loadalldatabystudent.php', initialize, {'studentId' : studentid}, true);
		function initialize(data)
		{
			if(data[0].length <= 0)
			{
				get_data('../../php/initializedata.php', addColorSystem, {'studentId' : studentid, 'guidanceId' : guidanceid }, false);
				addColorSystem();
			}
			else
			{
				dataArray[0].domains.forEach(function(domain)
				{
					domain.skills.forEach(function(skill)
					{
						skill.practices.forEach(function(practice)
						{
							var valid = false;
							data.forEach(function(evalArray)
							{
								evalArray[0].datapractices.forEach(function(eval)
								{
									if(eval.domainid == domain.domainid && eval.skillid == skill.skillid && eval.practiceid == practice.practiceid)
									{
										valid = true;
									}
								});
							});

							if(!valid)
							{
								var studentid= <?php echo $_SESSION['id']." ;";?>
								var o = {"id": domain.domainid + "-" + skill.skillid + "-" + practice.practiceid, "val": "1", "workshop": new Array()};
								var ar = new Array();
								ar.push(o);
								get_data('../../php/adddatastudent.php',process,{ 'answers': ar, 'studentid': studentid, 'guidanceid': guidanceid },true);
								function process(data)
								{
									return false;
								}
							}
						});
					});
				});
				addColorSystem();
			}
		}
	}
});

function addColorSystem()
{
	get_data('../../php/loadalldatabystudent.php', renameTableStudent, {'studentId' : studentid}, true,studentid);
}
// Renomme le tableau pour pouvoir mettre le système de couleur
function renameTableStudent(arDataStudent, studentid)
{
	var guidanceId = 1;
	$('#bodySkill').attr('id', 'student' + studentid);
	getPracticesForColorSystem(arDataStudent, guidanceId);
}

var oldPrintFunction = window.print;

window.print = function () {
	console.log('Gonna do some special stuff');
	$('#E-1').css('background-color', 'red');
    oldPrintFunction();
};

$("#btnPrint").click(function()
{
	// Evite bug de la page blanche
	$("#printZone").removeClass("col-lg-8");
	window.print();
	$("#printZone").addClass("col-lg-8");
});

$("#btnCancel").click(function(){
	window.location.href = "AccueilApprenti.php";
});
</script>
</html>
