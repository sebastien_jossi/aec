<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
require_once '../../php/security.php';
security();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- JQuery CDN -->
    <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
    <!-- Bootstrap CDN -->
    <link
            href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
            rel="stylesheet"
            integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
            crossorigin="anonymous">
    <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>
    <!-- CSS EE -->
    <link rel="stylesheet" href="../CSS/style.css"/>
    <title>Page d'évaluation d'une compétence</title>
</head>
<body>
<?php include_once '../navigation.php'; ?>

<div id="message" style="display: none;"></div>
<div class="col-md-2"></div>
<div class="container col-md-8 col-xs-12" id="tablepractice">
    <div class="container-fluid">
        <div class="col-xs-12" style="margin-top: 20px;">
            <h4 style="margin-bottom: 5px;">Définition de la compétence :</h4>
            <p id="defSkill"></p>
            <p class="text-justify"></p>
        </div>
    </div>
    <div class="container-fluid">
        <div class="col-xs-12 table-responsive">
            <table class="table table-bordered table-vcenter">
                <tr>
                    <th class="text-center">Compétence méthodologique</th>
                    <th class="text-center">Compétence sociale</th>
                    <th class="text-center">Compétence personelle</th>
                </tr>
                <tr id="mspSkill"></tr>
            </table>
        </div>
    </div>

    <div class="container-fluid">
        <div class="col-xs-12 table-responsive">
            <table class="table table-bordered table-vcenter" id="practices">
                <thead>
                <tr>
                    <th class="text-center col-xs-4" rowspan="2">Pratique
                        professionelle
                    </th>
                    <th class="text-center" rowspan="2"><label>Pas vu</label></th>
                    <th class="text-center" colspan="3">Contrôle des objectifs</th>
                    <th class="text-center" rowspan="2">Fait dans quels cours ?</th>
                    <th class="text-center" rowspan="2">Date de la dernière
                        modification
                    </th>
                </tr>
                <tr>
                    <th class="text-center">Expliqué</th>
                    <th class="text-center">Exercé</th>
                    <th class="text-center">Autonome</th>
                </tr>
                </thead>
                <tbody id="bodypractices"></tbody>
            </table>
        </div>

        <div class="col-xs-12">
            <button id="discardChange" name="discard"
                    class="btn btn-danger col-md-2 col-xs-12 pull-left">
                <span class="glyphicon glyphicon-remove"></span> Annuler
            </button>

            <button id="submitTable" name="submit"
                    class="btn btn-success col-md-2 col-xs-12 pull-right">
                <span class="glyphicon glyphicon-ok"></span> Valider
            </button>
        </div>
    </div>
    <div class="container-fluid" style="margin-top: 40px;">
        <div class="col-xs-12">
            <label id=""><h4>Si vous avez un commentaire, écrivez-le ci-dessous :</h4></label>
        </div>
        
        <div class="col-xs-12 table-responsive">
            <table class="table table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th class="text-left col-xs-4" colspan="3">Vos commentaires sur cette pratique professionnelle</th>
                    </tr>
                </thead>
                <tbody id="userComments">

                </tbody>
            </table>
        </div>
        
        <div class="col-xs-12">
				<textarea rows="2" style="resize: none;" id="comments"
                          class="col-md-10 col-xs-12"></textarea>
            <button name="comment" id="submitComment"
                    class="btn btn-primary col-md-2 col-xs-12" style="height: 45px;">
                Envoyer le commentaire <span class="glyphicon glyphicon-comment"></span>
            </button>
            
        </div>
    </div>
</div>
</body>
<script type="text/javascript" src="../../js/utilities.js"></script>
<script type="text/javascript" src="../../js/libraryDirection.js"></script>
<script type="text/javascript">

    var radioButtonChanged = false;
    var degreeid =<?php echo $_SESSION['degree'] . " ;";?>;
    var guidanceid = <?php echo $_SESSION['guidance'] . " ;";?>
    var domainid = getUrlVar("domainid");
    var skillid = getUrlVar("skillid");
    var entryWorkshopsArray = [];
    var dynamicW = [];
    let workshopsByLine = [];

    var studentid = <?php echo $_SESSION['id'] . " ;";?>

        $(document).ready(function () {
            get_data('../../php/getskill.php', displaySkill, {
                'guidanceId': guidanceid,
                'domainId': domainid,
                'skillId': skillid
            }, true);

            get_data('../../php/getcommentbyskill.php', displayComment, {
                'guidanceId': guidanceid,
                'domainId': domainid,
                'skillId': skillid
            }, true);

            get_data('../../php/getpractices.php', process, {
                'guidanceId': guidanceid,
                'domainId': domainid,
                'skillId': skillid
            }, true, studentid, degreeid, domainid);

            function process(data, studentid, degreeid, domainid) {
                    displayPractices(data, domainid, studentid);
                    addworkshops(degreeid, studentid);             
            }
        }); // end of ready

    function dynamicWorkshop(data, modal) {
        skillid = modal;
        if (data[skillid-1] !== undefined) {
            get_data('../../php/getworkshops.php', disp, {
                    'workshops': data[skillid - 1][0],
                }, true
            );
        }
        function disp(data) {
            $("#tdworkshop"+skillid + "> div").html(" ")
            if(data.names !=null){
                data.names.forEach(function(element) {
                $("#tdworkshop"+skillid + "> div").append(element.NAME+"<br>");
                });  
            }
                  
        }
    }

    //Permet d'afficher un message si quelque chose à changer dans la page et que l'élève veut quitter
    $(window).bind('beforeunload', function() {  
        if (radioButtonChanged)
           return "si vous quittez cette page, vous pedrez toutes les données non sauvegardées !";        
    });

    /**
    * Affiche les commentaires pour la pratique professionnelle
    *
    * @param data Données reçues de l'appell ajax
    */
    function displayComment(data) {
        $('#userComments').empty();
        data.forEach(function(comment) {
           // console.log(comment);
            if (<?= $_SESSION['id'] ?> == comment.student) {
                var com = '<tr><td>' + comment.commentary + '</td>';
                com += '<td class="text-right" style="max-width: 5%; min-width: 5%; width: 5%;">' + comment.dateComment + '</td>';
                com += '<td class="text-center" style="max-width: 3%; min-width: 3%; width: 3%;"><button class="btn btn-danger" type="button" onclick="deleteComment(' + comment.commentId + ')">Supprimer</button></td></tr>';


                $('#userComments').append(com);
            }
        });
    }

    /**
     * Supprime le commentaire en fonction de l'id
     * 
     * @param commentId Id du commentaire
     */
    function deleteComment(commentId) {
        $('.btn-delete').prop('disabled', false);
        get_data('../../php/deleteCommentById.php', deleteCommentStatus, {
            'commentId': commentId,
        }, true);
        //console.log("delete" + commentId);
    }
    
    function deleteCommentStatus(data) {
        displayMessage(data, 0);

        get_data('../../php/getcommentbyskill.php', displayComment, {
            'guidanceId': guidanceid,
            'domainId': domainid,
            'skillId': skillid
        }, true);
    }

   
    /**
     * @brief Cette fonction permet d'ajouter les ateliers
     *
     * @param degreeid l'identifiant du degré de l'élève
     * @param studentid l'identifiant de l'élève
     */
    function addworkshops(degreeid, studentid) {

        console.log('%c Degree id', 'font-weight: bold; color: #45b748', degreeid);
        get_data('../../php/getWorkshopByDegree.php', displayWorkshop, {'degreeId': degreeid,'domainId':domainid,'practiceId':skillid,'skillCode':2}, true, studentid);

    }

    /**
     * @brief Cette fonction permet de charger toutes les données de l'élève
     *
     * @param domainid l'identifiant du domaine
     * @param studentid l'identifiant de l'élève
     */
    function datastudent(studentid, domainid) {
        get_data('../../php/loadalldatabystudent.php', fillSkillPage, {'studentId': studentid}, true);
    }

    // Lorsque le bouton "annuler" est cliqué
    $("#discardChange").click(function () {
        document.location.href = "../VueEleve/AccueilApprenti.php";
    });

    // Lorsque le bouton pour valider l'évaluation est cliqué
    $("#submitTable").click(function () {
        radioButtonChanged = false;
        var valid = false;

        // Boucle à travers toutes les pratiques professionnelles
        $("#bodypractices").find('tr').each(function (index) {
            var input = $(this).find("input[type='radio']:checked");

            if ($(input).length) {
                // Si le radio bouton "pas vu"
                if ($(input).val() == '1') {
                    valid = true;
                    var modal = $(this).find("a").attr('data-target').split("-")[1];
                    $('#tableWorkshopPopUp' + modal + ' > .workshopContent > tr > td').find('input').each(function (index, input) {
                        $(input).prop('checked', false);
                    });
                }

                // Si le radio bouton "Expliqué", "Excercer" ou "Autonome" est séléectionné
                if ($(input).val() == '2' || $(input).val() == '3' || $(input).val() == '4') {
                    var modal = $(this).find("a").attr('data-target').split("-")[1];
                    var count = 0;

                    // Boucle à travers les ateliers sélectionner dans la modal
                    $('#tableWorkshopPopUp' + modal + ' > .workshopContent > tr > td').find('input').each(function (index, input) {
                        if ($(input).prop('checked'))
                            count++;
                    });

                    // Si il y a au moins un atelier de sélectionner
                    if (count > 0)
                        valid = true;
                    else {
                        var trName = $(input)[0].name.split('-');

                        var domainId = trName[0];
                        var practiceId = trName[1];
                        var skillCode = trName[2];

                        get_data('../../php/getworkshops.php', validatePractices, {
                            'practiceId': practiceId,
                            'domainId': domainId,
                            'skillCode': skillCode
                        }, true)

                        function validatePractices(data) {
                            if (data.entry > 0)
                                valid = true;
                            else
                                valid = false;
                        }
                    }
                }

                if (!valid)
                    return false;
            }
            else {
                valid = false;
                return false;
            }
        });

        if (valid) {
            var studentid = <?php echo $_SESSION['id'] . " ;";?>
            var ar = new Array();

            // Boucle à travers les pratiques professionnelles
            $('#bodypractices .practices').each(function (index) {
                var id = $(this).attr('id');
                var practiceid = id.split("-")[2];
                var checkValue = "";
                var sel = $(this).find("input[type='radio']:checked");

                if (sel.length > 0) {
                    checkValue = sel.val();
                }

                var o = {"id": id, "val": checkValue, "workshop": new Array()};

                // Boucle à travers les ateliers qui sont dans le modal
                $(".workshopContent").each(function (index, modal) {
                    var modalid = $(modal).attr("id");

                    // Vérifie si la modal est liée à la pratique professionnelle
                    if (modalid == practiceid) {
                        // Vérifie si un atelier est sélectionner et l'ajoute dans une tableau
                        $('#' + modalid + '> tr > td').find('input').each(function (index, checkbox) {
                            if ($(checkbox).is(":checked"))
                                o.workshop.push($(checkbox).val());
                        });
                    }
                });

                if (o.workshop.length == 0)
                    o.workshop = null;

                ar.push(o);
            });

            console.log('%c All selected workshops', 'font-weight: bold; color: #e88f12', ar);
            var arrayMergeResult = [];
            for (var i = 0; i <= entryWorkshopsArray.length - 1; i++) {
                if (ar[i].val == "1") {
                    arrayMergeResult.push({"id": ar[i].id, "val": ar[i].val, "workshop": ""})
                }
                else {
                    if (ar[i].workshop != null) {
                        if (ar[i].workshop.length >= entryWorkshopsArray[i].workshop.length) {
                            var tbWorkshop = [];
                            for (var j = 0; j <= ar[i].workshop.length - 1; j++) {
                                tbWorkshop.push(ar[i].workshop[j]);
                            }
                            arrayMergeResult.push({"id": ar[i].id, "val": ar[i].val, "workshop": tbWorkshop})
                        }
                        else {
                            var tbWorkshop = [];
                            if (ar[i].workshop.length > 1) {
                                for (var j = 0; j <= entryWorkshopsArray[i].workshop.length - 1; j++) {
                                    if (ar[i].workshop.includes(entryWorkshopsArray[i].workshop[j])) {
                                        tbWorkshop.push(entryWorkshopsArray[i].workshop[j]);
                                    }
                                }
                            } else {
                                tbWorkshop.push(ar[i].workshop[0]);
                            }
                            arrayMergeResult.push({"id": ar[i].id, "val": ar[i].val, "workshop": tbWorkshop})
                        }
                    }
                    else {
                        valid = false;
                    }
                }
            }
            console.table(arrayMergeResult);

            if (valid) {
                get_data('../../php/adddatastudent.php', process, {
                    'answers': arrayMergeResult,
                    'studentid': studentid,
                    'guidanceid': <?php echo $_SESSION['guidance']?>
                }, true);
            }
            else {
                displayMessage("Veuillez renseigner toutes les pratiques professionnelles vues et les ateliers. Sinon laissez pas vu", 1);
            }

            /**
             * @brief Permet d'afficher un message de status de validation et de rediriger l'élève
             *
             * @param data Reponse JSON
             */
            function process(data) {
                    document.location.href = "../VueEleve/AccueilApprenti.php";
            }
        }
        else {
            displayMessage("Veuillez renseigner toutes les pratiques professionnelles vues et les ateliers. Sinon laissez pas vu", 1);
        }
    });

    // Cette fonction permet d'envoyer un commentaire dans la base de données
    $("#submitComment").click(function () {
        var guidanceid = <?php echo $_SESSION['guidance'] . " ;"?>
        var domainid = getUrlVar("domainid");
        var skillid = getUrlVar("skillid");
        var studentid =<?php echo $_SESSION['id'] . ";"?>
        var comment = $("#comments").val();

        get_data('../../php/addcomment.php', comments, {
            'guidanceid': guidanceid,
            'domainid': domainid,
            'skillid': skillid,
            'studentid': studentid,
            'comment': comment
        }, true);

        /**
         * @brief Permet de reinitialiser le formulaire de commentaire, d'afficher un message de status et de rediriger l'élève
         */
        function comments(data) {
            $("#comments").val('');
            $("#comments").attr('readonly', true);

            displayMessage(data, 0);

            get_data('../../php/getcommentbyskill.php', displayComment, {
                'guidanceId': guidanceid,
                'domainId': domainid,
                'skillId': skillid
            }, true);
            setTimeout(function () {
                $("#submitComment").prop("disabled", false);
                $("#comments").attr('readonly', false);

            }, 4000);
        }
    });

    /**
     * @brief Cette fonction permet de remplir la page avec compétences méthodologique, sociale et personnelle
     *
     * @param arSkill le tabelau contenant les compétences
     */
    function displaySkill(arSkill) {
        var defSkill = $('#defSkill');
        var mspSkill = $('#mspSkill');
        // var fakeid = getUrlVar("fakeid");

        arSkill.forEach(function (skill) {
            var bold = $('<b>');
            bold.html(skill.owner + skill.skillid + ': ' + skill.skillname);
            var def = '<br/>' + skill.skilldefinition;
            defSkill.append(bold, def);
            //console.log(bold);

            var m = $('<td class="text-center">'), s = $('<td class="text-center">'), p = $('<td class="text-center">');
            m.html(skill.skillmethodologic);
            s.html(skill.skillsocial);
            p.html(skill.skillpersonnal);
            mspSkill.append(m, s, p);
        });
    }

    /**
     * @brief Cette fonction permet de remplir la page avec les toutes les pratiques professionnelles
     *
     * @param arPractices le tableau content tous les infos du tableau proncipal
     * @param domainid l'identifiant du domaine
     * @param studentid l'identifiant de l'élève
     */
    function displayPractices(arPractices, domainid, studentid) {
        var practices = $('#bodypractices');
        var rbNb = 0;
        //var fakeid = getUrlVar("fakeid");
        if(Array.isArray(arPractices)){         
            arPractices.forEach(function (practice) {
                rbNb++;
                var tr = $('<tr class="practices" id="' + domainid + "-" + practice.owner + "-" + practice.practiceid + '">');
                tr.html('<td >' + domainid + '.' + practice.owner + "." + rbNb + ": " + practice.practicedescription + ".</td>");

                for (var i = 1; i <= 4; i++) {
                    var td = $('<td class="text-center">');
                    var practiceId = domainid + "-" + practice.owner + "-" + practice.practiceid;
                    var rb = $('<input type="radio" name="' + practiceId + '" value="' + i + '">');

                    $(rb).click(function (event) {
                        disabledLink(this);
                        $("#submitTable").attr("disabled", false);
                    });

                    $(rb).change(function () {
                        radioButtonChanged = true;
                    });

                    td.append(rb);
                    tr.append(td);
                    var description = practice.practicedescription;
                    var descsplit = description.split(" ")[1];

                    if (descsplit == "Remarques:") {
                        $(rb).prop('disabled', true);
                        $(td).css('pointer-events', 'none');
                        //$("#comments").attr('readonly', true);
                        //$("#submitComment").attr('disabled', true);
                        $('#comments').remove();
                        $('#submitComment').remove();
                        $("#submitTable").remove();
                    }
                }

                var link = '<td id="tdworkshop' + practice.practiceid + '" class="text-center"><a class="test" id="workshop-' + practice.practiceid + '" data-toggle="modal" onclick="updateModalWorkshops(' + (practice.practiceid - 1) + ')" data-target="#modal-' + practice.practiceid + '"> Sélectionner un atelier</a>';
                tr.append(link);
                practices.append(tr);
                createModal(practice.practiceid, studentid);

                var td = $('<td class="text-center" id="modifyDate">');
                tr.append(td);
            });
        }
        $("#bodypractices > tr > td").click(function () {
            if (($(this).children()).is("input[type='radio']")) {
                if($(this).is(':enabled')) {
                    $(this).children().prop("checked", true);
                    disabledLink($(this).children());
                    $("#submitTable").attr("disabled", false);
                }
                if($(this).children().prop("value") ==1 && !$(this).children().is(":checked")){
                    //console.log("a");

                }
                    

            }
        });
    }

    /**
     * Ajoute les ateliers des années précédentes dans la modal de séléction d'ateliers
     */
    function updateModalWorkshops(lineId) {
        // Récupère la modal appartenant à la line cliqué
        let modalWorkshops = $('#modal-' + (lineId + 1) + ' .workshopContent').find('tr');

        // Liste tous les ids des ateliers étant dans la modal
        let ids = [];
        modalWorkshops.each((key, workshop) => {
            let input = $(workshop).find('input')[0];
            ids.push($(input).val());
        });

        // Ajoute l'atelier des années précédentes s'il n'est pas déjà dans la modal
        if (workshopsByLine.length !== 0) {
            for (let i = 0; i < workshopsByLine[lineId].length; i++) {
                if (!ids.includes(workshopsByLine[lineId][i].workshopid)) {
                    //$('#modal-' + (lineId + 1) + ' .workshopContent').append('<tr class="workshop-row"><td>' + workshopsByLine[lineId][i].workshopname + '</td><td class="text-center" data-children-count="1"><input type="checkbox" name="nomAtelier" class="cb230 checkworkshop" checked value="' + workshopsByLine[lineId][i].workshopid + '" data-degree="4"></td></tr>');
                }
            }
        }
    }

    /**
     * Supprime tout les doubles dans le tableau donner
     */
    function removeDuplicates(myArr, prop) {
        return myArr.filter((obj, pos, arr) => {
            return arr.map(mapObj => mapObj[prop]).indexOf(obj[prop]) === pos;
        });
    }

    /**
     * @brief Cette fonction permet d'afficher les ateliers dans la pop-up
     *
     * @param arWorkshop le tableau contenant tous les ateliers cochés
     * @param studentid l'identifiant de l'élève
     */
    function displayWorkshop(arWorkshop, studentid) {
        arWorkshop = removeDuplicates(arWorkshop, 'workshopname');
        var workshops = $('.workshopContent');
        arWorkshop.forEach(function (workshop) {
            if (degreeid != 0) {
                if (workshop.owner == degreeid) {
                    var tr = $('<tr class="workshop-row">');
                    var ckbox = '<input type="checkbox"name="nomAtelier" class="cb' + workshop.workshopid + ' checkworkshop" value="' + workshop.workshopid + '" data-degree="' + workshop.owner + '">'; // owner -> degreeid
                    tr.html('<td>' + workshop.workshopname + '</td>' + '<td class="text-center">' + ckbox + '</td>');
                    workshops.append(tr);
                }
            } else {
                var tr = $('<tr class="workshop-row">');
                var ckbox = '<input type="checkbox"name="nomAtelier" class="cb' + workshop.workshopid + ' checkworkshop" value="' + workshop.workshopid + '" data-degree="' + workshop.owner + '">'; // owner -> degreeid
                tr.html('<td>' + workshop.workshopname + '</td>' + '<td class="text-center">' + ckbox + '</td>');
                workshops.append(tr);
            }
        });



        // Appel de la fonction pour remplir le tableau avec les données de l'apprenti.
        datastudent(studentid);
    }

    /**
     * @brief Cette fonction permet de remplir la page avec les données de l'apprenti
     *
     * @param arSkillPage le tableau contenant toutes les informations
     */
    function fillSkillPage(arSkillPage) {

        console.log('%c Student data', 'font-weight: bold; color: #e88f12;', arSkillPage);
        arSkillPage[0][0].datapractices.forEach(function(work) {
            if (work.domainid == domainid &&  work.skillid == skillid){    
                worka = [];
                worka.push(work.workshops);
                dynamicW.push(worka);

            }        
        });
        console.log('%c Dynamic workshop', 'font-weight: bold; color: #1195e8;', dynamicW);


        $('#bodypractices > tr > td').find('input').each(function (tdcase, input) {
            arSkillPage[0].forEach(function (student) {
                student.datapractices.forEach(function (data) {
                    var skillId = $(input).attr('name').split("-")[1];
                    var practiceId = $(input).attr('name').split("-")[2];
                    var domainId = $(input).attr('name').split("-")[0];

                    if ((data.practiceid == practiceId) && (data.domainid == domainId) && (data.skillid == skillId) && (data.stateid == $(input).val())) {
                        $(input).prop('checked', true);
                        disabledLink(input);

                        showWorkshop(data.domainid, data.practiceid, data.skillid);

                        data.workshops.forEach(function (workshopid) {
                            $('#tableWorkshopPopUp' + data.practiceid + ' > .workshopContent > tr > td').find('input').each(function (tdcase, input) {
                                if ((workshopid == $(input).attr('value'))) {
                                    $(input).prop('checked', true);
                                    var modal = $("#modal-" + data.practiceid);
                                    setModalWorkshop(modal);
                                }
                            });
                        });

                        var date = $(input).parent().parent().find("#modifyDate");
                        date.html(data.date);
                    }
                });
            });
        });

        console.log('%c Workshops by line', 'font-weight: bold; color: #e88f12', workshopsByLine);

        // Affiche les ateliers des années précédentes dans la modale
        for (let l in workshopsByLine) {
            let line = parseInt(l) + 1;
            let workshops = $('#modal-' + line + ' .workshopContent');

            for (let workshop of workshopsByLine[l]) {
                let tr = $(`<tr class="workshop-row">`);
                let ckbox = '<input type="checkbox"name="nomAtelier" class="cb' + workshop.workshopid + ' checkworkshop" checked value="' + workshop.workshopid + '">'; // owner -> degreeid
                tr.html('<td>' + workshop.workshopname + '</td>' + '<td class="text-center">' + ckbox + '</td></tr>');

                // Test si l'atelier se trouve déjà dans la liste via son id
                let basedCkbox = $(workshops).find('.checkworkshop');
                let ids = [];
                for (let c of basedCkbox) {
                    ids.push($(c).val());
                }

                // Ajoute l'atelier si il n'existe pas encore
                if (!ids.includes(workshop.workshopid)) {
                    workshops.append(tr);
                }
            }
        }

        $("#bodypractices > tr > td > input").parent().click(function () {
            var target = $(".workshopContent > tr > td > input").parent();
            if (target.is('td')) {
                $(this).children().prop("checked", !($(this).children().prop("checked")));
                if($(this).children().is(':enabled')) {
                    $(this).children().prop("checked", true);
                    disabledLink($(this).children());
                    $("#submitTable").attr("disabled", false);
                }
            }
        });
        
        $("#bodypractices > tr > td > input").click(function () {
            $(this).prop("checked", !($(this).prop("checked")));
        });


        // Boucle à travers les pratiques professionnelles
        $('#bodypractices .practices').each(function (index) {
            var id = $(this).attr('id');
            var practiceid = id.split("-")[2];
            var checkValue = "";
            var sel = $(this).find("input[type='radio']:checked");

            if (sel.length > 0) {
                checkValue = sel.val();
            }

            var o = {"id": id, "val": checkValue, "workshop": new Array()};

            // Boucle à travers les ateliers qui sont dans le modal
            $(".workshopContent").each(function (index, modal) {
                var modalid = $(modal).attr("id");

                // Vérifie si la modal est liée à la pratique professionnelle
                if (modalid == practiceid) {
                    // Vérifie si un atelier est sélectionner et l'ajoute dans une tableau
                    $('#' + modalid + '> tr > td').find('input').each(function (index, checkbox) {
                        if ($(checkbox).is(":checked"))
                            o.workshop.push($(checkbox).val());
                    });
                }
            });

            if (o.workshop.length == 0)
                o.workshop = "";

            entryWorkshopsArray.push(o);
        });

       console.log('%c Entry workshops', 'font-weight: bold; color: #4571b7', entryWorkshopsArray);
    }

    /**
     * @brief Cette fonction permet de créer les pop-up pour les ateliers
     *
     * @param practiceid l'identifiant de la pratique
     * @param studentid l'identifiant de l'élève
     */
    function createModal(practiceid, studentid) {
        var modal = '<div class="modal fade" id="modal-' + practiceid + '" role="dialog"> <div class="modal-dialog"><div class="modal-content"> <div class="modal-header"><h4 class="modal-title">Dans cette section, vous devez cocher les ateliers dans lesquels votre compétence apparaît</h4></div><div class="modal-body"><table  id="tableWorkshopPopUp' + practiceid + '" class="table table-responsive table-bordered table-striped" style="width: 100%;"><thead><tr><th colspan="2" style="background-color: #cccccc">Ateliers</th></tr></thead><tbody class="workshopContent" id="' + practiceid + '"></tbody></table></div><div class="modal-footer"><button type="button" class="btn btn-danger" data-dismiss="modal"  style="float: left;">Retour</button></div></div></div></div>';
        $("#tablepractice").after(modal);
    }

    /**
     * @brief Cette fonction permet de créer les pop-up pour les ateliers
     *
     * @param studentid l'identifiant de l'élève
     * @param button le bouton valider de la pop-up
     * @param modalid l'identifiant de la pop-up
     */
    function clearPopup(studentid, button, modalid) {
        get_data('../../php/loadalldatabystudent.php', process, {'studentid': studentid}, true, button, modalid);

        function process(data, button, modalid) {
            var btnOk = null;
            var modal = "#modal-" + modalid;

            if ($(button).attr("type") == "button") {
                $('#bodypractices > tr > td').find('input').each(function (tdcase, input) {
                    data.Data[0].forEach(function (student) {
                        student.datapractices.forEach(function (data) {
                            var skillId = $(input).attr('name').split("-")[1];
                            var practiceId = $(input).attr('name').split("-")[2];
                            var domainId = $(input).attr('name').split("-")[0];

                            if ((data.practiceid == practiceId) && (data.domainid == domainId) && (data.skillid == skillId) && (data.stateid == $(input).val())) {

                                data.workshops.forEach(function (workshopid) {
                                    $('#tableWorkshopPopUp' + data.practiceid + ' > .workshopContent > tr > td').find('input').each(function (tdcase, input) {
                                        if ((workshopid == $(input).attr('value'))) {
                                            $(input).prop('checked', true);
                                        }
                                    });
                                });
                            }
                        });
                    });
                });
                btnOk = $(button).parent().children()[1];
            }
            else
                btnOk = $("#btnOk-" + $(button).attr('name').split("-")[2]);

            $(btnOk).trigger('click');
        }
    }

    function setModalWorkshop(modal) {
        if ($(modal).is('[id^="modal"]')) {
            var modalid = $(modal).attr('id');
        }
        else {
            var modalid = $(modal).parent().parent().parent().parent().attr('id');
        }


        var td = "#tdworkshop" + modalid.split("-")[1];
        var a = $(td).find("#workshop-" + modalid.split("-")[1]);
        var trProfessionalPractice = $(td).parent().attr('id').split('-');
        // console.log(trProfessionalPractice);
        //$(td).empty();
        //$(td).append(a);

        $("#" + modalid + " > .modal-dialog > .modal-content > .modal-body > table > tbody > tr").each(function (index, tr) {
            // console.log(tr);
        });
    }

    /**
     * @brief Permet d'afficher le nom des ateliers que l'élève à sélectionner
     *
     * @param domainid Id du domaine de la pratique professionnelle
     * @param practiceid Id de la pratqiue professionnelle
     * @param skillid Id de la compétence
     */
    function showWorkshop(domainid, practiceid, skillid) {
        var td = practiceid != null ? "#tdworkshop" + practiceid : '';
        var domainId = domainid != null ? domainid : '';
        var practiceId = practiceid != null ? practiceid : '';
        var skillCode = skillid != null ? skillid : '';

        if (td != '' && domainid != '' && practiceid != '' && skillid != '') {
            get_data('../../php/getworkshops.php', showWorkshopsBellowLink, {
                practiceId,
                domainId,
                skillCode
            }, false);
        }


        /**
         * @brief Permet d'afficher les atelier qui ont été sélectionné par l'élève en dessous du lien de sélection
         *
         * @param data Reponse JSON
         */
        function showWorkshopsBellowLink(data) {
            console.log('%c Workshops bellow link', 'font-weight: bold; color: #4db75d', data);
            $(td).append('<div>');
            if (data.workshops != '') {
                data = data.workshops
                
                //console.log('awdouawdzawvgdw', data);
                if (Array.isArray(data)) {
                    let lineWorkshops = [];
                    data.forEach(function (wInfo) {
                        $(td + '> div').append(wInfo.NAME + "</br>");
                        lineWorkshops.push({workshopid: wInfo.ID, workshopname: wInfo.NAME});
                    });
                    workshopsByLine.push(lineWorkshops);
                }
                else
                {
                    $(td + '> div').append(data.NAME + "</br>");
                    workshopsByLine.push([{workshopid: data.ID, workshopname: data.NAME}]);
                }
               
            }
            $(td).append('</div>');
        }
    }

    /**
     * Cette fonction permet de ne pas cliquer sur le lien, qui permet de sélectionner des ateliers, quand le radio bouton est coché sur pas vu
     * @param input le radio bouton qui permet de savoir si il faut disabled le lien
     */
    function disabledLink(input) {
        var studentid = <?php echo $_SESSION['id'] . " ;";?>
        var link = $(input).parent().parent().find("[id^=tdworkshop]").children();
        var modal = $(input).attr('name').split("-")[2];
        if ($(input).val() == 1) {

            $(link).addClass("disabled");
            $(link).css("color", "black");
           
            clearPopup(studentid, input, modal);
             $(input).parent().parent().find("[id^=tdworkshop]").children().not(':first').html("");

        }
        else {
            $(link[0]).css("color", "#337AB7");
            $(link[0]).removeClass("disabled");
            dynamicWorkshop(dynamicW,modal);

        }

        $(link).css("cursor", "pointer");
    }

    // Evenement click pour la selection des ateliers dans les differentes modales
    $(document).on('click', '.workshop-row', function() {
        var target = $(".workshop-row > td > .checkworkshop");

        $(this).children().children().prop("checked", !($(this).children().children().prop("checked")));
        modal = $(this).parent().parent().parent().parent().parent().parent().attr("id").split("-")[1];

        if(dynamicW[modal-1][0].includes($(this).children().children().attr("value"))) {
            var index = dynamicW[modal-1][0].indexOf($(this).children().children().attr("value"));
            dynamicW[modal-1][0].splice(index, 1);
        }
        else {
            dynamicW[modal-1][0].push($(this).children().children().attr("value"));
        }
        radioButtonChanged =true;
        console.log('%c Dynamic workshops', 'font-weight: bold; color: #e88f12;', dynamicW);

        dynamicWorkshop(dynamicW, modal);
    });

    // Evenement click pour la checkbox elle-meme
    $(document).on('click', '.checkworkshop', function() {
        $(this).prop("checked", !($(this).prop("checked")));
    });

</script>
</html>
