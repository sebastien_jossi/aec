<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
require_once '../../php/security.php';
security();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- JQuery CDN -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<!-- Bootstrap CDN -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- Lightbox link -->
<link href="../CSS/lightbox/dist/css/lightbox.css" rel="stylesheet">
<script src="../CSS/lightbox/dist/js/lightbox.js" rel="stylesheet"></script>
<!-- CSS EE -->
<link rel="stylesheet" href="../CSS/style.css" />
<title>Page d'aide de l'apprenti</title>
</head>
<body>
<?php include_once '../navigation.php'; ?>

	<section class="col-xs-0 col-sm-3">
		<section id="tableMatiere">
			<table class="table table-responsive table-bordered table-striped">
				<thead class="thead-inverse">
					<tr>
						<th>Table des matières</th>
					</tr>
				</thead>
				<tbody  id="bodyM">
					<tr>
						<td><a href="#accueil">La page d'accueil de l'apprenti</a></td>
					</tr>
					<tr>
						<td><a href="#evaluationcompetence">La page d'évaluation d'une compétence</a></td>
					</tr>
					<tr>
						<td><a href="#popup">La Pop-Up des ateliers</a></td>
					</tr>
					<tr>
						<td><a href="#codeCouleurs">Le code couleur</a></td>
					</tr>
				</tbody>
			</table>
		</section>
	</section>
	<section class="col-xs-12 col-sm-6">
		<section class="col-xs-12 mrgSection" id="accueil">
			<article class="col-xs-12">
				<article class="thead-inverse fgcAide">
					<h3 class="text-center">La page d'accueil de l'apprenti</h3>
				</article>
				<figure  class="col-xs-12 col-sm-6 col-center-help frgStyle">
					<a href="../img/imgEleve/AccueilApprenti.png" data-lightbox="apprenti">
						<img class="img-responsive" alt="Image de la page d'acceuil" src="../img/imgEleve/AccueilApprenti.png">
					</a>
				</figure>
			</article>
			<article class="col-xs-12">
				<ol>
					<li>La barre de navigation permet d'aller à la page d'aide ou de se déconnecter.</li>
					<li>Ce bouton permet d'imprimer le tableau des compétences avec les couleurs.</li>
					<li>Lors du passage de la souris sur les légendes, met en surbrillance les case correspondantes.</li>
					<li>Les cases du tableau redirigent vers la page d'évaluation d'une compétence.</li>
				</ol>
			</article>
		</section>
		<section class="col-xs-12 mrgSection" id="evaluationcompetence">
			<article class="col-xs-12">
				<article class="thead-inverse fgcAide">
						<h3 class="text-center">La page d'évaluation d'une compétence</h3>
				</article>
				<figure class="col-xs-12 frgStyle">
					<a href="../img/imgEleve/EvaluationCompetence.png" data-lightbox="apprenti">
						<img  class="col-xs-12 col-sm-6 col-center-help frgStyle" alt="Image de la page du récapitulatif d'une compétence" src="../img/imgEleve/EvaluationCompetence.png"/>
					</a>
				</figure>
			</article>
			<article class="col-xs-12">
				<ol>
					<li>La barre de navigation permet d'aller à la page d'accueil, d'aide ou de se déconnecter.</li>
					<li>Affiche les informations sur la compétence sélectionnée à la page d'accueil et ses pratiques professionnelles.</li>
					<li>Ces radioboutons seront là pour évaluer la compétence en question et sur la page d'accueil, la case où la compétence apparaîtra sera coloriée avec une certaine couleur(voir le code couleur), ceci dépend de quel radiobouton vous avez coché. Vous pouvez aussi juste cliquer sur la case qui contient le radiobouton afin que ce dernier soit coché.</li>
					<li>Ouvre une pop-up qui vous permet de sélectionner dans quel(s) atelier(s) vous avez vu la compétence en question.</li>
					<li>Ce bouton vous permet de valider les choix que vous avez fait et un message apparaît pour vous dire que votre évaluation à bien été enregistrée dans la base de données. Pour que vous puissiez valider, vous devez au moins cocher "Pas vu" dans chaque case à cocher, et dans les compétences "Exercé, Expliqué et autonome", vous devez sélectionner au moins un atelier.</li>
					<li>Vous permet de faire un commentaire et vous pouvez l'envoyez en cliquant sur le bouton. Mais attention les commentaires ne servent pas à poser une question, vous devez poster un commentaire seulement si vous voulez faire une remarque !  </li>
				</ol>
			</article>
		</section>
		<section class="col-xs-12 mrgSection" id="popup">
			<article class="col-xs-12">
				<article class="thead-inverse fgcAide">
						<h3 class="text-center">La Pop-Up des ateliers</h3>
				</article>
				<figure class="col-xs-12 frgStyle">
					<a href="../img/imgEleve/PopupAtelier.PNG" data-lightbox="apprenti">
						<img  class="col-xs-12 col-sm-6 col-center-help frgStyle" alt="Image de la popup des atelier" src="../img/imgEleve/PopupAtelier.PNG"/>
					</a>
				</figure>
			</article>
			<article class="col-xs-12">
				<ol>
					<li>Les noms des ateliers viennent de la base de données et s'affichent suivant l'année dans laquelle vous êtes.</li>
					<li>Les cases à cocher vous permettent de cocher dans quel atelier vous avez vu une certaine compétence.</li>
					<li>Le bouton annuler vous permet de revenir à l'écran principal. (Vous pouvez aussi cliquer à côté de la popup)</li>
				</ol>
			</article>
		</section>
		<section class="col-xs-12 mrgSection" id="codeCouleurs">
			<article class="col-xs-12">
				<article class="thead-inverse fgcAide">
					<h3 class="text-center">Le code couleur</h3>
				</article>
				<figure class="col-xs-12 col-sm-6 col-center-help frgStyle" id="fgrcodecouleurs">
					<a href="../img/imgDirection/symboliqueCouleurs.png" data-lightbox="apprenti">
						<img class="img-responsive" alt="Symbolique des couleurs" src="../img/imgEleve/symboliqueCouleurs.png">
					</a>
				</figure>
			</article>
			<article class="col-xs-12">
				<ul>
					<li><font color="#ff3f3f" style="font-weight: bold;">Rouge</font> - Vous n'avez ni vu, ni eu d'expliquation, ni excercé les pratiques.</li>
					<li><font color="#ffa6a6" style="font-weight: bold;">Rose</font> - Vous avez eu des explications pour au moins 1 pratique.</li>
					<li><font color="#ff7400" style="font-weight: bold;">Orange</font> - Vous avez eu des explications pour toutes les pratiques.</li>
					<li><font color="#e2b70b" style="font-weight: bold;">Jaune</font> - Vous avez excercé au moins 1 pratique.</li>
					<li><font color="#9f9800" style="font-weight: bold;">Kaki</font> - Vous avez excercé toutes les pratiques.</li>
					<li><font color="#00ff1e" style="font-weight: bold;">Vert claire</font> - Vous êtes autonome dans au moins 1 pratique.</li>
					<li><font color="#00a714" style="font-weight: bold;">Vert foncé</font> - Vous êtes autonomes dans toutes les pratiques.</li>
				</ul>
			</article>
		</section>
	</section>
	<button class="btn btn-danger col-xs-6 asideHelpPage" id="returnButtonDirectionHelpPage"><a style="color: white!important;"><span class="glyphicon glyphicon-arrow-left text-small-button"></span> <span class="text-large-button">Retour à la page précédente</span></a></button>
			<!--<a class="btn btn-danger col-xs-6" id="returnButtonDirectionHelpPage"><span class="glyphicon glyphicon-arrow-left"></span> Retour à la page précédente</a>-->
</body>
<script>
$('#bodyM > tr > td').click(function()
{
	if("#" + document.location.href.split('#')[1] != $(this).children().attr('href'))
	{
		var nb = typeof $('.asideHelpPage').attr('id') === 'undefined' ? 1 : parseInt($('.asideHelpPage').attr('id')) + 1;
		$('.asideHelpPage').attr('id', nb);
	}
});
$("#returnButtonDirectionHelpPage").click(function()
{
	window.history.back();
});

if ($(document).width() < 755) {
	$(document).scroll(function() {
    
		if ($(document).scrollTop() > 300) {
			$('.asideHelpPage').addClass("fixedToTop");
		} else {
			$('.asideHelpPage').removeClass("fixedToTop");
		}
		
	});
}
</script>
</html>
