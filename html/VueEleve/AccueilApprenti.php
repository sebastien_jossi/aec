<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
require_once '../../php/security.php';
security();

//Reprend message de réussite venant de EvaluationCompetence.php
$message ="";
if(isset($_SESSION["message"]) && $_SESSION["message"]!=null){
	$message =$_SESSION["message"];
	unset($_SESSION["message"]);
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- JQuery CDN -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<!-- Bootstrap CDN -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- CSS EE -->
<link rel="stylesheet" href="../CSS/style.css"/>
<link rel="stylesheet" href="../CSS/print.css" media="print"/>
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
<title>Page d'accueil de l'apprenti</title>
</head>
<body>
<?php include_once '../navigation.php'; ?>

	<section class="col-lg-2 col-md-1"></section>
	<div id="message" style="display: none;"></div>
	<section class="container col-lg-8 col-md-10 col-xs-12">
		<div class="container-fluid row">
			<div class="table-responsive col-lg-10">
				<table class="table table-vcenter">
					<tr>
						<td class="td-pasVu ColorTd" >Tout pas vu
						</td>
						<td class="td-1Exp ColorTd">Au moins 1 expliqué
						</td>
						<td class="td-exp ColorTd">Tout expliqué
						</td>
						<td class="td-1Exc ColorTd">Au moins 1 exercé
						</td>
						<td class="td-exc ColorTd" >Tout exercé
						</td>
						<td class="td-1Aut ColorTd" >Au moins 1 autonome
						</td>
						<td class="td-aut ColorTd">Tout autonome
						</td>
					</tr>
				</table>
			</div>
			<div class="notpadding col-lg-2">
				<button type="button" id="btnPDF" class="btn btn-success noprint" style="width: 100%;">Générer un PDF</button>
			</div>
		</div>
		<div class="container-fluid table-responsive" style="padding-left: 0px; padding-right: 1px;">
			<table class="table table-bordered table-vcenter" id="skillsTable">
				<tr id="domainsCodes">
					<th rowspan="2">Domaine de compétence</th>
				</tr>
				<tr id="domainsDescriptions">

				</tr>
				<tbody id="bodySkill">
					<tr id="row0">
						<th id="thSkill" rowspan="6">Compétences opérationelles</th>
					</tr>
				</tbody>
			</table>
		</div>
		
		<section class="container" style="word-break: break-all;" id="commentRecap"></section>
	</section>
</body>
<!-- JS -->
<script src="../../js/displayTableHome.js"></script>
<script src="../../js/utilities.js"></script>
<script src="../../js/colorSystem.js"></script>
<script src="../../js/libraryDirection.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.min.js"></script>
<script type="text/javascript">
//Affichage du message de réussite venant de EvaluationCompetence.php
var message= "<?php echo $message?>";
if(message!=""){
	displayMessage(message, 0);
}

var studentid = <?php echo $_SESSION['id'].";";?>

$(document).ready(function()
{

	$('.ColorTd').hover(function() {
		$('.'+$(this).prop("class").split(" ")[0]).not(':first').addClass('bordered');

		$('#skillsTable').find('tbody:last td:not(.bordered)').css('opacity', '0.7');
	}, function() {
		$('.'+$(this).prop("class").split(" ")[0]).not(':first').removeClass('bordered');
		$('#skillsTable').find('tbody:last td:not(.bordered)').css('opacity', '1');
	});

	/** Récupérer les données de l'utilisateur **/
	var guidanceid = <?php echo $_SESSION['guidance'].";";?>


	/* Appels ajax pour la page */
	get_data('../../php/loadallhierarchybyguidance.php', getDataArray, {'guidanceId' : guidanceid}, true);
	function getDataArray(dataArray)
	{
		displayDomainAndSkills(dataArray, "EvaluationCompetence.php");
		get_data('../../php/loadalldatabystudent.php', initialize, {'studentId' : studentid}, true);
		function initialize(data)
		{
			if(data[0].length <= 0)
			{
				get_data('../../php/initializedata.php', addColorSystem, {'studentId' : studentid, 'guidanceId' : guidanceid }, false);
				addColorSystem();
			}
			else
			{
				dataArray[0].domains.forEach(function(domain)
				{
					domain.skills.forEach(function(skill)
					{
						skill.practices.forEach(function(practice)
						{
							var valid = false;
							data.forEach(function(evalArray)
							{
								evalArray[0].datapractices.forEach(function(eval)
								{
									if(eval.domainid == domain.domainid && eval.skillid == skill.skillid && eval.practiceid == practice.practiceid)
									{
										valid = true;
									}
								});
							});

							if(!valid)
							{
								var studentid= <?php echo $_SESSION['id']." ;";?>
								var o = {"id": domain.domainid + "-" + skill.skillid + "-" + practice.practiceid, "val": "1", "workshop": new Array()};
								var ar = new Array();
								ar.push(o);
								get_data('../../php/adddatastudent.php',process,{ 'answers': ar, 'studentid': studentid, 'guidanceid': guidanceid },true);
								function process(data)
								{
									return false;
								}
							}
						});
					});
				});
				addColorSystem();
			}
		}
	}

		$.ajax({
			url: "../../php/getAllComments.php",
			type: 'post',
			data: {
				getByStudent: true
			},
			success: function(data) {
				data = JSON.parse(data);
				fillComments(data.allComments);
			}
	});
});

$(document).on("click", '#commentRecap > table > tbody > tr', function() {
	window.location.replace($(this).find('a').attr('href'));
});

function fillComments(comments) {
	$('#commentRecap').html("");
	$('#commentRecap').append('<table class="table table-bordered table-vcenter" style="table-layout:fixed;"><thead><tr><th class="text-center" id="showComment">Les commentaires de la compétence<span class="pull-right glyphicon"></span></th></tr></thead><tbody id="bodyComment">');
	comments.forEach(function(c) {
			$('#commentRecap table tbody').append('<tr title="' + c.DOMAINS_ID + '-' + c.SKILL_CODE + '"><td style="word-wrap:break-word;">' + c.COMMENT + '<p></p><label class="pull-right">' + c.schoolEmail + '</label><a href="/html/VueEleve/EvaluationCompetence.php?guidanceid=' + c.GUIDANCES_CODE + '&domainid=' + c.DOMAINS_ID + '&skillid=' + c.SKILL_CODE +'"></a></td></tr>');
	});
	$('#commentRecap').append('</tbody></table>');
}

function addColorSystem()
{
	get_data('../../php/loadalldatabystudent.php', renameTableStudent, {'studentId' : studentid}, true,studentid);
}
// Renomme le tableau pour pouvoir mettre le système de couleur
function renameTableStudent(arDataStudent, studentid)
{
	var guidanceId = 1;
	$('#bodySkill').attr('id', 'student' + studentid);
	getPracticesForColorSystem(arDataStudent, guidanceId);
}

$('#btnPDF').on('click', function() {
    let data = $($($(document.querySelector('#skillsTable')).parent())[0]).html();
    data = data.replace(/href="([^"]*)"/g, ""); // Remplace tous les href="*" par rien
    $.ajax({
        url: "../../php/impression.php",
        type: 'POST',
        dataType: 'json',
        data: {
            data
        },
        success: function(data) {
            window.open('../../pdf-downloads/' + data.name);
        },
        error: function(err) {
            console.log(err);
        }
    });
});
</script>
</html>
