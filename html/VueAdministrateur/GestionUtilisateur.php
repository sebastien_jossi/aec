<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
require_once '../../php/security.php';
security();


?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- JQuery CDN -->
        <script	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <!-- Bootstrap CDN -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
        <link href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" rel="stylesheet">


        
        <!-- CSS EE -->
        <link rel="stylesheet" href="../CSS/style.css" />
        <title>Gestion des utilisateurs</title>
    </head>
    <body>
    <?php include_once '../navigation.php'; ?>

    <section class="col-lg-2">  </section>
       

        <section class="container col-lg-8 col-xs-12 notpadding" id="bodypage">
            <section class="container-fluid">
            <div id="message"></div>
            <div class="card">
                <div class="card-body bg-secondary text-white">
                <p>Les élèves en formation accélérée ne font pas la 3ème année (année école entreprise). Ils passent donc de la 2ème à la 4ème d'un point de vue théorique.</p>
                <p>Les élèves en formation "3+2" sont considérés comme des élèves en formation standard(4 ans).</p> 
                <p>L'année des élèves est déterminée par sa classe</p>                  

                <p><strong>L'email permet à l'utilisateur de se connecter attention lors de la modification !!! Sinon l'utilisateur ne pourrait plus se connecter !!</strong></p>
                  
                </div>
            </div>
            <div id="DivbtnNewYear">   </div>
           
            
                <section class="container-fluid table-responsive removeRefresh" style="padding-left: 0px; padding-right: 1px;" id="tableData">
                <div class="text-center" id="FiltreRole">
                   
                </div>
                    <input type="hidden" name="RechercheInputValue">

                    <div id="dataTablePersonn">
                   
                    </div>
                    

                </section>
            </section>
        </section>
               <!-- MODAL EDIT -->
               <div class="modal fade" id="ModalEditPersonn" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h2 class="modal-title" id="titleModalEdit" style="text-align: center"> Gestion d'un utilisateur</h2>
                                </div>
                                <div class="modal-body">
                                    <table class="table table-responsive"  style="width: 100%;">
                                        <tbody>
                                            <tr>
                                                <td>Prenom :</td>
                                                <td><input id="prenomPersonne" type="text" name="prenomPersonne" /> </td>
                                            </tr>
                                            <tr>
                                                <td>Nom :</td>
                                                <td><input id="nomPersonne" type="text" name="nomPersonne"/> </td>
                                            </tr>
                                            <tr>
                                                <td>Email :</td>
                                                <td><input id="emailPersonne" type="text" name="emailPersonne"/> </td>
                                            </tr>
                                            <tr>
                                                <td>Actif :</td>
                                                <td>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="actifPersonne" id="CompteActiver" value="1" checked>
                                                    <label class="form-check-label" for="CompteActiver">
                                                        Compte activer
                                                    </label>
                                                </div>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="actifPersonne" id="CompteDesactiver" value="0">
                                                    <label class="form-check-label" for="CompteDesactiver">
                                                    Compte désactiver
                                                    </label> 
                                                </div>                                                  
                                               </td>
                                            </tr>
                                            <tr>
                                                <td>Rôle :</td>
                                                <td><select id="RolePersonne"  name="rolePersonn"></select>
                                            </tr>
                                            <tr class="inputEleve">
                                                <td>Classe :</td>
                                                <td><select id="classPersonne"  name="classPersonne"></select>
                                            </tr>
                                            <tr class="inputEleve">
                                                <td>Année :</td>
                                                <td><input id="anneePersonne" type="text" name="anneePersonne" readonly disabled /> </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal"  style="float: left;">Annuler</button>
                                    <button type="button" value="1" id="btnOkEdit" class="btn btn-success submitmodal" onclick="editPersonn()" data-dismiss="modal" style="float: right;">Valider</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END MODAL EDIT -->

                    <!-- MODAL SUPR -->
               <div class="modal fade" id="ModalSuprPersonn" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h2 class="modal-title" id="titleModalEdit" style="text-align: center"><i class="fas fa-exclamation-triangle"></i> Suprimmer un utilisateur <i class="fas fa-exclamation-triangle"></i></h2>
                                </div>
                                <div class="modal-body">
                                   <p>Voulez-vous vraiment suprimmer l'utilisateur : <strong><span id="UtilisateurSupr"></span></strong></p>
                                   <p><strong>Si vous supprimez un utilisateur toutes les données de l'utilisateur sont supprimé définitivement!!</strong></p>
                                </div>
                                <div class="modal-footer">
                                    <input type="hidden" name="idPersonnSupr" type="text" value="" id="idPersonnSupr">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal"  style="float: left;">Annuler</button>
                                    <button type="button" value="1" id="btnOkEdit" class="btn btn-success submitmodal btn-danger" onclick="deletePersonn()" data-dismiss="modal" style="float: right;"><i class="fas fa-exclamation-triangle"></i> SUPPRIMER <i class="fas fa-exclamation-triangle"></i></button>
                                </div>
                            </div>
                        </div>
                </div>
                <!-- END MODAL SUPR -->


                 <!-- MODAL Nouvelle Année -->
                <div class="modal fade" id="ModalNewYear" role="dialog">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h2 class="modal-title" id="titleModalEdit" style="text-align: center"><i class="fas fa-exclamation-triangle"></i> Nouvelle Année <i class="fas fa-exclamation-triangle"></i></h2>
                            </div>
                            <div class="modal-body">
                                <p>Voulez-vous vraiment finir l'année actuelle et commencer une nouvelle ?</p>
                                <div class="accordion" id="accordionExample">
                                    <div class="card">
                                        <div class="card-header" id="headingOne">
                                            <h5 class="mb-0">
                                                <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                                    Affichez les remarques
                                                </button>
                                            </h5>
                                        </div>
                                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordionExample">
                                            <div class="card-body">
                                                <p>Tout les élèves passeront à l'année supérieur !</p>
                                                <p>Les élèves qui sont actuellement en dernière année(4ème) auront leur compte désactivé (ils ne pourront plus se connecter)</p>
                                                <p>Pour gérer les élèves qui ne passeront pas l'année, il faut simplement modifier leur classe après la création de la nouvelle année </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal" style="float: left;">Annuler</button>
                                    <button type="button" value="1" id="btnNewYear" class="btn btn-success submitmodal btn-danger" onclick="" data-dismiss="modal" style="float: right;"><i class="fas fa-exclamation-triangle"></i> NOUVELLE ANNÉE <i class="fas fa-exclamation-triangle"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END MODAL Nouvelle Année -->

                 <!-- MODAL Nouvelle Année -->
               <div class="modal fade" id="ModalEndCreationNewYear" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h2 class="modal-title" style="text-align: center">La nouvelle année a été créée</i></h2>
                                </div>
                                <div class="modal-body">
                                    <p>La nouvelle année a bien été créée</p>
                                    <p><strong>N'oubliez pas de modifier les élèves qui n'ont pas passé l'année !!!</strong></p>
                                </div>
                                <div class="modal-footer">
                                <button type="button" class="btn btn-secondary  btn-block" data-dismiss="modal"  style="float: left;">Ok</button>

                                </div>
                            </div>
                        </div>
                </div>
                <!-- END MODAL Nouvelle Année -->
    </body>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/jquery.dataTables.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.13/js/dataTables.bootstrap4.min.js"></script>

    <script src="../../js/libraryDirection.js"></script>
    <script src="../../js/utilities.js"></script>
    <script src="../../js/displayTableHome.js"></script>
    <script src="../../js/colorSystem.js"></script>
    <script>
     table = null; 
     order = [1, "asc"];

        /**
         * Charge les données au chargement de la page
         */
        $(document).ready(function () {            
            CreateDataTable();            
            get_data("../../php/getAllPerson.php", DisplayPersonn, undefined, false);          
            get_data("../../php/getclassrooms.php",DisplayClassroomModal,undefined, false);
            get_data("../../php/getRoles.php",DisplayRolesModal,undefined, false);
            get_data("../../php/getyears.php",DisplayButtonNewYear,undefined, false);            
            get_data("../../php/getRoles.php", callBackFiltreRole, undefined, false);
        });
        
        /**
         * Créer une nouvelle année et fait passé les élèves a l'année superieur 
         */
        $("#btnNewYear").click(function() {
            get_data("../../php/addNewYear.php", NewYearIsCreate, undefined, false);      
        });

        /**
         * Permet d'affiche les champs classe / année seulement si role selectionner est "Eleve"
         */
        $( "#RolePersonne" ).change(function() {
            if($(this).val()!=1)
                AfficherClasseAnnee(false);
            else
                AfficherClasseAnnee(true);
        });

        /**
         * Sauvegarde le trier sur les colonnes
         */
        $('#dataTablePersonn').on('draw.dt', function () {
            order = table.order()[0];
        });

        /**
         * CallBack ajax addNewYear 
         * Ouvre une modal de confirmation et refresh les données
         */
        function NewYearIsCreate(data){
            if(data!=null){
                $('#ModalEndCreationNewYear').modal('show');
                refresh();
            }
        }

        /**
         * Remplie la modal pour la modification d'un utilisateur
         */
        function setDataEditPerson(id,prenom,nom,email,actif,idrole,idClasse,annee,isStudent){

            $('input[name=actifPersonne]').prop('checked',false);
            $("#prenomPersonne").val(prenom);
            $("#nomPersonne").val(nom);
            $("#emailPersonne").val(email);
            $("#anneePersonne").val(annee);
            $("#btnOkEdit").val(id);
            $('input[name=actifPersonne][value='+actif+']').prop('checked', "checked");           
            $('#RolePersonne').val(idrole);
            $('#classPersonne').val(idClasse);
            isStudentBool = (isStudent === 'true');           
            AfficherClasseAnnee(isStudentBool);
        }

        /**
         * Remplie la modal pour la suppression d'un utilisateur
         */
        function setDataSuprPerson(id,prenom,nom){
            $("#UtilisateurSupr").html(prenom+" "+nom);
            $("#idPersonnSupr").val(id);            
        }

        /**
         * Remplie le select classes dans la modal modification d'un utilisateur
         */
        function DisplayClassroomModal(data) {
            var select = $("#classPersonne");
            $.each(data, function (index, array) {
                select.append('<option value="' + array.classroomid + '">' + array.classroomname + '</option>');
            });
        }

        /**
         * Remplie le select role dans la modal modification d'un utilisateur
         */
        function DisplayRolesModal(data){
            var select = $("#RolePersonne");
            $.each(data, function(index, array) {	
                select.append('<option value="' + array.codeRole +'">' + array.Name + '</option>'); 
		
            });
        }

        /**
         * Affiche ou pas  les inputs (classe/année) dans la modal modification d'un utilisateur
         */
        function AfficherClasseAnnee(Afficher){
            if(Afficher)
                $(".inputEleve").show();
            else
                $(".inputEleve").hide();
        }  
        
        /**
         * Valide la modif d'un utilisateur onclick sur le modal
         */
        function editPersonn(){            
            prenom = $("#prenomPersonne").val();
            nom = $("#nomPersonne").val();
            email  = $("#emailPersonne").val();
            annee =$("#anneePersonne").val();
            id =$("#btnOkEdit").val();
            actif =  $("input[name='actifPersonne']:checked" ).val()       
            idRole =$('#RolePersonne').val();
            idClasse = $("#classPersonne option:selected").val();
            if(idRole!=1){
                idClasse = null;
                annee = null;
            }
            get_data('../../php/modifyPerson.php', AjaxCallBack, {'idPerson' : id,'firstName' : prenom,'lastName' : nom,'email' : email,'isActive' : actif,'role' : idRole,'inClass' : idClasse,'degreesId' : annee}, true);
        }
       
        /**
         * Refresh les données des utilisateurs dans le tableau(datatable)
         */
        function refresh(){          
            $('input[name=RechercheInputValue]').val($('input[type=search]').val());
            $("#tableWorkshop").DataTable().destroy(true);
            CreateDataTable();
            get_data("../../php/getAllPerson.php", DisplayPersonn, undefined, false);        

        }
        
        /**
         * Créer une DataTable pour afficher les utilisateurs
         */
        function CreateDataTable(){
            $('#dataTablePersonn').append('<table class="table table-striped table-bordered" id="tableWorkshop"><thead><tr id="row0"><th id="">id</th><th id="">Prenom</th><th id="">Nom</th><th id="">Email</th><th id="">Actif</th><th id="">Rôle</th><th id="">Classe</th><th id="">Année</th><th id="">Action</th></tr></thead><tbody id="bodyWork"></tbody></table>');
        }
        
        $("select[name='classPersonne']").change(function() {
            ClassName  = $(this).find("option:selected").text();

            annee = ClassName.match(/\d+/);

            if(annee==3 && ClassName.toLowerCase().indexOf("fa") >= 0){
                annee=4;
            }
            if(annee == null){
                annee  =1;
            }


            $("input[name='anneePersonne']").val(annee);
        });

        function deletePersonn(){
            idPerson= $("#idPersonnSupr").val();
            get_data("../../php/deletePerson.php", AjaxCallBack, {'personId': idPerson}, false);         
        }
        /**
         * CallBack des fonction ajax (deletePerson,modifyPerson)
         */
        function AjaxCallBack(data){
            displayMessage(data, 0);
            refresh();
        }
         //Affichage des btn roles
         function callBackFiltreRole(data){
            data.forEach(element => {
                $("#FiltreRole").append('<button type="button" class=" btnControlRole btn btn-primary col s4" value="'+element.Name+'">'+element.Name+'</button>');
                console.log(element);

            }); 
        }


    </script>
</html>
