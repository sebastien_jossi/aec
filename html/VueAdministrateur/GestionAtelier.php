<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
require_once '../../php/security.php';
security();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- JQuery CDN -->
        <script	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <!-- Bootstrap CDN -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
        <!-- CSS EE -->
        <link rel="stylesheet" href="../CSS/style.css" />
        <title>Accueil Direction</title>
    </head>
    <body>
    <?php include_once '../navigation.php'; ?>

    <section class="col-lg-2"></section>
        <section class="container col-lg-8 col-xs-12 notpadding" id="bodypage">
            <section class="container-fluid">
                <section class="container-fluid table-responsive removeRefresh" style="padding-left: 0px; padding-right: 1px;" id="tableData">
                    <button style="float: right" onclick="setDataEdit('', '', '', 1, 'Add')" class="btn btn-primary" data-toggle="modal" data-target="#modalWorkShopEdit">Ajouter un atelier</button>
                    <br/><br/>
                    <table class="table table-bordered table-vcenter" id="tableWorkshop">
                        <thead>
                            <tr id="row0">
                                <th id="">ID</th>
                                <th id="">Nom atelier</th>
                                <th id="">Degré</th>
                                <th id="">Description</th>
                                <th id="">Opération</th>
                            </tr>
                        </thead>
                        <tbody id="bodyWork">

                        </tbody>
                    </table>
                    <!-- MODAL EDIT -->
                    <div class="modal fade" id="modalWorkShopEdit" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h2 class="modal-title" id="titleModalEdit" style="text-align: center">Modifier un atelier</h2>
                                </div>
                                <div class="modal-body">
                                    <table class="table table-responsive"  style="width: 100%;">
                                        <tbody>
                                            <tr>
                                                <td>Nom :</td>
                                                <td><input id="nameWorkshop" type="text" name="nameWorkshop"/> </td>
                                            </tr>
                                            <tr>
                                                <td>Degré :</td>
                                                <td>
                                                    <select id="degreWorkshop" name="degreWorkshop">
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Description :</td>
                                                <td>
                                                    <textarea id="descWorkshop" rows="5" cols="50" name="descWorkshop"></textarea>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal"  style="float: left;">Annuler</button>
                                    <button type="button" value="1" id="btnOkEdit" class="btn btn-success submitmodal" onclick="editWorkshop()" data-dismiss="modal" style="float: right;">Modifier l'atelier</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END MODAL EDIT -->
                    <div class="modal fade" id="modalWorkShopDelete" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h2 class="modal-title" style="text-align: center">Supprimer un atelier</h2>
                                </div>
                                <div class="modal-body">
                                    Voulez-vous vraiment supprimer cet Atelier ?
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal"  style="float: right;">Annuler</button>
                                    <button type="button" value="1" id="btnOkDel" class="btn btn-success submitmodal" onclick="deleteWorkshop()" data-dismiss="modal" style="float: left;">Oui</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </section>
        </section>
    </body>
    <script src="../../js/libraryDirection.js"></script>
    <script src="../../js/utilities.js"></script>
    <script src="../../js/displayTableHome.js"></script>
    <script src="../../js/colorSystem.js"></script><script>

                                        $(document).ready(function ()
                                        {
                                            get_data("../../php/getWorkshop.php", displayWorkShop, undefined, false);
                                            get_data("../../php/getdegrees.php", setSelect, undefined, false);
                                        });

                                        /**
                                         * Remet a jour le tableau des ateliers
                                         * @param array data
                                         */
                                        function refresh(data)
                                        {
                                            $("#bodyWork").html("");
                                            get_data("../../php/getWorkshop.php", displayWorkShop, undefined, true);
                                        }
                                        /**
                                         * Cette fonction valide l'ajout ou la modif d'un atelier onclick sur le modal
                                         * @param string cmd
                                         *
                                         */
                                        function editWorkshop(cmd)
                                        {
                                            var id = $("#btnOkEdit").val();
                                            var name = $("#nameWorkshop").val();
                                            var desc = $("#descWorkshop").val();
                                            var degre = $("#degreWorkshop").val();
                                            if (cmd == "Edit")
                                            {
                                                get_data("../../php/modifyWorkshop.php", refresh, {'idWorkshop': id, 'nameWorkshop': name, 'descWorkshop': desc, 'degreWorkshop': degre}, true);
                                            } else
                                            {
                                                get_data("../../php/addNewWorkshop.php", refresh, {'nameWorkshop': name, 'descWorkshop': desc, 'degreWorkshop': degre}, true);
                                            }

                                        }
                                        /**
                                         * Cette fonction Valide la suppresion
                                         */
                                        function deleteWorkshop()
                                        {
                                            var id = $("#btnOkDel").val();
                                            get_data("../../php/deleteCurrentWorkshop.php", refresh, {'idWorkshop': id}, true);
                                        }

                                        /**
                                         * Met a jour le modal edit/add
                                         * @param int id
                                         * @param string name
                                         * @param string desc
                                         * @param int degre
                                         * @param string cmd
                                         */
                                        function setDataEdit(id, name, desc, degre, cmd)
                                        {
                                            $("#nameWorkshop").val(name);
                                            $("#descWorkshop").val(desc.replace("<br>", "\n"));
                                            $("#degreWorkshop").val(degre);
                                            $("#btnOkEdit").val(id);
                                            if (cmd == "Edit")
                                            {
                                                $("#titleModalEdit").html("Modifier un atelier");
                                                $("#btnOkEdit").html("Modifier l'atelier");
                                                $("#btnOkEdit").attr("onclick", "editWorkshop(\'Edit\')");
                                            } else
                                            {
                                                $("#titleModalEdit").html("Ajouter un atelier");
                                                $("#btnOkEdit").html("Ajouter l'atelier");
                                                $("#btnOkEdit").attr("onclick", "editWorkshop(\'Add\')");
                                            }

                                        }
                                        /**
                                         * Met a jour le modal supprimer
                                         * @param int id
                                         */
                                        function setDataDel(id)
                                        {
                                            $("#btnOkDel").val(id);
                                        }
                                        /**
                                         * Met à jour le select du degrée
                                         * @param array data
                                         */
                                        function setSelect(data)
                                        {
                                            $.each(data, function (index, value)
                                            {
                                                var option = $('<option>');
                                                option.val(value.degreename);
                                                if (value.degreename == 1)
                                                {
                                                    option.html(value.degreename + "ère");
                                                } else
                                                {
                                                    option.html(value.degreename + "ème");
                                                }
                                                $("#degreWorkshop").append(option);
                                            });

                                        }
    </script>
</html>
