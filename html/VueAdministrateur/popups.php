	<!-- Modal Pop-up addDomain() -->
	<div class="modal fade" id="addDomain" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<div style="display:none;" class="missingField"></div>

				<div class="modal-body">
				<h3 class="modal-title text-center" style="text-align: center; margin-bottom: 30px;">Ajouter un domaine de compétence</h3>
					<label class="col-xs-12 text-center">Nom du domaine de compétence <label style="color: red;">*</label> : </label>
					<textarea  class="form-control addTextarea emptyForbiddenField" id="domainDescription" style="resize: none;" autofocus></textarea>
					<p> Les champs marqués d'un <label style="color: red;">*</label> sont obligatoires</p>
				</div>
				</div>
				<!-- Modal footer-->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger cancelBtn " data-dismiss="modal"
							style="float: left;">Annuler</button>
						<button id="addDomainButton" type="button" class="btn btn-success" style="float: right;">ajouter le domaine de compétence</button>
					</div>
			</div>
		</div>
	</div>
	<!-- END Modal -->


				<!-- Modal Pop-up modifyDomain() -->
	<div class="modal fade" id="modifyDomain" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			<div class="modal-header">
				<div style="display:none;" class="missingField"></div>

				<div class="modal-body">
				<h3 class="modal-title text-center" style="text-align: center; margin-bottom: 30px;">Modifier un domaine de compétence</h3>
				<label class="col-xs-12">ID du domaine de compétence à modifier : </label>
				<select id="modifyDomainModalContent" class="form-control text-center  emptyForbiddenField" autofocus></select>
				<label class="col-xs-12">Nom du domaine de compétence <label style="color: red;">*</label>: </label>
				<textarea id="domainDescritpionOfModifyDomain" class="form-control  emptyForbiddenField" style="resize: none;"></textarea>
				<p> Les champs marqués d'un <label style="color: red;">*</label> ne peuvent pas être vides</p>
				</div>
				</div>
				<!-- Modal footer-->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger cancelBtn " data-dismiss="modal"
							style="float: left;">Annuler</button>
						<button id="modifyDomainButton" type="button" class="btn btn-success" style="float: right;"> modifier le domaine de compétence</button>
					</div>
			</div>
		</div>
	</div>
	<!-- END Modal -->



				<!-- Modal Pop-up deleteDomain() -->
	<div class="modal fade" id="deleteDomain" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
			<div class="modal-header">
			<div style="display:none;" class="missingField"></div>
				<div class="modal-body" id="deleteSkillModalContent">
			 <h3 class="modal-title text-center" style="text-align: center; margin-bottom: 30px;"> Supprimer un ou plusieurs domaine(s) de compétence</h3>

				<label class="col-xs-12 text-center">ID du domaine de compétence à supprimer : </label>
				</div>
				</div>
				<!-- Modal footer-->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger cancelBtn " data-dismiss="modal" style="float: left;">Annuler</button>
						<button id="deleteDomainButton" type="button" class="btn btn-success" style="float: right; ">Supprimer le(s) domaine(s) de compétence</button>
					</div>
			</div>
		</div>
	</div>
	<!-- END Modal -->


	<!-- Modal Pop-up addOperationnalSkill()-->
	<div class="modal fade" id="addOperationnalSkill" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
				<div style="display:none;" class="missingField"></div>

					<div class="modal-body">
								<h3 class="text-center">Ajouter une compétence opérationnelle</h3>
								<div class="form-group">
								<label for="addOperationnalSkillModalContent" class="col-xs-12">ID du domaine</label>
								<select id="addOperationnalSkillModalContent" class="form-control text-center"></select>
								<label for="askillName" class="col-xs-12" >Nom de la compétence  <label style="color: red;">*</label> </label>
								<input class="form-control col-xs-6 addTextarea  emptyForbiddenField" id="askillName" type="text">
								<label for="askillDef" class="col-xs-12">Définition de la compétence <label style="color: red;">*</label></label>
								<textarea class="form-control addTextarea emptyForbiddenField" id="askillDef"></textarea>
								<label for="amethodologicSkill" class="col-xs-12">Compétence Méthodologique</label>
								<textarea class="form-control addTextarea" id="amethodologicSkill"></textarea>
								<label for="asocialSkill" class="col-xs-12">Compétence Sociale</label>
								<textarea class="form-control addTextarea" id="asocialSkill"></textarea>
								<label for="apersonnalSkill" class="col-xs-12">Compétence Personnelle</label>
								<textarea class="form-control addTextarea" id="apersonnalSkill"></textarea>
								<p> Les champs marqués d'un <label style="color: red;">*</label> sont obligatoires</p>

								</div>
					<!-- Modal footer-->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger cancelBtn" data-dismiss="modal"
							style="float: left;">Annuler</button>
						<button id="addOperationnalSkillButton" type="button" class="btn btn-success" style="float: right;">Ajouter la compétence</button>
					</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END Modal -->


		<!-- Modal Pop-up modifyOperationnalSkill()-->
	<div class="modal fade" id="modifyOperationnalSkill" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<div style="display:none;" class="missingField"></div>

					<h3 class="text-center">Modifier une compétence opérationnelle</h3>
					<div class="modal-body">
								<div class="form-group">
								<label for="modifyOperationnalSkillModalContent" class="col-xs-12">Domaine de compétence</label>
									<select class="form-control text-center" id="modifyOperationnalSkillModalContent">
									</select>
									<label for="idSkillToModify" class="col-xs-12">ID de la compétence à modifier</label>
									<select class="form-control text-center" id="idSkillToModify">
									</select>
									<label for="mskillName" class="col-xs-12">Nom de la compétence<label style="color: red;">*</label> </label>
									<textarea class="form-control col-xs-6 emptyForbiddenField" id="mskillName" type="text"></textarea>
									<label for="mskillDef" class="col-xs-12">Définition de la compétence<label style="color: red;">*</label> </label>
									<textarea class="form-control emptyForbiddenField" id="mskillDef"></textarea>
									<label for="mMethodologicSkill" class="col-xs-12">Compétence Méthodologique</label>
									<textarea class="form-control" id="mMethodologicSkill"></textarea>
									<label for="msocialSkill" class="col-xs-12">Compétence Sociale</label>
									<textarea class="form-control" id="msocialSkill"></textarea>
									<label for="mpersonnalSkill" class="col-xs-12">Compétence Personnelle</label>
									<textarea class="form-control" id="mpersonnalSkill"></textarea>
									<p> Les champs marqués d'un <label style="color: red;">*</label> ne peuvent pas être vides</p>

								</div>
					</div>
					<!-- Modal footer-->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger cancelBtn" data-dismiss="modal" style="float: left;">Annuler</button>
						<button id="modifyOperationnalSkillButton" type="button" class="btn btn-success" style="float: right;">Modifier la compétence</button>
					</div>
				</div>
			</div>
		</div>
		</div>
		<!-- END Modal -->


		<!-- Modal Pop-up deleteOperationnalSkill()-->
	<div class="modal fade" id="deleteOperationnalSkill" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
				<h3 class="text-center">Supprimer une compétence opérationnelle</h3>
					<div class="modal-body">
								<div class="form-group">
									<label for="deleteOperationnalSkillModalContent" class="col-xs-12">Domaine de compétence</label>
									<select class="form-control text-center" id="deleteOperationnalSkillModalContent">
									</select>
									<label for="idSkillToDelete" class="col-xs-12">ID de la compétence à supprimer</label>
									<select class="form-control text-center" id="idSkillToDelete">
									</select>
					</div>
				</div>
					<!-- Modal footer-->
						<div class="modal-footer">
						<button type="button" class="btn btn-danger cancelBtn" data-dismiss="modal" style="float: left;">Annuler</button>
						<button id="deleteOperationnalSkillButton" type="button" class="btn btn-success" style="float: right;">Supprimer la compétence</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END Modal -->


	<!-- Modal Pop-up addProfessionnalPractice()-->
	<div class="modal fade" id="addProfessionnalPractice" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<div style="display:none;" class="missingField"></div>

					<div class="modal-body">
							<h3 class="text-center">Ajouter une pratique professionnelle</h3>
								<div id="aProfessionnalPracticeContent">
								<label for="aprofessionnalPracticeFromSkill" class="col-xs-12">Pratique professionnelle de la compétence <label style="color: red;">*</label> :</label>
								<textarea class="form-control addTextarea emptyForbiddenField" id="aprofessionnalPracticeFromSkill"></textarea>
								<p> Les champs marqués d'un <label style="color: red;">*</label> sont obligatoires</p>
								</div>
							</div>

					<!-- Modal footer-->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger cancelBtn" data-dismiss="modal" style="float: left;">Annuler</button>
						<button id="addProfessionnalPracticeButton" type="button" class="btn btn-success" style="float: right;"> Ajouter la pratique professionnelle</button>

					</div>
				</div>
			</div>
		</div>
	</div>
		<!-- END Modal -->


		<!-- Modal Pop-up modifyProfessionnalPractice()-->
	<div class="modal fade" id="modifyProfessionnalPractice" role="dialog">
		<div class="modal-dialog">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<div style="display:none;" class="missingField"></div>
					<div class="modal-body">

					<h3 class="text-center">Modifier une ou plusieurs pratique(s) professionnelle(s) <label style="color: red;">*</label></h3>
						<div id="mProfessionnalPracticeContent"></div>

						<p> <label style="color: red;">*</label>Les champs de cette section ne peuvent pas être vides</p>
						</div>
						</div>
					<!-- Modal footer-->
					<div class="modal-footer">
						<button type="button" class="btn btn-danger cancelBtn" data-dismiss="modal" style="float: left;">Annuler</button>
						<button id="modifyProfessionnalPracticeButton" type="button" class="btn btn-success" style="float: right;">Valider</button>
						</div>

					</div>
				</div>
			</div>
		<!-- END Modal -->


		<!-- Modal Pop-up deleteProfessionnalPractice()-->
	<div class="modal fade" id="deleteProfessionnalPractice" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<div style="display:none;" class="missingField"></div>

					<div class="modal-body">

				<h3 class="text-center">Supprimer une ou plusieurs pratique(s) professionnelle(s)</h3>
				<p> Cochez l'/les élément(s) que vous souhaitez supprimer</p>
						<div id="dProfessionnalPracticeContent"></div>
					</div>
					<!-- Modal footer-->
						<div class="modal-footer">
						<button type="button" class="btn btn-danger cancelBtn" data-dismiss="modal" style="float: left;">Annuler</button>
						<button id="deleteProfessionnalPracticelButton" type="button" class="btn btn-success" style="float: right;">Supprimer</button>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END Modal -->
