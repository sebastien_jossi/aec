<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
require_once '../../php/security.php';
security();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- JQuery CDN -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<!-- Bootstrap CDN -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">

<!-- CSS EE -->
<!-- Lightbox link -->
<link href="../CSS/lightbox/dist/css/lightbox.css" rel="stylesheet">
<script src="../CSS/lightbox/dist/js/lightbox.js" rel="stylesheet"></script>
<!-- CSS EE -->
<link rel="stylesheet" href="../CSS/style.css" />
<title>Administration</title>
</head>
<body id="body">
<div id="message" class="" style="display:none; "></div>
<?php include_once '../navigation.php'; ?>

	<section class="col-md-2"></section>
	<section class="container col-md-8 col-xs-12">
		<div class="container-fluid table-responsive"
			style="padding-left: 0px; padding-right: 1px;">
			<div class="col-md-3 divselect notpadding .espacementG pull-right"
				style="margin-bottom: 15px;">
				<label>Orientation :</label><select class="select form-control guidanceSelect" id="guidanceSelect">
				</select>
			</div>
				<table class="table table-bordered table-vcenter" id="skillsTable">
					<tr id="domainsCodes">
						<th>Domaine de compétence
							<div id="span" style="padding-top: 30px;">
								<a id="deleteDomainGlyphicon" data-toggle="modal" data-target="#deleteDomain" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-trash pull-right pull-bottom" style="color: white;" onclick=""></span></a>
								<a id="modifyDomainGlyphicon" data-toggle="modal" data-target="#modifyDomain" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-pencil pull-right pull-bottom" onclick="" style="margin-right: 10px; color: white;"></span></a>
								<a id="addDomainGlyphicon" data-toggle="modal" data-target="#addDomain" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-plus pull-right pull-bottom" onclick="" style="margin-right: 10px; color: white;"></span></a>
							</div>
						</th>
					</tr>
					<tr id="domainsDescriptions">
					</tr>
					<tr id="row0">
						<th id="thSkill">Compétences opérationelles
							<div id="span" style="padding-top: 30px;">
								<a id="deleteOperationnalSkillGlyphicon" data-toggle="modal" data-target="#deleteOperationnalSkill" data-backdrop="static" data-keybord="false"><span  class="glyphicon glyphicon-trash pull-right pull-bottom" style="color: white;"></span></a>
								<a id="modifyOperationnalSkillGlyphicon" data-toggle="modal" data-target="#modifyOperationnalSkill" data-backdrop="static" data-keybord="false"><span  class="glyphicon glyphicon-pencil pull-right pull-bottom" style="margin-right: 10px; color: white;"></span></a>
								<a id="addOperationnalSkillGlyphicon"data-toggle="modal" data-target="#addOperationnalSkill" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-plus pull-right pull-bottom" style="margin-right: 10px; color: white;"></span></a>
							</div>
						</th>
					</tr>
				</table>
			</div>
			<div id="professionnalPracticesManager">
				<table class="table table-bordered table-vcenter" id="professionnalPracticesTable">
					<tr id="practicesCodes">
						<th id="professionnalPracticesTitle">Pratiques Professionnelles
							<div id="span" style="padding-top: 30px;">
								<p id="professionnalPracticeIndication" style="color: white; text-align: center;" > Cliquer sur une compétence pour gérer les pratiques professionnelles qui lui sont associées</p>
								<a hidden id="deleteProfessionnalPracticeGlyphicon" data-toggle="modal" data-target="#deleteProfessionnalPractice" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-trash pull-right pull-bottom" style="color: white;" ></span></a>
								<a hidden id="modifyProfessionnalPracticeGlyphicon" data-toggle="modal" data-target="#modifyProfessionnalPractice" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-pencil pull-right pull-bottom" style="margin-right: 10px; color: white;"></span></a>
								<a hidden id="addProfessionnalPracticeGlyphicon" data-toggle="modal" data-target="#addProfessionnalPractice" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-plus pull-right pull-bottom" style="margin-right: 10px; color: white;"></span></a>
							</div>
						</th>
					</tr>
				</table>
			</div>
	</section>
	<div id="popUps"></div>
	<div id="info"></div>
</body>
<script src="../../js/displayTableHome.js"></script>
<script src="../../js/utilities.js"></script>
<script src="../../js/autosize.js"></script>
<script src="../../js/libraryDirection.js"></script>
<script type="text/javascript">

	var guidanceid = 1;
	var domaindescritpion = "";
	var domainid = 1;
	var domainidLetter = "A";
	var skillid = 1;
	var fakeid = 1;
	var fieldtorefresh = 0;
	var practiceid = [];
	var message = "";
	var resizeTa = true;

	// Quand la page a fini de se charger
	$(document).ready(function() {
		get_data("../../php/getguidances.php", fillOrientationDropList, params = {}, true);
		ajaxFillDomainTable();
	});

	// Fonction appellée pour le get_data
	//@param message - le message qui sera affiché lors du succès ou l'échec de l'opération
	//@param statut - le statut du message 0:succès; 1:échec, 2:valeurs manquantes
	function groupFunction(data, message, statut){
		hidepopup();
		ajaxFillDomainTable();
		fillPopUps();
		displayMessage(message,statut);
	}

	//Quand le filtre orientation change
	$("#guidanceSelect").change(function() {
		guidanceid = $(this).children(":selected").attr("id");
		ajaxFillDomainTable();
		fillPopUps();
		$('#professionnalPracticesManager').html('<table class="table table-bordered table-vcenter" id="professionnalPracticesTable"><tr id="practicesCodes"><th id="professionnalPracticesTitle">Pratiques Professionnelles<div id="span" style="padding-top: 30px;"><p id="professionnalPracticeIndication" style="color: white; text-align: center;" > Cliquer sur une compétence pour gérer les pratiques professionnelles qui lui sont associées</p><a hidden id="deleteProfessionnalPracticeGlyphicon" data-toggle="modal" data-target="#deleteProfessionnalPractice" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-trash pull-right pull-bottom" style="color: white;" ></span></a><a hidden id="modifyProfessionnalPracticeGlyphicon" data-toggle="modal" data-target="#modifyProfessionnalPractice" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-pencil pull-right pull-bottom" style="margin-right: 10px; color: white;"></span></a><a hidden id="addProfessionnalPracticeGlyphicon" data-toggle="modal" data-target="#addProfessionnalPractice" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-plus pull-right pull-bottom" style="margin-right: 10px; color: white;"></span></a></div></th></tr></table>');
	});

	//détermine le style des checkbox dans deleteDomain
	$(document).on('change', '.checkboxDomainId', function() {
    	if(this.checked) {
    		$('#docheck' + $(this).val()).css({'background-color':'#007ba7', 'color': '#fff'});
    		$('#doglyph' + $(this).val()).removeClass("hidden");
    	}
    	else{
    		$('#docheck' + $(this).val()).css({'background-color':'#fff', 'color': 'black'});
    		$('#doglyph' + $(this).val()).addClass("hidden");
    	}
	});

	//détermine le style des checkbox dans deleteProfessionnalPractices
	$(document).on('change', '.professionnalPracticeCheckbox', function() {
    	if(this.checked) {
    		$('#ppcheck' + $(this).val()).css({'background-color':'#007ba7', 'color': '#fff'});
    		$('#ppglyph' + $(this).val()).removeClass("hidden");
    	}
    	else{
    		$('#ppcheck' + $(this).val()).css({'background-color':'#fff', 'color': 'black'});
    		$('#ppglyph' + $(this).val()).addClass("hidden");
    	}
	});

	//Modifie les liens en ajoutant des données supplémentaires et en créant des évenement click
	function modifyURLRedirectionCase()
	{
		$('#bodySkill > tr').find('td').each(function(tdcase, td){
			var a = $(td).find("a");
			$(a).attr('href', $(td).attr('id'));

			var link = $(a);
			link.click(function(event){
				event.preventDefault();
				sendDataProfessionnalPractice($(td).attr('id'));
			});
		});
	}

	// Savoir dans quel case du tableau l'administrateur a cliqué
	//@linkid Contient le domaine et l'id de la pratique professionnelle séparé par un tiret
	function sendDataProfessionnalPractice(linkid){
		var domainid = linkid.split("-")[0];
		var skillid = linkid.split("-")[1];

		get_data("../../php/getpractices.php", displayProfessionnalsPractices, {'guidanceId' : guidanceid,'domainId' : domainid, 'skillId' : skillid}, true, linkid);

	}

	// Chargement de la structure des pop-ups
	$("#popUps").load("./popups.php", function() {

		// Quand une textArea à le focus, sa hauteur s'ajuste à celle du texte qu'elle contient.
		$('textarea').focus(function(){
			if ($(this)[0].scrollHeight > $(this).innerHeight()) {
				resizeTa = true;
				autosize($(this));
			}
		});

		// Quand une textArea perds le focus, sa hauteur se fixe à 40px.
		$('textarea').focusout(function(){
			resizeTa = false;
			$(this).height(40);
		});

		// rafraichît la pop up modifier un domaine quand le domaine change
		$('#modifyDomainModalContent').change(function(){
			domainidLetter = $('#modifyDomainModalContent option:selected').val();
			fillDomainDescription();
		});

		// rafraichît la pop up modifier une compétence quand le domaine change
		$('#modifyOperationnalSkillModalContent').change(function(){
			domainidLetter = $('#modifyOperationnalSkillModalContent option:selected').val();
			fieldtorefresh = 1;
			fillPopUps();
		});

		// rafraichît la pop up modifier une compétence quand l'id de la compétence change
		$('#idSkillToModify').change(function(){
			fieldtorefresh = 2;
 			fillPopUps();
		});

		// rafraichît la pop up supprimer une compétence quand le domaine de la compétence change
		$('#deleteOperationnalSkillModalContent').change(function(){
			domainidLetter = $('#deleteOperationnalSkillModalContent option:selected').val();
			fieldtorefresh = 1;
			fillPopUps();
		});

		// Ajoute un domaine
		$('#addDomainButton').click(function addDomain(){
			var domaindescritpion = $("#domainDescription").val().trim();
			get_data("../../php/adddomain.php", groupFunction, {'guidanceId' : guidanceid, 'domaindescritpion' : domaindescritpion}, true,"Domaine ajouté avec succès",0);
			fillPopUps();
		});

		// Modifie le domaine
		$('#modifyDomainButton').click(function modifyDomain(){
			var domaindescritpion = $("#domainDescritpionOfModifyDomain").val().trim();
			var domainid = $("#modifyDomainModalContent").val().trim();
			if(domaindescritpion == ""){
				displayMessage("Veuillez remplir les champs marqué d'un *",2);
				return;
			}

			get_data("../../php/modifydomain.php", groupFunction, {'guidanceId' : guidanceid, 'domainId' : domainid, 'domaindescritpion' : domaindescritpion}, true, "Domaine modifié avec succès",0);
			fillPopUps();
		});

		// Supprime le domaine
		$('#deleteDomainButton').click(function deleteDomain(){
			var domainid = [];
		          $('#deleteSkillModalContent').find("input").each(function(test, checkbox){
		        	if($(checkbox).prop('checked') === true)
		        	  	domainid.push($(checkbox).val());
		          });

			  if(domainid.length !== 0){
	              // Reconstruction du menu des pratiques professionnelles
				  $('#professionnalPracticesTable').empty();
				  $('#professionnalPracticesTable').append('<tr id="practicesCodes"><th id="professionnalPracticesTitle">Pratiques Professionnelles<div id="span" style="padding-top: 30px;"><p id="professionnalPracticeIndication" style="color: white; text-align: center;" > Cliquer sur une compétence pour gérer les pratiques professionnelles qui lui sont associées</p><a hidden id="deleteProfessionnalPracticeGlyphicon" data-toggle="modal" data-target="#deleteProfessionnalPractice" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-trash pull-right pull-bottom" style="color: white;" ></span></a><a hidden id="modifyProfessionnalPracticeGlyphicon" data-toggle="modal" data-target="#modifyProfessionnalPractice" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-pencil pull-right pull-bottom" style="margin-right: 10px; color: white;"></span></a><a hidden id="addProfessionnalPracticeGlyphicon" data-toggle="modal" data-target="#addProfessionnalPractice" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-plus pull-right pull-bottom" style="margin-right: 10px; color: white;"></span></a></div></th></tr>');
				  get_data("../../php/deletedomain.php", groupFunction, {	'guidanceId' : guidanceid, 'domainId' : domainid}, true, "Domaine supprimé avec succès",0);
			  }
			  else
				  displayMessage("Veuillez sélectionner au moins un domaine à supprimer.",2);

			  fillPopUps();
		});

		// Ajoute une compétence opérationnelle
		$('#addOperationnalSkillButton').click(function addOperationnalSkill() {
					if($("#addOperationnalSkillModalContent").val().trim()== "")
						var domainid = undefined;

					else
						 var domainid = $("#addOperationnalSkillModalContent").val().trim();

					if($("#idSkillToModify").val()== "")
						var skillid = undefined;

					else
						 var skillid = $("#idSkillToModify").val();

					if($("#askillName").val().trim()== "")
						var skillname = undefined;

					else
						 var skillname = $("#askillName").val().trim();

					if($("#askillDef").val().trim()== "")
						var skilldefinition = undefined;

					else
						 var skilldefinition = $("#askillDef").val().trim();

					var skillmethodologic = $("#amethodologicSkill").val().trim();
					var skillsocial = $("#asocialSkill").val().trim();
					var skillpersonnal = $("#apersonnalSkill").val().trim();

					get_data("../../php/addoperationnalskill.php", groupFunction, { 'guidanceId' : guidanceid, 'domainid' : domainid, 'skillname' : skillname, 'skilldefinition' : skilldefinition, 'skillmethodologic' : skillmethodologic, 'skillsocial' : skillsocial, 'skillpersonnal' : skillpersonnal}, true, "Compétence opérationnelle ajoutée avec succès",0);

					fillPopUps();
		});

		// Modifier une compétence opérationnelle
		$('#modifyOperationnalSkillButton').click(function modifyOperationnalSkill(){
			var domainid = $("#modifyOperationnalSkillModalContent option:selected").val();
			var skillid = $("#idSkillToModify").val();
			var skillname= $('#mskillName').val().trim();
			var skilldefinition= $('#mskillDef').val().trim();
			var skillmethodologic= $('#mMethodologicSkill').val().trim();
			var skillsocial= $('#msocialSkill').val().trim();
			var skillpersonnal= $('#mpersonnalSkill').val().trim();

			if(skillname == "" || skilldefinition == ""){
				displayMessage("Veuillez remplir les champs marqué d'un *",2);
				return;
			}
			if(skillid == null){
				displayMessage("Veuillez sélectionner un id. Si aucun id n'est disponible, veuillez ajouter au moins une compétence pour ce domaine ",2);
				return;
			}

			get_data("../../php/modifyoperationnalskill.php", groupFunction, { 'guidanceid' : guidanceid, 'domainid' : domainid, 'skillid' : skillid, 'skillname' : skillname, 'skilldefinition' : skilldefinition, 'skillmethodologic' : skillmethodologic, 'skillsocial' : skillsocial, 'skillpersonnal' : skillpersonnal}, true, "Compétence opérationnelle modifiée avec succès",0);
			domainidLetter = 'A';
			fieldtorefresh = 1;
			fillPopUps();
		});

		// Suprrime une compétence opérationnelle
		$('#deleteOperationnalSkillButton').click(function deleteOperationnalSkill(){
			var domainid = $('#deleteOperationnalSkillModalContent').val();
			var skillid = $('#idSkillToDelete').val();

			get_data("../../php/deleteoperationnalskill.php", process, { 'guidanceId' : guidanceid, 'domainid' : domainid, 'skillid' : skillid}, true);


			function process(){
				$('#professionnalPracticesTable').empty();
				$('#professionnalPracticesTable').append('<tr id="practicesCodes"><th id="professionnalPracticesTitle">Pratiques Professionnelles<div id="span" style="padding-top: 30px;"><p id="professionnalPracticeIndication" style="color: white; text-align: center;" > Cliquer sur une compétence pour gérer les pratiques professionnelles qui lui sont associées</p><a hidden id="deleteProfessionnalPracticeGlyphicon" data-toggle="modal" data-target="#deleteProfessionnalPractice" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-trash pull-right pull-bottom" style="color: white;" ></span></a><a hidden id="modifyProfessionnalPracticeGlyphicon" data-toggle="modal" data-target="#modifyProfessionnalPractice" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-pencil pull-right pull-bottom" style="margin-right: 10px; color: white;"></span></a><a hidden id="addProfessionnalPracticeGlyphicon" data-toggle="modal" data-target="#addProfessionnalPractice" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-plus pull-right pull-bottom" style="margin-right: 10px; color: white;"></span></a></div></th></tr>');
				groupFunction("","Compétence opérationnelle supprimée avec succès",0);
			}
			domainidLetter = 'A';
			fieldtorefresh = 1;
			fillPopUps();
		});

		// Ajoute une pratique professionnelle
		$('#addProfessionnalPracticeButton').click(function addProfessionnalPractice(){
			if($("#aprofessionnalPracticeFromSkill").val().trim()==""){
				var practicedescription= undefined;
			}
			else{
				var practicedescription= $("#aprofessionnalPracticeFromSkill").val().trim();
			}
			linkid = $('#professionnalPracticeIndication').attr('class');
			domainid = linkid.split('-')[0];
			skillid = linkid.split('-')[1];

			get_data("../../php/addprofessionnalpractice.php", process, { 'guidanceId' : guidanceid, 'domainid' : domainid, 'skillid' : skillid, 'practicedescription' : practicedescription}, true);

			function process(){
				sendDataProfessionnalPractice(linkid);
				hidepopup();
				displayMessage("Pratique professionnelle ajoutée avec succès",0);
			}
			fillPopUps();
		});

		// Modifie une pratique professionnelle
		$('#modifyProfessionnalPracticeButton').click(function modifyProfessionnalPractice(){
			linkid = $('#professionnalPracticeIndication').attr('class');
			var practiceinfos = new Array();
			var cmptEmptyField = 0;
			domainid = linkid.split('-')[0];
			skillid = linkid.split('-')[1];

			$('#mProfessionnalPracticeContent').find('textarea').each(function(index, txta){
				var practiceid = ($(this).attr('id'));
				var practicedescription = $(this).val().trim();

				var o  = {"practiceid" : practiceid, "practicedescription" :practicedescription}
				practiceinfos.push(o);
			});

			practiceinfos.forEach(function(element){
				if(element.practicedescription.trim().length == 0){
					cmptEmptyField++;
				}
			});

			if(cmptEmptyField == 0){

				get_data("../../php/modifyprofessionnalpractice.php", process, { 'guidanceId' : guidanceid, 'domainId' : domainid, 'skillId' : skillid, 'practiceInfos' : practiceinfos}, true);

				function process(){
					sendDataProfessionnalPractice(linkid);
					hidepopup();
					displayMessage("Pratique professionnelle modifiée avec succès",0);
				}
			}
			else{
				displayMessage("Veuillez remplir les champs marqué d'un *",2);
			}
			fillPopUps();
		});

		// Supprime une pratique professionnelle
		$('#deleteProfessionnalPracticelButton').click(function deleteProfessionnalPractice(){
			linkid = $('#professionnalPracticeIndication').attr('class');
			var practiceinfos = new Array();
			domainid = linkid.split('-')[0];
			skillid = linkid.split('-')[1];

			$('#dProfessionnalPracticeContent').find('input').each(function(index, checkbox){
				var practiceid = ($(this).attr('id').split('-')[1]);

				if($(checkbox).is(":checked")){
					var o  = {"practiceid" : practiceid};
					practiceinfos.push(o);
				}
			});

			if(practiceinfos.length !==0){

				get_data("../../php/deleteprofessionnalpractice.php", process, { 'guidanceId' : guidanceid, 'domainId' : domainid, 'skillId' : skillid, 'practiceInfos' : practiceinfos}, true);

				function process(){
					sendDataProfessionnalPractice(linkid);
					hidepopup();
					displayMessage("Pratique professionnelle supprimée avec succès",0);
				}
			}
			else{
				displayMessage('Veuillez sélectionner au moins une pratique professionnelle à supprimer.',2);
			}
			fillPopUps();
		});

		//Quand l'utilisateur clique sur un bouton Annuler
		$('.cancelBtn').click(function(){
			fillPopUps();
			hidepopup();
		});


	});// end of popup load

	//Quand l'action entreprise par l'utilisateur réussi (case:0) ou échoue (case:1)
	//@param message - le message à afficher
	//@param statut - le statut du message 0:succès 1:échec
	function displayMessage(message, statut){
		 var div = $("#message");
		 var popUpDiv = $(".missingField");

		 switch (statut) {
		case 0:
			if($(div).css("display", "none"))
			  {
				  $(div).attr('class',' alert alert-success');
				  $(div).fadeIn(750);
				  $(div).html('<span class="glyphicon glyphicon-ok"></span> ' + message);
				  $(div).css("display", "inline");
				  $('html, body').animate({scrollTop: '0px'}, 300);
				  $(div).fadeOut(3250);
			  }
			  else{
				  $(div).css("display", "none");
			  }
			break;

		case 1:
			 if($(div).css("display", "none"))
			  {
				  $(div).attr('class',' alert alert-danger');
				  $(div).fadeIn(750);
				  $(div).html('<span class="glyphicon glyphicon-remove"></span> ' + message);
				  $(div).css("display", "inline");
				  $('html, body').animate({scrollTop: '0px'}, 300);
				  $(div).fadeOut(3250);
			  }
			  else
			  {
				  $(div).css("display", "none");
			  }
			break;

		case 2:
			 if($(popUpDiv).css("display", "none"))
			  {
				  $(popUpDiv).addClass('col-sm-12 alert alert-danger');
				  $(popUpDiv).fadeIn(750);
				  $(popUpDiv).html('<span class="glyphicon glyphicon-remove"></span> ' + message);
				  $(popUpDiv).css("display", "inline");
				  $('html, body').animate({scrollTop: '0px'}, 300)
				  $(popUpDiv).fadeOut(3250);
			  }
			  else
			  {
				  $(popUpDiv).css("display", "none");
			  }
			break;
		}
	}

	function hidepopup(){
		//on vide les champs d'ajout
		$('.addTextarea').val('');

		// on fait disparaître le popup
		$('.modal').hide();
		$('.modal-backdrop').hide();
		$('#body').removeClass('modal-open');
	}

	// Rempli la table des domaines
	function ajaxFillDomainTable(){

		// Supression du tableau afin de le reconstruire
		$('#skillsTable').empty();
		var buttonsDomain = '<div id="span" style="padding-top: 30px;"><a id="deleteDomainGlyphicon" data-toggle="modal" data-target="#deleteDomain" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-trash pull-right pull-bottom" style="color: white;" onclick=""></span></a> <a id="modifyDomainGlyphicon" data-toggle="modal" data-target="#modifyDomain" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-pencil pull-right pull-bottom" style="margin-right: 10px; color: white;"></span></a><a id="addDomainGlyphicon" data-toggle="modal" data-target="#addDomain" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-plus pull-right pull-bottom" style="margin-right: 10px; color: white;"></span></a></div>';
		var buttonsSkill = '<div id="span" style="padding-top: 30px;"><a id="deleteOperationnalSkillGlyphicon" data-toggle="modal" data-target="#deleteOperationnalSkill" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-trash pull-right pull-bottom" style="color: white;"></span></a> <a id="modifyOperationnalSkillGlyphicon" data-toggle="modal" data-target="#modifyOperationnalSkill" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-pencil pull-right pull-bottom" style="margin-right: 10px; color: white;"></span></a><a id="addOperationnalSkillGlyphicon" data-toggle="modal" data-target="#addOperationnalSkill" data-backdrop="static" data-keybord="false"><span class="glyphicon glyphicon-plus pull-right pull-bottom" style="margin-right: 10px; color: white;"></span></a></div>';
		var table = ('<thead><tr id="domainsCodes"><th rowspan="2" id="thSkill">Domaine de compétence '+ buttonsDomain + '</th></tr><tr id="domainsDescriptions"></tr></thead><tbody id="bodySkill"><tr id="row0"><th id="thSkill" rowspan="5000">Compétences opérationelles ' + buttonsSkill + '</th></tr></tbody>');
		$('#skillsTable').append(table);

		get_data("../../php/loadallhierarchybyguidance.php", process, {'guidanceId' : guidanceid}, true);

		function process(data){
			displayDomainAndSkills(data, "pageGestionCompetence.php");
			modifyURLRedirectionCase();
			fillPopUps();
			var cmptTd = 0;
			$('#domainsDescriptions').find('td').each(function(index, td){
				if($(td).html()==""){
					$('#deleteDomainGlyphicon').hide();
					$('#modifyDomainGlyphicon').hide();
					$('#deleteOperationnalSkillGlyphicon').hide();
					$('#modifyOperationnalSkillGlyphicon').hide();
					cmptTd++;
					return true;
				}
				else{
					$('#addOperationnalSkillGlyphicon').show();
					$('#deleteDomainGlyphicon').show();
					$('#modifyDomainGlyphicon').show();
					$('#deleteOperationnalSkillGlyphicon').hide();
					$('#modifyOperationnalSkillGlyphicon').hide();
					cmptTd++;
					return false;
				}
			});

			$('#bodySkill > tr').find('td').each(function(index2, td2){
				if($(td2).html()==""){
					$('#addOperationnalSkillGlyphicon').show();
					$('#deleteDomainGlyphicon').show();
					$('#modifyDomainGlyphicon').show();
					$('#deleteOperationnalSkillGlyphicon').hide();
					$('#modifyOperationnalSkillGlyphicon').hide();
					cmptTd++;
					return true;
				}
				else{
					$('#addOperationnalSkillGlyphicon').show();
					$('#deleteDomainGlyphicon').show();
					$('#modifyDomainGlyphicon').show();
					$('#deleteOperationnalSkillGlyphicon').show();
					$('#modifyOperationnalSkillGlyphicon').show();
					cmptTd++;
					return false;
				}
			});

			if(cmptTd == 0){
					$('#deleteOperationnalSkillGlyphicon').hide();
					$('#modifyOperationnalSkillGlyphicon').hide();
					$('#addOperationnalSkillGlyphicon').hide();
					$('#deleteDomainGlyphicon').hide();
					$('#modifyDomainGlyphicon').hide();
					}

		}
	}

	//Appelle les méthodes qui vont remplir les pop-up
	function fillPopUps(){

		get_data("../../php/loadallhierarchybyguidance.php", process, {'guidanceId' : guidanceid}, true);

		function process(data){
			switch (fieldtorefresh){
			case 0: // initialisation
				fillCheckboxesInDeleteDomain(data);
				fillOptionsInModifySkill(data);
				fillOptionsInAddOperationnalSkill(data);
				fillOptionsForDomainInModifyOperationnalSkill(data);
				fillDomainDescription();
				fillOptionsForIDInModifyOperationnalSkill(data);
				fillSkillFieldsInModifyOperationnalSkill(data);
				fillOptionsForDomainInDeleteOperationnalSkill(data);
				fillOptionsForIDInDeleteOperationnalSkill(data);
				break;
			case 1: //changement de domaine dans modifyoperationnalskill
				fillOptionsForIDInModifyOperationnalSkill(data);
				fillSkillFieldsInModifyOperationnalSkill(data);
				fillOptionsForIDInDeleteOperationnalSkill(data);
				break;
			case 2: //changement d'ID dans modifyoperationnalskill
				fillSkillFieldsInModifyOperationnalSkill(data);
				break;
			case 3 :
			}
			fieldtorefresh =0;
		}
	}

	//Rempli la liste déroulante des orientations
	//@arGuidance contient le tableau de toutes les orientations
	function fillOrientationDropList(arGuidance){
		var guidancesDropList = $('.guidanceSelect');
		arGuidance.forEach(function(guidance){
			guidancesDropList.append('<option id="'+guidance.guidanceid+'">'+guidance.guidancename+'</option>')
		})
	}

	// Rempli les cases à cocher dans la pop-up supprimer un domaine
	//@hierarchy contient toute la hierarchie de l'orientation correspondant à celle sélectionnée (guidanceid)
	function fillCheckboxesInDeleteDomain(hierarchy){
		var delDiv = $('#deleteSkillModalContent');
		delDiv.empty();
		delDiv.append('<div class="modal-body" id="deleteSkillModalContent"><h3 class="modal-title text-center" style="text-align: center; margin-bottom: 30px;"> Supprimer un ou plusieurs domaine(s) de compétence</h3><label class="col-xs-12 text-center">ID du domaine de compétence à supprimer : </label></div>')
		hierarchy.forEach(function(guidances){
			if(guidances.guidanceid == guidanceid){
			guidances.domains.forEach(function(domain){
			delDiv.append('<label class="chexkbox-inline text-center btn btn-default"  style="width:15%; margin: 0 2.5% 2.5% 2.5% ;" id="docheck'+domain.domainid+'">'+domain.domainid+'<input class="hidden checkboxDomainId" type="checkbox" value="'+domain.domainid+'" style="margin-left:10px;"/> <span id="doglyph'+domain.domainid+'"  class=" hidden glyphicon glyphicon-ok glyphicon-lg" style="position:relative;left: 10px;"></span> </label>')
			});
			}
		});
	}

	// Rempli la description du domaines dans la pop-up modifier domaine en fonction du domaine sélectionné
	function fillDomainDescription(){
		domainid = $('#modifyDomainModalContent option:selected').val();
		domaindescription = $('#domaindescription'+domainid).text();
		$('#domainDescritpionOfModifyDomain').val(domaindescription);

	}

	// Rempli la liste déroulante dans la pop-up modifier les domaines
	//@hierarchy contient toute la hierarchie de l'orientation correspondant à celle sélectionnée (guidanceid)
	function fillOptionsInModifySkill(hierarchy){
		var modDiv = $('#modifyDomainModalContent');
		modDiv.empty();
		hierarchy.forEach(function(guidances){
			if(guidances.guidanceid == guidanceid){
			guidances.domains.forEach(function(domain){
			modDiv.append('<option value="'+domain.domainid+'">'+domain.domainid+'</option>')
			});
			}
		});
	}

	// Rempli la liste déroulante dans la pop-up ajouter des compétences opérationnelles
	//@hierarchy contient toute la hierarchie de l'orientation correspondant à celle sélectionnée (guidanceid)
	function fillOptionsInAddOperationnalSkill(hierarchy){
		var addopDiv = $('#addOperationnalSkillModalContent');
		addopDiv.empty();
		hierarchy.forEach(function(guidances){
			if(guidances.guidanceid == guidanceid){
			guidances.domains.forEach(function(domain){
				addopDiv.append('<option value="'+domain.domainid+'">'+domain.domainid+'</option>')
			});
			}
		});
	}

	// Rempli la liste déroulante des domaines dans la pop-up modifier des compétences opérationnelles
	//@hierarchy contient toute la hierarchie de l'orientation correspondant à celle sélectionnée (guidanceid)
	function fillOptionsForDomainInModifyOperationnalSkill(hierarchy){
		var modopDiv = $('#modifyOperationnalSkillModalContent');
		var cptIdSkills = 0;
		modopDiv.empty();
		hierarchy.forEach(function(guidances){
			if(guidances.guidanceid == guidanceid){
				guidances.domains.forEach(function(domain){
					domainidLetter = domain.domainid;
					domain.skills.forEach(function(skill){
						if(skill.owner == domainidLetter){
							cptIdSkills++;
						}
					});
					if(cptIdSkills > 0){
						modopDiv.append('<option value="'+domain.domainid+'">'+domain.domainid+'</option>');
						cptIdSkills =0;
					}
				});
			}
		});
	}

	// Rempli la liste déroulante des ID dans la pop-up modifier des compétences opérationnelles
	//@hierarchy contient toute la hierarchie de l'orientation correspondant à celle sélectionnée (guidanceid)
	function fillOptionsForIDInModifyOperationnalSkill(hierarchy){
		var modopDiv = $('#idSkillToModify');
		var cptIdSkills = 1;
		domainidLetter = $('#modifyOperationnalSkillModalContent').val();
		modopDiv.empty();
		hierarchy.forEach(function(guidances){
			if(guidances.guidanceid == guidanceid){
				guidances.domains.forEach(function(domain){
					domain.skills.forEach(function(skill){
						if(skill.owner == domainidLetter){
							modopDiv.append('<option value="'+skill.skillid+'">'+cptIdSkills+'</option>')
							cptIdSkills++;
						}
					});
				});
			}
		});
	}

	// Rempli les champs dans la pop-up modifier les compétences opérationnelles
	//@hierarchy contient toute la hierarchie de l'orientation correspondant à celle sélectionnée (guidanceid)
	function fillSkillFieldsInModifyOperationnalSkill(hierarchy){
		var skillNameDiv = $('#mskillName');
		var skillDefDiv = $('#mskillDef');
		var methoSkillDiv = $('#mMethodologicSkill');
		var socialSkillDiv = $('#msocialSkill');
		var persoSkillDiv = $('#mpersonnalSkill');
		skillid = $('#idSkillToModify').val();
		domainidLetter = $('#modifyOperationnalSkillModalContent').val();
		skillNameDiv.empty();
		skillDefDiv.html("");
		methoSkillDiv.empty();
		socialSkillDiv.empty();
		persoSkillDiv.empty();
		hierarchy.forEach(function(guidances){
			if(guidances.guidanceid == guidanceid){
				guidances.domains.forEach(function(domain){
					domain.skills.forEach(function(skill){
						if(skill.owner == domainidLetter && skill.skillid == skillid){
							skillNameDiv.val(skill.skillname);
							skillDefDiv.val(skill.skilldefinition);
							methoSkillDiv.val(skill.skillmethodologic);
							socialSkillDiv.val(skill.skillsocial);
							persoSkillDiv.val(skill.skillpersonnal);
						}
					});// fin domain.skill.forEach
				});// fin guidances.domain.forEach
			}
		});// fin hierarchy.forEach
	}

	// Rempli la liste déroulante des domaines dans la pop-up supprimer des compétences opérationnelles
	//@hierarchy contient toute la hierarchie de l'orientation correspondant à celle sélectionnée (guidanceid)
	function fillOptionsForDomainInDeleteOperationnalSkill(hierarchy){
		var delopDiv = $('#deleteOperationnalSkillModalContent');
		delopDiv.empty();
		hierarchy.forEach(function(guidances){
			if(guidances.guidanceid == guidanceid){
				guidances.domains.forEach(function(domain){
					delopDiv.append('<option value="'+domain.domainid+'">'+domain.domainid+'</option>')
				});
			}
		});
	}

	// Rempli la liste déroulante des ID dans la pop-up supprimer des compétences opérationnelles
	//@hierarchy contient toute la hierarchie de l'orientation correspondant à celle sélectionnée (guidanceid)
	function fillOptionsForIDInDeleteOperationnalSkill(hierarchy){
		var delopDiv = $('#idSkillToDelete');
		var cptIdSkills = 1;
		domainidLetter = $('#deleteOperationnalSkillModalContent').val();
		delopDiv.empty();
		hierarchy.forEach(function(guidances){
			if(guidances.guidanceid == guidanceid){
				guidances.domains.forEach(function(domain){
					domain.skills.forEach(function(skill){
						if(skill.owner == domainidLetter){
							delopDiv.append('<option value="'+skill.skillid+'">'+cptIdSkills+'</option>')
							cptIdSkills++;
						}
					});
				});
			}
		});
	}

	// Affiche les pratiques professionnelles en fonction le l'ID de la compétence à laquelle elles sont liées
	//@arPractice contient le tableau de toutes les pratiques professionnelles
	//@linkid Contient le domaine et l'id de la pratique professionnelle séparé par un tiret
	function displayProfessionnalsPractices(arPractice, linkid){
		var professionnalPracticeDiv = $('#practicesCodes');
		var cmptTd = 0;
		//initialisation de la taille de la th de practice
		$('#professionnalPracticesTitle').width($('#thSkill').width());
		professionnalPracticeDiv.find('td').each(function(tdcase, td){
			$(td).remove();
		});

		fakeid = $('#'+ linkid).find('b').text();

		domainid = linkid.split('-')[0];
		skillid = linkid.split('-')[1];

		// Renvoie une chaîne de caractères uniquement composés de digit (ici, supprime le domaine de fakeid)
		fakeSkillid = fakeid.replace(/\D/g,'');

		$('#professionnalPracticeIndication').attr('class', linkid);
		$('#professionnalPracticeIndication').html(fakeid);

		$('#mProfessionnalPracticeContent').empty();
		$('#dProfessionnalPracticeContent').empty();

        if (arPractice.empty === undefined) {
            arPractice.forEach(function (practice) {
                cmptTd++;
                professionnalPracticeDiv.append('<td style="position:relative;"><b>' + domainid + '.' + fakeSkillid + '.' + cmptTd + '</b> :' + practice.practicedescription + '</td>');
                $('#mProfessionnalPracticeContent').append('<textarea class="form-control" id="' + practice.practiceid + '">' + practice.practicedescription + ' </textarea>');
                $('#dProfessionnalPracticeContent').append('<label for="deletePPCheckbox-' + practice.practiceid + '" class="lblDeletePP"><div id="ppcheck' + practice.practiceid + '" class="divDeletePractice"><p>' + practice.practicedescription + '</p><input id="deletePPCheckbox-' + practice.practiceid + '" class="hidden professionnalPracticeCheckbox" type="checkbox" value="' + practice.practiceid + '" style="margin-left:10px;"/> <span id="ppglyph' + practice.practiceid + '"  class=" hidden glyphicon glyphicon-ok glyphicon-lg" style="display:block; text-align:center; margin:10px;"></span></div></label>');
                $('textarea').focus(function () {
                    if ($(this)[0].scrollHeight > $(this).innerHeight()) {
                        resizeTa = true;
                        autosize($(this));
                    }
                });

                $('textarea').focusout(function () {
                    resizeTa = false;
                    $(this).height(40);
                });
            });
        }

        if(cmptTd == 0){
			$('#addProfessionnalPracticeGlyphicon').show();
			$('#deleteProfessionnalPracticeGlyphicon').hide();
			$('#modifyProfessionnalPracticeGlyphicon').hide();
			cmptTd = 0;
			return true;
		}

		else{
			$('#deleteProfessionnalPracticeGlyphicon').show();
			$('#modifyProfessionnalPracticeGlyphicon').show();
			$('#addProfessionnalPracticeGlyphicon').show();
			cmptTd = 0;
			return false;
		}
	}
</script>
<html>
