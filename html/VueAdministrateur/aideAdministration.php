<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
require_once '../../php/security.php';
security();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- JQuery CDN -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<!-- Bootstrap CDN -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- CSS EE -->
<!-- Lightbox link -->
<link href="../CSS/lightbox/dist/css/lightbox.css" rel="stylesheet">
<script src="../CSS/lightbox/dist/js/lightbox.js" rel="stylesheet"></script>
<!-- CSS EE -->
<link rel="stylesheet" href="../CSS/style.css" />
<title>Aide Administration</title>
</head>
<body>
<?php include_once '../navigation.php'; ?>

<section class="col-xs-0 col-sm-3">
		<section id="tableMatiere">
			<table class="table table-responsive table-bordered table-striped">
				<thead class="thead-inverse">
					<tr>
						<th>Table des matières</th>
					</tr>
				</thead>
				<tbody id="bodyM">
					<tr>
						<td><a href="#homepage">La page d'accueil</a></td>
					</tr>
					<tr>
						<td><a href="#skillDomainHelp">Gestion des domaines de
								compétence</a></td>
					</tr>
					<tr>
						<td><a href="#operationnalSkillHelp">Gestion des
								compétences opérationnelles</a></td>
					</tr>
					<tr>
						<td><a href="#professionnalPracticeHelp">Gestion des
								pratiques professionnelles</a></td>
					</tr>
				</tbody>
			</table>
		</section>
	</section>
	<section class="col-xs-12 col-sm-6">
		<section class="col-xs-12 mrgSection" id="homepage">
			<article class="col-xs-12">
				<article class="thead-inverse fgcAide">
					<h3 class="text-center">La page d'accueil</h3>
				</article>
				<figure class="col-xs-12 col-sm-6 col-center-help frgStyle">
					<a href="../img/imgAdmin/gererCompetences.png"
						data-lightbox="direction"> <img class="img-responsive"
						alt="Image de la page d'acceuil"
						src="../img/imgAdmin/gererCompetences.png">
					</a>
				</figure>
				<figure class="col-xs-12 col-sm-6 col-center-help frgStyle">
					<a href="../img/imgAdmin/gererCompetences2.png"
						data-lightbox="direction"> <img class="img-responsive"
						alt="Image de la page d'acceuil 2"
						src="../img/imgAdmin/gererCompetences2.png">
					</a>
				</figure>
			</article>
			<article class="col-xs-12">
				<ol>
					<li>La barre de navigation permet d'aller à la page d'aide ou
						de se déconnecter.</li>
					<li>Le filtre Orientation permet d'afficher les compétences en
						fonction de l'orientation choisie.</li>
					<li>Au clic, permet d'afficher la gestion des pratiques
						professionnelles. Les champs se mettent à jour en fonction de la
						compétence sélectionnée quand l'utilisateur a cliqué sur une des
						cases du tableau (Voir figure 2).</li>
					<li>Menus permettant la gestion des domaines de compétence
						(c.f. Gestion des domaines de compétence), des compétences
						opérationnelles (c.f. Gestion des compétences opérationnelles) et
						des pratiques professionnelles (c.f. Gestion des pratiques
						professionnelles).</li>
				</ol>
			</article>
		</section>

		<section class="col-xs-12 mrgSection" id="skillDomainHelp">
			<article class="col-xs-12">
				<article class="thead-inverse fgcAide">
					<h3 class="text-center">Gestion des domaines de compétence</h3>
				</article>
				<figure class="col-xs-12 frgStyle">
					<a href="../img/imgAdmin/gestionDomaineCompetence.png"
						data-lightbox="direction"> <img
						class="col-xs-12 col-sm-6 col-center-help frgStyle"
						alt="Image de la page de gestion des domaines de compétence"
						src="../img/imgAdmin/gestionDomaineCompetence.png" />
					</a>
				</figure>
			</article>
			<article class="col-xs-12">
				<ol>
					<li>Remplir les champs / cocher les cases.</li>
					<li>Valider l'action.</li>
					<li>Annuler l'action et réinitialiser les champs.</li>
				</ol>
				<label>* aspect de la case cochée. Quand le bouton
					"Supprimer le(s) domaine(s) de compétence" est cliqué (voir 2), les
					éléments cochés seront supprimés de la base de données.</label>
			</article>
		</section>

		<section class="col-xs-12 mrgSection" id="operationnalSkillHelp">
			<article class="col-xs-12">
				<article class="thead-inverse fgcAide">
					<h3 class="text-center">Gestion des compétences
						opérationnelles</h3>
				</article>
				<figure class="col-xs-12 frgStyle">
					<a href="../img/imgAdmin/gestionCompetenceOperationnelle.png"
						data-lightbox="direction"> <img
						class="col-xs-12 col-sm-6 col-center-help frgStyle"
						alt="Image de la page de gestion des compétences opérationnelles"
						src="../img/imgAdmin/gestionCompetenceOperationnelle.png" />
					</a>
				</figure>
			</article>
			<article class="col-xs-12">
				<ol>
					<li>Remplir les champs / choisir les options.</li>
					<li>Valider l'action.</li>
					<li>Annuler l'action et réinitialiser les champs.</li>
				</ol>
			</article>
		</section>

		<section class="col-xs-12 mrgSection" id="professionnalPracticeHelp">
			<article class="col-xs-12">
				<article class="thead-inverse fgcAide">
					<h3 class="text-center">Gestion des pratiques professionnelles</h3>
				</article>
				<figure class="col-xs-12 frgStyle">
					<a href="../img/imgAdmin/gestionPratiqueProfessionnelle.png"
						data-lightbox="direction"> <img
						class="col-xs-12 col-sm-6 col-center-help frgStyle"
						alt="Image de la page de gestion des pratiques professionnelles"
						src="../img/imgAdmin/gestionPratiqueProfessionnelle.png" />
					</a>
				</figure>
			</article>
			<article class="col-xs-12">
				<ol>
					<li>Remplir les champs / cocher les cases.</li>
					<li>Valider l'action.</li>
					<li>Annuler l'action et réinitialiser les champs.</li>
				</ol>
				<label>* aspect de la case cochée. Quand le bouton
					"Supprimer" est cliqué (voir 2), les éléments cochés seront
					supprimés de la base de données.</label>
			</article>
		</section>
		<section class="col-xs-12 mrgSection" id="aide_video">
			<article class="col-xs-12">
				<article class="thead-inverse fgcAide">
					<h3 class="text-center">La vidéo d'aide</h3>
				</article>
				<div class="col-lg-6">
				<table class="table table-responsive table-bordered table-striped">
						<thead class="thead-inverse">
							<tr>
								<th>Table des matières</th>
							</tr>
						</thead>
						<tbody id="bodyM">
							<tr>
								<td><a onClick="timeVideo(0);">Page d'accueil - Filtres + Historique</a></td>
							</tr>
							<tr>
								<td><a onClick="timeVideo(28)">Page d'accueil - Impression</a></td>
							</tr>
							<tr>
								<td><a onClick="timeVideo(49)">Gestion des compétences</a></td>
							</tr>
							<tr>
								<td><a onClick="timeVideo(136)">Gestion des utilisateurs</a></td>
							</tr>
		          <tr>
		            <td><a onClick="timeVideo(199)">Gestion des ateliers</td>
		          </tr>
							<tr>
		            <td><a onClick="timeVideo(235)">Aide</td>
		          </tr>
							<tr>
		            <td><a onClick="timeVideo(250)">À propos</td>
		          </tr>
						</tbody>
					</table>
				</div>
				<figure class="col-xs-12 col-lg-6 frgStyle">
					<a href="../vid/aide_aec.mp4" data-lightbox="direction">
            <video id="help_video" controls class="col-lg-12 frgStyle" alt="Page d'impression modulable" preload="auto">
							<source id="help_source" src="../vid/aide_aec.mp4"/>
						</video>
					</a>
				</figure>
			</article>
		</section>
	</section>
	<aside class="col-xs-0 col-sm-3 asideHelpPage">
			<a class="btn btn-danger" id="returnButtonDirectionHelpPage"><span class="glyphicon glyphicon-arrow-left"></span> Retour à la page précédente</a>
	</aside>
</body>
<script>
$('#bodyM > tr > td').click(function()
{
	if("#" + document.location.href.split('#')[1] != $(this).children().attr('href'))
	{
		var nb = typeof $('.asideHelpPage').attr('id') === 'undefined' ? 1 : parseInt($('.asideHelpPage').attr('id')) + 1;
		$('.asideHelpPage').attr('id', nb);
	}
});
$("#returnButtonDirectionHelpPage").click(function()
{
	var nb = typeof $('.asideHelpPage').attr('id') === 'undefined' ? 1 : parseInt($('.asideHelpPage').attr('id')) + 1;
	history.go(-nb);
});
function timeVideo(time) {
	var video = document.getElementById('help_video');
	var source = document.getElementById('help_source');
	source.setAttribute('src', '../vid/aide_aec.mp4#t=' + time);
	video.load();
  video.play();
}
</script>
</html>
