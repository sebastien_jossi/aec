<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
require_once '../php/security.php';
security();
?>
<!DOCTYPE html>
<html>
	<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<!-- JQuery CDN -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
	<!-- Bootstrap CDN -->
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
	<!-- CSS EE -->
	<!-- Lightbox link -->
	<link href="./CSS/lightbox/dist/css/lightbox.css" rel="stylesheet">
	<script src="./CSS/lightbox/dist/js/lightbox.js" rel="stylesheet"></script>
	<!-- CSS EE -->
	<link rel="stylesheet" href="CSS/style.css" />

	<style>
		.fixedToTop {
			position: fixed !important;
			top: 1% !important;
		}

		@media screen and (max-width: 993px) {
			.asideHelpPage {
				position: absolute;
				padding: 10px;
				top: 18%;
				left: 1%;
				width: 35px;
			}

			.asideHelpPage .text-large-button {
				display: none;
			}
		}
	</style>
	<title>Aide Administration</title>
	</head>
	<body>
		<header>
			<nav class="navbar navbar-default col-xs-12">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1" aria-expanded="false" style="padding-bottom: 5px;">
					<span class="sr-only">Barre de navigation</span>
					<span class="glyphicon glyphicon-menu-hamburger"></span>
				</button>
				<a class="navbar-brand" href="/index.php" style="padding-top: 5px;">
					<img class="img-responsive" id="logoEE" name="logoEE" alt="Logo_EE" src="img/LogoAecTexte.png"/>
				</a>
			</div>
			<div>
				<div class="text-right" style="float: right;">
					<a class="navbar-brand" href="/index.php" style="padding-top: 5px;">
						<span class="navbar-brand"  style="padding-left: 5px;">École d'informatique</span>
						<img class="img-responsive" id="logoEE" name="logoEE" alt="Logo_EE" src="img/logoEntrepriseEcole.png" style="padding-top: 5px;"/>
					</a>
				</div>
			</div>
				<div class="collapse navbar-collapse" id="navbar1">
					<div class="container">
						<ul class="nav navbar-nav col-xs-12 text-center" style="line-height: 52px;">
							<li><a href="VueDirection/AccueilDirection.php"><span class="glyphicon glyphicon-home"></span> Home</a></li>
							<li><a href="../index.php?logout=true"><span class="glyphicon glyphicon-log-out"></span> Déconnexion</a></li>
						</ul>
					</div>
				</div>
			</nav>
		</header>
		<h1 class="col-xs-12 col-sm-6 col-md-offset-3" style="padding: 100px;">L'autoévaluation pour une meilleure formation</h1>
		<section class="col-xs-12 col-sm-6 col-md-offset-3">
			<div id="mission" class="well">
				<h3 style="margin:0px 0px 10px 0px;">Notre mission : </h3>
				<article>Permettre aux étudiants de s’autoévaluer sur des critères fixés par ICT et aux enseignants d’adapter leur cours en fonction. </article>
			</div>
			<div id="history" class="well">
				<h3 style="margin:0px 0px 10px 0px;">Historique  : </h3>
				<article>Suivi Apprenti a été développé dans le cadre de l’École Entreprise qui a vu le jour en 2016. C’est l’une des applications pionnières de cette entreprise intra scolaire qui a permis aux apprenti développeur d’apprendre et d’appliquer des connaissances essentielles au métier de développeur d’application.  </article>
			</div>
			<div id="authors" class="well">
			<h3 style="margin:0px 0px 10px 0px;">Auteurs :</h3>
			<article>
				<ul>
					<li>Ronaldo Loureiro Pinto - ronaldo.lrrpnt@eduge.ch</li>
					<li>Floran Stucki - floran.stck@eduge.ch</li>
					<li>Romain Sauser - romain.ssr@eduge.ch</li>
					<li>Quentin Fasler - quentin.fslr@eduge.ch</li>
					<li>Diogo Canas - diogo.cnslm@eduge.ch</li>
					<li>Tanguy Cavagna  - tanguy.cvgn@eduge.ch</li>
				</ul>
			</article>
			</div>
			<div id="responsible" class="well">
				<h3 style="margin:0px 0px 10px 0px;">Responsable :</h3>
				<article>
					<ul>
						<li>Sébastien Jossi – edu-jossis@eduge.ch</li>
					</ul>
				</article>
			</div>
			<div id="responsible" class="well">
				<h3 style="margin:0px 0px 10px 0px;">Bug report :</h3>
				<article>
					<ul>
						<li>Adresse mail - inv-eentrepriseinfgen2@eduge.ch</li>
						<li><a href="https://docs.google.com/forms/d/e/1FAIpQLSec1KbMW91_FY2FoxKZaSxxXPxK9-g0xyNqTT4Qxazhyd1fDQ/viewform?usp=sf_link" target="_blank">Formulaire de bug report</a></li>
					</ul>
				</article>
			</div>
		</section>
		<button class="btn btn-danger col-xs-6 asideHelpPage" id="returnButtonDirectionHelpPage"><a style="color: white!important;"><span class="glyphicon glyphicon-arrow-left text-small-button"></span> <span class="text-large-button">Retour à la page précédente</span></a></button>
	</body>
	<script>
		$('#bodyM > tr > td').click(function()
		{
			if("#" + document.location.href.split('#')[1] != $(this).children().attr('href'))
			{
				var nb = typeof $('.asideHelpPage').attr('id') === 'undefined' ? 1 : parseInt($('.asideHelpPage').attr('id')) + 1;
				$('.asideHelpPage').attr('id', nb);
			}
		});
		$("#returnButtonDirectionHelpPage").click(function()
		{
			window.history.back();
		});

		if ($(document).width() < 755) {
			$(document).scroll(function() {

				if ($(document).scrollTop() > 300) {
					$('.asideHelpPage').addClass("fixedToTop");
				} else {
					$('.asideHelpPage').removeClass("fixedToTop");
				}

			});
		}
</script>
</html>
