<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
require_once '../../php/security.php';
security();
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- JQuery CDN -->
        <script	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <!-- Bootstrap CDN -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
        <!-- CSS EE -->
        <link rel="stylesheet" href="../CSS/style.css" />
        <title>Accueil Direction</title>
    </head>
    <body>
    <?php include_once '../navigation.php'; ?>

    <section class="col-lg-2"></section>
        <section class="container col-lg-8 col-xs-12 notpadding" id="bodypage">
            <section class="container-fluid" style="margin-top: 10px; margin-bottom: 20px;">
                <section class="col-lg-1" style="padding: 0px">
                    <button type="button" class="btn btn-danger col-xs-12" id="btnfilter" aria-expanded="false" style="padding-bottom: 5px; margin-bottom: 5px;">
                        <span class="sr-only">Filtres</span>
                        <span class="glyphicon glyphicon glyphicon-filter"></span>
                        <label style="margin: 0px;">Filtres</label>
                    </button>
                </section>
                <section class="col-xs-12 col-lg-10 collapse notpadding" id="filter" style="height: 34px;">
                    <section class="col-lg-3 sectionselect notpadding espacementG">
                        <a data-toggle="tooltip" title="Orientation" style="text-decoration: none;">
                            <select	class="select form-control selectfilter" id="guidanceSelect">
                                <optgroup label="Orientation"></optgroup >
                            </select>
                        </a>
                    </section>
                    <section class="col-lg-2 sectionselect notpadding espacementG">
                        <a data-toggle="tooltip" title="Degré" style="text-decoration: none;">
                            <select class="select form-control selectfilter" id="degreeSelect">
                                <optgroup label="Degré"></optgroup >
                                <option>Tous</option>
                            </select>
                        </a>
                    </section>
                    <section class="col-lg-2 sectionselect notpadding espacementG">
                        <a data-toggle="tooltip" title="Classe" style="text-decoration: none;">
                            <select class="select form-control selectfilter" id="classroomSelect">
                                <optgroup label="Classe"></optgroup >
                                <option>Toutes</option>
                            </select>
                        </a>
                    </section>
                    <section class="col-lg-2 sectionselect notpadding espacementG">
                        <a data-toggle="tooltip" title="État" style="text-decoration: none;">
                            <select	class="select form-control selectfilter" id="stateSelect">
                                <optgroup label="État"></optgroup >
                            </select>
                        </a>
                    </section>
                    <section class="col-lg-2 col-xs-12 text-center sectionselect notpadding espacementG">
                        <button id="submitFilters" name="submitFilters" class="select btn btn-warning col-xs-12"/><span id="glyFilter" class="glyphicon glyphicon-refresh"></span> <b id="bFilterText">Réinitialiser</b></button>
                    </section>
                </section>
            </section>
            <section class="container-fluid">
                <section class="col-xs-12" style="padding: 0px;">
                    <section class="col-sm-6 col-xs-12 pull-left notpadding espacementH">
                        <section class="form-inline">
                            <section class="input-group col-xs-12 col-sm-6 col-lg-3">
                                <section class="input-group-addon" style="color: white; background-color: #1B81E5; border:none;"><span class="fa fa-history"></span></section>
                                <a data-toggle="tooltip" title="Historique" style="text-decoration: none;">
                                    <select	class="form-control" id="yearSelect">
                                    </select>
                                </a>
                            </section>
                        </section>
                    </section>
                    <section class="col-sm-6 col-xs-12 notpadding espacementH">
                        <button type="button" class="btn btn-success col-xs-12 col-sm-4 col-lg-3 pull-right" id="btnImprimer"><span class="glyphicon glyphicon-print"></span> <b>Imprimer</b></button>
                    </section>
                </section>
                <section class="container-fluid table-responsive removeRefresh" style="padding-left: 0px; padding-right: 1px;" id="tableData">
                    <table class="table table-bordered table-vcenter" id="skillsTable">
                        <thead>
                            <tr id="domainsCodes">
                                <th rowspan="2">Domaine de compétence</th>
                            </tr>
                            <tr id="domainsDescriptions">

                            </tr>
                        </thead>
                        <tbody id="bodySkill">
                            <tr id="row0">
                                <th id="thSkill" rowspan="1000">Compétences opérationelles</th>
                            </tr>
                        </tbody>
                    </table>
                </section>
            </section>
        </section>
        
        <section class="container" style="word-break: break-all;" id="commentRecap"></section>
    </body>

    <script src="../../js/libraryDirection.js"></script>
    <script src="../../js/utilities.js"></script>
    <script src="../../js/displayTableHome.js"></script>
    <script src="../../js/colorSystem.js"></script>
    <script>
        $(document).ready(function ()
        {
            // Variable de session
            var guidanceId = <?php if (isset($_SESSION['guidanceId'])) echo $_SESSION['guidanceId'] . ";";
                                else echo "null;"; ?>
            var yearId = <?php if (isset($_SESSION['yearId'])) echo $_SESSION['yearId'] . ";";
                                else echo "null;"; ?>
            var classroomId = <?php if (isset($_SESSION['classroomId'])) echo $_SESSION['classroomId'] . ";";
                                else echo "null;"; ?>
            var degreeId = <?php if (isset($_SESSION['degreeId'])) echo $_SESSION['degreeId'] . ";";
                                else echo "null;"; ?>
            var stateId = <?php if (isset($_SESSION['stateId'])) echo $_SESSION['stateId'] . ";";
                                else echo "null;"; ?>



            // Effectue les appels ajax pour remplir les filtres
            get_data("../../php/getguidances.php", fillDropDown, undefined, false, "#guidanceSelect", "guidanceid", "guidancename", guidanceId);
            get_data("../../php/getyears.php", fillDropDown, undefined, false, "#yearSelect", "yearid", "yearname", yearId);
            get_data("../../php/getstates.php", fillDropDown, undefined, false, "#stateSelect", "stateid", "statename", stateId);
            get_data("../../php/getclassrooms.php", fillDropDown, undefined, false, "#classroomSelect", "classroomid", "classroomname", classroomId);
            get_data("../../php/getdegrees.php", fillDropDown, undefined, false, "#degreeSelect", "degreeid", "degreename", degreeId);

            refreshData();

            // Mets en forme les tooltips
            $('[data-toggle="tooltip"]').tooltip();



            $.ajax({
                url: "../../php/getAllComments.php",
                type: 'post',
                data: {
                    getAll: true
                },
                success: function(data) {
                    data = JSON.parse(data);
                    fillComments(data.allComments);
                }
            });

            
        });

        $(document).on("click", '#commentRecap > table > tbody > tr', function() {
            window.location.replace($(this).find('a').attr('href'));
        });

        function fillComments(comments) {
            $('#commentRecap').html("");
            $('#commentRecap').append('<table class="table table-bordered table-vcenter" style="table-layout:fixed;"><thead><tr><th class="text-center" id="showComment">Récapitulatif de tout les commentaires<span class="pull-right glyphicon"></span></th></tr></thead><tbody id="bodyComment">');
            comments.forEach(function(c) {
                $('#commentRecap table tbody').append('<tr title="' + c.DOMAINS_ID + '-' + c.SKILL_CODE + '"><td style="word-wrap:break-word;">' + c.COMMENT + '<p></p><label class="pull-right">' + c.schoolEmail + '</label><a href="/html/VueDirection/RecapitulatifCompetence.php?guidanceid=' + c.GUIDANCES_CODE + '&domainid=' + c.DOMAINS_ID + '&skillid=' + c.SKILL_CODE + '&degreeid=-1&classroomid=-1&yearid=3&historic=false"></a></td></tr>');
            });
            $('#commentRecap').append('</tbody></table>');
        }

        /* Appelle la fonction qui permet de rafraichir le tableau avec les bonnes données
         *
         */
        function refreshData()
        {
            var guidanceId = typeof $("#guidanceSelect option:selected").attr("id") === 'undefined' ? 1 : $("#guidanceSelect option:selected").attr("id"); // Attribution d'une valeur par défaut si le filtre n'est pas rempli
            var yearId = typeof $("#yearSelect option:selected").attr("id") === 'undefined' ? 1 : $("#yearSelect option:selected").attr("id");
            var degreeId = $("#degreeSelect option:selected").attr("id");
            var classroomId = $("#classroomSelect option:selected").attr("id");
            var stateId = typeof $("#stateSelect option:selected").attr("id") === 'undefined' ? 1 : $("#stateSelect option:selected").attr("id");

            completeArray(guidanceId, yearId, degreeId, classroomId, stateId);
        }

        /* Modifie les liens en ajoutant des données supplémentaires
         * @param degreeId L'id  du degré
         * @param classroomId L'id  de la classe
         * @param yearId L'id  de l'année
         *
         */
        function modifyURLRedirectionCase(degreeId = - 1, classroomId = - 1, yearId = - 1)
        {
            var historic;

            if ($("#yearSelect").prop("selectedIndex") == 0)
                historic = false;
            else
                historic = true;

            $('#bodySkill > tr > td').find('a').each(function (tdcase, a) {
                $(a).attr('href', $(a).attr('href') + '&degreeid=' + degreeId + '&classroomid=' + classroomId + '&yearid=' + yearId + '&historic=' + historic);
                $(a).addClass('avgPlace');
            });
        }

        /**
         * Recupère les filtres actuellement utilisé
         *
         * @return object Propriété des des filtres
         */
        function getFilter() {
            let guidanceSelect = $('#guidanceSelect').val();
            let degreeSelect = $('#degreeSelect').val();
            let classroomSelect = $('#classroomSelect').val();
            let stateSelect = $('#stateSelect').val();

            return {
                guidanceSelect,
                degreeSelect,
                classroomSelect,
                stateSelect
            };
        }

        function appendFilterToURL(obj) {
            let url = obj.getAttribute('href');
            let filters = getFilter();
            let filterProperties = '';
            if (url.indexOf('?') !== -1) {
                Object.keys(filters).forEach(function(key) {
                    filterProperties += '&' + key + '=' + filters[key];
                });
                obj.setAttribute('href', url + filterProperties);
            }
        }

        /* Rempli le tableau avec les données adéquate quand l'année scolaire change
         *
         */
        $('#yearSelect').on('change', function ()
        {
            var guidanceId = $("#guidanceSelect option:selected").attr("id");
            var yearId = $("#yearSelect option:selected").attr("id");
            var degreeId = $("#degreeSelect option:selected").attr("id");
            var classroomId = $("#classroomSelect option:selected").attr("id");
            var stateId = $("#stateSelect option:selected").attr("id");

            get_data('../../php/setfiltersession.php', refreshData, {'guidanceId': guidanceId, 'yearId': yearId, 'classroomId': classroomId, 'degreeId': degreeId, 'stateId': stateId}, true);
        });

        /* Filtre les classes pour matcher avec le degré scolaire
         *
         */
        $('#degreeSelect').change(function ()
        {
            $('#classroomSelect').empty();
            $('#classroomSelect').append('<option>Toutes</option>');
            get_data("../../php/getclassrooms.php", process, undefined, false, "#classroomSelect", "classroomid", "classroomname");

            function process(data)
            {
                var degreeid = $("#degreeSelect option:selected").attr("id");
                if (degreeid != undefined)
                {
                    var nbDel = [];
                    data.forEach(function (classroom, index)
                    {
                        if (classroom["classroomname"].substr(6, 1) != degreeid)
                            nbDel.unshift(index);
                    });

                    nbDel.forEach(function (index)
                    {
                        data.splice(index, 1);
                    });
                }

                fillDropDown(data, "#classroomSelect", "classroomid", "classroomname");
            }
        });

        /* Active le bouton "Appliquer" quand la valeur d'un filtre change
         *
         */
        $(".selectfilter").on('change', function ()
        {
            $("#glyFilter").removeClass("glyphicon-refresh");
            $("#glyFilter").addClass("glyphicon-ok");
            $("#bFilterText").text("Appliquer");
            $("#submitFilters").prop("disabled", false);
        });

        /* Rempli le tableau avec les données adéquate quand le bouton "Appliquer" les filtres est cliqué
         *
         */
        $('#submitFilters').on('click', function ()
        {
            if ($("#bFilterText").text() == "Réinitialiser")
            {
                $('select').each(function (index, select)
                {
                    $(select).prop("selectedIndex", 0);
                });
                $("#degreeSelect").trigger("change");
                $("#submitFilters").prop("disabled", true);
            }

            var guidanceId = $("#guidanceSelect option:selected").attr("id");
            var yearId = $("#yearSelect option:selected").attr("id");
            var degreeId = $("#degreeSelect option:selected").attr("id");
            var classroomId = $("#classroomSelect option:selected").attr("id");
            var stateId = $("#stateSelect option:selected").attr("id");

            get_data('../../php/setfiltersession.php', process, {'guidanceId': guidanceId, 'yearId': yearId, 'classroomId': classroomId, 'degreeId': degreeId, 'stateId': stateId}, true);
            function process(data)
            {
                refreshData();
                $("#glyFilter").removeClass("glyphicon-ok");
                $("#glyFilter").addClass("glyphicon-refresh");
                $("#bFilterText").text("Réinitialiser");
            }
        });

        /* Animation pour les filtres
         *
         */
        $("#btnfilter").click(function ()
        {
            if ($(window).width() > 1200)
            {
                $("#btnfilter").attr("data-toggle", "");
                $("#btnfilter").attr("data-target", "");

                if ($("#filter").hasClass("in"))
                    $("#filter").animate({"width": "0px"}, "slow", function ()
                    {
                        $("#filter").removeClass("in");
                    });
                else
                if ($(window).width() > 1200)
                {
                    $("#filter").css("width", "0px");
                    $("#filter").addClass("in");
                    $("#filter").animate({"width": "91.6%"}, "slow");
                }
            } else
            {
                $("#btnfilter").attr("data-toggle", "collapse");
                $("#btnfilter").attr("data-target", "#filter");
                $("#filter").css("width", "100%");
            }
        });

        /* Redirige vers la page ImpressionModulable
         *
         */
        $('#btnImprimer').on('click', function ()
        {

            var guidanceId = typeof $("#guidanceSelect option:selected").attr("id") === 'undefined' ? 1 : $("#guidanceSelect option:selected").attr("id"); // Attribution d'une valeur par défaut si le filtre n'est pas rempli
            var yearId = $("#yearSelect option:selected").attr("id");
            var degreeId = typeof $("#degreeSelect option:selected").attr("id") === 'undefined' ? -1 : $("#degreeSelect option:selected").attr("id");
            var classroomId = typeof $("#classroomSelect option:selected").attr("id") === 'undefined' ? -1 : $("#classroomSelect option:selected").attr("id");
            var stateId = typeof $("#stateSelect option:selected").attr("id") === 'undefined' ? 1 : $("#stateSelect option:selected").attr("id");
            var historic = false;
            if ($("#yearSelect").prop("selectedIndex") == 0)
                historic = false;
            else
                historic = true;

            document.location.href = "ImpressionModulable.php?guidanceid=" + guidanceId + "&yearid=" + yearId + "&degreeid=" + degreeId + "&classroomid=" + classroomId + "&stateid=" + stateId + "&historic=" + historic;
        });
    </script>
</html>
