<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
require_once '../../php/security.php';
security();
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<!-- JQuery CDN -->
		<script
			src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
		<!-- Bootstrap CDN -->
		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
		<!-- CSS EE -->
		<link rel="stylesheet" href="../CSS/style.css" />
		<title>Récapitulatif d'une compétence</title>
	</head>
	<body>
    <?php include_once '../navigation.php'; ?>

    <section class="col-lg-2"></section>
		<section class="col-xs-12 col-lg-8 bodyZone" id="filterInfo"></section>
	</body>
	<script src="../../js/libraryDirection.js"></script>
	<script src="../../js/utilities.js"></script>
	<script>
        /**
         * Cherche et donne la valeur du paramettre get que l'on veut
         * @see https://stackoverflow.com/questions/5448545/how-to-retrieve-get-parameters-from-javascript
         *
         * @param parameterName Nom du paramettre que l'on veut
         * @return {string} valeur du paramettre
         */
        function findGetParameter(parameterName) {
            let result = null,
                tmp = [];
            location.search
                .substr(1)
                .split("&")
                .forEach(function (item) {
                    tmp = item.split("=");
                    if (tmp[0] === parameterName) result = decodeURIComponent(tmp[1]);
                });
            return result;
        }

        function showFilterProperties() {
            let filterInfo = $('#filterInfo');
            let guidanceSelect = findGetParameter('guidanceSelect');
            let degreeSelect = findGetParameter('degreeSelect');
            let classroomSelect = findGetParameter('classroomSelect');
            let stateSelect = findGetParameter('stateSelect');
            filterInfo.append('<p><b>Filtres => Orientation : </b>' + guidanceSelect + ', <b>Degré : </b>' + degreeSelect + ', <b>Classe : </b>' + classroomSelect + ', <b>État : </b>' + stateSelect + '</p>');
        }

	$(document).ready(function()
	{
		// Récupère les variables de l'URL
		var guidanceid = getUrlVar("guidanceid");
		var domainid = getUrlVar("domainid");
		var skillid = getUrlVar("skillid");
		var yearid = getUrlVar("yearid");
		var degreeid = getUrlVar("degreeid");
		var classroomid = getUrlVar("classroomid");
		var fakeid = getUrlVar("skillid");
		var historic = getUrlVar("historic") == 'false' ? false : true;

		getInfoSkill(guidanceid, domainid, skillid, yearid, degreeid, classroomid, domainid + fakeid, false, historic);

        showFilterProperties();
	});
	</script>
</html>
