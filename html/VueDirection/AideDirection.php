<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
require_once '../../php/security.php';
security();
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- JQuery CDN -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
<!-- Bootstrap CDN -->
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<!-- Lightbox link -->
<link href="../CSS/lightbox/dist/css/lightbox.css" rel="stylesheet">
<script src="../CSS/lightbox/dist/js/lightbox.js" rel="stylesheet"></script>
<!-- CSS EE -->
<link rel="stylesheet" href="../CSS/style.css" />
<title>Aide Direction</title>
</head>
<body>
<?php include_once '../navigation.php'; ?>

	<section class="col-lg-3"></section>
	<section id="tableMatiere">
		<table class="table table-responsive table-bordered table-striped">
				<thead class="thead-inverse">
					<tr>
						<th>Table des matières</th>
					</tr>
				</thead>
				<tbody id="bodyM">
					<tr>
						<td><a href="#home">La page d'accueil</a></td>
					</tr>
					<tr>
						<td><a href="#recapitulationcompetence">La page du récapitulatif d'une compétence</a></td>
					</tr>
					<tr>
						<td><a href="#ColorCode">Le code couleur</a></td>
					</tr>
					<tr>
						<td><a href="#print">La page d'impression</a></td>
					</tr>
          <tr>
            <td><a href="#aide_video">La vidéo d'aide</td>
          </tr>
				</tbody>
			</table>
	</section>
	<section class="col-xs-12 col-lg-6 bodyAide">
		<section class="col-xs-12 mrgSection" id="home">
			<article class="col-xs-12">
				<article class="thead-inverse fgcAide">
					<h3 class="text-center">La page d'accueil</h3>
				</article>
				<figure  class="col-xs-12 col-lg-6 col-center-help frgStyle">
					<a href="../img/imgDirection/AccueilDirection.png" data-lightbox="direction">
						<img class="img-responsive" alt="Image de la page d'acceuil" src="../img/imgDirection/AccueilDirection.png">
					</a>
				</figure>
			</article>
			<article class="col-xs-12">
				<ol>
					<li>La barre de navigation permet d'aller à la page d'aide ou de se déconnecter.</li>
					<li>Les filtres (Orientation / Degré scolaire / Classe / État) permettent d'afficher des statistiques (5). En cas de sélection d'une classe, le tableau de chaque élève de la classe s'affiche (voir 6).</li>
					<li>Permet de voir les données des années précédentes.</li>
					<li>Permet de choisir les parties que l'on souhaite imprimer (voir page d'impression).</li>
					<li>Affiche le pourcentage d'apprentis qui ont la note de l'état sélectionné dans les filtres (Vert = +75% / Orange = +25% / Rouge = -25%). Les cases sont cliquables, ce qui vous amènera à son récapitulatif (voir page Récapitulatif Compétence).</li>
					<li>Les tableaux de chaque apprenti sont affichés avec le code couleur si une classe spécifique est chosie dans les filtres et si l'apprenti a au moins fait une évaluation.</li>
				</ol>
			</article>
		</section>
		<section class="col-xs-12 mrgSection" id="recapitulationcompetence">
			<article class="col-xs-12">
				<article class="thead-inverse fgcAide">
						<h3 class="text-center">La page du récapitulatif d'une competence</h3>
				</article>
				<figure class="col-xs-12 frgStyle">
					<a href="../img/imgDirection/RecapitulatifCompetence.png" data-lightbox="direction">
						<img  class="col-xs-12 col-lg-6 col-center-help frgStyle" alt="Image de la page du récapitulatif d'une compétence" src="../img/imgDirection/RecapitulatifCompetence.png"/>
					</a>
				</figure>
			</article>
			<article class="col-xs-12">
				<ol>
					<li>La barre de navigation permet d'aller à la page d'accueil, d'aide ou de se déconnecter.</li>
					<li>Affiche les informations sur la compétence sélectionnée à la page d'accueil et ses pratiques professionnelles.</li>
					<li>Affiche le nombre d'élèves qui ont évalué la pratique professionnelle à (Pas vu, Expliqué, Exercé, Autonome). Les données ont été prises avec les filtres choisis en page d'accueil.</li>
					<li>Affiche les ateliers dans lesquels les élèves ont dit avoir fait la pratique professionnelle et le nombre d'élèves ayant sélectionné l'atelier.</li>
					<li>Affiche les commentaires que les élèves ont écrit en cliquant sur le bouton Commentaires.</li>
				</ol>
			</article>
		</section>
		<section class="col-xs-12 mrgSection" id="ColorCode">
			<article class="col-xs-12">
				<article class="thead-inverse fgcAide">
					<h3 class="text-center">Le code couleurs</h3>
				</article>
				<figure class="col-xs-12 col-lg-6 col-center-help frgStyle" id="fgrcodecouleurs">
					<a href="../img/imgDirection/symboliqueCouleurs.png" data-lightbox="direction">
						<img class="img-responsive" alt="Symbolique des couleurs" src="../img/imgDirection/symboliqueCouleurs.png">
					</a>
				</figure>
			</article>
			<article class="col-xs-12">
				<ul>
					<li><font color="black">Blanc</font> - L’apprenti n’a rien rempli pour la compétence.</li>
					<li><font color="red">Rouge</font> - L’apprenti a au moins sélectionné une fois « Pas vu » sur une pratique professionnelle de la compétence.</li>
					<li><font color="#FABC0F">Jaune</font> - Les pratiques professionnelles de la compétence ont toutes été expliquées.</li>
					<li><font color="#6A0888">Violet</font> - Les pratiques professionnelles de la compétence ont toutes été exercées.</li>
					<li><font color="#07A73E">Vert</font> - Les pratiques professionnelles de la compétence sont toutes autonomes.</li>
				</ul>
			</article>
		</section>
		<section class="col-xs-12 mrgSection" id="print">
			<article class="col-xs-12">
				<article class="thead-inverse fgcAide">
					<h3 class="text-center">La page d'impression</h3>
				</article>
				<figure class="col-xs-12 col-lg-4  frgStyle frgGroup">
					<a href="../img/imgDirection/BasicPrint.png" data-lightbox="direction">
						<img class="img-responsive img-thumbnail" alt="Page d'impression modulable" src="../img/imgDirection/BasicPrint.png">
					</a>
				</figure>
				<figure class="col-xs-12 col-lg-4  frgStyle frgGroup">
					<a href="../img/imgDirection/StudentTablePrint.png" data-lightbox="direction">
						<img class="img-responsive img-thumbnail" alt="Page d'impression modulable" src="../img/imgDirection/StudentTablePrint.png">
					</a>
				</figure>
				<figure class="col-xs-12 col-lg-4  frgStyle frgGroup">
					<a href="../img/imgDirection/DetailedPrint.png" data-lightbox="direction">
						<img class="img-responsive img-thumbnail" alt="Page d'impression modulable" src="../img/imgDirection/DetailedPrint.png">
					</a>
				</figure>
			</article>
			<article class="col-xs-12">
				<ol>
					<li>La barre de navigation permet d'aller à la page d'accueil, d'aide ou de se déconnecter.</li>
					<li>En cliquant sur une case blanche, un tableau concernant le détail de la compétence apparaîtra en-dessous (N°7)</li>
					<li>Le bouton sert à annuler et redirige l’utilisateur à la page d'accueil.</li>
					<li>Le bouton ouvre la fenêtre d'impression du navigateur pour pouvoir imprimmer la page.</li>
					<li>Si une classe a été sélectionnée dans les filtres de la page d'accueil, le tableau des apprentis de celle-ci apparaît en dessous, le tableau des apprentis qui n'ont fait aucune évaluation n'apparaîtra pas.</li>
					<li>Le tableau des apprentis peut être supprimer en cliquant sur la croix. Si le tableau d'un apprenti est enlevé, il faut rechargé la page pour le remettre à nouveau. Un message demandant de confirmer apparaîtra.</li>
					<li>En cliquant sur la croix, la partie du détail s'enlèvera. Pour faire réapparaitre les parties supprimées, il faut cliquer sur la case du tableau principal (N°2).</li>
				</ol>
			</article>
		</section>
    <section class="col-xs-12 mrgSection" id="aide_video">
			<article class="col-xs-12">
				<article class="thead-inverse fgcAide">
					<h3 class="text-center">La vidéo d'aide</h3>
				</article>
				<div class="col-lg-6">
				<table class="table table-responsive table-bordered table-striped">
						<thead class="thead-inverse">
							<tr>
								<th>Table des matières</th>
							</tr>
						</thead>
						<tbody id="bodyM">
							<tr>
								<td><a onClick="timeVideo(0);">Page d'accueil - Filtres + Historique</a></td>
							</tr>
							<tr>
								<td><a onClick="timeVideo(28)">Page d'accueil - Impression</a></td>
							</tr>
							<tr>
								<td><a onClick="timeVideo(49)">Gestion des compétences</a></td>
							</tr>
							<tr>
								<td><a onClick="timeVideo(136)">Gestion des utilisateurs</a></td>
							</tr>
		          <tr>
		            <td><a onClick="timeVideo(199)">Gestion des ateliers</td>
		          </tr>
							<tr>
		            <td><a onClick="timeVideo(235)">Aide</td>
		          </tr>
							<tr>
		            <td><a onClick="timeVideo(250)">À propos</td>
		          </tr>
						</tbody>
					</table>
				</div>
				<figure class="col-xs-12 col-lg-6 frgStyle">
					<a href="../vid/aide_aec.mp4" data-lightbox="direction">
            <video id="help_video" controls class="col-lg-12 frgStyle" alt="Page d'impression modulable" preload="auto">
							<source id="help_source" src="../vid/aide_aec.mp4"/>
						</video>
					</a>
				</figure>
			</article>
		</section>
	</section>
	<aside class="col-xs-0 col-sm-3 asideHelpPage">
			<a class="btn btn-danger nb-1" id="returnButtonDirectionHelpPage"><span class="glyphicon glyphicon-arrow-left"></span> Retour à la page précédente</a>
	</aside>
</body>
<script>
$('#bodyM > tr > td').click(function()
{
	if("#" + document.location.href.split('#')[1] != $(this).children().attr('href'))
	{
		var nb = typeof $('.asideHelpPage').attr('id') === 'undefined' ? 1 : parseInt($('.asideHelpPage').attr('id')) + 1;
		$('.asideHelpPage').attr('id', nb);
	}
});
$("#returnButtonDirectionHelpPage").click(function()
{
	var nb = typeof $('.asideHelpPage').attr('id') === 'undefined' ? 1 : parseInt($('.asideHelpPage').attr('id')) + 1;
	history.go(-nb);
});

function timeVideo(time) {
	var video = document.getElementById('help_video');
	var source = document.getElementById('help_source');
	source.setAttribute('src', '../vid/aide_aec.mp4#t=' + time);
	video.load();
  video.play();
}
</script>
</html>
