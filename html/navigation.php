<?php
require_once '../../config/conparam.php';
?>
<header>
    <nav class="navbar navbar-default">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar1" aria-expanded="false" style="padding-bottom: 5px;">
                    <span class="sr-only">Barre de navigation</span>
                    <span class="glyphicon glyphicon-menu-hamburger"></span>
                </button>
                <a class="navbar-brand" href="/index.php" style="padding-top: 5px;">
                    <img class="img-responsive" id="logoEE" name="logoEE" alt="Logo_EE" src="../img/LogoAecTexte.png" style="padding-left: 5px;"/>
                </a>
            </div>
            <div>
                <div class="text-right" style="float: right;">
                    <a class="navbar-brand" href="/index.php" style="padding-top: 5px;">
                        <span class="navbar-brand"  style="padding-left: 5px;">École d'informatique</span>
                        <img class="img-responsive" id="logoEE" name="logoEE" alt="Logo_EE" src="../img/logoEntrepriseEcole.png" style="padding-top: 5px;"/>
                    </a>
                </div>
            </div>
            <div class="collapse navbar-collapse" id="navbar1">
                <div class="container">
                    <ul class="nav navbar-nav container text-center" style="line-height: 52px;">
                        <?php foreach(NAV_LINKS[$_SESSION['role']] as $link): ?>
                            <li><a href="<?= $link['link'] ?>"><span class="<?= $link['icon'] ?>"></span><?= $link['name'] ?></a></li>
                        <?php endforeach; ?>
                        <li><a href="../aPropos.php"><span class="glyphicon glyphicon-question-sign"></span> À Propos</a></li>
                        <li><a href="../../php/logout.php"><span class="glyphicon glyphicon-log-out"></span> Déconnexion</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</header>