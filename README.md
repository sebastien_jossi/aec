

# **AEC (Auto Évaluations Compétences)**

---

[![Website shields.io](https://img.shields.io/website-up-down-green-red/http/shields.io.svg)](http://shields.io/) [![Generic badge](https://img.shields.io/badge/version-1.0-blue.svg)](https://shields.io/) [![Generic badge](https://img.shields.io/badge/use-git%20%7C%20composer%20%7C%20php%20%7C%20js%20%7C%20html%20%7C%20css-orange.svg)](https://shields.io/) [![Generic badge](https://img.shields.io/badge/code%20quality-%C2%AF%5C%28%E3%83%84%29%2F%C2%AF-orange.svg)](https://shields.io/) [![Generic badge](https://img.shields.io/badge/built%20by-developpers%20%3C/%3E-purple.svg)](https://shields.io/)

AEC est une application web permettant aux élèves / étudiants de s'auto évaluer pour des compétences professionnelles définies par les différents professeurs / directions.

➡️ [aec.cfpti.ch](https://aec.cfpti.ch) | ⚙️ [dev.aec.cfpti.ch](https://dev.aec.cfpti.ch) | [ofpc.aec.cfpti.ch](https://ofpc.aec.cfpti.ch)

# Dépendances

---

> Les sous dépendances de paquets sont installer automatiquement lors de l'installation du paquet principal

* mpdf/mpdf

  [MPDF/MPDF](https://packagist.org/packages/mpdf/mpdf)

# Installation

---

## [PHP](<https://windows.php.net/download#php-7.3>)

Premièrement, il vous faut installer **PHP 7.3** sur votre post. Prenez le zip le plus à jour. Une fois télécharger, il vous faut l’extraire à la racine de votre disque.

## [GIT](<https://git-scm.com/downloads>)

De plus, il va vous falloir GIT. 

## [UwAmp](<https://www.uwamp.com/fr/?page=download>)

Pour que le site marche bien en local durant le développement il suffit d’installer **UwAmp 3.1.0** de préférence.

## [Composer](<https://getcomposer.org/download/>)

Le présent projet demande d’avoir certaines dépendances. 

## php.ini

Quelques modification dans le fichier suivant sont nécessaires.

~~~
C:\php\php.ini
~~~

Vous devez activer certaines extensions php pour que projet fonctionne correctement. Voici celles qu'il fait activer :

~~~
extension=php_mbstring.dll
extension=php_mysqli.dll
extension=php_openssl.dll
extension=php_pdo_mysql.dll
~~~

## Installation locale

Pour le déploiement du site sur votre environnement local, allez dans le dossier 

~~~
C:\UwAmp\www
~~~

Faite un click-droit n’importe où dans le dossier et ouvrez Git Bash. Faite ensuite la commande : 

~~~
git clone https://bitbucket.org/sebastien_jossi/aec.git
~~~

Allez dans le projet, ouvrez le cmd et tapez la commande suivante :

~~~
 composer install
~~~

Vous aurez maintenant plus qu’à aller dans le localhost et cliquer sur « aec ».

# Credits

---

|  Développeurs  |   Responsable   |               Client                |
| :------------: | :-------------: | :---------------------------------: |
| Tanguy CAVAGNA | Sébastien JOSSI | Direction CFPT-I (Georges MARTINEZ) |
|  Diogo CANAS   |                 |                                     |
| Quentin FASLER |                 |                                     |