<?php
/**
 * @brief @brief Charge toutes les évaluations en fonction des filtres et compte le nombre d'étudiant contenu dans les filtres
 * @param $guidanceId L'identifiant de l'orientation /!\ OBLIGATOIRE
 * @param $degreeId L'identifiant du degrée 
 * @param $classroomId L'identifiant de la classe 
 * @param $yearId L'identifiant de l'année /!\ OBLIGATOIRE
 * 
 * @author ronaldo.lrrpnt@eduge.ch
 */
require_once './inc.all.php';

$guidanceId =  - 1;
$domainId = - 1;
$skillId = - 1;
$yearId = - 1;
$classroomId = - 1;
$degreeId = - 1;

if(isset($_POST['guidanceId']))
	$guidanceId = $_POST['guidanceId'];

if(isset($_POST['domainId']))
	$domainId = $_POST['domainId'];

if(isset($_POST['skillId']))
	$skillId = $_POST['skillId'];
	
if(isset($_POST['yearId']))
	$yearId = $_POST['yearId'];

if(isset($_POST['classroomId']) && $_POST['classroomId'] != "")
	$classroomId = $_POST['classroomId'];

if(isset($_POST['degreeId']) && $_POST['degreeId'] != "")
	$degreeId = $_POST['degreeId'];
	
// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');
if(($guidanceId != - 1) && ($yearId != - 1))
{
	$data = EDataManager::getInstance()->loadAllDataByFilter($guidanceId, $domainId, $skillId, $degreeId, $classroomId, $yearId);
	if ($data === false)
	{
		echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de loadAllDataByFilter()"}';
		exit();
	}
	
	$jsn = json_encode($data);
	if ($jsn == FALSE){
		$code = json_last_error();
		echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json ('.$code.'"}';
		exit();
	}
	echo '{"ReturnCode": 0, "Data": '.utf8_encode($jsn).'}';
	exit();
}

// Si j'arrive ici, c'est pas bon
echo '{ "ReturnCode": 1, "Message": "Il manque les paramètres guidanceId et/ou yearId"}';

?>