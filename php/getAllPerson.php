<?php

/**
 * @copyright quentin.fslr@eduge.ch 2018-2019
 * @brief Récupère toutes les personnes
 */
require_once './inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');


$personns = EUtilitiesManager::getInstance()->LoadAllPersonn();
if ($personns === false) {
    echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de getAllPersonn()"}';
    exit();
}

$jsn = json_encode($personns);

if ($jsn == FALSE) {
    $code = json_last_error();
    echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json (' . $code . '"}';
    exit();
}

echo '{ "ReturnCode": 0, "Data": ' . utf8_encode($jsn) . '}';
exit();