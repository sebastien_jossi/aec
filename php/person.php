<?php

/**
 * @copyright floran.stck@eduge.ch 2016-2017
 */

/**
 * Classe d'orientation
 * @author FS
 */

class EPerson{
	
	/**
	 * 
	 * @param $InIdPerson L'identifiant de la personne
	 * @param $InFirstName Le prénom de la personne
	 * @param $InLastName Le nom de famille de la personne
	 * @param $InEmail L'email de la personne
	 * @param $InRole Le role de la personne
	 * @param $InIsActive Si la personne est active
	 * @param $InIsStudent Si la personne est un élève
	 */
	
	public function __construct($InIdPerson = -1, $InFirstName = "",$InLastName = "",$InEmail = "",$InRole = -1,$InIsActive = false,$InIsStudent = false){
		
		$this->idPerson = $InIdPerson;
		$this->firstname = $InFirstName;
		$this->lastname = $InLastName;
		$this->email = $InEmail;
		$this->isActive = $InIsActive;
		$this->isStudent = $InIsStudent;
	}

	public function  getidPerson(){
		return $this->idPerson;
	}
	
	public function getFirstName(){
		
		return $this->firstname;
	}
	
	public function getLastName(){
	
		return $this->lastname;
	}
		
	public function getEmail(){
	
		return $this->email;
	}
	
	/**
	 * @brief Est-ce que cet utilisateur est un élève
	 *
	 * @return True si il fait partie des élèves, autrement false
	 */
	public function isStudent(){
	
		return $this->isStudent;
	}
	/**
	 * @brief Est-ce que cet utilisateur est actif
	 *
	 * @return True s'il est actif, autrement false
	 */
	public function isActive(){
	
		return $this->isActive;
	}
	
	public $idPerson;
	public $firstname;
	public $lastname;
	public $email;
	public $isStudent;
	public $isActive;
}