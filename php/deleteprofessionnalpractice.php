<?php
/**
 * @brief	Permet d'envoyer les variables à appManager
* 			pour supprimer les pratiques liées à une compétence dans la base de données
* @author 	romain.ssr@eduge.ch
*/
require_once './inc.all.php';

$guidanceid = -1;
$domainid ="";
$skillid =-1;
$practiceinfos = -1;
$practiceid =-1;

if (isset($_POST['guidanceId'])&&isset($_POST['domainId'])&&isset($_POST['skillId'])&&isset($_POST['practiceInfos'])){
	$guidanceid = $_POST['guidanceId'];
	$domainid = $_POST['domainId'];
	$skillid = $_POST['skillId'];
	$practiceinfos = $_POST['practiceInfos'];
}
else {
	echo '{ "ReturnCode": 1, "Message": "Erreur dans les paramètres. Valeurs manquantes."}';
	exit();
}


// J'envoie les informations nécessaires pour ajouter une compétence dans la base de donée.
foreach($practiceinfos as $i){
	$practiceid = $i["practiceid"];

	if (!EAppManager::getInstance()->deletePractice($guidanceid, $domainid, $skillid, $practiceid)){
		echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de loadAllGuidance()"}';
		exit();
	}
}

echo '{ "ReturnCode": 0}';
?>