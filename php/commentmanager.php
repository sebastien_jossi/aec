<?php
require_once '../php/database.php';
require_once '../php/comment.php';

/**
 * @brief Helper class pour gérer les commentaires du site
 *
 * @author ronaldo.lrrpnt@eduge.ch
 */
class ECommentManager
{
	private static $objInstance;
	
	/**
	 * @brief Class Constructor - Create a new ECommentManager if one doesn't exist
	 * Set to private so no-one can create a new instance via ' = new
	 * ECommentManager();'
	 */
	private function __construct ()
	{
		$this->comments = array();
	}
	
	/**
	 * @brief Like the constructor, we make clone private so nobody can clone
	 * the instance
	 */
	private function __clone ()
	{}
	
	/**
	 * @brief Retourne notre instance ou la crée
	 *
	 * @return $objInstance;
	 */
	public static function getInstance ()
	{
		if (!self::$objInstance)
		{
			try
			{
				self::$objInstance = new ECommentManager();
	
			} catch (Exception $e)
			{
				echo "EDataManager Error: " . $e;
			}
		}
		return self::$objInstance;
	}
	
	/**
	 * Retourne le tableau de tout les commentaires
	 *
	 * @return Le tableau des EComment
	 */
	public function getComments()
	{
		return $this->comments;
	}
	
	/**
	 * Charge tout les commentaires d'une compétence spécifié
	 * @param $InGuidanceId Identifiant de l'orientation
	 * @param $InDomainId Identifiant du domaine
	 * @param $InSkillId Identifiant de la compétence
	 * @return Le tableau des EComment | false si une erreur se produit
	 */
	public function loadCommentBySkill ($InGuidanceId, $InDomainId, $InSkillId)
	{
		$this->comments = array();
		$sql = 'SELECT ID, SKILL_CODE, Person_idPerson, SchoolEmail, COMMENT, DATE_FORMAT(DATECOMMENT,\'%d.%m.%Y\')DATECOMMENT FROM COMMENTS JOIN Person ON Person_idPerson = idPerson WHERE GUIDANCES_CODE = :GuId AND DOMAINS_ID = :DoId AND SKILL_CODE = :SkId ORDER BY ID DESC';
		try
		{
			$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute(array(':GuId' => $InGuidanceId, ':DoId' => $InDomainId, ':SkId' => $InSkillId));
	
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT))
			{
				// Création du commentaire avec les données provenant de la
				// base de données
				$c = new EComment($row['ID'], $row['SKILL_CODE'], $row['Person_idPerson'], $row['COMMENT'], $row['DATECOMMENT'], $row['SchoolEmail']);
				array_push($this->comments, $c);
			}
		} catch (PDOException $e)
		{
			echo "ECommentManager:loadCommentBySkill Error: " . $e->getMessage();
			return false;
		}
		// OK
		return $this->comments;
	}

	/**
	 * Supprime le commentaire en fonction de l'id
	 *
	 * @param int $id
	 * @return boolean
	 */
	public function delCommentById($CommentId) {
		$sql = "DELETE FROM COMMENTS WHERE ID = :CommentId AND Person_idPerson = :PersonId";
		$sqlStudentID = "SELECT * FROM `COMMENTS` WHERE Person_idPerson = :StudentId and ID = :CommentId";
		try
		{
			$getCom = EDatabase::prepare($sqlStudentID, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$getCom->execute(array(':StudentId' => $_SESSION['id'], ':CommentId' => $CommentId));
			$data = $getCom->fetch();

			if ($data) {
				$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
				$stmt->execute(array(':CommentId' => $CommentId, ':PersonId' => $_SESSION['id']));
	
				return true;
			} else {
				return false;
			}
		} 
		catch (PDOException $e)
		{
			echo "ECommentManager:delComment Error: " . $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Ajoute un nouveau commentaire dans la base de donnée
	 * @param $InGuidanceId l'identifiant de l'orientation
	 * @param $InDomainId L'identifiant du domaine qui lui correspond
     * @param $InSkillId Identifiant de la compétence
     * @param $InStudentId Identifiant de l'apprenti
	 * @return true OK | false si une erreur se produit
	 */
	public function addComment($InGuidanceId, $InDomainId, $InSkillId, $InStudentId, $InComment)
	{
		$sql = "INSERT INTO COMMENTS(SKILL_CODE, DOMAINS_ID, GUIDANCES_CODE, Person_idPerson, COMMENT) VALUES (:SkId, :DoId, :GuId, :PeId, :CoTx)";
	
		try
		{
			$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute(array(':GuId' => $InGuidanceId, ':DoId' => $InDomainId, ':SkId' => $InSkillId, ':PeId' => $InStudentId, ':CoTx' => $InComment));
			return true;
		} catch (PDOException $e)
		{
			echo "ECommentManager:addComment Error: " . $e->getMessage();
			return false;
		}
	}

	/**
	 * Récupère tout les commentaires
	 *
	 * @return string Tout les commentaires | Message d'erreur
	 */
	public static function getAllComments() {
		$sql = "SELECT Person.schoolEmail, COMMENTS.COMMENT, COMMENTS.SKILL_CODE, COMMENTS.DOMAINS_ID, COMMENTS.GUIDANCES_CODE FROM COMMENTS JOIN Person ON COMMENTS.Person_idPerson = Person.idPerson";

		try
		{
			$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute();
			$allComments = $stmt->fetchAll(PDO::FETCH_ASSOC);

			return $allComments;
		} catch (PDOException $e) {
			return "ECommentManager:getAllComments Error: " . $e->getMessage();
		}
	}

	/**
	 * Récupère tout les commentaires de l'utilisateur en question
	 *
	 * @return string Tout les commentaires | Message d'erreur
	 */
	public static function getAllCommentsByStudent($studentId) {
		$sql = "SELECT Person.schoolEmail, COMMENTS.COMMENT, COMMENTS.SKILL_CODE, COMMENTS.DOMAINS_ID, COMMENTS.GUIDANCES_CODE FROM COMMENTS JOIN Person ON COMMENTS.Person_idPerson = Person.idPerson WHERE COMMENTS.Person_idPerson = :studentId";

		try
		{
			$stmt = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute(array(':studentId' => $studentId));
			$allComments = $stmt->fetchAll(PDO::FETCH_ASSOC);

			return $allComments;
		} catch (PDOException $e) {
			return "ECommentManager:getAllComments Error: " . $e->getMessage();
		}
	}
	
	/** Tableau de commentaire */
	private $comments;
}
