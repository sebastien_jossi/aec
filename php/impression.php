<?php
/**
 * author: Tanguy CAVAGNA
 * description: Génère un pdf pour un élève
 */
require_once '../vendor/autoload.php';
require_once '../php/inc.all.php';

// Récupère le tableau de l'élève sans sanitize pour récupérer les tags html
$data = filter_input(INPUT_POST, 'data');

if ($data !== null) {
    $user = EUserManager::getInstance()->findUserByEmail($_SESSION['email']);

    // Formate le pdf en uth-8 et en paysage
    $mpdf = new \Mpdf\Mpdf(['mode' => 'utf-8', 'format' => 'A4-L']);
    $mpdf->WriteHTML(file_get_contents('../html/CSS/print.css'), \Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML('', \Mpdf\HTMLParserMode::HEADER_CSS);
    $mpdf->WriteHTML($data, \Mpdf\HTMLParserMode::HTML_BODY);
    $mpdf->WriteHTML($user->firstname . '.' . $user->lastname . ' - ' . EUserManager::getInstance()->getClassNameById($user->studentclass) . ' - ' . date('d.m.Y'), \Mpdf\HTMLParserMode::HTML_BODY);
    $pdfName = date('d.m.Y') . '-' . $user->firstname . '.' . $user->lastname . '.pdf';
    $mpdf->Output('../pdf-downloads/' . $pdfName, 'F');

    echo json_encode(['name' => $pdfName]);
    exit();
}

echo json_encode([]);
exit();