<?php
/**
 * @brief @brief Charge toutes les évaluations en fonction des filtres et compte le nombre d'étudiant contenu dans les filtres
 * @param $guidanceId L'identifiant de l'orientation /!\ OBLIGATOIRE
 * @param $degreeId L'identifiant du degrée
 * @param $classroomId L'identifiant de la classe
 * @param $yearId L'identifiant de l'année /!\ OBLIGATOIRE
 * @param $stateId L'identifiant de l'état /!\ OBLIGATOIRE
 *
 * @author ronaldo.lrrpnt@eduge.ch
 */
if (session_status() == PHP_SESSION_NONE) {     session_start(); }

$_SESSION['guidanceId'] = $_POST['guidanceId'];

$_SESSION['yearId'] = $_POST['yearId'];

if (isset($_POST['classroomId']))
	$_SESSION['classroomId'] = $_POST['classroomId'];
else 
	$_SESSION['classroomId'] = null;

if (isset($_POST['degreeId']))
	$_SESSION['degreeId'] = $_POST['degreeId'];
else 
	$_SESSION['degreeId'] = null;

$_SESSION['stateId'] = $_POST['stateId'];

echo '{"ReturnCode": 0, "Data": "OK"}';
exit();

?>