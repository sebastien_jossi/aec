<?php
/**
 * @copyright ronaldo.lrrpnt@eduge.ch 2016-2017
 */

/**
 * Classe qui contient les données de chaque pratique professionnel des apprentis 
 * @author RLP
 */
class EDataPractice implements JsonSerializable
{
	/**
	 * @brief Class Constructor
	 * @param $InGuidanceId L'identifiant de l'orientation
	 * @param $InDomainId L'identifiant du domaine
	 * @param $InSkillId L'identifiant de la compétence
	 * @param $InPracticeId L'identifiant de la pratique professionnel
	 * @param $InOwnerId L'identifiant du parent (EDataStudent)
	 * @param $InStateId L'identifiant de l'état
	 * @param $InYearId L'identifiant de l'année de modification
	 */
	public function __construct ($InGuidanceId = - 1 ,$InDomainId = - 1, $InSkillId = - 1, $InPracticeId = - 1, $InOwnerId = - 1, $InStateId = - 1, $InYearId = - 1, $InDate = "")
	{
		$this->guidanceid = $InGuidanceId;
		$this->domainid = $InDomainId;
		$this->skillid = $InSkillId;
		$this->practiceid = $InPracticeId;
		$this->owner = $InOwnerId;
		$this->stateid = $InStateId;
		$this->yearid = $InYearId;
		$this->yearid = $InYearId;
		$this->date = $InDate;
		$this->workshops = array();
	}
	
	/**
	 * @brief On ne laisse pas cloner une données de l'apprentis
	 */
	private function __clone ()
	{}
	
	/**
	 * @brief Est-ce que cet objet est valide
	 *
	 * @return True si valide, autrement false
	 */
	public function isValid ()
	{
		return (($this->guidanceid == - 1) || ($this->domainid == - 1) || ($this->skillid == - 1) || ($this->practiceid == - 1) || ($this->owner == - 1) || ($this->stateid == - 1) || ($this->yearid == - 1)) ? false : true;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return L'identifiant de l'orientation
	 */
	public function getGuidanceId ()
	{
		return $this->guidanceid;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return L'identifiant du domaine
	 */
	public function getDomainId ()
	{
		return $this->domainid;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return L'identifiant de la compétence 
	 */
	public function getSkillId ()
	{
		return $this->skillid;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return L'identifiant de la practique professionnel
	 */
	public function getPracticeId ()
	{
		return $this->practiceid;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return L'identifiant du parent (EDataStudent) 
	 */
	public function getOwnerId ()
	{
		return $this->owner;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return L'identifiant de l'état
	 */
	public function getStateId ()
	{
		return $this->stateid;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return L'identifiant de l'année de modification
	 */
	public function getYearId ()
	{
		return $this->yearid;
	}
	
	
	/**
	 * Getter
	 *
	 * @return Contient les ateliers(EWorkshop) que l'apprentis aura mentionné
	 */
	public function getWorkshops()
	{
		return $this->workshops;
	}
	
	/***
	 * Transforme l'objet en json
	 * @return L'objet en json
	 */
	public function jsonSerialize()
	{
		return get_object_vars($this);
	}
	
	/**
	 * Setter
	 */
	public function modifyStateDataPractice($InStateId)
	{
		$this->stateid = $InStateId;
	}	
	
	/**
	 * Setter
	 */
	public function addWorkshopDataPractice($InWorkshop)
	{
		array_push($this->workshops, $InWorkshop);
	}
	
	/** L'identifiant de l'orientation */
	private $guidanceid;
	/** L'identifiant du domaine */
	private $domainid;
	/** L'identifiant de la compétence */
	private $skillid;
	/** L'identifiatn de la practique professionnel */
	private $practiceid;
	/** L'identifiant du parent (EDataStudent) */
	private $owner;
	/** L'identifiant de l'état */
	private $stateid;
	/** L'identifiant de l'année de modification */
	private $yearid;
	/** Contient les ateliers(EWorkshop) que l'apprentis aura mentionné */
	private $workshops;
	
	private $date;
}