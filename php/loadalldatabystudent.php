<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
/**
 * @brief @brief Charge toutes les évaluations en fonction des filtres et compte le nombre d'étudiant contenu dans les filtres
* @param $studentId L'identifiant de l'apprenti /!\ OBLIGATOIRE
*
* @author floran.stck@eduge.ch
*/
require_once './inc.all.php';


	$studentId = $_SESSION['id'];

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');
if(($studentId != - 1))
	{
		// Je récupère toutes les orientations
		$data = EDataManager::getInstance()->loadAllDataByStudent($studentId);
			if ($data === false){
				echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de loadAllDataByStudent()"}';
				exit();
				}
		$jsn = json_encode($data);
			if ($jsn == FALSE){
			$code = json_last_error();
			echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json ('.$code.'"}';
			exit();
			}
		echo '{"ReturnCode": 0, "Data": '.utf8_encode($jsn).'}';
		exit();
	}


// Si j'arrive ici, c'est pas bon
echo '{ "ReturnCode": 1, "Message": "Il manque les paramètres studentId"}';

?>