<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
require_once './database.php';
require_once './degree.php';
require_once './years.php';
require_once './states.php';
require_once './workshops.php';

/**
 * @copyright floran.stck@eduge.ch 2016-2017
 */

/**
 * Classe des compétences opérationnelles
 * @author FS
 */


class EUtilitiesManager{


	private static $objInstance;


	/**
	 * 
	 */
	public function __construct()
	{
		$this->degrees = array();
	}

	/**
	 * @brief On ne laisse pas cloner une compétence
	 */
	private function __clone(){}
	
	public static function getInstance()
	{
		if (! self::$objInstance)
		{
			try
			{
				self::$objInstance = new EUtilitiesManager();

			} catch (Exception $e)
			{
				echo "EUtilitiesManager Error: " . $e;
			}
		}
		return self::$objInstance;
	}

	/**
	 * On récupère les degrés dans la base de données
	 */
	public function loadAllDegree()
	{
		$this->degrees = array();
		$sql = 'SELECT ID, DEGREE FROM DEGREES';
		try
		{
			$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute();

			while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT))
			{
				// Création du degrée avec les données provenant de la
				// base de données
				$d = new EDegree($row['ID'], $row['DEGREE']);
				array_push($this->degrees, $d);
			}
		} catch (PDOException $e)
		{
			echo "EUtilitiesManager:loadAllDegree Error: " . $e->getMessage();
			return false;
		}
		return $this->degrees;
	}

	/**
	 * On récupère les années dans la base de données
	 * @return boolean|array
	 */
	public function loadAllYears()
	{
		$this->years = array();
		$sql = 'SELECT CODE, LABEL FROM YEARS ORDER BY LABEL DESC';
		try
		{
			$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute();
	
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT))
			{
				// Création du degrée avec les données provenant de la
				// base de données
				$y = new EYears($row['CODE'], $row['LABEL']);
				array_push($this->years, $y);
			}
		} catch (PDOException $e)
		{
			echo "EUtilitiesManager:loadAllYears Error: " . $e->getMessage();
			return false;
		}
		return $this->years;
	}
	
	/**
	 * On récupère les états dans la base de données
	 * @return boolean|array
	 */
	public function loadAllStates()
	{
		$this->states = array();
		$sql = 'SELECT CODE, LABEL FROM STATES';
		try
		{
			$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute();
	
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT))
			{
				// Création du degrée avec les données provenant de la
				// base de données
				$s = new EStates($row['CODE'], $row['LABEL']);
				array_push($this->states, $s);
			}
		} catch (PDOException $e)
		{
			echo "EUtilitiesManager:loadAllStates Error: " . $e->getMessage();
			return false;
		}
		return $this->states;
	}
	
	
	
	/**
	 * On récupère les ateliers dans la base de données suivant l'année de l'élève
	 * @return boolean|array
	 */
	public function loadWorkshopByDegree($InDegreeId,$domainId,$practiceId,$skillCode)
	{
		$allWorkshops = array();
		$tworkshop = array();
		$sqlCurrent = 'SELECT  ID, NAME, DEGREES_ID FROM CURRENT_WORKSHOPS WHERE DEGREES_ID = :DeId';
		$sqlWork = 'SELECT  ID,NAME,DEGREES_ID, DOMAINS_ID, SKILL_CODE FROM `PROFESSIONNAL_PRACTICE_WORKSHOPS` join WORKSHOPS on WORKSHOPS_ID=ID  WHERE `DOMAINS_ID`= :domainId and  idPerson = :studentId';
		try
		{
		    if ($InDegreeId == 0) {
		        $sqlAll = 'SELECT ID, NAME, DEGREES_ID FROM CURRENT_WORKSHOPS';

                $stmt = EDatabase::prepare($sqlAll, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
                $stmt->execute(array(':DeId' => $InDegreeId));

                $stmt = $stmt->fetchAll(PDO::FETCH_ASSOC);

                foreach ($stmt as $a) {
                    array_push($allWorkshops, $a);
                }
            } else {
                $stmt = EDatabase::prepare($sqlCurrent, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
                $stmt->execute(array(':DeId' => $InDegreeId));

                $stmt = $stmt->fetchAll(PDO::FETCH_ASSOC);

                foreach ($stmt as $a) {
                    array_push($allWorkshops, $a);
                }

                $stmt = EDatabase::prepare($sqlWork, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
                $stmt->execute(array(':domainId' => $domainId, ':studentId' => $_SESSION["id"]));

                $stmt = $stmt->fetchAll(PDO::FETCH_ASSOC);

                foreach ($stmt as $a) {
                    array_push($allWorkshops, $a);
                }

                $allWorkshops = array_unique($allWorkshops, SORT_REGULAR);
            }

            foreach ($allWorkshops as $row) {
                // Création de l'atelier avec les données provenant de la
                // base de données
                $w = new EWorkshop($row['ID'], $row['NAME'], $row['DEGREES_ID']);
                array_push($tworkshop, $w);
            }
		} catch (PDOException $e)
		{
			echo "EAppManager:loadWorkshopByDegree Error: " . $e->getMessage();
			return false;
		}
		return $tworkshop;
	}
	/**
	 * On récupère tous les ateliers dans la base de données 
	 * @return boolean|array
	 */
	public function loadCurrentWorkshop()
	{
        //SELECT ID, NAME,DESCRIPTION, 
        //(CASE WHEN DEGREES_ID = "1" THEN "1ère" 
        //WHEN DEGREES_ID = "2" THEN "2ème" 
        //WHEN DEGREES_ID = "3" THEN "3ème" 
        //WHEN DEGREES_ID = "4" THEN "4ème" END)
        //AS DEGREES_ID FROM CURRENT_WORKSHOPS
        
         //SELECT ID, NAME,DESCRIPTION, 
         //IF(DEGREES_ID = 1,CONCAT(DEGREES_ID,"ère"),CONCAT(DEGREES_ID,"ème"))
         //as DEGREES_ID FROM CURRENT_WORKSHOPS   
            
		$tworkshop = array();
		$sql = 'SELECT ID, NAME, DEGREES_ID, DESCRIPTION FROM CURRENT_WORKSHOPS';
		try
		{
			$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute();

			while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT))
			{
				// Création de l'atelier avec les données provenant de la
				// base de données
				$w = new ECurrentWorkshop($row['ID'],$row['NAME'],$row['DEGREES_ID'], $row['DESCRIPTION'] );
				array_push($tworkshop, $w);
			}
		} catch (PDOException $e)
		{
			echo "EAppManager:loadWorkshopByDegree Error: " . $e->getMessage();
			return false;
		}
		return $tworkshop;
	}
	
	/**
	 * On récupère les classes dans la base de données
	 * @return boolean|array
	 */
	public function loadAllClassrooms()
	{
		$this->classrooms = array();
		$sql = 'SELECT idClass, name FROM Class';
		try
		{
			$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute();
	
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT))
			{
				// Création du degrée avec les données provenant de la
				// base de données
				$c = new EClassroom($row['idClass'], $row['name']);
				array_push($this->classrooms, $c);
			}
		} catch (PDOException $e)
		{
			echo "EUtilitiesManager:loadAllClassrooms Error: " . $e->getMessage();
			return false;
		}
		return $this->classrooms;
	}
	/**
	 * Retourne un tableau des utilisateurs
	 *
	 * @return array Tableau des utilisateurs
	 */
	public function LoadAllPersonn(){
		$tPersonn = array();

		$sql = 'SELECT idPerson,firstName,lastName,schoolEmail,idRole,active,DEGREES_ID,Class.name,idClass FROM `Person` join Person_Belongs_Class using(idPerson) join Class using(idClass) ORDER BY `Person`.`idPerson` DESC';
		try
		{
			$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute();
	
			while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT))
			{
				// Création d'une personn avec les données provenant de la
				// base de données
				if($row['idRole']==1)
					$IsStudent= true;
				else
					$IsStudent = false;
				
				$p = new EAllPerson($row['idPerson'], $row['firstName'],$row['lastName'],$row['schoolEmail'],$row['idRole'],$row['active'],$IsStudent,$row['name'],$row['idClass'],$row['DEGREES_ID']);
				array_push($tPersonn, $p);
			}
		} catch (PDOException $e)
		{
			echo "EUtilitiesManager:loadAllClassrooms Error: " . $e->getMessage();
			return false;
		}
		return $tPersonn;
	}

	/**
	 * Permet de créez une nouvelle année (rentrée scolaire) et de faire passe les élèves a l'année superieur
	 * INFO :
	 * Eleves accélères passe de 2eme a 4ème
	 * Eleves qui finissent la 4eme sont desactive
	 * 
	 * !!! L'année est déterminer par la date du serveur !!!
	 *
	 * @return true si aucun problème
	 */
	public function NouvelleAnnee(){

		//Desactive toutes les élèves en 4eme
		$sqlDeactivate4eme = "UPDATE Person join Person_Belongs_Class  using(idPerson) join DEGREES on DEGREES_ID= DEGREES.ID  SET active=0 WHERE active=1 and idRole=1 and DEGREES.DEGREE=4";

		//Passe l'année pour les élèves non accelerer
		$sqlStudentNonAccelerer = "UPDATE Person join Person_Belongs_Class using(idPerson) join Class using(idClass) SET Person_Belongs_Class.DEGREES_ID=Person_Belongs_Class.DEGREES_ID+1,idClass=Class.idClassSuivant WHERE Class.accelerer = 0 and active=1 and idRole=1 and DEGREES_ID<4";

		//Passe l'année pour les élève accelerer
		$sqlStudentAccelerer =  "UPDATE Person join Person_Belongs_Class using(idPerson) join Class using(idClass) SET Person_Belongs_Class.DEGREES_ID = ( CASE WHEN Person_Belongs_Class.DEGREES_ID=1 THEN 2 WHEN Person_Belongs_Class.DEGREES_ID=2 THEN 4 END ) , idClass=Class.idClassSuivant  WHERE Class.accelerer = 1 and active=1 and idRole=1 and DEGREES_ID<4";
		$sqlNewAnnee = "INSERT INTO YEARS (LABEL) VALUES (:LabelAnnee)";

		//Permet de recupere l'annuel actuel du serveur
		$sqlDernierAnneeBDD = "SELECT * FROM `YEARS` ORDER BY `YEARS`.`CODE` DESC limit 1";

		//Delete les commentaires
		$sqlDeleteCommentaires  ="TRUNCATE COMMENTS";

		$LabelAnnee =  date('Y') . "-" . (date('Y')+1);



		try
		{		
			$reqVeriferAnnee = EDatabase::prepare($sqlDernierAnneeBDD, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$reqVeriferAnnee->execute();
			$dataDeactive = $reqVeriferAnnee->fetch();
			if($dataDeactive["LABEL"]==$LabelAnnee){
				return false;
				exit();
			}
			else{
				EDatabase::getInstance()->beginTransaction();
				$reqDeactive4eme = EDatabase::prepare($sqlDeactivate4eme, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
				$reqDeactive4eme->execute();
				
				$reqStudentNonAccelerer = EDatabase::prepare($sqlStudentNonAccelerer, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
				$reqStudentNonAccelerer->execute();
	
				$reqStudentAccelerer = EDatabase::prepare($sqlStudentAccelerer, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
				$reqStudentAccelerer->execute();
	
				$reqNewAnnee = EDatabase::prepare($sqlNewAnnee, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
				$reqNewAnnee->execute(array( ':LabelAnnee' => $LabelAnnee ));

				$reqDelComm = EDatabase::prepare($sqlDeleteCommentaires, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
				$reqDelComm->execute();
				EDatabase::getInstance()->commit();

				return true;
			}
		
		
		} catch (PDOException $e)
		{
			echo "EUtilitiesManager:NouvelleAnnee Error: " . $e->getMessage();
			EDatabase::getInstance()->rollBack();

			return false;
		}
	}


	/** Le degré de l'élève*/
	private $degrees;
	
	/** Si la compétence à été vue, exercée ou autonome*/
	private $states;
	
	/** L'année de l'élève*/
	private $years;
	
	/**	La classe de l'élève */
	private $classrooms;
}