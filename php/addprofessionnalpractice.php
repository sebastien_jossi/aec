<?php
/**
 * @brief	Permet d'envoyer les variables à appManager
* 			pour ajouter les pratiques liées à une compétence dans la base de données
* @author 	romain.ssr@eduge.ch
*/
require_once './inc.all.php';

$guidanceid = -1;
$domainid ="";
$practicedescription="";
$skillid = -1;

 if (isset($_POST['guidanceId'])&&isset($_POST['domainid'])&&isset($_POST['skillid'])&&isset($_POST['practicedescription'])){
	$guidanceid = $_POST['guidanceId'];
	$domainid = $_POST['domainid'];
	$skillid = $_POST['skillid'];
	$practicedescription =  filter_var($_POST['practicedescription'], FILTER_SANITIZE_STRING);
 }
else {
	echo '{ "ReturnCode": 1, "Message": "Erreur dans les paramètres. Valeurs manquantes."}';
	exit();
}


// J'envoie les informations nécessaires pour ajouter une compétence dans la base de donée.
if (!EAppManager::getInstance()->addPractice($guidanceid,$domainid,$skillid, $practicedescription)){
	echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de loadAllGuidance()"}';
	exit();
}

echo '{ "ReturnCode": 0}';
?>

