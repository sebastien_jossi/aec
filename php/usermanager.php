<?php
require_once './student.php';
require_once './manager.php';


class EUserManager{
	private static $objInstance;
	
	/**
	 * @brief Class Constructor
	 * @param string $InUser
	 */
	public function __construct(){
		$this->users = array();
	}

	/**
	 * @brief On ne laisse pas cloner une compétence
	 */
	private function __clone(){
}	
	
	
	public static function getInstance() {
		if (! self::$objInstance) {
			try {
	
				self::$objInstance = new EUserManager();
			} catch ( Exception  $e ) {
				echo "EUserManager Error: " . $e;
			}
		}
		return self::$objInstance;
	}

	public function addUser(array $data)
    {
        $sql = 'INSERT INTO Person(schoolEmail, firstName, lastName, idRole, active) VALUES(:email, :firstName, :lastName, 1, :active)';
        $sqlPersonBelongsClass = 'INSERT INTO Person_Belongs_Class(idPerson, idClass, DEGREES_ID, GUIDANCES_CODE) VALUES(:userId, :classId, :degreesId, :guidanceId)';

        try {
			//permet de prendre juste le lastName
			if (strpos($data["lastName"], '.') !== false) {
				$data["lastName"] = explode('.', $data["lastName"])[1];
			}
		
            $requestAddUser = EDatabase::getInstance()->prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $requestAddUser->execute(array(':email'=> $data['email'], ':firstName'=> $data['firstName'], ':lastName'=> $data['lastName'], 'active' => $data['active']));

			$idUser = $this->getUserIdByEmail($data['email']);
			
            $requestAddPersonBelongsClass = EDatabase::getInstance()->prepare($sqlPersonBelongsClass, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $requestAddPersonBelongsClass->execute(array(':userId' => $idUser, ':classId' => $this->getDefaultClassId(), ':degreesId' => 1, ':guidanceId' => 1));

			$_SESSION['person'] = $person;
            $person = EUserManager::getInstance()->findUserByEmail($_SESSION['email']);

            if (isset($person)){
                $role = $person->getRole();
                $_SESSION['role'] = $role;
            }

            if ($person === false){
                echo json_encode([
                    'ReturnCode' => 2,
                    'Message' => 'Un problème de récupération des données de findUserByEmail()'
                ]);
                exit();
            }

            $guidance = $person->getGuidance();
            $_SESSION['guidance'] = $guidance;
            $_SESSION['degree'] = $person->getDegree();
            $id = $person->getIdPerson();
            $_SESSION['id'] = $id;
        }catch (Exception $e) {
            die($e);
        }
	}
	/**
	 * Permet de modifier un utilisateur
	 * @return true si modification réussite
	 */
	public function ModifyUser($idPerson,$firstName,$lastName,$email,$isActive,$role,$inClass,$degreesId ){
		$sqlModifyUser = 'UPDATE Person SET firstName=:PersonnFirstName,lastName=:PersonnLastName,schoolEmail=:PersonEmail,idRole=:PersonRole,active=:PersonActive WHERE idPerson=:PersonId';
		$sqlModfierUserStudent = "UPDATE Person_Belongs_Class SET idClass=:PersonClass,DEGREES_ID=:PersonDegree WHERE idPerson=:PersonId";

		try {
			$requestModifyUser = EDatabase::getInstance()->prepare($sqlModifyUser, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$requestModifyUser->execute(array(':PersonnFirstName'=>$firstName, ':PersonnLastName'=>$lastName, ':PersonEmail'=> $email, 'PersonRole' =>$role, 'PersonActive' =>$isActive, 'PersonId' =>$idPerson));
			
			if($role==ER_STUDENT){
				//Est un eleve 
				$requestModifyStudent = EDatabase::getInstance()->prepare($sqlModfierUserStudent, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
				$requestModifyStudent->execute(array('PersonClass' =>$inClass,'PersonDegree' =>$degreesId,'PersonId' =>$idPerson));
				
			}

			return true;
		}catch (Exception $e) {
			die($e);
		}
	}
	/**
	 * Permet de suprimmer un utilisateur
	 * @param [int] $id Id de l'utilisateur a suprimmer
	 * @return void
	 */
	public function deletePerson($id){
		$sqlDeletePerson = "DELETE FROM Person WHERE idPerson=:PersonId";
		try {
			$requesDeletePerson = EDatabase::getInstance()->prepare($sqlDeletePerson, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$requesDeletePerson->execute(array(':PersonId'=> $id));
            return true;
        }
        catch (Exception $e)
        {
            return false;
        }
	}

    public function getDefaultClassId()
    {
        $DEFAULT_CLASS_NAME = "defaultClass";

        $sqlInsertClass = 'INSERT INTO Class(name) VALUES(:n)';

        try {
            if ($this->getClassIdByName($DEFAULT_CLASS_NAME) != null) {
                return $this->getClassIdByName($DEFAULT_CLASS_NAME);
            } else {
                $requestAdd = EDatabase::getInstance()->prepare($sqlInsertClass, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
                $requestAdd->execute(array(':n'=> $DEFAULT_CLASS_NAME));

                return $this->getClassIdByName($DEFAULT_CLASS_NAME);
            }
        }
        catch (Exception $e)
        {
            return -1;
        }
    }

    public function getClassIdByName($InName)
    {
        $sql = 'SELECT c.idClass FROM Class as c WHERE name LIKE :n';

        try {
            $request = EDatabase::getInstance()->prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $request->execute(array(':n'=> $InName));
            $id = $request->fetch(PDO::FETCH_ASSOC);

            if ($request->rowCount() > 0)
                return $id['idClass'];
            else
                return null;
        }
        catch (Exception $e)
        {
            return null;
        }
	}
	
	public function getClassNameById($InId)
    {
        $sql = 'SELECT c.name FROM Class as c WHERE idClass LIKE :n';

        try {
            $request = EDatabase::getInstance()->prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $request->execute(array(':n'=> $InId));
            $id = $request->fetch(PDO::FETCH_ASSOC);

            if ($request->rowCount() > 0)
                return $id['name'];
            else
                return null;
        }
        catch (Exception $e)
        {
            return null;
        }
	}

    public function getUserIdByEmail($InEmail)
    {
        $sql = 'SELECT p.idPerson FROM Person as p WHERE schoolEmail LIKE :e';

        try {
            $request = EDatabase::getInstance()->prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $request->execute(array(':e'=> $InEmail));
            $id = $request->fetch(PDO::FETCH_ASSOC);

            return $id['idPerson'];
        }
        catch (Exception $e)
        {
            return -1;
        }
	}
	
	public function getEmailByUserId($InId)
    {
        $sql = 'SELECT p.schoolEmail FROM Person as p WHERE idPerson LIKE :id';

        try {
            $request = EDatabase::getInstance()->prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $request->execute(array(':id'=> $InId));
            $email = $request->fetch(PDO::FETCH_ASSOC);

            return $email['schoolEmail'];
        }
        catch (Exception $e)
        {
            return -1;
        }
    }

	/**
	 * @brief Récupère l'utilisateur en fonction de son email
	 * @param $InEmail L'email de l'utilisateur
	 * @return Un objet EUser correspondant à l'email ou false si pas trouvé.
	 */
	public function findUserByEmail($InEmail) 
	{
		// On cherche si l'utilisateur existe dans le tableau
		foreach ($this->users as $u){
			if ($InEmail == $u->getEmail())
				return $u;
		}# end foreach
		$sqlRole = 'SELECT idRole FROM Person WHERE schoolEmail = :e';

			try{
			$stmt = EDatabase::prepare($sqlRole, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute(array( ':e' => $InEmail ));
	
			$result = $stmt->fetchAll(PDO::FETCH_ASSOC);

			if (count($result) > 0)
			{
				// Création du user avec les données provenant de la base de données
				$u = null;
				switch (intval($result[0]['idRole']))
				{
					case 1:	// Apprenti
						$sqlEleve = 'SELECT p.idPerson, p.firstName, p.lastName, p.schoolEmail, p.idRole, p.active, pbc.idClass, pbc.GUIDANCES_CODE, pbc.DEGREES_ID FROM Person p NATURAL JOIN Person_Belongs_Class pbc  WHERE p.schoolEmail = :e';
						$stmt = EDatabase::prepare($sqlEleve, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
						$stmt->execute(array( ':e' => $InEmail ));
						$result = $stmt->fetchAll();
						$u = new EStudent($result[0]['idPerson'],$result[0]['firstName'],$result[0]['lastName'], $result[0]['schoolEmail'],$result[0]['active'],$result[0]['idClass'],$result[0]['GUIDANCES_CODE'],$result[0]['DEGREES_ID']);	
						break;
					case 2:	// Membre de la direction
						$sqlAdmin = 'SELECT idPerson, firstName, lastName, schoolEmail, idRole, active FROM Person WHERE Person.schoolEmail = :e';
						$stmt = EDatabase::prepare($sqlAdmin, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
						$stmt->execute(array( ':e' => $InEmail ));
						$result = $stmt->fetchAll();
						$u = new EManager($result[0]['idPerson'], false, $result[0]['firstName'],$result[0]['lastName'], $result[0]['schoolEmail'],$result[0]['active']);
						break;
					case 3:	// Administrateur
						$sqlAdmin = 'SELECT idPerson, firstName, lastName, schoolEmail, idRole, active FROM Person WHERE Person.schoolEmail = :e';
						$stmt = EDatabase::prepare($sqlAdmin, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
						$stmt->execute(array( ':e' => $InEmail ));
						$result = $stmt->fetchAll();
						$u = new EManager($result[0]['idPerson'], true, $result[0]['firstName'],$result[0]['lastName'], $result[0]['schoolEmail'],$result[0]['active']);
						break;
						
					// Error
					case 0:
					default:
						return false;
				}
				if ($u != null){
					array_push($this->users, $u);
					return $u;
				}
				// fail
				return false;
			} #end if
	
		}catch(PDOException  $e )
		{
			echo "EUserManager:findUserByEmail Error: ".$e->getMessage();
			return false;
		}
		// J'ai pas trouvé le user
		return false;
	}# end method
	
	public function findStudentById($InId)
	{	
		$sql = 'SELECT idPerson, idClass, DEGREES_ID, GUIDANCES_CODE FROM Person_Belongs_Class WHERE idPerson = :id';
		try{
			$stmt = EDatabase::prepare(($sql), array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
			$stmt->execute(array( ':id' => $InId ));
	
			$result = $stmt->fetchAll();
			if (count($result) > 0)
			{
	
				// Création du user avec les données provenant de la base de données
				$u = new EStudent($result[0]['idPerson'],$result[0]['idClass'],$result[0]['DEGREES_ID'],$result[0]['GUIDANCES_CODE']);
				array_push($this->users, $u);
				return $u;
			} #end while
	
		} catch(PDOException  $e) {
			echo "EUserManager:findStudentById Error: ".$e->getMessage();
			return false;
		}
		// J'ai pas trouvé le user
		return false;
	}# end method
	
	private $users;

}