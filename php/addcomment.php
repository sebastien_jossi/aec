<?php
require_once './inc.all.php';
/**
 * @brief ajoute un commentaire dans la base de données
 * @param $studentId L'identifiant de l'apprenti /!\ OBLIGATOIRE
 * @param $guidanceid  L'identifiant de l'orientation
 * @param $domainid  L'identifiant du domaine
 * @param $skillid   L'identifiant de la compétence
 *
 */
// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Je récupère l'id de guidance

$guidanceid = - 1;
$domainid = - 1;
$skillid = - 1;
$studentid = - 1;
$comment = "";

if (isset($_POST['guidanceid'])) $guidanceid = filter_input(INPUT_POST, 'guidanceid', FILTER_SANITIZE_STRING);

if (isset($_POST['domainid'])) $domainid = filter_input(INPUT_POST, 'domainid', FILTER_SANITIZE_STRING);

if (isset($_POST['skillid'])) $skillid = filter_input(INPUT_POST, 'skillid', FILTER_SANITIZE_STRING);

if (isset($_POST['studentid'])) $studentid = filter_input(INPUT_POST, 'studentid', FILTER_SANITIZE_STRING);

if (isset($_POST['comment'])){
	$comments = filter_input(INPUT_POST, 'comment');
	$comment = htmlentities ($comments);
}

if (($guidanceid !== - 1) && ($domainid !== - 1) && ($skillid !== - 1) && ($studentid !== - 1) && ($comment !== ""))
{
	$data = ECommentManager::getInstance()->addComment($guidanceid, $domainid, $skillid, $studentid, $comment);
	if ($data === false)
	{
		echo '{ "ReturnCode": 2, "Data": "Un problème de récupération des données de addComment()"}';
		exit();
	}
	echo '{ "ReturnCode": 0, "Data": "Votre commentaire a été enregistré"}';
	exit();
}

// Si j'arrive ici, c'est pas bon
echo '{ "ReturnCode": 1, "Data": "Il manque un ou plusieurs paramètre(s)"}';
