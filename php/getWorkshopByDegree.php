<?php

/**
 * @copyright ronaldo.lrrpnt@eduge.ch 2016-2017
 * @brief Récupère les ateliers par un degré spécifié
 */

require_once './inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Je récupère l'id du degré scolaire

$degreeid = -1;
$domainId = -1;
$practiceId=-1;
$skillCode=-1;
if (isset($_POST['degreeId']) && isset($_POST['domainId']) && isset($_POST['practiceId']) && isset($_POST['skillCode']))
	$degreeid = $_POST['degreeId'];
	$domainId = $_POST['domainId'];
	$practiceId=$_POST['practiceId'];
	$skillCode=$_POST['skillCode'];

	if ($degreeid > -1){
		$workshops = EUtilitiesManager::getInstance()->loadWorkshopByDegree($degreeid,$domainId,$practiceId,$skillCode);
		if ($workshops === false){
			echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de getWorkshopByDegree()"}';
			exit();
		}

		$jsn = json_encode($workshops);
		if ($jsn == FALSE){
			$code = json_last_error();
			echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json ('.$code.'"}';
			exit();
		}
		echo '{ "ReturnCode": 0, "Data": '.utf8_encode($jsn).'}';
		exit();

	}

	// Si j'arrive ici, c'est pas bon
	echo '{ "ReturnCode": 1, "Message": "Il manque le paramètre degreeid"}';
