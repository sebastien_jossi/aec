<?php

require_once './user.php';


class EStudent extends EUser implements JsonSerializable{
	
	/**
	 * @brief Class Constructor
	 */
	public function __construct($InIdPerson = -1, $InFirstName = "",$InLastName = "", $InEmail = "",$InIsActive = false, 
								$InClass = "", $InGuidance = "", $InDegree = "")
	{
		parent::__construct($InIdPerson, ER_STUDENT, $InFirstName, $InLastName, $InEmail, $InIsActive);
		$this->studentclass = $InClass;
		$this->studentguidance = $InGuidance;
		$this->studentdegree = $InDegree;
		$this->studentactive = $InIsActive;
	}
	
	
	/**
	 * retourne la classe dans laquelle l'élève se trouve 	
	 */
	public function getClass(){
	
		return $this->studentclass;
	}

	
	/**
	 * retourne l'orientation dans laquelle l'élève se trouve
	 */
	public function getGuidance(){
		
		return $this->studentguidance;
	}
	
	public function getDegree(){
		
		return $this->studentdegree;
	}

	public function getActive() {
		return $this->studentactive;
	}
	
	/***
	 * Transforme l'objet en json
	 * @return L'objet en json
	 */
	public function jsonSerialize()
	{
		return get_object_vars($this);
	}
	
	/**
	 * @var string
	 */
	public $studentclass;

	/**
	 * @var string
	 */
	private $studentguidance;

	/**
	 * @var string
	 */
	public $studentdegree;

	/**
	 * @var string
	 */
	private $studentactive;
}