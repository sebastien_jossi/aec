<?php
/**
 * @brief	Permet d'envoyer les variables à appManager
* 			pour supprimer une compétence dans la base de données
* @author 	romain.ssr@eduge.ch
*/
require_once './inc.all.php';

$guidanceid = -1;
$domainid ="";
$skillid ="";


if (isset($_POST['guidanceId'])&&isset($_POST['domainid'])&&isset($_POST['skillid'])){
	$guidanceid = $_POST['guidanceId'];
	$domainid =$_POST['domainid'];
	$skillid =$_POST['skillid'];
	}
else {
	echo '{ "ReturnCode": 1, "Message": "Erreur dans les paramètres. Valeurs manquantes."}';
	exit();
}


// J'envoie les informations nécessaires pour ajouter une compétence dans la base de donée.
if (!EAppManager::getInstance()->deleteSkill($guidanceid,$domainid,$skillid)){
	echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de loadAllGuidance()"}';
	exit();
}

echo '{ "ReturnCode": 0}';
?>