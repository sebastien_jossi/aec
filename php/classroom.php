<?php
/**
 * @copyright floran.stck@eduge.ch 2016-2017
*/

/**
 * Classe des classes
* @author FS
*/


class EClassroom implements JsonSerializable{
	
	public function __construct($InClassromId = - 1, $InClassroomName = ""){
		
		$this->classroomid = $InClassromId;
		$this->classroomname = $InClassroomName;
	
	}
	
	private function __clone(){}
	
	public function getClassroomId ()
	{
		return $this->classroomid;
	}
	
	public function getClassroom ()
	{
		return $this->classroomname;
	}
	

	
	
	public function jsonSerialize()
	{
		return get_object_vars($this);
	}
	
	private $classroomid;
	private $classroomname;
}