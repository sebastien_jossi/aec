<?php
/**
 * @copyright ronaldo.lrrpnt@eduge.ch 2016-2017
 */

/**
 * Classe des données des apprentis
 * @author RLP
 */
 
class EDataStudent implements JsonSerializable
{
	/**
	 * @brief Class Constructor
	 * @param $InStudentId L'identifiant de l'apprentis
	 */
	public function __construct ($InStudentId = - 1)
	{
		$this->studentid = $InStudentId;
		$this->datapractices = array();
	}
	
	/**
	 * @brief On ne laisse pas cloner une données de l'apprentis
	 */
	private function __clone ()
	{}
	
	/**
	 * @brief Est-ce que cet objet est valide
	 *
	 * @return True si valide, autrement false
	 */
	public function isValid ()
	{
		return ($this->studentid == - 1) ? false : true;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return L'identifiant unique
	 */
	public function getId ()
	{
		return $this->studentid;
	}
		
	/**
	 * Getter
	 *
	 * @return Le tableau des EDataPractices
	 */
	public function getDomains ()
	{
		return $this->datapractices;
	}
	
	/***
	 * Transforme l'objet en json
	 * @return L'objet en json
	 */
	public function jsonSerialize ()
	{
		return get_object_vars($this);
	}
	
	/**
	 * Ajoute les données de l'apprentis pour une pratique professionnel au tableau des datapractices
	 * @param $InGuidanceId L'identifiant de l'orientation
	 * @param $InDomainId L'identifiant du domaine
	 * @param $InSkillId L'identifiant de la compétence
	 * @param $InPracticeId L'identifiant de la pratique professionnel
	 * @param $InStateId L'identifiant de l'état
	 * @param $InYearId L'identifiant de l'année de modification
	 * @return true | false si une erreur se produit
	 */
	public function addDataPracticeAttr ($InGuidanceId = - 1, $InDomainId = - 1, $InSkillId = - 1, $InPracticeId = - 1, $InOwnerId = - 1, $InStateId = - 1, $InYearId = - 1)
	{
		try
		{
			array_push($this->datapractices, new EDataPractice($InGuidanceId, $InDomainId, $InSkillId, $InPracticeId, $InOwnerId, $InStateId, $InYearId));
			return true;
		} catch (Exception $e)
		{
			echo "EDataStudent:addDataPracticeAttr Error: " . $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Ajoute un domaine au tableau des domaines
	 * @param $InDataPractice L'objet EDataPractice
	 * @return Le tableau des datapractices | false si une erreur se produit
	 */
	public function addDataPractice ($InDataPractice)
	{
		try
		{
			array_push($this->datapractices, $InDataPractice);
			return true;
		} catch (Exception $e)
		{
			echo "EDataStudent:addDataPractice Error: " . $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Ajoute un tableau de domaine au tableau des domaines
	 * @param $InArrDataPractices Un tableau contenant des objets EDataPractice à ajouter
	 * @param $clearAll Efface le contenu du tableau avant d'ajouter si true
	 * @return true | false si une erreur se produit
	 */
	public function addDataPractices ($InArrDataPractices, $clearAll = false)
	{
		try
		{
			if ($clearAll == true)
				$this->datapractices = $InArrDataPractices;
				else
					array_push($this->datapractices, $InArrDataPractices);
				
				return true;
		} catch (Exception $e)
		{
			echo "EDataStudent:addDataPractices Error: " . $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Modifie les données d'un apprentis
	 * @param $InGuidanceId L'identifiant de l'orientation
	 * @param $InDomainId L'identifiant du domaine
	 * @param $InSkillId L'identifiant de la compétence
	 * @param $InPracticeId L'identifiant de la pratique professionnel
	 * @param $InStateId L'identifiant de l'état
	 * @param $InArrWorkshops Tableaux d'atelier
	 * @return true | false si une erreur se produit
	 */
	public function modifyDataPractice ($InGuidanceId, $InDomainId, $InSkillId, $InPracticeId, $InStateId, $InArrWorkshops)
	{
		try
		{
			foreach ($this->datapractices as &$dp)
			{
				if (($dp->getGuidanceId() === $InGuidanceId) && ($dp->getDomainId() === $InDomainId) && ($dp->getSkillId() === $InSkillId) && ($dp->getPracticeId() === $InPracticeId) && ($dp->getOwnerId() === $this->studentid))
				{
					$dp->modifyDataPractice($InStateId, $InArrWorkshops);
					return true;
				}
			}
		} catch (Exception $e)
		{
			echo "EDataStudent:modifyDataPractice Error: " . $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Supprime les données d'une évaluation
	 * @param $InGuidanceId L'identifiant de l'orientation	 
	 * @param $InDomainId L'identifiant du domaine
	 * @param $InSkillId L'identifiant de la compétence
	 * @param $InPracticeId L'identifiant de la pratique professionnel
	 * @return Le tableau true | false si une erreur se produit
	 */
	public function deleteDataPractice ($InGuidanceId, $InDomainId, $InSkillId, $InPracticeId)
	{
		try
		{
			foreach ($this->datapractices as &$dp)
			{
				if (($dp->getGuidanceId() === $InGuidanceId) && ($dp->getDomainId() === $InDomainId) && ($dp->getSkillId() === $InSkillId) && ($dp->getPracticeId() === $InPracticeId) && ($dp->getOwnerId() === $this->studentid))
				{
					unset($dp);
					return true;
				}
			}
		} catch (Exception $e)
		{
			echo "EDataStudent:deleteDataPractice Error: " . $e->getMessage();
			return false;
		}
	
	}
	
	/** L'identifiant de l'apprentis */
	private $studentid;
	/** Contient le tableaux des EDataPractice */
	private $datapractices;
}