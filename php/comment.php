<?php
/**
 * @copyright ronaldo.lrrpnt@eduge.ch 2016-2017
 */

/**
 * Classe des commentaires
 * @author RLP
 */
class EComment implements JsonSerializable /* Changer Script BD */
{
	/**
	 * @brief Class Constructor
	 * @param $InSkillId L'identifiant du skill
	 * @param $InStudentId L'identifiant de l'élève
	 * @param $InCommentary Le commentaire de l'élève
	 * @param $InDateComment La date de création du commentaire
	 */
	public function __construct ($commentId = -1, $InSkillId = - 1, $InStudentId = -1, $InCommentary = "", $InDateComment = "", $studentEmail = "")
	{
		$this->commentId = $commentId;
		$this->skill= $InSkillId;
		$this->student = $InStudentId;
		$this->commentary = $InCommentary;
		$this->dateComment = $InDateComment;
		$this->studentEmail = $studentEmail;
	}
	
	/**
	 * @brief On ne laisse pas cloner un commentaire
	 */
	private function __clone ()
	{}
	
	/**
	 * @brief Est-ce que cet objet est valide
	 *
	 * @return True si valide, autrement false
	 */
	public function isValid ()
	{
		return (($this->skill == - 1) || ($this->student == - 1)) ? false : true;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return L'identifiant de l'élève
	 */
	public function getStudent ()
	{
		return $this->student;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return L'identifiant du skill
	 */
	public function getSkill ()
	{
		return $this->skill;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return Le commentaire de l'élève
	 */
	public function getCommentary ()
	{
		return $this->commentary;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return La date de création du commentaire
	 */
	public function getDateCommentary ()
	{
		return $this->dateComment;
	}
	
	/***
	 * Transforme l'objet en json
	 * @return L'objet en json
	 */
	public function jsonSerialize() 
	{		
		return get_object_vars($this);
	}
	
	
	/** Identifiant du skill*/
	private $skill;
	/** Identifiant de l'élève */
	private $student;
	/** Commentaire de l'élève */
	private $commentary;
	/** Date de création du commentaire et id du commentaire*/
	private $dateComment;
}