<?php

/**
 * @copyright ronaldo.lrrpnt@eduge.ch 2016-2017
 * @brief Récupère toutes les états
 */

require_once './inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$states = EUtilitiesManager::getInstance()->loadAllStates();
if ($states === false){
	echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de loadAllStates()"}';
	exit();
}
$jsn = json_encode($states);
if ($jsn == FALSE){
	$code = json_last_error();
	echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json ('.$code.'"}';
	exit();
}
echo '{ "ReturnCode": 0, "Data": '.utf8_encode($jsn).'}';
exit();