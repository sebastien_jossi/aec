<?php 
require_once './database.php';
require_once './guidance.php';

/**
 * @brief Helper class pour gérer les données du site
 * 
 * @author ronaldo.lrrpnt@eduge.ch
 */
class EAppManager
{

    private static $objInstance;

    /**
     * @brief Class Constructor - Create a new EAppManger if one doesn't exist
     * Set to private so no-one can create a new instance via ' = new
     * EAppManager();'
     */
    private function __construct ()
    {
        $this->guidances = array();
    }

    /**
     * @brief Like the constructor, we make clone private so nobody can clone
     * the instance
     */
    private function __clone ()
    {}

    /**
     * @brief Retourne notre instance ou la crée
     *
     * @return $objInstance;
     */
    public static function getInstance ()
    {
        if (!self::$objInstance)
        {
            try
            {         
                self::$objInstance = new EAppManager();
                
            } catch (Exception $e)
            {
                echo "EAppManager Error: " . $e;
            }
        }
        return self::$objInstance;
    }
    
    
    /**
     * Initialize la hiérarchie complète des objets guidances.
     * $InYearId L'identifiant de l'année (facultatif)
     * @remark	Cette va charger la hiérarchie de tous les objets de définition
     * 			Guidance (EGuidance)
     * 				-> Domaines (EDomain)
     * 					-> Compétences (ESkill)
     * 						-> Practices  (EPractice)
     */
    public function LoadAllHierarchy($InYearId = "")
    {
    	$this->loadAllGuidance();
    	// Pour chaque guidance, je vais charger les domaines associés
    	foreach ($this->guidances as &$guidance)
    	{
    		$domains = $this->loadDomainsByGuidance($guidance->getId(), $InYearId);
    		if ($domains != FALSE)
    		{
    			$guidance->addDomains ($domains, true);
    			foreach ($guidance->getDomains() as &$domain)
    			{
    				$skills = $this->loadSkillByDomainGuidance($domain->getOwnerId(), $domain->getId());
    				if($skills != FALSE)
    				{
    					$domain->addSkills($skills, true);
    					foreach($domain->getSkills() as &$skill)
    					{
    						$practices = $this->loadPracticesBySkillDomainGuidance($domain->getOwnerId(),$skill->getOwnerId(), $skill->getId());
    						if($practices != FALSE)
    						{
    							$skill->addPractices($practices, true);
    						}
    					}
    				}
    			}
    		}
    	}
    	return $this->guidances;
    }
    
    /**
     * Initialize la hiérarchie complète d'une orientations spécifié
     * $InYearId L'identifiant de l'année (facultatif)
     * @remark	Cette va charger la hiérarchie de tous les objets de définition
     * 			Guidance (EGuidance)
     * 				-> Domaines (EDomain)
     * 					-> Compétences (ESkill)
     * 						-> Practices  (EPractice)
     */
    public function LoadAllHierarchyByGuidance($InGuidanceId, $InYearId = "")
    {
    	$this->loadGuidance($InGuidanceId);
    	// Pour chaque guidance, je vais charger les domaines associés
    	foreach ($this->guidances as &$guidance)
    	{
    		$domains = $this->loadDomainsByGuidance($guidance->getId(), $InYearId);
    		if ($domains != FALSE)
    		{
    			$guidance->addDomains ($domains, true);
    			foreach ($guidance->getDomains() as &$domain)
    			{
    				$skills = $this->loadSkillByDomainGuidance($domain->getOwnerId(), $domain->getId());
    				if($skills != FALSE)
    				{
    					$domain->addSkills($skills, true);
    					foreach($domain->getSkills() as &$skill)
    					{
    						$practices = $this->loadPracticesBySkillDomainGuidance($domain->getOwnerId(),$skill->getOwnerId(), $skill->getId());
    						if($practices != FALSE)
    						{
    							$skill->addPractices($practices, true);
    						}
    					}
    				}
    			}
    		}
    	}
    	return $this->guidances;
    }
    
    
    /**
     * Charge toutes les orientations
     * 
     * @return Le tableau des EGuidance | false si une erreur se produit
     */
    public function loadAllGuidance ()
    {
        $this->guidances = array();
        $sql = 'SELECT CODE, LABEL FROM GUIDANCES ORDER BY CODE';
        try
        {
            $stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt->execute();
            
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT))
            {
                // Création de l'orientation avec les données provenant de la
                // base de données
                $g = new EGuidance($row['CODE'], $row['LABEL']);
                array_push($this->guidances, $g);
            }
        } catch (PDOException $e)
        {
            echo "EAppManager:getGuidances Error: " . $e->getMessage();
            return false;
        }
        // OK
        return $this->guidances;
    }

    /**
     * Charge une guidance en fonction de son id
     * @param $InGuidanceId L'identifiant de la guidance
     * @return L'objet EGuidance. Attention ça n'est pas une référence.
     * 		   | false si une erreur se produit
     */
    public function loadGuidance($InGuidanceId)
    {
    	$this->guidances = array();
    	$sql = 'SELECT CODE, LABEL FROM GUIDANCES WHERE CODE = :id';
    	try
    	{
    		$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array( ':id' => $InGuidanceId ));
    
    		$result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    		if (count($result) > 0)
    		{
    			// Création de l'orientation avec les données provenant de la
    			// base de données
    			$g = new EGuidance($result[0]['CODE'], $result[0]['LABEL']);
    			array_push($this->guidances, $g);
    			return $g;
    		}
    	} catch (PDOException $e)
    	{
    		echo "EAppManager:loadGuidance Error: " . $e->getMessage();
    		return false;
    	}
    	// not found
    	return false;
    }
    
    /**
     * Retourne le tableau de toutes les orientations
     * 
     * @return Le tableau des EGuidance
     */
    public function getGuidances()
    {
        return $this->guidances;
    }
    
    /***
	 * Récupère l'objet guidance selon son id
     * @param $InGuidanceId L'identifiant de la guidance
     * @return L'objet EGuidance | null si pas trouvé.
     */
     public function getGuidance($InGuidanceId)
     {
     	foreach ($this->guidances as $guid)
     	{
     		if ($guid->getId() == $InGuidanceId)
     			return $guid;
     	}
     	// Not found
     	return null;
     }
     
    /**
     * Charge tous les domaines d'une orientation
     * @param $InGuidanceId L'identifiant de l'orientation
     * @param $InYearId L'identifiant de l'année (facultatif)
     * @return Le tableau des EDomain | false si une erreur se produit
     */
    public function loadDomainsByGuidance($InGuidanceId, $InYearId = "")
    {
    	$tdomains = array();
    	$arrExec = array(':GuId' => $InGuidanceId);
    	
    	if($InYearId === "")
    		$sql = 'SELECT ID, NAME, GUIDANCES_CODE, LABEL FROM DOMAINS d, YEARS y WHERE GUIDANCES_CODE = :GuId AND d.YEARS_CODE = y.CODE ORDER BY ID';		
    	else 
    	{
    		$sql = 'SELECT ID, NAME, GUIDANCES_CODE, LABEL FROM DOMAINS d, YEARS y WHERE GUIDANCES_CODE = :GuId AND d.YEARS_CODE <= :YeId AND d.YEARS_CODE = y.CODE ORDER BY ID';
    		$arrExec[':YeId'] = $InYearId;
    	}
    	
    	try
    	{
    		$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute($arrExec);
    	
    		while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT))
    		{
    			// Création de l'orientation avec les données provenant de la
    			// base de données
    			$d = new EDomain($row['GUIDANCES_CODE'], $row['ID'], $row['NAME'], $row['LABEL']);
    			array_push($tdomains, $d);
    		}
    	} catch (PDOException $e)
    	{
    		echo "EAppManager:loadDomainsByGuidance Error: " . $e->getMessage();
    		return false;
    	}
    	return $tdomains;
    }
    
    
    /**
     * Ajoute un nouveau domaine dans la base de donnée
     * @param $InGuidanceId l'identifiant de l'orientation
     * @param $InDomainDescription la description du domaine
     * @return true OK | false si une erreur se produit
     */
    public function addDomain($InGuidanceId, $InDomainDescription)
    {
    	$sql = "INSERT INTO DOMAINS(ID, NAME, GUIDANCES_CODE, YEARS_CODE) VALUES (CASE(CHAR(ORD((SELECT ID FROM DOMAINS s2 WHERE GUIDANCES_CODE = :GuId ORDER BY ID DESC LIMIT 1)) + 1))WHEN '' THEN 'A' ELSE (CHAR(ORD((SELECT ID FROM DOMAINS s2 WHERE GUIDANCES_CODE = :GuId ORDER BY ID DESC LIMIT 1)) + 1)) END, :DoDesc, :GuId, (SELECT CODE FROM YEARS ORDER BY LABEL DESC LIMIT 1))";
    	
    	try
    	{
    		$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array( ':GuId' => $InGuidanceId, ':DoDesc' => $InDomainDescription));
    		return true;
    	} catch (PDOException $e)
    	{
    		echo "EAppManager:addDomain Error: " . $e->getMessage();
    		return false;
    	}
    }
    
    
    /**
     * Modifie un domaine dans la base de donnée
     * @param $InGuidanceId l'identifiant de l'orientation
     * @param $InDomainId l'identifiant du domaine
     * @param $InDomainDescription la description du domaine
     * @return true OK | false si une erreur se produit
     */
    public function modifyDomain($InGuidanceId, $InDomainId, $InDomainDescription)
    {
    	$sql = "UPDATE DOMAINS SET NAME = :DoDesc WHERE GUIDANCES_CODE = :GuId AND ID = :DoId";
    	 
    	try
    	{
    		$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array( ':GuId' => $InGuidanceId, 'DoId'=> $InDomainId,':DoDesc' => $InDomainDescription));
    		return true;
    	} catch (PDOException $e)
    	{
    		echo "EAppManager:updateDomain Error: " . $e->getMessage();
    		return false;
    	}
    }
    
    /**
     * Suprimme un domaine et les compétences liés à celui-ci et les practices liés à celle-ci de la base de donnée
     * @param $InGuidanceId l'identifiant de l'orientation
     * @param $InDomainId l'identifiant du domaine
     * @return true OK | false si une erreur se produit
     */
    public function deleteDomain($InGuidanceId, $InDomainId)
    {
    	$sql = "DELETE d.*, s.*, p.* FROM DOMAINS d LEFT JOIN SKILL s ON d.ID = s.DOMAINS_ID AND d.GUIDANCES_CODE = s.GUIDANCES_CODE LEFT JOIN PROFESSIONNAL_PRACTICES p ON d.ID = p.DOMAINS_ID AND d.GUIDANCES_CODE = p.GUIDANCES_CODE WHERE d.ID = :DoId AND d.GUIDANCES_CODE = :GuId";
    
    	try
    	{
    		$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array( ':GuId' => $InGuidanceId, 'DoId'=> $InDomainId,));
    		return true;
    	} catch (PDOException $e)
    	{
    		echo "EAppManager:deleteDomain Error: " . $e->getMessage();
    		return false;
    	}
    }
    
    /**
     * Charge tous les skill d'un domaine
     * @param $InGuidanceId L'identifiant de l'orientation
     * @param $InDomainId L'identifiant du domaine
     * @return Le tableau des ESkill | false si une erreur se produit
     */
    public function loadSkillByDomainGuidance($InGuidanceId, $InDomainId)
    {
    	$tskills = array();
    	$sql = 'SELECT s.CODE, DOMAINS_ID, GUIDANCES_CODE, NAME, DEFINITION, METHODOLOGICSKILL, SOCIALSKILL, PERSONNALSKILL, LABEL FROM SKILL s, YEARS y WHERE s.YEARS_CODE = y.CODE AND GUIDANCES_CODE = :GuId AND DOMAINS_ID = :DoId ORDER BY s.CODE';
    	try
    	{
    		$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array( ':GuId' => $InGuidanceId, ':DoId' => $InDomainId));
    		 
    		while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT))
    		{
    			// Création de l'orientation avec les données provenant de la
    			// base de données
    			$s = new ESkill($row['DOMAINS_ID'], $row['CODE'], $row['NAME'], $row['DEFINITION'], $row['METHODOLOGICSKILL'], $row['SOCIALSKILL'], $row['PERSONNALSKILL'], $row['LABEL']);
    			array_push($tskills, $s);
    		}
    	} catch (PDOException $e)
    	{
    		echo "EAppManager:loadSkillByDomainGuidance Error: " . $e->getMessage();
    		return false;
    	}
    	return $tskills;
    }
    
    /**
     * Charge un skill spécifique
     * @param $InGuidanceId L'identifiant de l'orientation
     * @param $InDomainId L'identifiant du domaine
     * @param $InSkillId L'identifiant de la compétence
     * @return Le tableau des ESkill | false si une erreur se produit
     */
    public function loadSpecificSkillByDomainGuidance($InGuidanceId, $InDomainId, $InSkillId)
    {
    	$tskills = array();
    	$sql = 'SELECT s.CODE, DOMAINS_ID, GUIDANCES_CODE, NAME, DEFINITION, METHODOLOGICSKILL, SOCIALSKILL, PERSONNALSKILL, LABEL FROM SKILL s, YEARS y WHERE s.YEARS_CODE = y.CODE AND GUIDANCES_CODE = :GuId AND DOMAINS_ID = :DoId AND s.CODE = :SkId ORDER BY s.CODE';
    	try
    	{
    		$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array( ':GuId' => $InGuidanceId, ':DoId' => $InDomainId, ':SkId' => $InSkillId));
    		 
    		while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT))
    		{
    			// Création de l'orientation avec les données provenant de la
    			// base de données
    			$s = new ESkill($row['DOMAINS_ID'], $row['CODE'], $row['NAME'], $row['DEFINITION'], $row['METHODOLOGICSKILL'], $row['SOCIALSKILL'], $row['PERSONNALSKILL'], $row['LABEL']);
    			array_push($tskills, $s);
    		}
    	} catch (PDOException $e)
    	{
    		echo "loadSpecificSkillByDomainGuidance Error: " . $e->getMessage();
    		return false;
    	}
    	return $tskills;
    }
    
    /**
     * Ajoute un nouveau domaine dans la base de donnée
     * @param $InGuidanceId l'identifiant de l'orientation
     * @param $InDomainId L'identifiant du domaine qui lui correspond
	 * @param $InSkillName Le nom de la compétence
	 * @param $InSkillDefinition La définition de la compétence
	 * @param $InSkillMethodologic La compétence méthodologique de la compétence
	 * @param $InSkillSocial La compétence social de la compétence
	 * @param $InSkillPersonnal La compétence personnelle de la compétence
     * @return true OK | false si une erreur se produit
     */
    public function addSkill($InGuidanceId, $InDomainId, $InSkillName, $InSkillDefinition, $InSkillMethodologic, $InSkillSocial, $InSkillPersonnal)
    {
    	$sql = "INSERT INTO SKILL(CODE, DOMAINS_ID, GUIDANCES_CODE, NAME, DEFINITION, METHODOLOGICSKILL, SOCIALSKILL, PERSONNALSKILL, YEARS_CODE) VALUES (:Code, :DoId, :GuId, :SkNa,:SkDe, :SkMe, :SkSo, :SkPe, (SELECT CODE FROM YEARS ORDER BY LABEL DESC LIMIT 1))";
    	$newCode = "SELECT COUNT(SKILL.CODE) as CodeId FROM SKILL WHERE DOMAINS_ID = :domainsId";

    	try
    	{
    	    $stmt = EDatabase::prepare($newCode, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    	    $stmt->execute([':domainsId' => $InDomainId]);
    	    $CodeId = (int)$stmt->fetch(PDO::FETCH_ASSOC)['CodeId'] + 1;
    		$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array(':Code' => $CodeId, ':GuId' => $InGuidanceId, ':DoId' => $InDomainId, ':SkNa' => $InSkillName, ':SkDe' => $InSkillDefinition, ':SkMe' => $InSkillMethodologic, ':SkSo' => $InSkillSocial, ':SkPe' => $InSkillPersonnal));
    		return true;
    	} catch (PDOException $e)
    	{
    		echo "EAppManager:addSkill Error: " . $e->getMessage();
    		return false;
    	}
    }
    
    /**
     * Modifie une compétence dans la base de donnée
     * @param $InGuidanceId l'identifiant de l'orientation
	 * @param $InDomainId L'identifiant du domaine qui lui correspond
	 * @param $InSkillId L'identifiant de la compétence
	 * @param $InSkillName Le nom de la compétence
	 * @param $InSkillDefinition La définition de la compétence
	 * @param $InSkillMethodologic La compétence méthodologique de la compétence
	 * @param $InSkillSocial La compétence social de la compétence
	 * @param $InSkillPersonnal La compétence personnelle de la compétence
     * @return true OK | false si une erreur se produit
     */
    public function updateSkill($InGuidanceId, $InDomainId, $InSkillId, $InSkillName, $InSkillDefinition, $InSkillMethodologic, $InSkillSocial, $InSkillPersonnal)
    {
    	$sql = "UPDATE SKILL SET NAME = :SkNa, DEFINITION = :SkDe, METHODOLOGICSKILL = :SkMe, SOCIALSKILL = :SkSo, PERSONNALSKILL = :SkPe WHERE GUIDANCES_CODE = :GuId AND DOMAINS_ID = :DoId AND CODE = :SkId";
    
    	try
    	{
    		$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array( ':GuId' => $InGuidanceId, ':DoId' => $InDomainId, ':SkId' => $InSkillId , ':SkNa' => $InSkillName, ':SkDe' => $InSkillDefinition, ':SkMe' => $InSkillMethodologic, ':SkSo' => $InSkillSocial, ':SkPe' => $InSkillPersonnal));
    		return true;
    	} catch (PDOException $e)
    	{
    		echo "EAppManager:updateSkill Error: " . $e->getMessage();
    		return false;
    	}
    }
    
    /**
     * Suprimme un domaine et les compétences liés à celui-ci et les practices liés à celle-ci de la base de donnée
     * @param $InGuidanceId l'identifiant de l'orientation
     * @param $InDomainId L'identiiant du domaine
	 * @param $InSkillId L'identifiant de la compétence
	 * @return true OK | false si une erreur se produit
     */
    public function deleteSkill($InGuidanceId, $InDomainId, $InSkillId)
    {
    	$sql = "DELETE s.*, p.* FROM SKILL s LEFT JOIN PROFESSIONNAL_PRACTICES p  ON s.DOMAINS_ID = p.DOMAINS_ID AND s.GUIDANCES_CODE = p.GUIDANCES_CODE AND s.CODE = p.SKILL_CODE WHERE s.CODE = :SkId AND s.GUIDANCES_CODE = :GuId AND s.DOMAINS_ID = :DoId";
    
    	try
    	{
    		$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array( ':GuId' => $InGuidanceId, ':DoId' => $InDomainId, ':SkId'=> $InSkillId));
    		return true;
    	} catch (PDOException $e)
    	{
    		echo "EAppManager:deleteDomain Error: " . $e->getMessage();
    		return false;
    	}
    }
    
    /**
     * Charge toutes les pratiques d'un skill
     * @param $InGuidanceId L'identifiant de l'orientation
     * @param $InDomainId L'identifiant du domaine
     * @param $InSkillId L'identifiant du skill
     * @return Le tableau des EPratices | false si une erreur se produit
     */
    public function loadPracticesBySkillDomainGuidance($InGuidanceId, $InDomainId, $InSkillId)
    {
    	$tpractices = array();
    	$sql = 'SELECT ID, SKILL_CODE, DOMAINS_ID, GUIDANCES_CODE, DESCRIPTION, LABEL FROM PROFESSIONNAL_PRACTICES p, YEARS y WHERE p.YEARS_CODE = y.CODE AND GUIDANCES_CODE = :GuId AND DOMAINS_ID = :DoId AND SKILL_CODE = :SkId ORDER BY ID';
    	try
    	{
    		$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array( ':GuId' => $InGuidanceId, ':DoId' => $InDomainId, ':SkId'=> $InSkillId));
    		 
    		while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT))
    		{
    			// Création de l'orientation avec les données provenant de la
    			// base de données
    			$p = new EPractice($row['SKILL_CODE'], $row['ID'], $row['DESCRIPTION'], $row['LABEL']);
    			array_push($tpractices, $p);
    		}
    	} catch (PDOException $e)
    	{
    		echo "EAppManager:loadPracticesBySkillDomainGuidance Error: " . $e->getMessage();
    		return false;
    	}
    	return $tpractices;
    }
    
    /**
     * Ajoute un nouvelle pratique dans la base de donnée
     * @param $InGuidanceId l'identifiant de l'orientation
     * @param $InDomainId l'identifiant du domaine
     * @param $InSkillId l'identifiant du skill
     * @param $InPracticeDescription La description de la pratique
     * @return true OK | false si une erreur se produit
     */
    public function addPractice($InGuidanceId, $InDomainId, $InSkillId, $InPracticeDescription)
    {
        $sql = "INSERT INTO PROFESSIONNAL_PRACTICES(ID, SKILL_CODE, DOMAINS_ID, GUIDANCES_CODE, DESCRIPTION, YEARS_CODE) VALUES (:Id, :SkId, :DoId, :GuId, :PrDesc, (SELECT CODE FROM YEARS ORDER BY LABEL DESC LIMIT 1))";
        $newPracticeId = "SELECT COUNT(*) + 1 as ID FROM PROFESSIONNAL_PRACTICES WHERE DOMAINS_ID = :Do AND SKILL_CODE = :Sc";

        try
        {
            $stmt = EDatabase::prepare($newPracticeId, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt->execute([':Do' => $InDomainId, ':Sc' => $InSkillId]);
            $newId = $stmt->fetch(PDO::FETCH_ASSOC)['ID'];
            $stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt->execute(array(':Id' => $newId,':GuId' => $InGuidanceId, ':DoId' => $InDomainId, ':SkId' => $InSkillId, ':PrDesc' => $InPracticeDescription));
            return true;
        } catch (PDOException $e)
        {
            echo "EAppManager:addPractice Error: " . $e->getMessage();
            return false;
        }
    }
    
    /**
     * Modifie une practice dans la base de donnée
     * @param $InGuidanceId l'identifiant de l'orientation
     * @param $InDomainId L'identifiant du domaine qui lui correspond
     * @param $InSkillId L'identifiant de la compétence
 	 * @param $InPracticeDescription La description de la pratique
     * @return true OK | false si une erreur se produit
     */
    public function updatePractice($InGuidanceId, $InDomainId, $InSkillId, $InPracticeId, $InPracticeDescription)
    {
    	$sql = "UPDATE PROFESSIONNAL_PRACTICES SET DESCRIPTION = :PrDesc WHERE GUIDANCES_CODE = :GuId AND DOMAINS_ID = :DoId AND SKILL_CODE = :SkId AND ID = :PrId";
    
    	try
    	{
    		$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array( ':GuId' => $InGuidanceId, ':DoId' => $InDomainId, ':SkId' => $InSkillId , ':PrId' => $InPracticeId, ':PrDesc' => $InPracticeDescription));
    		return true;
    	} catch (PDOException $e)
    	{
    		echo "EAppManager:updatePractice Error: " . $e->getMessage();
    		return false;
    	}
    }
    
    /**
     * Suprimme une pratique de la base de donnée
     * @param $InGuidanceId l'identifiant de l'orientation
     * @param $InDomainId L'identifiant du domaine
     * @param $InSkillId L'identifiant de la compétence
 	 * @param $InPracticeId L'identifiant de la pratique
     * @return true OK | false si une erreur se produit
     */
    public function deletePractice($InGuidanceId, $InDomainId, $InSkillId, $InPracticeId)
    {
    	$sql = "DELETE p.* FROM PROFESSIONNAL_PRACTICES p INNER JOIN SKILL s ON p.DOMAINS_ID = s.DOMAINS_ID AND p.GUIDANCES_CODE = s.GUIDANCES_CODE INNER JOIN DOMAINS d ON  d.ID = s.DOMAINS_ID AND d.GUIDANCES_CODE = s.GUIDANCES_CODE WHERE p.SKILL_CODE = :SkId AND p.GUIDANCES_CODE = :GuId AND p.DOMAINS_ID = :DoId AND p.ID = :PrId";
    
    	try
    	{
    		$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array( ':GuId' => $InGuidanceId, ':DoId' => $InDomainId, ':SkId'=> $InSkillId, ':PrId' => $InPracticeId));
    		return true;
    	} catch (PDOException $e)
    	{
    		echo "EAppManager:deletePractice Error: " . $e->getMessage();
    		return false;
    	}
    }
    
    /**
     * 
    * @param $InGuidanceId l'identifiant de l'orientation
     * @param  $InStudentId l'identifiant de l'élève
     * @return true OK | false si une erreur se produit
     */
    public function Initialize($InGuidanceId, $InStudentId){
    	
    	$result = $this->LoadAllHierarchy();
    	$sql = 'INSERT INTO PROFESSIONNAL_PRACTICE_PERSONS(GUIDANCES_CODE, DOMAINS_ID, SKILL_CODE, PROFESSIONNAL_PRACTICES_ID, idPerson, STATES_CODE, YEARS_CODE, MODIFYDATE, idClass, DEGREES_ID) VALUES ';
    	
    	
    	foreach ($result as $guidance) // FAire juste pour la guiddance de l'élève (IF)
    	{
    		if ($guidance->getId() == $InGuidanceId)
    		{
    			foreach ($guidance->getDomains() as &$domain)
    			{
    				$domainid = $domain->getId();
    				foreach($domain->getSkills() as &$skill)
    				{
    					$skillid = $skill->getId();
    					foreach ($skill->getPractices() as &$practice) // Chopper le bon domaineid, le bon skillid, la bonne practiceid de la practique
    					{
    						$practiceid = $practice->getId();
    						$sql .= '(:GuId,"'. $domainid .'",'. $skillid .','. $practiceid .', :SuId, 1, (SELECT CODE FROM YEARS ORDER BY LABEL DESC LIMIT 1), (SELECT NOW()), (SELECT idClass FROM Person_Belongs_Class WHERE idPerson = :SuId), (SELECT DEGREES_ID FROM Person_Belongs_Class WHERE idPerson = :SuId)),';
    					}
    				}    				
    			}
    		}
    	}
    	$sql = substr($sql,0,-1);
    	try
    	{
    		$stmt = EDatabase::prepare($sql, array( PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    		$stmt->execute(array( ':GuId' => $InGuidanceId, ':SuId' => $InStudentId));
    		return true;
    	} catch (PDOException $e)
    	{
    		echo "EAppManager:Initialize Error: " . $e->getMessage();
    		return false;
    	}
    }
    
    
    /** @var Contient le tableau des EGuidance */
    private $guidances;
}

?>