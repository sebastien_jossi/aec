<?php
require_once './inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Je récupère l'id de guidance

$guidanceid = -1;
$domainid = -1;
$id = -1;
if (isset($_POST['guidanceId'])&&isset($_POST['domainId'])){
$guidanceid = $_POST['guidanceId'];
$domainid = $_POST['domainId'];
}

	if ($id > 0){
		$domains = EAppManager::getInstance()->loadSkillByDomainGuidance($guidanceid,$domainid);
		if ($domains === false){
			echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de loadSkillByDomainGuidance()"}';
			exit();
		}

		$jsn = json_encode($domains);
		if ($jsn == FALSE){
			$code = json_last_error();
			echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json ('.$code.'"}';
			exit();
		}
		echo '{ "ReturnCode": 0, "Data": '.utf8_encode($jsn).'}';
		exit();

	}

	// Si j'arrive ici, c'est pas bon
	echo '{ "ReturnCode": 1, "Message": "Il manque le paramètre guidanceId"}';
