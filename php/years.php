<?php

class EYears implements JsonSerializable
{

	/**
	 * @brief Class Constructor
	 */
	
	/**
	 * 
	 * @param $InYearId L'identifiant de l'année
	 * @param $InYearName Le nom de l'année
	 */
	public function __construct($InYearId = -1, $InYearName = "")
	{
		$this->yearid = $InYearId;
		$this->yearname = $InYearName;
	}


	private function __clone()
	{}

	/***
	 * Transforme l'objet en json
	 * @return L'objet en json
	 */
	public function jsonSerialize()
	{
		return get_object_vars($this);
	}
	
	public function getId(){
	
		return $this->yearid;
	}
	
	public function getYear(){
	
		return $this->yearname;
	}
	
	private $yearid;
	private $yearname;
}