<?php

/**
 * @copyright ronaldo.lrrpnt@eduge.ch 2016-2017
 */

/**
 * Classe des compétences opérationnelles
 * @author RLP
 */
class ESkill implements JsonSerializable
{
	
	/***
	 * 
	 * @param $InDomainId L'identifiant du domaine qui lui correspond
	 * @param $InSkillId L'identifiant de la compétence
	 * @param $InSkillName Le nom de la compétence
	 * @param $InSkillDefinition La définition de la compétence
	 * @param $InSkillMethodologic La compétence méthodologique de la compétence
	 * @param $InSkillSocial La compétence social de la compétence
	 * @param $InSkillPersonnal La compétence personnelle de la compétence
	 * @param $InSkillYear L'année de création de la compétence
	 */
	public function __construct($InDomainId = - 1, $InSkillId = - 1, $InSkillName = "", $InSkillDefinition = "", $InSkillMethodologic = "", $InSkillSocial = "", $InSkillPersonnal = "", $InSkillYear = "")
	{
		$this->owner = $InDomainId;
		$this->skillid = $InSkillId;
		$this->skillname = $InSkillName;
		$this->skilldefinition = $InSkillDefinition;
		$this->skillmethodologic = $InSkillMethodologic;
		$this->skillsocial = $InSkillSocial;
		$this->skillpersonnal = $InSkillPersonnal;
		$this->skillyear = $InSkillYear;
		$this->practices = array();
	}
	
	/**
	 * @brief On ne laisse pas cloner une compétence
	 */
	private function __clone()
	{}
	
	
	/**
	 * @brief Est-ce que cet objet est valide
	 *
	 * @return True si valide, autrement false
	 */
	public function isValid()
	{
		return ($this->skillid == - 1) || ($this->owner = - 1) ? false : true;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return L'identifiant du parent
	 */
	public function getOwnerId()
	{
		return $this->owner;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return L'identifiant unique
	 */
	public function getId()
	{
		return $this->skillid;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return Le nom de la compétence
	 */
	public function getName()
	{
		return $this->skillname;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return  La definition de la compétence 
	 */
	public function getDefinition()
	{
		return $this->skilldefinition;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return La compétence méthodologique de la compétence
	 */
	public function getMethodologic()
	{
		return $this->skilldefinition;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return La compétence social de la compétence
	 */
	public function getSocial()
	{
		return $this->skilldefinition;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return  La compétence personnelle de la compétence
	 */
	public function getPersonnal()
	{
		return $this->skilldefinition;
	}
	
	/**
	 * @brief Getter
	 *
	 * @return L'année de création de la compétence
	 */
	public function getYear()
	{
		return $this->skillyear;
	}
	
	/**
	 * @brief Modification des valeurs du skill
	 * @param $InSkillName Le nom de la compétence
	 * @param $InSkillDefinition La définition de la compétence
	 * @param $InSkillMethodologic La compétence méthodologique de la compétence
	 * @param $InSkillSocial La compétence social de la compétence
	 * @param $InSkillPersonnal La compétence personnelle de la compétence
	 */
	public function modifySkill($InSkillName, $InSkillDefinition, $InSkillMethodologic, $InSkillSocial, $InSkillPersonnal )
	{
		$this->skillname = $InSkillName;
		$this->skilldefinition = $InSkillDefinition;
		$this->skillmethodologic = $InSkillMethodologic;
		$this->skillsocial = $InSkillSocial;
		$this->skillpersonnal = $InSkillPersonnal;
	}
	
	/***
	 * Transforme l'objet en json
	 * @return L'objet en json
	 */
	public function jsonSerialize() {
	
		/*return [
		 'id' => $this->domainid,
		 'description' => $this->domaindescription,
		 'year' => $this->domainyear,
			];*/
	
		return get_object_vars($this);
	}
	
	/**
	 * Getter
	 *
	 * @return Le tableau des EPractices | false si une erreur se produit
	 */
	public function getPractices()
	{
		return $this->practices;
	}
		
	/**
	 * Ajoute une pratique en créeant l'objet dans le tableau des practices
 	 * @param $InSkillId L'identifiant de la compétence
 	 * @param $InPracticceId L'identifiant de la pratique professionnelle
 	 * @param $InPracticeDescription La description de la pratique professionnelle
 	 * @param $InPracticeYear L'année de création de la pratique professionnelle 
	 */
	public function addPracticeAttr ($InSkillId = - 1, $InPracticeId = - 1, $InPracticeDescription = "", $InPracticeYear = "")
	{
		try
		{
			array_push($this->practices, new ESkill($this->skillid, $InPracticeId, $InPracticeDescription, $InPracticeYear));
			return true;
		} catch (Exception $e)
		{
			echo "ESkill:addPractice Error: " . $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Ajoute un objet practice au tableau des practices
	 * @param $InPractice L'objet EPractice à ajouter
	 * @return Le tableau des practices | false si une erreur se produit
	 */
	public function addPractice ($InPractice)
	{
		try
		{
			array_push($this->practices, $InPractice);
			return true;
		} catch (Exception $e)
		{
			echo "ESkill:addPractice Error: " . $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Ajoute un tableau de practice au tableau des practices
	 * @param $InArrPractices Un tableau contenant des objets EPractice à ajouter
	 * @param $clearAll Efface le contenu du tableau avant d'ajouter si true
	 * @return Le tableau des practices | false si une erreur se produit
	 */
	public function addPractices ($InArrPractices, $clearAll = false)
	{
		try
		{
			if ($clearAll == true)
				$this->practices = $InArrPractices;
				else
					array_push($this->practices, $InArrPractices);
	
					return true;
		} catch (Exception $e)
		{
			echo "ESkill:addPractices Error: " . $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Modifie une practice du tableau des practices
	 * @param $InPracticeId L'identifiant de la pratique
	 * @param $InPracticeDescription Le nom de la pratique
	 * @return Le tableau des practices | false si une erreur se produit
	 */
	public function modifyPractice($InPracticeId, $InPracticeDescription)
	{
		try 
		{
			foreach ($this->practices as &$p)
			{
				if ($p->getId() === $InPracticeId)
				{
					$p->modifyDescription($InPracticeDescription);
					return true;
				}
			}
		} catch (Exception $e)
		{
			echo "ESkill:modifyPractice Error: " . $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Supprime une practice du tableau des practices
	 * @param $InPracticeId L'identifiant de la practice
	 * @return Le tableau des practices | false si une erreur se produit
	 */
	public function deletePractice($InPracticeId)
	{
		try
		{
			foreach ($this->practices as &$p)
			{
				if ($p->getId() === $InPracticeId)
				{
					unset($p);
					return true;
				}
			}
		} catch (Exception $e)
		{
			echo "ESkill:deletePractice Error: " . $e->getMessage();
			return false;
		}
	}
	
	/** L'identifiant de la compétence */
	private $skillid;

	/** L'identifiant du domaine */
	private $owner;

	/** Le nom de la compétence*/
	private $skillname;
	
	/** La definition de la compétence */
	private $skilldefinition;
	
	/** La compétence méthodologique de la compétence*/
	private $skillmethodologic;
	
	/** La compétence social de la compétence*/
	private $skillsocial;
	
	/** La compétence personnelle de la compétence*/
	private $skillpersonnal;
	
	/** L'année de création de la compétence*/
	private $skillyear;
	
	/** Le tableau de EPractice*/
	private $practices;
	
}

?>