<?php
/**
 * @copyright floran.stck@eduge.ch
 */


/**
 * Classe d'exemple d'un utilisateur
 * @author FS
 *
 */
require_once './person.php';
require_once './constants.php';


class EUser extends EPerson implements JsonSerializable{
	
	public function __construct($InIdPerson = -1, $InRole = "", $InFirstName = "",$InLastName = "",$InEmail = "",$InIsActive = false,$InIsStudent = false){
		
 		parent::__construct($InIdPerson, $InFirstName, $InLastName, $InEmail, ($InRole == ER_STUDENT),$InIsActive,$InIsStudent);
		$this->role = $InRole;
	}

	
	/**
     * @brief Est-ce que cet utilisateur est admin
     * 
     * @return True si il est admin, autrement false
     */
	public function isAdmin(){
		return ($this->role == ER_ADMINISTRATOR);
	}
	/**
	 * @brief Est-ce que cet utilisateur est membre de la direction
	 *
	 * @return True si il fait partie des membres de la direction, autrement false
	 */
	public function isDirMember(){
		return ($this->role == ER_DIRECTIONMEMBER);
	}
	/**
	 * @brief Est-ce que cet utilisateur est un élève
	 *
	 * @return True si il fait partie des élèves, autrement false
	 */
	public function isStudent(){
		return ($this->role == ER_STUDENT) && parent::isStudent();
	}
	
	/**
	 * On retourne le rôle de l'utilisateur
	 */
	public function getRole(){
		return $this->role;
		
	}
	/***
	 * Transforme l'objet en json
	 * @return L'objet en json
	 */
	public function jsonSerialize()
	{
		return get_object_vars($this);
	}
	
	protected $role;
}