<?php
/**
 * @brief	Permet d'envoyer les variables à appManager
* 			pour supprimer un ou plusieur domaine(s) dans la base de données
* @author 	romain.ssr@eduge.ch
*
* @date 10.02.2017
*/
require_once './inc.all.php';

$guidanceid = -1;

if (isset($_POST['guidanceId']))
	$guidanceid = $_POST['guidanceId'];

	$domainid = -1;

	if (isset($_POST['domainId']))
		$domainid = $_POST['domainId'];

			// J'envoie les informations nécessaires pour ajouter un domaine dans la base de donée.
		for ($i = 0; $i < count($domainid); $i++) {
		$modifiedDomain = EAppManager::getInstance()->deleteDomain($guidanceid,$domainid[$i]);
			}
			if ($modifiedDomain === false){
				echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de loadAllGuidance()"}';
				exit();
			}

			echo '{ "ReturnCode": 0}';
