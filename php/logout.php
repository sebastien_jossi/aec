<?php
/**
 * Page de deconnexion
 */

if (session_status() == PHP_SESSION_NONE) {     session_start(); }
$CompteCreer = false;
if(isset($_SESSION["CompteCreer"] )){
    //Sauvegarde variable compte creer
    $CompteCreer = 	$_SESSION["CompteCreer"]; 
}

//destruction session
session_unset();
session_destroy();
session_start();

//ReCreer variable session

$_SESSION["CompteCreer"] = $CompteCreer;

if(isset($_GET['session'])) {
    header('Location: ../index.php');
}
?>
<script src="https://apis.google.com/js/platform.js"></script>        
<script type="text/javascript" src="./js/eelauth.js"></script>
<script>
var popupDisconnectWindow = null;
    /**
     * Call-back quand on click sur le bouton login
     * @param {type} event
     */
    // alwaysLowered est mis à 1 afin de placer la fenêtre de déconnexion derrière
    // une fois qu'on remet le focus sur la fenêtre principale
    var strWindowFeatures = "menubar=no,location=no,resizable=no,scrollbars=no,status=no,alwaysLowered=1";
    popupDisconnectWindow = window.open('https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout', "LogoutEEL", strWindowFeatures);
    // Fermer la fenêtre de déconnexion dans 2 secondes
    setTimeout(closeDisconnectWindow, 1000);

    /**
     * On ferme la fenêtre de déconnexion EEL
     * et on remet le focus sur la fenêtre principale
     */
    function closeDisconnectWindow() {
        if (popupDisconnectWindow != null && !popupDisconnectWindow.closed) {
            popupDisconnectWindow.close();
            popupDisconnectWindow = null;
        }
        window.location.href = "logout.php?session=1";
    }
</script>