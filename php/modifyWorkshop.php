<?php

require_once './inc.all.php';
/**
 * @brief modifie un atelier dans la base de données
 * @param $id L'indentifiant de l'atelier
 * @param $name  Le nom de l'atelier
 * @param $desc  La description de l'atelier
 * @param $degre Le degrée de l'atelier
 */
$id = - 1;
$name = "";
$desc = "";
$degre = - 1;
// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

if (isset($_POST['idWorkshop']))
    $id = filter_input(INPUT_POST, 'idWorkshop', FILTER_SANITIZE_STRING);

if (isset($_POST['nameWorkshop']))
    $name = filter_input(INPUT_POST, 'nameWorkshop', FILTER_SANITIZE_STRING);

if (isset($_POST['descWorkshop']))
    $desc = filter_input(INPUT_POST, 'descWorkshop', FILTER_SANITIZE_STRING);

if (isset($_POST['degreWorkshop']))
    $degre = filter_input(INPUT_POST, 'degreWorkshop', FILTER_SANITIZE_STRING);

if ($name != -1  && $degre != -1 && $id != -1) {

    if (is_numeric($id)) {
        if ($id > 0) {
            if (EDataManager::getInstance()->updateWorkshop($id, $name, $desc, $degre)) {
                echo '{ "ReturnCode": 0, "Message": "Tous s\'est bien passé"}';
                exit();
            } else {
                echo '{ "ReturnCode": 2, "Message": "Une erreur de la fonction updateWorkshop()"}';
                exit();
            }
        }
    }






    /*      if ($id !== -1) {//edit
      }
      $result = EDataManager::getInstance()->getIdDataStudent($guidanceid, $domainid, $skillid, $practiceid, $studentid, $stateid);
      if (count($result) >= 0) {
      if (!EDataManager::getInstance()->addNewWorkshop($guidanceid, $domainid, $skillid, $practiceid, $studentid, $arrWorkshop)) {
      echo '{ "ReturnCode": 2, "Message": "Un problème de la fonction addDataWorkshop()"}';
      exit();
      }
      } else {
      if (!EDataManager::getInstance()->deleteDataWorkshop($guidanceid, $domainid, $skillid, $practiceid, $studentid, $arrWorkshop)) {
      echo '{ "ReturnCode": 2, "Message": "Un problème de la fonction deleteDataWorkshop()"}';
      exit();
      }
      } */
}
// Si j'arrive ici, c'est TOUT bon
echo '{ "ReturnCode": 0, "Message": ""}';

