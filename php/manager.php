<?php

require_once './user.php';


class EManager extends EUser implements JsonSerializable{
	
	/**
	 * @brief Class Constructor
	 */
	public function __construct($InIdPerson = -1, $InIsAdmin = false, $InFirstName = "", $InLastName = "", $InEmail = "",$InRole = - 1,$InIsActive = false)
	{
		parent::__construct($InIdPerson, ($InIsAdmin == false) ? ER_DIRECTIONMEMBER : ER_ADMINISTRATOR, $InFirstName, $InLastName, $InEmail, $InRole, $InIsActive);
		$this->manageractive = $InIsActive;
	}
	

	public function getActive() {
		return $this->manageractive;
	}
	
	/***
	 * Transforme l'objet en json
	 * @return L'objet en json
	 */
	public function jsonSerialize()
	{
		return get_object_vars($this);
	}
}