<?php
/**
 * @brief @brief Charge toutes les évaluations en fonction des filtres et compte le nombre d'étudiant contenu dans les filtres
 * @param $guidanceId L'identifiant de l'orientation /!\ OBLIGATOIRE
 * @param $classroomId L'identifiant de la classe /!\ OBLIGATOIRE
 *
 * @author ronaldo.lrrpnt@eduge.ch
 */
require_once './inc.all.php';

$classroomId = - 1;
$yearId = - 1;
				
if(isset($_POST['classroomId']) && $_POST['classroomId'] != "")
	$classroomId = $_POST['classroomId'];

if(isset($_POST['classroomId']))
	$yearId= $_POST['yearId'];
	
// Nécessaire lorsqu'on retourne du json
header ( 'Content-Type: application/json' );
if (($classroomId!= - 1) && (($yearId != - 1))) {
	$data = EDataManager::getInstance ()->loadAllDataStudentByClassroom($classroomId, $yearId);
	if ($data === false)
	{
		echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de loadAllDataStudentByClassroom()"}';
		exit ();
	}
	
	$jsn = json_encode ( $data );
	if ($jsn == FALSE) {
		$code = json_last_error ();
		echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json (' . $code . '"}';
		exit ();
	}
	echo '{"ReturnCode": 0, "Data": ' . utf8_encode ( $jsn ) . '}';
	exit ();
}

// Si j'arrive ici, c'est pas bon
echo '{ "ReturnCode": 1, "Message": "Il manque les paramètres yearId et/ou classroomId"}';

?>