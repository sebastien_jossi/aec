<?php
/**
 * @brief Charge toutes la Hierarchy
 */
require_once './inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Récupère l'id l'année
$yearid = "";
if(isset($_POST['yearId']))
	$yearid = $_POST['yearId'];

$base = EAppManager::getInstance()->LoadAllHierarchy($yearid);
if ($base === false){
	echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de LoadAllHierarchy()"}';
	exit();
}
	$jsn = json_encode($base);
if ($jsn == FALSE){
	$code = json_last_error();
	echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json ('.$code.'"}';
	exit();
}
echo '{ "ReturnCode": 0, "Data": '.utf8_encode($jsn).'}';
