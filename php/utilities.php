<?php


function json_encode_objs($item){
	if(!is_array($item) && !is_object($item)){
		return json_encode($item);
	}else{
		$pieces = array();
		foreach($item as $k=>$v){
			$pieces[] = "\"$k\":".json_encode_objs($v);
		}
		return '{'.implode(',',$pieces).'}';
	}
}


function utf8_encode_all($dat) // -- It returns $dat encoded to UTF8
{
	if (is_string($dat)) 
		return utf8_encode($dat);
	if (!is_array($dat)) 
		return $dat;
	$ret = array();
	foreach($dat as $i=>$d) 
		$ret[$i] = utf8_encode_all($d);
	return $ret;
}