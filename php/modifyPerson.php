<?php
/**
 * @brief Permet de modifier un utilisateur
 * @author 	quentin.fslr@eduge.ch
 */
require_once './inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$idPerson  = -1;
$firstName = "";
$lastName  = "";
$email = "";
$isActive  = FALSE;
$role      = -1;
$inClass   = -1;
$degreesId = -1;

if (isset($_POST['idPerson']) && isset($_POST['firstName']) && isset($_POST['lastName'])  && isset($_POST['email']) && isset($_POST['isActive'])&& isset($_POST['role'])&& isset($_POST['inClass'])&& isset($_POST['degreesId'])){
	$idPerson  = $_POST['idPerson'];
	$firstName = filter_var($_POST['firstName'], FILTER_SANITIZE_STRING);
    $lastName  = filter_var($_POST['lastName'], FILTER_SANITIZE_STRING);
    $email = filter_var($_POST['email'], FILTER_SANITIZE_STRING);
	$isActive  = $_POST['isActive'];
	$role      = $_POST['role'];
	$inClass   = $_POST['inClass'];
    $degreesId = $_POST['degreesId'];

    if(EUserManager::getInstance()->ModifyUser($idPerson,$firstName ,$lastName,$email,$isActive,$role,$inClass,$degreesId)){
        echo '{ "ReturnCode": 0, "Data": "les modifications ont été enregistrées"}';
	    exit();
    }
    

}
else {
	echo '{ "ReturnCode": 1, "Message": "Erreur dans les paramètres. Valeurs manquantes."}';
	exit();
}