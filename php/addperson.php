<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
/**
 * !!!!!!!!!!!!!! ANCIEN FICHIER UTILISER GETPERSON.PHP !!!!!!!!!!!!
 * !!!!!!!!!!!!!! ANCIEN FICHIER UTILISER GETPERSON.PHP !!!!!!!!!!!!
 * !!!!!!!!!!!!!! ANCIEN FICHIER UTILISER GETPERSON.PHP !!!!!!!!!!!!
 * @copyright romain.ssr@eduge.ch
 * @author RS
 * @brief objet permettant de récupérer les informations relatives à une personne dans la base de donnée
 * @param $email contient l'email de l'élève
 * @param $person contient toutes les informations de l'élève

 * !!!!!!!!!!!!!! ANCIEN FICHIER UTILISER GETPERSON.PHP !!!!!!!!!!!!
 * !!!!!!!!!!!!!! ANCIEN FICHIER UTILISER GETPERSON.PHP !!!!!!!!!!!!
 * !!!!!!!!!!!!!! ANCIEN FICHIER UTILISER GETPERSON.PHP !!!!!!!!!!!!
 */

require_once './inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$email = "";
$firstName = "";
$lastName = "";
$active = 0;

if (isset($_POST['email']) && isset($_POST['firstName']) && isset($_POST['lastName']))
{
    $email = $_POST['email'];
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];
    $idRole = $_POST['idRole'];
    $userInfos = [
        'email' => $email,
        'firstName' => $firstName,
        'lastName' => $lastName,
        'active' => $active
    ];
}
$_SESSION['email'] = $email;

//if (strlen($_SESSION['email']) == 0){
//    echo '{ "ReturnCode": 1, "Message": "Il faut impérativement un email pour rechercher un utilisateur."}';
//    exit();
//}

// Je récupère l'utilisateur par son email
$person = EUserManager::getInstance()->findUserByEmail($_SESSION['email']);
$_SESSION['person'] = serialize($person);

// Creer le compte si il n'existe pas
if (!$person){
    EUserManager::getInstance()->addUser($userInfos);

    echo json_encode([
        'ReturnCode' => 0,
        'Message' => './html/VueEleve/AccueilApprenti.php'
    ]);
    exit();
}
//
//if ($person === false){
//    echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de findUserByEmail()"}';
//    exit();
//}
//switch ($_SESSION['role']) {
//    case 1:
//        $guidance = $person->getGuidance();
//        $_SESSION['guidance'] = $guidance;
//        $_SESSION['degree'] = $person->getDegree();
//        $id = $person->getIdPerson();
//        $_SESSION['id'] = $id;
//
//        $page = "./html/VueEleve/AccueilApprenti.php";
//        break;
//    case 2:
//        $page = "./html/VueDirection/AccueilDirection.php";
//        break;
//    case 3:
//        $page = "./html/VueDirection/AccueilDirection.php";
//        break;
//}

echo json_encode([
    'ReturnCode' => 4,
    'Message' => 'Rien ne va plus avec ' . $_SESSION['email']
]);
//echo '{ "ReturnCode": 0, "Message": "'. $page . '"}';
//echo '{ "ReturnCode": 4, "Message": "Rien ne va plus"}';


