<?php
/**
 * @brief Permet de suprimmer un utilisateur
 * @author 	quentin.fslr@eduge.ch
 */
require_once './inc.all.php';

$personnId = -1;

if (isset($_POST['personId']))
	$personnId = $_POST['personId'];

if($personnId!= -1){
    if(EUserManager::getInstance()->deletePerson($personnId)){
        echo '{ "ReturnCode": 0, "Data": "Utilisateur suprimmé"}';
        exit();
    }
    else{
        echo '{ "ReturnCode": 6}';
        exit();
    }
}
else{
    echo '{ "ReturnCode": 1, "Message": "Erreur dans les paramètres. Valeurs manquantes."}';
	exit();
}