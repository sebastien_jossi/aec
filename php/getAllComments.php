<?php
session_start();
require './inc.all.php';

if (isset($_POST['getAll'])) {
  echo (json_encode(array("allComments" => ECommentManager::getAllComments())));
  exit();
}

if (isset($_POST['getByStudent'])) {
  echo (json_encode(array("allComments" => ECommentManager::getAllCommentsByStudent($_SESSION['id']))));
  exit();
}