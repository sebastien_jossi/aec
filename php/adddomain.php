<?php
/**
 * @brief	Permet d'envoyer les variables à appManager 
 * 			pour ajouter un domaine dans la base de données
 * @param $guidanceid L'identifiant de l'orientation
 * @param $domaindestription La description du domaine
 * @author 	romain.ssr@eduge.ch
 */
require_once './inc.all.php';

$guidanceid = -1;
if (isset($_POST['guidanceId']))
	$guidanceid = $_POST['guidanceId'];

$domaindescription = "";
if (isset($_POST['domaindescritpion']))
	$domaindescription = filter_var($_POST['domaindescritpion'], FILTER_SANITIZE_STRING);

if ($guidanceid == -1 || strlen($domaindescription) == 0){
	echo '{ "ReturnCode": 1, "Message": "Erreur dans les paramètres. Valeurs manquantes."}';
	exit();
}

	
// J'envoie les informations nécessaires pour ajouter un domaine dans la base de donée. 
if (!EAppManager::getInstance()->addDomain($guidanceid,$domaindescription)){
	echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de loadAllGuidance()"}';
	exit();
}

echo '{ "ReturnCode": 0}';
