<?php

require_once './database.php';
require_once './datastudent.php';

/**
 * @brief Helper class pour gérer les données des apprentis
 *
 * @author ronaldo.lrrpnt@eduge.ch
 */
class EDataManager {

    private static $objInstance;

    /**
     * @brief Class Constructor - Create a new EDataManager if one doesn't exist
     * Set to private so no-one can create a new instance via ' = new
     * EDataManager();'
     */
    private function __construct() {
        $this->datastudent = array();
    }

    /**
     * @brief Like the constructor, we make clone private so nobody can clone
     * the instance
     */
    private function __clone() {
        
    }

    /**
     * @brief Retourne notre instance ou la crée
     *
     * @return $objInstance;
     */
    public static function getInstance() {
        if (!self::$objInstance) {
            try {
                self::$objInstance = new EDataManager();
            } catch (Exception $e) {
                echo "EDataManager Error: " . $e;
            }
        }
        return self::$objInstance;
    }

    /**
     * Charge toutes les évaluations d'un apprenti
     *
     * @return Le tableau des EDataStudent | false si une erreur se produit
     */
    public function loadAllDataByStudent($InStudentId) {
        $this->datastudent = array();
        $sql1 = 'SELECT p1.idPerson, p1.PROFESSIONNAL_PRACTICES_ID, p1.SKILL_CODE, p1.DOMAINS_ID, p1.GUIDANCES_CODE, p1.YEARS_CODE, p1.STATES_CODE, DATE_FORMAT(p1.MODIFYDATE,\'%d.%m.%Y\')MODIFYDATE FROM PROFESSIONNAL_PRACTICE_PERSONS p1 WHERE p1.idPerson = :PeId AND p1.YEARS_CODE = (SELECT MAX(p2.YEARS_CODE) FROM PROFESSIONNAL_PRACTICE_PERSONS p2 WHERE p2.idPerson = p1.idPerson AND p2.PROFESSIONNAL_PRACTICES_ID =  p1.PROFESSIONNAL_PRACTICES_ID AND p2.SKILL_CODE = p1.SKILL_CODE AND p2.DOMAINS_ID = p1.DOMAINS_ID AND p2.GUIDANCES_CODE = p1.GUIDANCES_CODE) GROUP BY idPerson, PROFESSIONNAL_PRACTICES_ID, SKILL_CODE, DOMAINS_ID, GUIDANCES_CODE';
        $sql2 = 'SELECT idPerson, GUIDANCES_CODE, DOMAINS_ID, SKILL_CODE, PROFESSIONNAL_PRACTICES_ID, WORKSHOPS_ID FROM PROFESSIONNAL_PRACTICE_WORKSHOPS WHERE idPerson = :PeId';
        try {
            // Récupère les évaluations de l'apprenti
            $stmt1 = EDatabase::prepare($sql1, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt1->execute(array(':PeId' => $InStudentId));
            // Rècupère les ateliers des évaluations de l'apprenti
            $stmt2 = EDatabase::prepare($sql2, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt2->execute(array(':PeId' => $InStudentId));

            // Mets les résultat des requetes sql dans un tableau
            $result1 = $stmt1->fetchAll();
            $result2 = $stmt2->fetchAll();

            if (count($result1) > 0) {
                // Crée l'objet pour mettre les données de l'apprenti
                $ds = new EDataStudent($InStudentId);
                array_push($this->datastudent, $ds);


                foreach ($result1 as $row1) {
                    // Création des données de l'aprentis avec les données provenant de la base de données
                    $dp = new EDataPractice($row1['GUIDANCES_CODE'], $row1['DOMAINS_ID'], $row1['SKILL_CODE'], $row1['PROFESSIONNAL_PRACTICES_ID'], $row1['idPerson'], $row1['STATES_CODE'], $row1['YEARS_CODE'], $row1['MODIFYDATE']);

                    // Rempli le tableaux Workshops de EDataPractice avec les données qui correspondent
                    foreach ($result2 as $row2) {
                        if (($row1['GUIDANCES_CODE'] === $row2['GUIDANCES_CODE']) && ($row1['DOMAINS_ID'] === $row2['DOMAINS_ID']) && ($row1['SKILL_CODE'] === $row2['SKILL_CODE']) && ($row1['PROFESSIONNAL_PRACTICES_ID'] === $row2['PROFESSIONNAL_PRACTICES_ID'])) {
                            $dp->addWorkshopDataPractice($row2['WORKSHOPS_ID']);
                        }
                    }

                    $ds->addDataPractice($dp);
                }
            }
            // Retourner sous forme de tableau pour convenir à la fonction /js/colorSystem
            return array($this->datastudent);
        } catch (PDOException $e) {
            echo "EDataManager:loadAllDataByStudent Error: " . $e->getMessage();
            return false;
        }
    }

    /**
     * @brief Charge toutes les évaluations en fonction des filtres et compte le nombre d'étudiant contenu dans les filtres et récupère les données nécessaire à l'affichage des moyennes
     * @param $InGuidanceId L'identifiant de l'orientation /!\ OBLIGATOIRE
     * @param $InDegreeId L'identifiant du degrée 
     * @param $InClassroomId L'identifiant de la classe 
     * @param $InYearId L'identifiant de l'année /!\ OBLIGATOIRE
     * @return Le tableau des EDataStudent, Le nombre d'étudiant contenu dans le filtrage, Les ateliers | false si une erreur se produit
     */
    public function loadAllDataByFilter($InGuidanceId = - 1, $InDomainId = - 1, $InSkillId = - 1, $InDegreeId = - 1, $InClassroomId = - 1, $InYearId = - 1) {
        $this->datastudent = array();
        $arrExec = array(':GuId' => $InGuidanceId, ':YeId' => $InYearId);
        // Contrôle des paramètres
        if (($InGuidanceId != - 1) && ($InYearId != - 1)) {
            // Prend les évaluatons des élèves en fonction des filtres
            $sql1 = 'SELECT idPerson, GUIDANCES_CODE, DOMAINS_ID, SKILL_CODE, PROFESSIONNAL_PRACTICES_ID, STATES_CODE, YEARS_CODE FROM PROFESSIONNAL_PRACTICE_PERSONS WHERE GUIDANCES_CODE = :GuId AND YEARS_CODE = :YeId';
            // Prend les ateliers des évaluation des élèves en fonction des filtres
            $sql2 = 'SELECT idPerson, GUIDANCES_CODE, DOMAINS_ID, SKILL_CODE, PROFESSIONNAL_PRACTICES_ID, WORKSHOPS_ID FROM PROFESSIONNAL_PRACTICE_WORKSHOPS WHERE GUIDANCES_CODE = :GuId AND YEARS_CODE = :YeId';
            // Compte le nombre d'apprenti qui appartient à l'orientation
            $sql3 = 'SELECT COUNT(DISTINCT idPerson) `TotalStudent` FROM PROFESSIONNAL_PRACTICE_PERSONS WHERE GUIDANCES_CODE = :GuId AND YEARS_CODE = :YeId';
            // Prend les ateliers
            $sql4 = 'SELECT ID, NAME, DEGREES_ID FROM WORKSHOPS';
            // Compte le nombre de pratique pour chaque skill qui est dans l'orientation
            $sql5 = 'SELECT DOMAINS_ID domainid, SKILL_CODE skillid, COUNT(ID) TotalPractice FROM PROFESSIONNAL_PRACTICES WHERE GUIDANCES_CODE = :GuId GROUP BY DOMAINS_ID, SKILL_CODE';

            try {
                // Ajoute une condition afin de pouvoir filtrer les apprentis par classe
                if ($InClassroomId != - 1) {
                    $condition = ' AND (idPerson IN (SELECT idPerson FROM PROFESSIONNAL_PRACTICE_PERSONS WHERE idClass = :ClId))';
                    $sql1 .= $condition;
                    $sql2 .= $condition;
                    $sql3 .= $condition;
                    $arrExec [':ClId'] = $InClassroomId;
                }

                // Ajoute une condition afin de pouvoir filtrer les apprentis par degrée scolaire
                if ($InDegreeId != - 1) {
                    $condition = ' AND (idPerson IN (SELECT idPerson FROM PROFESSIONNAL_PRACTICE_PERSONS WHERE DEGREES_ID = :DeId))';
                    $sql1 .= $condition;
                    $sql2 .= $condition;
                    $sql3 .= $condition;
                    $arrExec [':DeId'] = $InDegreeId;
                }


                // Exécute cette requête avant pour évité un problème avec $arrExec
                $stmt3 = EDatabase::prepare($sql3, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
                $stmt3->execute($arrExec);


                // Ajoute une condition afin de pouvoir filtrer les évaluations par domaine
                if ($InDomainId != - 1) {
                    $condition = ' AND DOMAINS_ID = :DoId';
                    $sql1 .= $condition;
                    $sql2 .= $condition;
                    $arrExec [':DoId'] = $InDomainId;
                }

                // Ajoute une condition afin de pouvoir filtrer les évaluations par compétence
                if ($InSkillId != - 1) {
                    $condition = ' AND SKILL_CODE = :SkId';
                    $sql1 .= $condition;
                    $sql2 .= $condition;
                    $arrExec [':SkId'] = $InSkillId;
                }

                // Bessoin de trié pour que la création d'un nouveau EDataStudent soit plus facile
                $order = " ORDER BY idPerson";
                $sql1 .= $order;
                $sql2 .= $order;

                $stmt5 = EDatabase::prepare($sql5, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
                $stmt5->execute(array(':GuId' => $InGuidanceId));

                // Le faire maintenant évite un problème de nombre d'attribut dans $arrExec
                $stmt1 = EDatabase::prepare($sql1, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
                $stmt1->execute($arrExec);

                $stmt2 = EDatabase::prepare($sql2, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
                $stmt2->execute($arrExec);

                $stmt4 = EDatabase::prepare($sql4, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
                $stmt4->execute();

                // Ancien idPerson
                $oldPeId = 0;
                // Apprenti actuel
                $ds = null;

                // Mets lees résultat des requetes sql dans un tableau
                $result1 = $stmt1->fetchAll();
                $result2 = $stmt2->fetchAll();
                foreach ($result1 as $row1) {
                    // Crée un nouvel apprentis s'il n'est pas encore dans le tableau
                    if ($oldPeId != $row1['idPerson']) {
                        // Création des données de l'apprenti avec les données provenant de la base de données
                        $ds = new EDataStudent($row1['idPerson']);
                        array_push($this->datastudent, $ds);
                        $oldPeId = $row1['idPerson'];
                    }

                    // Création des données de l'aprentis avec les données provenant de la base de données
                    $dp = new EDataPractice($row1['GUIDANCES_CODE'], $row1['DOMAINS_ID'], $row1['SKILL_CODE'], $row1['PROFESSIONNAL_PRACTICES_ID'], $row1['idPerson'], $row1['STATES_CODE'], $row1['YEARS_CODE']);

                    // Rempli le tableaux Workshops de EDataPractice avec les données qui correspondent
                    foreach ($result2 as $row2) {
                        if (($row1['GUIDANCES_CODE'] === $row2['GUIDANCES_CODE']) && ($row1['DOMAINS_ID'] === $row2['DOMAINS_ID']) && ($row1['SKILL_CODE'] === $row2['SKILL_CODE']) && ($row1['PROFESSIONNAL_PRACTICES_ID'] === $row2['PROFESSIONNAL_PRACTICES_ID']) && ($row1['idPerson'] === $row2['idPerson'])) {
                            $dp->addWorkshopDataPractice($row2['WORKSHOPS_ID']);
                        }
                    }

                    $ds->addDataPractice($dp);
                }

                // OK
                return array($this->datastudent, $stmt3->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT), $stmt4->fetchAll(PDO::FETCH_ASSOC), $stmt5->fetchAll(PDO::FETCH_ASSOC));
            } catch (PDOException $e) {
                echo "EDataManager:loadAllDataByFilter Error: " . $e->getMessage();
                return false;
            }
        }
    }

    /**
     * Récupère les données nécessaire à la création du tableau des élèves quand une classe est sélectionné dans les filtres
     * @param  $InClassroomId L'identifiant de la classe
     * @param  $InYearId L'identifiant de l'année
     * @return $this->datastudent |boolean
     */
    public function loadAllDataStudentByClassroom($InClassroomId = - 1, $InYearId = - 1) {
        $this->datastudent = array();
        $sql1 = 'SELECT p1.idPerson, p1.PROFESSIONNAL_PRACTICES_ID, p1.SKILL_CODE, p1.DOMAINS_ID, p1.GUIDANCES_CODE, p1.YEARS_CODE, p1.STATES_CODE, p1.MODIFYDATE FROM PROFESSIONNAL_PRACTICE_PERSONS p1 WHERE p1.idPerson IN (SELECT DISTINCT(idPerson) FROM PROFESSIONNAL_PRACTICE_PERSONS WHERE YEARS_CODE = :YeId AND idClass = :ClId) AND p1.YEARS_CODE = (SELECT MAX((p2.YEARS_CODE)) FROM PROFESSIONNAL_PRACTICE_PERSONS p2 WHERE p2.YEARS_CODE <= :YeId AND p2.idPerson = p1.idPerson AND p2.PROFESSIONNAL_PRACTICES_ID =  p1.PROFESSIONNAL_PRACTICES_ID AND p2.SKILL_CODE = p1.SKILL_CODE AND p2.DOMAINS_ID = p1.DOMAINS_ID AND p2.GUIDANCES_CODE = p1.GUIDANCES_CODE) GROUP BY idPerson, PROFESSIONNAL_PRACTICES_ID, SKILL_CODE, DOMAINS_ID, GUIDANCES_CODE ORDER BY p1.idPerson';
        try {
            // Récupère les évaluations de l'apprenti
            $stmt1 = EDatabase::prepare($sql1, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt1->execute(array(':ClId' => $InClassroomId, ':YeId' => $InYearId));

            $oldPeId = 0;

            // Mets lees résultat des requetes sql dans un tableau
            $result1 = $stmt1->fetchAll();
            foreach ($result1 as $row1) {
                // Crée un nouvel apprentis s'il n'est pas encore dans le tableau
                if ($oldPeId != $row1['idPerson']) {
                    // Création des données de l'apprenti avec les données provenant de la base de données
                    $ds = new EDataStudent($row1['idPerson']);
                    array_push($this->datastudent, $ds);
                    $oldPeId = $row1['idPerson'];
                }

                // Création des données de l'aprentis avec les données provenant de la base de données
                $dp = new EDataPractice($row1['GUIDANCES_CODE'], $row1['DOMAINS_ID'], $row1['SKILL_CODE'], $row1['PROFESSIONNAL_PRACTICES_ID'], $row1['idPerson'], $row1['STATES_CODE'], $row1['YEARS_CODE'], $row1['MODIFYDATE']);

                $ds->addDataPractice($dp);
            }

            // Retourner sous forme de tableau pour convenir à la fonction /js/colorSystem
            return array($this->datastudent);
        } catch (PDOException $e) {
            echo "EDataManager:loadAllDataByStudent Error: " . $e->getMessage();
            return false;
        }
    }

    /**
     * Ajoute une nouvelle évaluation dans la base de données
     * @param $InGuidanceId L'identifiant de l'orientation
     * @param $InDomainId L'identifiant du domaine
     * @param $InSkillId L'identifiant de la compétence
     * @param $InPracticeId L'identifiant de la pratique professionnel
     * @param $InStudentId L'identifiant de l'apprenti
     * @param $InStateId L'identifiant de l'état
     * @return true OK | false si une erreur se produit
     */
    public function addDataStudent($InGuidanceId, $InDomainId, $InSkillId, $InPracticeId, $InStudentId, $InStateId) {
        $sql1 = 'INSERT INTO PROFESSIONNAL_PRACTICE_PERSONS(GUIDANCES_CODE, DOMAINS_ID, SKILL_CODE, PROFESSIONNAL_PRACTICES_ID, idPerson, STATES_CODE, YEARS_CODE, MODIFYDATE, idClass, DEGREES_ID) VALUES (:GuId, :DoId, :SkId, :PrId, :SuId, :StId, (SELECT CODE FROM YEARS ORDER BY LABEL DESC LIMIT 1), (SELECT NOW()), (SELECT idClass FROM Person_Belongs_Class WHERE idPerson = :SuId), (SELECT DEGREES_ID FROM Person_Belongs_Class WHERE idPerson = :SuId))';

        // Faire select pour voir si existe  
        try {
            $arrExec = array(':GuId' => $InGuidanceId, ':DoId' => $InDomainId, ':SkId' => $InSkillId, ':PrId' => $InPracticeId, ':SuId' => $InStudentId, ':StId' => $InStateId);

            $stmt1 = EDatabase::prepare($sql1, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt1->execute($arrExec);

            return true;
        } catch (PDOException $e) {
            echo "EDataManager:addDataStudent Error: " . $e->getMessage();
            return false;
        }
    }

    /**
     * Récupère l'id de l'étudiant
     * @param $InGuidanceId L'identifiant de l'orientation
     * @param $InDomainId L'identifiant du domaine
     * @param $InSkillId L'identifiant de la compétence
     * @param $InPracticeId L'identifiant de la pratique professionnel
     * @param $InStudentId L'identifiant de l'apprenti
     */
    function getIdDataStudent($InGuidanceId, $InDomainId, $InSkillId, $InPracticeId, $InStudentId) {
        $sql1 = 'SELECT idPerson, GUIDANCES_CODE, DOMAINS_ID, SKILL_CODE, PROFESSIONNAL_PRACTICES_ID FROM PROFESSIONNAL_PRACTICE_PERSONS WHERE idPerson = :SuId AND GUIDANCES_CODE = :GuId AND DOMAINS_ID = :DoId AND  SKILL_CODE = :SkId AND PROFESSIONNAL_PRACTICES_ID = :PrId AND YEARS_CODE = (SELECT CODE FROM YEARS ORDER BY LABEL DESC LIMIT 1)';

        try {
            $arrExec = array(':GuId' => $InGuidanceId, ':DoId' => $InDomainId, ':SkId' => $InSkillId, ':PrId' => $InPracticeId, ':SuId' => $InStudentId);

            $stmt1 = EDatabase::prepare($sql1, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt1->execute($arrExec);

            return $stmt1->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            echo "EDataManager:getIdDataStudent Error: " . $e->getMessage();
            return false;
        }
    }

    /**
     * Récupère l'id de l'atelier
     * @param $InGuidanceId L'identifiant de l'orientation
     * @param $InDomainId L'identifiant du domaine
     * @param $InSkillId L'identifiant de la compétence
     * @param $InPracticeId L'identifiant de la pratique professionnel
     * @param $InStudentId L'identifiant de l'apprenti
     */
    function getIdDataWorkshop($InGuidanceId, $InDomainId, $InSkillId, $InPracticeId, $InStudentId) {
        $sql1 = 'SELECT WORKSHOPS_ID FROM PROFESSIONNAL_PRACTICE_WORKSHOPS WHERE idPerson = :SuId AND GUIDANCES_CODE = :GuId AND DOMAINS_ID = :DoId AND  SKILL_CODE = :SkId AND PROFESSIONNAL_PRACTICES_ID = :PrId AND YEARS_CODE = (SELECT CODE FROM YEARS ORDER BY LABEL DESC LIMIT 1)';

        try {
            $arrExec = array(':GuId' => $InGuidanceId, ':DoId' => $InDomainId, ':SkId' => $InSkillId, ':PrId' => $InPracticeId, ':SuId' => $InStudentId);

            $stmt1 = EDatabase::prepare($sql1, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt1->execute($arrExec);

            $result = $stmt1->fetchAll();

            $arrWorkshop = array();

            foreach ($result as $workshopid) {
                array_push($arrWorkshop, $workshopid['WORKSHOPS_ID']);
            }

            return $arrWorkshop;
        } catch (PDOException $e) {
            echo "EDataManager:getIdDataStudent Error: " . $e->getMessage();
            return false;
        }
    }

    /**
     * Ajoute des atelier de l'évaluation dans la base de données
     * @param $InGuidanceId L'identifiant de l'orientation
     * @param $InDomainId L'identifiant du domaine
     * @param $InSkillId L'identifiant de la compétence
     * @param $InPracticeId L'identifiant de la pratique professionnel
     * @param $InStudentId L'identifiant de l'apprenti
     * @param $InStateId L'identifiant de l'état
     * @param $InWorkshopId L'identifiant de l'atelier
     * @return true OK | false si une erreur se produit
     */
    public function addDataWorkshop($InGuidanceId, $InDomainId, $InSkillId, $InPracticeId, $InStudentId, $InWorkshopId) {


        $sql1 = 'INSERT INTO PROFESSIONNAL_PRACTICE_WORKSHOPS(GUIDANCES_CODE, DOMAINS_ID, SKILL_CODE, PROFESSIONNAL_PRACTICES_ID, idPerson, WORKSHOPS_ID, YEARS_CODE) VALUES  (:GuId, :DoId, :SkId, :PrId, :SuId, :WoId, (SELECT CODE FROM YEARS ORDER BY LABEL DESC LIMIT 1))';

        $sqlCheckIfWorkshopExist = 'SELECT * FROM WORKSHOPS WHERE ID = :id';
        $sqlGetCurrent = 'SELECT * FROM CURRENT_WORKSHOPS WHERE ID = :id';
        $sqlInsertWorkshop = 'INSERT INTO WORKSHOPS(ID, NAME, DEGREES_ID) VALUES(:id, :name, :degreesId)';

        try {
            $checkIfWorkshopExist = EDatabase::prepare($sqlCheckIfWorkshopExist, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $checkIfWorkshopExist->execute(array(':id' => $InWorkshopId));

            if ($checkIfWorkshopExist->rowCount() == 0)
            {
                $getCurrent = EDatabase::prepare($sqlGetCurrent, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
                $getCurrent->execute(array(':id' => $InWorkshopId));
                $getCurrent = $getCurrent->fetchAll(PDO::FETCH_ASSOC)[0];

                $addWorkshop = EDatabase::prepare($sqlInsertWorkshop, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
                $addWorkshop->execute(array(':id' => $getCurrent['ID'], ':name' => $getCurrent['NAME'], ':degreesId' => $getCurrent['DEGREES_ID']));
            }

            $arrExec = array(':GuId' => $InGuidanceId, ':DoId' => $InDomainId, ':SkId' => $InSkillId, ':PrId' => $InPracticeId, ':SuId' => $InStudentId, ':WoId' => $InWorkshopId);

            $stmt1 = EDatabase::prepare($sql1, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt1->execute($arrExec);

            return true;
        } catch (PDOException $e) {
            echo "EDataManager:addDataWorkshop Error: " . $e->getMessage();
            return false;
        }
    }

    /**
     * Ajoute d'un atelier dans la table current_workshop de la base de données
     * @param $id l'id de l'atelier
     * @param $name le nom de l'atelier
     * @param $desc la description de l'atelier
     * @param $degre le degré de l'atelier
     * @return boolean
     */
    public function addNewWorkshop($name, $desc, $degre) {
        $sql1 = 'INSERT INTO CURRENT_WORKSHOPS(NAME, DESCRIPTION, DEGREES_ID) VALUES (:name, :desc, :degre)';
        $sql4 = 'INSERT INTO WORKSHOPS(ID, NAME, DEGREES_ID) VALUES (:id, :name, :degre)';
        $sql2 = 'INSERT INTO PROFESSIONNAL_PRACTICE_WORKSHOPS(idPerson, PROFESSIONNAL_PRACTICES_ID, SKILL_CODE, DOMAINS_ID, GUIDANCES_CODE, YEARS_CODE, WORKSHOPS_ID) VALUES (:idPerson, 1, 1, "A", 1, 18, :wId)';
        $sql3 = 'SELECT idPerson FROM Person WHERE schoolEmail LIKE "contenu@dynamic.ch"';

        try {
            $dynamicContentUser = EDatabase::prepare($sql3, [PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL]);
            $dynamicContentUser->execute();
            $dynamicContentUser = $dynamicContentUser->fetch(PDO::FETCH_ASSOC);
            
            $arrExec = array(':name' => $name, ':desc' => $desc,':degre' => $degre);
            $stmt1 = EDatabase::prepare($sql1, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt1->execute($arrExec);
            
            $tempWorkshopId = EDatabase::lastInsertId();

            $arrExec = array(':id' => $tempWorkshopId,':name' => $name, ':degre' => $degre);
            $stmt3 = EDatabase::prepare($sql4, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt3->execute($arrExec);

            $tempId = $dynamicContentUser['idPerson'];

            $arrExec = array(':idPerson' => $tempId, ':wId' => $tempWorkshopId);
            $stmt2 = EDatabase::prepare($sql2, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt2->execute($arrExec);

            return true;
        } catch (PDOException $e) {
            echo "EDataManager:addNewWorkshop Error: " . $e->getMessage();
            return false;
        }
    }

    /**
     * modification d'un atelier dans la table current_workshop de la base de données
     * @param $id l'id de l'atelier
     * @param $name le nom de l'atelier
     * @param $desc la description de l'atelier
     * @param $degre le degré de l'atelier
     * @return boolean
     */
    public function updateWorkshop($id, $name, $desc, $degre) {
        $sqlCurrentWorkshops = 'UPDATE CURRENT_WORKSHOPS SET NAME = :name , DESCRIPTION = :desc , DEGREES_ID = :degre WHERE ID = :id';
        $sqlWorkshops = 'UPDATE WORKSHOPS SET NAME = :name, DEGREES_ID = :degre WHERE ID = :id';

        try {
            //Update table CURRENT_WORKSHOPS
            $arrExecCurrentWorkshops = array(':id' => $id, ':name' => $name, ':desc' => $desc, ':degre' => $degre);
            $stmt1 = EDatabase::prepare($sqlCurrentWorkshops, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt1->execute($arrExecCurrentWorkshops);

            //Update table WORKSHOPS
            $arrExecWorkshops = array(':id' => $id, ':name' => $name,':degre' => $degre);
            $stmt1 = EDatabase::prepare($sqlWorkshops, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt1->execute($arrExecWorkshops);

            return true;
        } catch (PDOException $e) {
            echo "EDataManager:updateWorkshop Error: " . $e->getMessage();
            return false;
        }
    }

    /**
     * supprime un atelier dansla table current_workshop
     * @param $id l'id de l'atelier
     * @return boolean
     */
    public function deleteWorkshop($id) {
        $sql = 'DELETE FROM CURRENT_WORKSHOPS WHERE ID = :id';
        try {
            $arrExec = array(':id' => $id);
            $stmt1 = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt1->execute($arrExec);

            return true;
        } catch (PDOException $e) {
            echo "EDataManager:updateWorkshop Error: " . $e->getMessage();
            return false;
        }
    }

    /**
     * Mets à jour une évaluation dans la base de données
     * @param $InGuidanceId L'identifiant de l'orientation
     * @param $InDomainId L'identifiant du domaine
     * @param $InSkillId L'identifiant de la compétence
     * @param $InPracticeId L'identifiant de la pratique professionnel
     * @param $InStudentId L'identifiant de l'apprenti
     * @param $InStateId L'identifiant de l'état
     * @return true OK | false si une erreur se produit
     */
    public function updateDataStudent($InGuidanceId, $InDomainId, $InSkillId, $InPracticeId, $InStudentId, $InStateId) {
        $sql1 = 'UPDATE PROFESSIONNAL_PRACTICE_PERSONS SET STATES_CODE = :StId, MODIFYDATE = (SELECT NOW()) WHERE GUIDANCES_CODE = :GuId AND DOMAINS_ID = :DoId AND SKILL_CODE = :SkId AND PROFESSIONNAL_PRACTICES_ID = :PrId AND idPerson = :SuId AND YEARS_CODE = (SELECT CODE FROM YEARS ORDER BY LABEL DESC LIMIT 1)';
        $sql2 = 'SELECT STATES_CODE FROM PROFESSIONNAL_PRACTICE_PERSONS WHERE idPerson = :IdPe AND PROFESSIONNAL_PRACTICES_ID = :PrId AND SKILL_CODE = :SkCd AND DOMAINS_ID = :DmId AND GUIDANCES_CODE = :GdId';

        try {
            $arrExec = [
                ':IdPe' => $InStudentId,
                ':PrId' => $InPracticeId,
                ':SkCd' => $InSkillId,
                ':DmId' => $InDomainId,
                ':GdId' => $InGuidanceId
            ];

            $stmt2 = EDatabase::prepare($sql2, [PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL]);
            $stmt2->execute($arrExec);
            $current = $stmt2->fetch(PDO::FETCH_ASSOC);

            if ($current['STATES_CODE'] != $InStateId || $current['STATES_CODE'] != 1) {
                $arrExec = array(':GuId' => $InGuidanceId, ':DoId' => $InDomainId, ':SkId' => $InSkillId, ':PrId' => $InPracticeId, ':SuId' => $InStudentId, ':StId' => $InStateId);

                $stmt1 = EDatabase::prepare($sql1, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
                $stmt1->execute($arrExec);
            }

            return true;
        } catch (PDOException $e) {
            echo "EDataManager:updateDataStudent Error: " . $e->getMessage();
            return false;
        }
    }

    /**
     * Supprime une évaluation et les données ateliers associé à celui-ci
     * @param $InGuidanceId L'identifiant de l'orientation
     * @param $InDomainId L'identifiant du domaine
     * @param $InSkillId L'identifiant de la compétence
     * @param $InPracticeId L'identifiant de la pratique professionnel
     * @param $InStudentId L'identifiant de l'apprenti
     * @return true OK | false si une erreur se produit
     */
    public function deleteDataStudent($InGuidanceId, $InDomainId, $InSkillId, $InPracticeId, $InStudentId) {
        $sql1 = 'DELETE ps.*, pw.* FROM PROFESSIONNAL_PRACTICE_WORKSHOPS pw INNER JOIN PROFESSIONNAL_PRACTICE_PERSONS ps ON ps.GUIDANCES_CODE = pw.GUIDANCES_CODE AND ps.DOMAINS_ID = pw.DOMAINS_ID AND ps.SKILL_CODE = pw.SKILL_CODE AND ps.PROFESSIONNAL_PRACTICES_ID = pw.PROFESSIONNAL_PRACTICES_ID AND ps.idPerson = pw.idPerson WHERE ps.GUIDANCES_CODE = :GuId AND ps.DOMAINS_ID = :DoId AND ps.SKILL_CODE = :SkId AND ps.PROFESSIONNAL_PRACTICES_ID = :PrId AND ps.idPerson = :StId';

        try {
            $arrExec = array(':GuId' => $InGuidanceId, ':DoId' => $InDomainId, ':SkId' => $InSkillId, ':PrId' => $InPracticeId, ':SuId' => $InStudentId);

            $stmt1 = EDatabase::prepare($sql1, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt1->execute($arrExec);

            return true;
        } catch (PDOException $e) {
            echo "EDataManager:deleteDataStudent Error: " . $e->getMessage();
            return false;
        }
    }

    /**
     * Supprime une donnée atelier d'un apprenti  
     * @param $InGuidanceId L'identifiant de l'orientation
     * @param $InDomainId L'identifiant du domaine
     * @param $InSkillId L'identifiant de la compétence
     * @param $InPracticeId L'identifiant de la pratique professionnel
     * @param $InStudentId L'identifiant de l'apprenti
     * @param $InWorkshopId L'identifiant de l'ateliers
     * @return true OK | false si une erreur se produit
     */
    public function deleteDataWorkshop($InGuidanceId, $InDomainId, $InSkillId, $InPracticeId, $InStudentId, $InWorkshopId) {
        $sql1 = 'DELETE pw.* FROM PROFESSIONNAL_PRACTICE_WORKSHOPS pw WHERE pw.GUIDANCES_CODE = :GuId AND pw.DOMAINS_ID = :DoId AND pw.SKILL_CODE = :SkId AND pw.PROFESSIONNAL_PRACTICES_ID = :PrId AND pw.idPerson = :SuId AND pw.WORKSHOPS_ID = :WoId AND YEARS_CODE = (SELECT CODE FROM YEARS ORDER BY LABEL DESC LIMIT 1)';

        try {
            $arrExec = array(':GuId' => $InGuidanceId, ':DoId' => $InDomainId, ':SkId' => $InSkillId, ':PrId' => $InPracticeId, ':SuId' => $InStudentId, ':WoId' => $InWorkshopId);

            $stmt1 = EDatabase::prepare($sql1, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
            $stmt1->execute($arrExec);

            return true;
        } catch (PDOException $e) {
            echo "EDataManager:deleteDataWorkShop Error: " . $e->getMessage();
            return false;
        }
    }

    /** Contient le tableau des EDataStudent */
    private $datastudent;

}
