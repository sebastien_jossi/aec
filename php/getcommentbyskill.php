<?php

/**
 * @copyright ronaldo.lrrpnt@eduge.ch 2016-2017
 * @brief Récupère les comentaire d'une compétence spécifique
 */

require_once './inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Je récupère l'id de l'orientation, du domaine, du skill
$guidanceid = -1;
$domainid = -1;
$skillid = -1;

if (isset($_POST['guidanceId']) && isset($_POST['domainId']) && isset($_POST['skillId']))
{
	$guidanceid = $_POST['guidanceId'];
	$domainid = $_POST['domainId'];
	$skillid = $_POST['skillId'];
}

	if ($guidanceid > 0 && $domainid != -1 && $skillid > 0){
		$comment = ECommentManager::getInstance()->loadCommentBySkill($guidanceid, $domainid, $skillid);
		if ($comment === false){
			echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de loadCommentBySkill()"}';
			exit();
		}

		$jsn = json_encode($comment);
		if ($jsn == FALSE){
			$code = json_last_error();
			echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json ('.$code.'"}';
			exit();
		}
		echo '{ "ReturnCode": 0, "Data": '.utf8_encode($jsn).'}';
		exit();

	}

	// Si j'arrive ici, c'est pas bon
	echo '{ "ReturnCode": 1, "Message": "Il manque les paramètres guidanceId et/ou domainId et/ou skillId"}';