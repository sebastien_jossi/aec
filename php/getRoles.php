<?php
/**
 * @brief Récupère les roles
 * @author 	quentin.fslr@eduge.ch
 */
require_once './inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$sql = 'SELECT codeRole,Name FROM `Role` ORDER BY `name` ASC';

$roles = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
$roles->execute(array());

$roles = $roles->fetchAll(PDO::FETCH_ASSOC);

$jsn = json_encode($roles);
echo '{ "ReturnCode": 0, "Data": '.utf8_encode($jsn).'}';
exit();