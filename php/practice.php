<?php

/**
 * @copyright ronaldo.lrrpnt@eduge.ch 2016-2017
 */

/**
 * Classe des pratiques professionnelles
 * @author RLP
 */
 class EPractice implements JsonSerializable
 {
 	
 	/**
 	 * 
 	 * @param $InSkillId L'identifiant de la compétence
 	 * @param $InPracticceId L'identifiant de la pratique professionnelle
 	 * @param $InPracticeDescription La description de la pratique professionnelle
 	 * @param $InPracticeYear L'année de création de la pratique professionnelle 
 	 */
 	public function __construct($InSkillId = - 1, $InPracticeId = - 1, $InPracticeDescription = "", $InPracticeYear = "")
 	{
 		$this->owner = $InSkillId;
 		$this->practiceid = $InPracticeId;
 		$this->practicedescription = $InPracticeDescription;
 		$this->practiceyear = $InPracticeYear;
 	}
 	
 	/**
 	 * @brief Getter
 	 *
 	 * @return L'identifiant unique
 	 */
 	public function getId()
 	{
 		return $this->practiceid;
 	}
 	
 	/**
 	 * @brief Getter
 	 *
 	 * @return L'identifiant unique
 	 */
 	public function getOwnerId()
 	{
 		return $this->owner;
 	}
 	
 	/**
 	 * @brief Getter
 	 *
 	 * @return L'identifiant unique
 	 */
 	public function getDescription()
 	{
 		return $this->practicedescription;
 	}
 	
 	/**
 	 * @brief Getter
 	 *
 	 * @return L'identifiant unique
 	 */
 	public function getYear()
 	{
 		return $this->practiceyear;
 	}
 	
 	public function modifyDescription($InPracticeDescription)
 	{
 		$this->practicedescription = $InPracticeDescription;
 	}
 	
 	/***
 	 * Transforme l'objet en json
 	 * @return L'objet en json
 	 */
 	public function jsonSerialize() {
 	
 		/*return [
 		 'id' => $this->domainid,
 		 'description' => $this->domaindescription,
 		 'year' => $this->domainyear,
 			];*/
 	
 		return get_object_vars($this);
 	}
 	
 	/** L'identifiant de la pratique professionnelle */
 	private $practiceid;
 	
 	/** L'identifiant de la compétence */
 	private $owner;
 	
 	/** La description de la pratique professionnelle */
 	private $practicedescription;
 	
 	/** L'année de création de la pratique professionnelle  */
 	private $practiceyear;
 }