<?php
/**
 * Tache auto pour supr les pdf du dossier "pdf-downloads"
 */
$files = glob('../pdf-downloads/*'); //Dossier a clear
foreach($files as $file){
    $lastModifiedTime = filemtime($file);
    $currentTime = time();
    $timeDiff = abs($currentTime - $lastModifiedTime)/(60*60); // Nombre d'heure depuis la modification
    if(is_file($file) && $timeDiff > 1){ 
        //modifié il y a plus de 1 heure
        unlink($file); //delete file
    }
}
?>
