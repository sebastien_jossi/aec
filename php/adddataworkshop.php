<?php
require_once './inc.all.php';
/**
 * @brief ajoute un atelier dans la base de données
 * @param $studentId L'identifiant de l'apprenti /!\ OBLIGATOIRE
 * @param $guidanceid  L'identifiant de l'orientation
 * @param $domainid  L'identifiant du domaine
 * @param $skillid   L'identifiant de la compétence
 * @param $practiceid L'identifiant de la pratique
 * @param $arrWorkshop Le tableau contenat les ateliers 
 */
// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Je récupère l'id de guidance

$guidanceid = - 1;
$domainid = - 1;
$skillid = - 1;
$practiceid = - 1;
$studentid = - 1;
$arrWorkshop = - 1;
if (isset($_POST['guidanceid'])) $guidanceid = $_POST['guidanceid'];

if (isset($_POST['domainid'])) $domainid = $_POST['domainid'];

if (isset($_POST['skillid'])) $skillid = $_POST['skillid'];

if (isset($_POST['practiceid'])) $practiceid = $_POST['practiceid'];

if (isset($_POST['studentid'])) $studentid = $_POST['studentid'];

if (isset($_POST['arrWorkshop'])) $arrWorkshop = $_POST['arrWorkshop'];


if ($domainid !== -1 && $skillid !== -1 && $practiceid !== -1 && $stateid !== -1)
{
	$result = EDataManager::getInstance()->getIdDataStudent($guidanceid, $domainid, $skillid, $practiceid, $studentid, $stateid);
	if (count($result) >= 0)
	{
		if (!EDataManager::getInstance()->addDataWorkshop($guidanceid, $domainid, $skillid, $practiceid, $studentid, $arrWorkshop))
		{
			echo '{ "ReturnCode": 2, "Message": "Un problème de la fonction addDataWorkshop()"}';
			exit();
		}
	}
	else
	{
		if (!EDataManager::getInstance()->deleteDataWorkshop($guidanceid, $domainid, $skillid, $practiceid, $studentid, $arrWorkshop))
		{
			echo '{ "ReturnCode": 2, "Message": "Un problème de la fonction deleteDataWorkshop()"}';
			exit();
		}
	}

}

// Si j'arrive ici, c'est TOUT bon
echo '{ "ReturnCode": 0, "Message": ""}';

