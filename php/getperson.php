<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
/**
 * @copyright romain.ssr@eduge.ch
 * @author RS
 * @brief objet permettant de récupérer les informations relatives à une personne dans la base de donnée
 * @param $email contient l'email de l'élève
 * @param $person contient toutes les informations de l'élève
 *
 */
 
require_once './inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$email = "";
$firstName = "";
$lastName = "";
$idRole = -1;
$active = 0;

if (isset($_POST['email']) && isset($_POST['firstName']) && isset($_POST['lastName']) && isset($_POST['idRole']))
{
    $email = $_POST['email'];
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];
		$idRole = $_POST['idRole'];
    $userInfos = [
        'email' => $email,
        'firstName' => $firstName,
        'lastName' => $lastName,
        'idRole' => $idRole,
        'active' => $active
    ];
}
$_SESSION['email'] = $email;
	
if (strlen($_SESSION['email']) == 0){
	echo '{ "ReturnCode": 1, "Message": "Il faut impérativement un email pour rechercher un utilisateur."}';
	exit();
}

// Je récupère l'utilisateur par son email
$person = EUserManager::getInstance()->findUserByEmail($_SESSION['email']);

//Verifie si l'utilisateur existe
if($person == false){

	//Vérifier l'utilisateur peut créer un compte
	if(isset($_SESSION["CompteCreer"]) && $_SESSION["CompteCreer"]==true){
		//--> bloque
		echo json_encode([
			'ReturnCode' => 4,
			'Message' => 'Vous avez déjà créez un compte !!!'
		]);
		exit();
	}
	else{
		//Création du compte
		EUserManager::getInstance()->addUser($userInfos);
		$_SESSION["CompteCreer"] = true; 
	}
	
}
$_SESSION['person'] = serialize($person);

$actif = $person->isActive();

if ($actif == 0 || $actif == false) {
	echo '{ "ReturnCode": 4, "Message": "Votre compte n\'a pas été validé."}';
	exit();
}
$role = $person->getRole();
$_SESSION['role'] = $role;




switch ($_SESSION['role']) {
	case 1:
		$guidance = $person->getGuidance();
		$_SESSION['guidance'] = $guidance;
		$_SESSION['degree'] = $person->getDegree();
		$id = $person->getIdPerson();
		$_SESSION['id'] = $id;
			
		$page = "./html/VueEleve/AccueilApprenti.php";
	break;
	case 2:
		$page = "./html/VueDirection/AccueilDirection.php";
	break;
	case 3:
		$page = "./html/VueDirection/AccueilDirection.php";
	break;
}

echo '{ "ReturnCode": 0, "Message": "'. $page . '"}';


