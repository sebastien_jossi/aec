<?php

/**
 * @copyright ronaldo.lrrpnt@eduge.ch 2016-2017
 * @brief Récupère les pratiques d'une compétence
 * @param $guidanceid l'identifiant de l'orientation
 * @param $domainid l'identifiant du domaine
 * @param $skillid l'identifiant de la compétence
 */
require_once './inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Je récupère l'id de guidance, l'id du domain et l'id de skill
$guidanceid = -1;
$domainid = -1;
$skillid = -1;

if (isset($_POST['guidanceId']) && isset($_POST['domainId']) && isset($_POST['skillId'])){
	$guidanceid = $_POST['guidanceId'];
	$domainid = $_POST['domainId'];
	$skillid = $_POST['skillId'];
}
	if ($guidanceid > 0 && $domainid !== -1 && $skillid > 0 ){
		$practices = EAppManager::getInstance()->loadPracticesBySkillDomainGuidance($guidanceid,$domainid,$skillid);
		if ($practices === false){
			echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de loadPracticesBySkillDomainGuidance()"}';
			exit();
		}
		if (empty($practices)){
			echo '{ "ReturnCode": 7, "Data": ' . json_encode(['empty' => true]) . '}';
			exit();
		}
		$jsn = json_encode($practices);
		if ($jsn == FALSE){
			$code = json_last_error();
			echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json ('.$code.'"}';
			exit();
		}
		// Si j'arrive ici, ouf... c'est tout bon
		echo '{ "ReturnCode": 0, "Data": '.utf8_encode($jsn).'}';
		exit();
	
	}
	else{
		echo '{ "ReturnCode": 4, "Message": "paramètres invalide --> no data"}';
		exit();
	}

	// Si j'arrive ici, c'est pas bon
	echo '{ "ReturnCode": 1, "Message": "Il manque les paramètres guidanceId, domainId et skillId"}';
	?>