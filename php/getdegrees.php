<?php

/**
 * @copyright ronaldo.lrrpnt@eduge.ch 2016-2017
 * @brief Récupère touts les degrées scolaires
 */

require_once './inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Je récupère toutes les orientations
$degrees = EUtilitiesManager::getInstance()->loadAllDegree();
if ($degrees === false){
	echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de loadAllDegree()"}';
	exit();
}

$jsn = json_encode($degrees);
if ($jsn == FALSE){
	$code = json_last_error();
	echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json ('.$code.'"}';
	exit();
}
echo '{ "ReturnCode": 0, "Data": '.utf8_encode($jsn).'}';

?>