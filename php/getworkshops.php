<?php
/**
 * @copyright nous mais pas vous
 * @author EE
 * @brief objet permettant de récupérer les informations relatives à un atelier dans la base de données
 */
if (session_status() == PHP_SESSION_NONE) {     session_start(); }

require_once './inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$workshops = -1;
if (isset($_POST['workshops'])) {
    $workshops = $_POST['workshops'];
  
    $sql = 'SELECT DISTINCT NAME, ID FROM `PROFESSIONNAL_PRACTICE_WORKSHOPS` join WORKSHOPS on WORKSHOPS_ID=ID  WHERE ID IN ('.implode(",", $workshops).')';

    $workshop = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    $workshop->execute(array());

    $workshop = $workshop->fetchAll(PDO::FETCH_ASSOC);
 
    echo json_encode([
        'ReturnCode' => 0,
        'Data' => [
            'names' => $workshop,
        ]
    ]);
} else {

    // Vérifications des variables nécessaires pour récupérer les informations d'un atelier
    // par rapport à une pratique professionnelle.
    $domain = "";
    if (isset($_POST['domainId']))
    {
        $domain = $_POST['domainId'];
    }

    $practiceId = "";
    if (isset($_POST['practiceId']))
    {
        $practiceId = $_POST['practiceId'];
    }

    $skillCode = "";
    if (isset($_POST['skillCode']))
    {
        $skillCode = $_POST['skillCode'];
    }

    // Récupération en base
    $sql = 'SELECT DISTINCT NAME, ID FROM `PROFESSIONNAL_PRACTICE_WORKSHOPS` join WORKSHOPS on WORKSHOPS_ID=ID  WHERE `DOMAINS_ID`= :domainId and `PROFESSIONNAL_PRACTICES_ID`= :practiceId AND SKILL_CODE= :skillCode AND idPerson = :studentId';

    $workshop = EDatabase::prepare($sql, array(PDO::ATTR_CURSOR, PDO::CURSOR_SCROLL));
    $workshop->execute(array(
        ':domainId' => $domain,
        ':practiceId' => $practiceId,
        ':skillCode' => $skillCode,
        ':studentId' => $_SESSION['id']
    ));
    $entries = $workshop->rowCount();
    $workshop = $workshop->fetchAll(PDO::FETCH_ASSOC);

    // Vérifie si la requete sql nous renvoye un tableau de valeur ou rien
    if (isset($workshop[0])) {
        echo json_encode([
            'ReturnCode' => 0,
            'Data' => [
                'workshops' => $workshop,
                'entry' => $entries
            ]
        ]);
    }
    else {
        echo json_encode([
            'ReturnCode' => 0,
            'Data' => [
                'workshops' => '',
                'entry' => ''
            ]
        ]);
    }
}
