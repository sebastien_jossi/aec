<?php

/**
 * @copyright floran.stck@eduge.ch
 */

/**
 * Classe d'exemple d'un utilisateur
 * @author FS
 *
 */
class ECurrentWorkshop implements JsonSerializable {

    /**
     * 
     * @param $InWorkshopId l'identifiant de l'atelier
     * @param $InWorkshopName le nom de l'atelier
     * @param $InOwnerId l'identifiant du degré dans lequel se trouve l'atelier
     */
    public function __construct($InWorkshopId = -1, $InWorkshopName = "", $InOwnerId = -1, $InWorkshopDesc = "") {

        $this->workshopid = $InWorkshopId;
        $this->workshopname = $InWorkshopName;
        $this->owner = $InOwnerId;
        $this->workshopdescription = $InWorkshopDesc;
    }

    /**
     * @brief	On ne laisse pas cloner un user
     */
    private function __clone() {
        
    }

    public function getOwnerId() {
        return $this->owner;
    }

    public function getId() {
        return $this->workshopid;
    }

    public function getName() {
        return $this->workshopname;
    }

    public function getDescription() {
        return $this->workshopdescription;
    }

    /*     * *
     * Transforme l'objet en json
     * @return L'objet en json
     */

    public function jsonSerialize() {
        return get_object_vars($this);
    }

    private $workshopid;
    private $workshopname;
    private $owner;
    private $workshopdescription;

}
