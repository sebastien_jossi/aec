<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
require_once './inc.all.php';

/**
 * @brief Ajoute l'évaluation de l'élève
 * @param $studentId L'identifiant de l'apprenti /!\ OBLIGATOIRE
 * @param $guidanceid L'identifiant de l'orientation
 */

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

$guidanceid = -1;
if (isset($_POST['guidanceid']))
    $guidanceid = $_POST['guidanceid'];

$studentid = -1;
if (isset($_POST['studentid']))
    $studentid = $_POST['studentid'];

// Je récupère l'id de guidance
$answers = array();

if (isset($_POST['answers']))
    $answers = $_POST['answers'];

if ($guidanceid != -1 && $studentid != -1) {
    foreach ($answers as $ans) {
        $idExplode = explode("-", $ans['id']);

        if (count($idExplode) >= 3) {
            $domainid = $idExplode[0];
            $skillid = intval($idExplode[1]);
            $practiceid = intval($idExplode[2]);
            $stateid = intval($ans['val']);

            if ($domainid != -1 && $skillid != -1 && $practiceid != -1 && $stateid != -1) {
                $resultStudent = EDataManager::getInstance()->getIdDataStudent($guidanceid, $domainid, $skillid, $practiceid, $studentid, $stateid);
                if (count($resultStudent) <= 0) {
                    if (!EDataManager::getInstance()->addDataStudent($guidanceid, $domainid, $skillid, $practiceid, $studentid, $stateid)) {
                        echo '{ "ReturnCode": 2, "Data": "Un problème de la fonction addDataStudent()"}';
                        exit();
                    }
                } else {
                    if (!EDataManager::getInstance()->updateDataStudent($guidanceid, $domainid, $skillid, $practiceid, $studentid, $stateid)) {
                        echo '{ "ReturnCode": 2, "Data": "Un problème de la fonction updateDataStudent()"}';
                        exit();
                    }
                }
            }


            $resultWorkshop = EDataManager::getInstance()->getIdDataWorkshop($guidanceid, $domainid, $skillid, $practiceid, $studentid);

            if ($ans['workshop'] != "") {
                // Cherche si dans le tableau de la base, il y a l'atelier. Si non, il est ajouté
                foreach ($ans['workshop'] as $workshopid) {
                    if (!in_array($workshopid, $resultWorkshop)) {
                        if (!EDataManager::getInstance()->addDataWorkshop($guidanceid, $domainid, $skillid, $practiceid, $studentid, $workshopid)) {
                            echo '{ "ReturnCode": 2, "Data": "Un problème de la fonction addDataWorkshop()"}';
                            exit();
                        }
                    }

                }

            }
          
            // Cherche si dans le tableau reçu, il y a l'atelier. Si non, il est supprimé
            foreach ($resultWorkshop as $workshopid) {
                if (!in_array($workshopid, $ans['workshop'] == "" ? [] : $ans['workshop'])) {
                    if (!EDataManager::getInstance()->deleteDataWorkshop($guidanceid, $domainid, $skillid, $practiceid, $studentid, $workshopid)) {
                        echo '{ "ReturnCode": 2, "Data": "Un problème de la fonction deleteDataWorkshop()"}';
                        exit();
                    }
                }
               
            }
            
            
           
        }
    }
}

// Si j'arrive ici, c'est TOUT bon
$_SESSION["message"] = "Votre évaluation a été enregistrée";
echo '{ "ReturnCode": 0, "Data": "Votre évaluation a été enregistrée"}';

