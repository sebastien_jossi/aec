<?php
/**
 * @brief	Permet d'envoyer les variables à appManager
 * 			pour modifier une compétence dans la base de données
 * @author 	romain.ssr@eduge.ch
 */

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

require_once './inc.all.php';

$guidanceid = -1;
$domainid ="";
$skillid = -1;
$skillname ="";
$skilldefinition="";
$skillmethodologic="";
$skillsocial="";
$skillpersonnal="";

if (isset($_POST['guidanceid'])&&isset($_POST['domainid'])&&isset($_POST['skillid'])&&isset($_POST['skillname'])&&isset($_POST['skilldefinition'])&&isset($_POST['skillmethodologic'])&&isset($_POST['skillsocial'])&&isset($_POST['skillpersonnal'])){
	$guidanceid = $_POST['guidanceid'];
	$domainid =$_POST['domainid'];
	$skillid = $_POST['skillid'];
	$skillname = filter_var($_POST['skillname'], FILTER_SANITIZE_STRING);
	$skilldefinition=  filter_var($_POST['skilldefinition'], FILTER_SANITIZE_STRING);
	$skillmethodologic=$_POST['skillmethodologic'];
	$skillsocial=$_POST['skillsocial'];
	$skillpersonnal=$_POST['skillpersonnal'];
}
else {
	echo '{ "ReturnCode": 1, "Message": "Erreur dans les paramètres. Valeurs manquantes."}';
	exit();
}


// J'envoie les informations nécessaires pour ajouter une compétence dans la base de donée.
if (!EAppManager::getInstance()->updateSkill($guidanceid,$domainid,$skillid,$skillname,$skilldefinition,$skillmethodologic,$skillsocial,$skillpersonnal)){
	echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de loadAllGuidance()"}';
	exit();
}

echo '{ "ReturnCode": 0}';
?>