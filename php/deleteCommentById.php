<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
require_once './inc.all.php';
/**
 * @brief ajoute un commentaire dans la base de données
 * @param $commentId L'id du commentaire à supprimer /!\ OBLIGATOIRE
 *
 */
// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Je récupère l'id de guidance

$commentId = - 1;

if (isset($_POST['commentId']))  {
    $commentId = filter_input(INPUT_POST, 'commentId', FILTER_SANITIZE_STRING);

    if ($commentId !== -1)
    {
        $isDeleted = ECommentManager::getInstance()->delCommentById($commentId);
       // var_dump($isDeleted, $commentId);
        //die();

        if (!$isDeleted)
        {
            echo '{ "ReturnCode": 5, "Data": "Le commentaire n\'appartient pas à l\'utilisateur"}';
            die();
        } else {
            echo '{ "ReturnCode": 0, "Data": "Votre commentaire a été supprimé"}';
            die();
        }
    }    
}
 
// Si j'arrive ici, c'est pas bon
echo '{ "ReturnCode": 1, "Data": "Il manque un ou plusieurs paramètre(s)"}';
