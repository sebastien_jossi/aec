<?php

/**
 * @copyright ronaldo.lrrpnt@eduge.ch 2016-2017
 */

/**
 * Classe des domaines
 * @author RLP    
 */
class EDomain implements JsonSerializable
{

	/**
	 * @brief Class Constructor
	 * @param $InGuidanceId L'identifiant de l'orientation
     * @param $InDomainId L'identifiant du domaine
     * @param $InDomainDescription La description de l'orientation
     * @param $InDomainYear L'année de création du domaine
	 */
	public function __construct($InGuidanceId = - 1, $InDomainId = - 1, $InDomainDescription = "", $InDomainYear = "")
	{
		$this->owner = $InGuidanceId;
		$this->domainid = $InDomainId;
		$this->domaindescription = $InDomainDescription;
		$this->domainyear = $InDomainYear;
		$this->skills = array();
	}

	/**
	 * @brief On ne laisse pas cloner un domaine
	 */
	private function __clone()
	{
	}

	/** Initialise les skills. Doit être invoqué juste après un appel au constructeur.
	public function loadSkills()
	{
		return null;
	}*/

	/**
	 * @brief Est-ce que cet objet est valide
	 * 
	 * @return True si valide, autrement false
	 */
	public function isValid()
	{
		return ($this->domainid == - 1) || ($this->owner = - 1) ? false : true;
	}

	/**
	 * @brief Getter
	 * 
	 * @return L'identifiant 
	 */
	public function getId()
	{
		return $this->domainid;
	}

	/**
	 * @brief Getter
	 * 
	 * @return La description du domaine
	 */
	public function getDescription()
	{
		return $this->domaindescription;
	}

	/**
	 * @brief Getter
	 * 
	 * @return L'année de création du domaine
	 */
	public function getYear()
	{
		return $this->domainyear;
	}

	/**
	 * @brief Getter
	 * 
	 * @return L'identifiant du parent
	 */
	public function getOwnerId()
	{
		return $this->owner;
	}
	
	/**
	 * @brief Modification de domaine
	 * @param $InDomainDescription La description du domaine
	 */
	public function modifyDescription($InDomainDescription)
	{
		$this->domaindescription = $InDomainDescription;
	}

	/***
	 * Transforme l'objet en json
	 * @return L'objet en json
	 */
	public function jsonSerialize() 
	{	
		return get_object_vars($this);
	}
	
	/**
	 * Getter
	 *
	 * @return Le tableau des ESkill | false si une erreur se produit
	 */
	public function getSkills()
	{
		return $this->skills;
	}
	
	/**
	 * Ajoute un skill en créeant l'objet au tableau des skills
	 * @param $InDomainId L'identifiant du domaine qui lui correspond
	 * @param $InSkillId L'identifiant de la compétence
	 * @param $InSkillName Le nom de la compétence
	 * @param $InSkillDefinition La définition de la compétence
	 * @param $InSkillMethodologic La compétence méthodologique de la compétence
	 * @param $InSkillSocial La compétence social de la compétence
	 * @param $InSkillPersonnal La compétence personnelle de la compétence
	 * @param $InSkillYear L'année de création de la compétence
	 * @return True | false si une erreur se produit
	 */
	public function addSkillAttr ($InDomainId = - 1, $InSkillId = - 1, $InSkillName = "", $InSkillDefinition = "", $InSkillMethodologic = "", $InSkillSocial = "", $InSkillPersonnal = "", $InSkillYear = "")
	{
		try
		{
			array_push($this->skills, new ESkill($this->domainid, $InSkillId, $InSkillName, $InSkillDefinition, $InSkillMethodologic, $InSkillSocial, $InSkillPersonnal, $InSkillYear));
			return true;
		} catch (Exception $e)
		{
			echo "EDomain:addSkill Error: " . $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Ajoute un objet skill au tableau des skills
	 * @param $InSkill L'objet ESkill à ajouter
	 * @return Le tableau des skills | false si une erreur se produit
	 */
	public function addSkill ($InSkill)
	{
		try
		{
			array_push($this->skills, $InSkill);
			return true;
		} catch (Exception $e)
		{
			echo "EDomain:addSkill Error: " . $e->getMessage();
			return false;
		}
	}
	
	/**
	 * Ajoute un tableau de domaine au tableau des domaines
	 * @param $InArrSkills Un tableau contenant des objets ESkills à ajouter
	 * @param $clearAll Efface le contenu du tableau avant d'ajouter si true
	 * @return Le tableau des domains | false si une erreur se produit
	 */
	public function addSkills ($InArrSkills, $clearAll = false)
	{
		try
		{
			if ($clearAll == true)
				$this->skills = $InArrSkills;
				else
					array_push($this->domains, $InArrSkills);
	
					return true;
		} catch (Exception $e)
		{
			echo "EDomain:addSkills Error: " . $e->getMessage();		
			return false;
		}
	}
	
	/**
	 * Modifie un skill du tableau des skills
	 * @param $InSkillId L'identifiant de la compétence à changer
	 * @param $InSkillName Le nom de la compétence
	 * @param $InSkillDefinition La définition de la compétence
	 * @param $InSkillMethodologic La compétence méthodologique de la compétence
	 * @param $InSkillSocial La compétence social de la compétence
	 * @param $InSkillPersonnal La compétence personnelle de la compétence
	 * @return Le tableau des skills | false si une erreur se produit
	 */
	public function modifySkill ($InSkillId, $InSkillName, $InSkillDefinition, $InSkillMethodologic, $InSkillSocial, $InSkillPersonnal)
	{
		try
		{
			foreach ($this->skills as &$s)
			{
				if ($s->getId() === $InSkillId)
				{
					$s->modifySkill($InSkillName, $InSkillDefinition, $InSkillMethodologic, $InSkillSocial, $InSkillPersonnal);
					return true;
				}
			}
		} catch (Exception $e)
		{
			echo "EDomain:modifySkill Error: " . $e->getMessage();
			return false;
		}
	}
	
	
	/**
	 * Supprime un skill du tableau des skills
	 * @param $InSkillId L'identifiant de la compétence
	 * @return Le tableau des domains | false si une erreur se produit
	 */
	public function deleteSkill ($InSkillId)
	{
		try
		{
			foreach ($this->skills as &$s)
			{
				if ($s->getId() === $InSkillId)
				{
					unset($s);
					return true;
				}
			}
		} catch (Exception $e)
		{
			echo "EDomain:deleteSkill Error: " . $e->getMessage();
			return false;
		}
	}
	
	
	/**
	 * @brief L'identifiant unique provenant de la base de données
	 */
	private $domainid;

	/**
	 * @brief La description du domaine
	 */
	private $domaindescription;

	/**
	 * @brief L'année de création du domaine
	 */
	private $domainyear;

	/**
	 * @brief Id du parent (EGuidance)
	 */
	private $owner;

	/**
	 * @brief Le tableau contenant les compétences
	 */
	private $skills;
}

?>