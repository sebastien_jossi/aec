<?php
/**
 * @copyright ronaldo.lrrpnt@eduge.ch 2016-2017
 */

/**
 * Classe d'orientation
 * @author RLP
 */
class EGuidance implements JsonSerializable
{

	/**
	 * @brief Class Constructor
	 * @param InGuidanceId L'identifiant de l'orientation
	 * @param InGuidanceName La description de l'orientation
	 */
	public function __construct ($InGuidanceId = - 1, $InGuidanceName = "")
	{
		$this->guidanceid = $InGuidanceId;
		$this->guidancename = $InGuidanceName;
		$this->domains = array();
	}

	/**
	 * @brief On ne laisse pas cloner une orientation
	 */
	private function __clone ()
	{}

	/**
	 * @brief Est-ce que cet objet est valide
	 *
	 * @return True si valide, autrement false
	 */
	public function isValid ()
	{
		return ($this->guidanceid == - 1) ? false : true;
	}

	/**
	 * @brief Getter
	 *
	 * @return L'identifiant unique
	 */
	public function getId ()
	{
		return $this->guidanceid;
	}

	/**
	 * @brief Getter
	 *
	 * @return Le nom de la l'orientation
	 */
	public function getGuidanceName ()
	{
		return $this->guidancename;
	}

	/**
	 * Getter
	 *
	 * @return Le tableau des EDomain
	 */
	public function getDomains()
	{
		return $this->domains;
	}

	/***
	 * Transforme l'objet en json
	 * @return L'objet en json
	 */
	public function jsonSerialize()
	{
		return get_object_vars($this);
	}

	/**
	 * Ajoute un domaine au tableau des domaines
	 * @param $InGuidanceId L'identifiant de l'orientation
	 * @param $InDomainId L'identifiant du domaine
	 * @param $InDomainDescription La description de l'orientation
	 * @param $InDomainYear L'année de création du domaine
	 * @return True | false si une erreur se produit
	 */
	public function addDomainAttr ($InGuidanceId = - 1, $InDomainId = - 1, $InDomainDescription = "", $InDomainYear = "")
	{
		try
		{
			array_push($this->domains, new EDomain($this->guidanceid, $InDomainId, $InDomainDescription, $InDomainYear));
			return true;
		} catch (Exception $e)
		{
			echo "EGuidance:addDomain Error: " . $e->getMessage();
			return false;
		}
	}

	/**
	 * Ajoute un domaine au tableau des domaines
	 * @param $InDomain L'objet EDomain à ajouter
	 * @return Le tableau des domains | false si une erreur se produit
	 */
	public function addDomain ($InDomain)
	{
		try
		{
			array_push($this->domains, $InDomain);
			return true;
		} catch (Exception $e)
		{
			echo "EGuidance:addDomain Error: " . $e->getMessage();
			return false;
		}
	}

	/**
	 * Ajoute un tableau de domaine au tableau des domaines
	 * @param $InArrDomains Un tableau contenant des objets EDomain à ajouter
	 * @param $clearAll Efface le contenu du tableau avant d'ajouter si true
	 * @return Le tableau des domains | false si une erreur se produit
	 */
	public function addDomains ($InArrDomains, $clearAll = false)
	{
		try
		{
			if ($clearAll == true)
				$this->domains = $InArrDomains;
				else
					array_push($this->domains, $InArrDomains);

					return true;
		} catch (Exception $e)
		{
			echo "EGuidance:addDomains Error: " . $e->getMessage();
			return false;
		}
	}

	/**
	 * Modifie un domaine du tableau des domaines
	 * @param $InDomainId L'identifiant du domaine
	 * @param $InDomainDescription La description de l'orientation
	 * @return true | false si une erreur se produit
	 */
	public function modifyDomain ($InDomainId, $InDomainDescription)
	{
		try
		{
			foreach ($this->domains as &$d)
			{
				if ($d->getId() === $InDomainId)
				{
					$d->modifyDescription($InDomainDescription);
					return true;
				}
			}
		} catch (Exception $e)
		{
			echo "EGuidance:modifyDomain Error: " . $e->getMessage();
			return false;
		}
	}

	/**
	 * Supprime un domaine du tableau des domaines
	 * @param $InDomainId L'identifiant du domaine
	 * @return Le tableau des domains | false si une erreur se produit
	 */
	public function deleteDomain ($InDomainId)
	{
		try
		{
			foreach ($this->domains as &$d)
			{
				if ($d->getId() === $InDomainId)
				{
					unset($d);
					return true;
				}
			}
		} catch (Exception $e)
		{
			echo "EGuidance:deleteDomain Error: " . $e->getMessage();
			return false;
		}

	}

	/**
	 * @brief L'identifiant unique provenant de la base de données
	 */
	private $guidanceid;

	/**
	 * @brief Le nom de l'orientation
	 */
	private $guidancename;

	/**
	 * @brief Le tableau contenant les domaines
	 */
	private $domains;
}
?>