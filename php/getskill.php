<?php

/**
 * @copyright ronaldo.lrrpnt@eduge.ch 2016-2017
 * @brief Récupère une compétence spécifique
 */

require_once './inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Je récupère l'id de guidance
$guidanceid = -1;
$domainid = -1;
$skillid = -1;
if (isset($_POST['guidanceId']) && isset($_POST['domainId']) && isset($_POST['skillId']))
{
	$guidanceid = $_POST['guidanceId'];
	$domainid = $_POST['domainId'];
	$skillid = $_POST['skillId'];
}

	if ($guidanceid > 0 && $domainid !==  -1){
		$skill = EAppManager::getInstance()->loadSpecificSkillByDomainGuidance($guidanceid, $domainid, $skillid);
		if ($skill === false){
			echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de loadSkillByDomainGuidance()"}';
			exit();
		}
		$jsn = json_encode($skill);
		if ($jsn == FALSE){
			$code = json_last_error();
			echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json ('.$code.'"}';
			exit();
		}
		echo '{ "ReturnCode": 0, "Data": '.utf8_encode($jsn).'}';
		exit();

	}

	// Si j'arrive ici, c'est pas bon
	echo '{ "ReturnCode": 1, "Message": "Il manque le paramètre guidanceId ou domainId"}';