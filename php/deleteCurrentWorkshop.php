<?php

require_once './inc.all.php';
/**
 * @brief supprimer un atelier de la base de donnée
 * @param $id L'identifiant de l'atelier
 */
$id = - 1;
// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

if (isset($_POST['idWorkshop']))
    $id = $_POST['idWorkshop'];

if ($id != -1) {
    if (is_numeric($id)) {
        if ($id > 0) {
            if (EDataManager::getInstance()->deleteWorkshop($id)) {
                echo '{ "ReturnCode": 0, "Message": "Tous s\'est bien passé"}';
                exit();
            } else {
                echo '{ "ReturnCode": 2, "Message": "Une erreur de la fonction deleteWorkshop()"}';
                exit();
            }
        }
    }
}
// Si j'arrive ici, c'est TOUT bon
echo '{ "ReturnCode": 0, "Message": ""}';

