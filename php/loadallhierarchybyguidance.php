<?php
/**
 * @brief Charge toutes la Hierarchy
 * @author ronaldo.lrrpnt@eduge.ch
 */
require_once './inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Récupère l'id de l'orientation et l'id l'année (facultatif)
$guidanceid = -1;
$yearid = "";
if(isset($_POST['guidanceId']))
	$guidanceid = $_POST['guidanceId'];

if(isset($_POST['yearId']))
	$yearid = $_POST['yearId'];


if($guidanceid > 0)
{
	$base = EAppManager::getInstance()->LoadAllHierarchyByGuidance($guidanceid, $yearid);
	if ($base === false){
		echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de LoadAllHierarchyByGuidance()"}';
		exit();
	}
	$jsn = json_encode($base);
	if ($jsn == FALSE){
		$code = json_last_error();
		echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json ('.$code.'"}';
		exit();
	}
	echo '{ "ReturnCode": 0, "Data": '.utf8_encode($jsn).'}';
	exit();
}

echo '{ "ReturnCode": 3, "Message": "Il manque l\'id de la guidance}';