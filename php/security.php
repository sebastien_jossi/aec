<?php
/**
 * @brief Cette page permet que personne n'entre sans être connectée, et si la personne change l'url avec une autre vue elle sera immédiatement renvoyée vers sa vue
 */
function security()
{
if (empty($_SESSION['email'])){
	header('Location: ../../index.php');
}
	$page = $_SERVER['REQUEST_URI'];
	if((strpos($page,"VueEleve")!== false) && $_SESSION['role'] != 1){
		if( $_SESSION['role'] == 2){
			header('Location: ../VueAdministrateur/pageGestionCompetence.php');
		}
		if( $_SESSION['role'] == 3){
			header('Location: ../VueDirection/AccueilDirection.php');
		}
	}	
	if((strpos($page,"VueAdministrateur")!== false) && $_SESSION['role'] != 3){
		if( $_SESSION['role'] == 1){
			header('Location: ../VueEleve/AccueilApprenti.php');
		}
		if( $_SESSION['role'] == 2){
			header('Location: ../VueDirection/AccueilDirection.php');
		}
	}
	if((strpos($page,"VueDirection")!== false) && $_SESSION['role'] != 2){
		if( $_SESSION['role'] == 1){
			header('Location: ../VueEleve/AccueilApprenti.php');
		}
	}
}