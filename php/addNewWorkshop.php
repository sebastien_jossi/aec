<?php

require_once './inc.all.php';
/**
 * @brief ajoute un atelier dans la base de données
 * @param $name  Le nom de l'atelier
 * @param $desc  La description de l'atelier
 * @param $degre Le degrée de l'atelier
 */
$name = "";
$desc = "";
$degre = - 1;
// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');


if (isset($_POST['nameWorkshop']))
    $name = filter_input(INPUT_POST, 'nameWorkshop', FILTER_SANITIZE_STRING);

if (isset($_POST['descWorkshop']))
    $desc = filter_input(INPUT_POST, 'descWorkshop', FILTER_SANITIZE_STRING);

if (isset($_POST['degreWorkshop']))
    $degre = filter_input(INPUT_POST, 'degreWorkshop', FILTER_SANITIZE_STRING);

if ($name != -1  && $degre != -1) {

    if (EDataManager::getInstance()->addNewWorkshop($name, $desc, $degre)) {
        echo '{ "ReturnCode": 0, "Message": "Tous s\'est bien passé"}';
        exit();
    } else {
        echo '{ "ReturnCode": 2, "Message": "Une erreur de la fonction addNewWorkshop()"}';
        exit();
    }
}
// Si j'arrive ici, c'est TOUT bon
echo '{ "ReturnCode": 0, "Message": ""}';

