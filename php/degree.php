<?php
/**
 * @copyright floran.stck@eduge.ch
*/


/**
 * Classe d'exemple d'un utilisateur
* @author FS
*
*/
class EDegree implements JsonSerializable
{
	
	/**
	 * 
	 * @param $InDegreeId l'identifiant du degré
	 * @param $InDegreeName le nom du degré
	 * @param $InWorkshops les ateliers liés au degré
	 */
	public function __construct($InDegreeId = -1, $InDegreeName = "", $InWorkshops = ""){

		$this->degreeid = $InDegreeId;
		$this->degreename = $InDegreeName;
		$this->workshops = $InWorkshops;

	}
	/**
	 * @brief	On ne laisse pas cloner un user
	 */
	private function __clone() {}

	/***
	 * Transforme l'objet en json
	 * @return L'objet en json
	 */
	public function jsonSerialize()
	{
		return get_object_vars($this);
	}
	
	
	public function getWorkshops(){

		return $this->workshops;
	}

	/**
	 * 
	 * @param unknown $InWorkshopId l'identifiant de l'atelier
	 * @param string $InWorkshopName le nom de l'atelier
	 * @return boolean
	 */
	public function addWorkshopAttr($InWorkshopId = -1, $InWorkshopName = ""){

		try
		{
			array_push($this->workshops, new EDegree($this->degreeid,$InWorkshopId,$InWorkshopName));
			return true;
		} catch (Exception $e)
		{
			echo "EDegree:addWorkshops Error: " . $e->getMessage();
			return false;
		}

	}
	
	/**
	 * 
	 * @param unknown $InWorkshop l'atelier sélectionné 
	 * @return boolean
	 */
	public function addWorkshop($InWorkshop){

		try
		{
			array_push($this->workshops, $InWorkshop);
			return true;
		} catch (Exception $e)
		{
			echo "EDegree:addWorkshops Error: " . $e->getMessage();
			return false;
		}

	}
	/**
	 * 
	 * @param $InArrWorkshop le tableau des ateliers
	 * @param $clearAll supprime tout
	 * @return boolean
	 */
	public function addWorkshops($InArrWorkshop, $clearAll = false){

		try
		{
			if ($clearAll == true)
				$this->workshops = $InArrWorkshop;
				else
					array_push($this->workshops, $InArrWorkshop);

					return true;
		} catch (Exception $e)
		{
			echo "EDegree:addWorkshops Error: " . $e->getMessage();
			return false;
		}

	}

	private $degreeid;
	private $degreename;
	private $workshops;
}