<?php

/**
 * @brief Classe utilisateur pour la gestion des utilisateur (GestionUtilisateur.php) 
 * @copyright quentin.fslr@eduge.ch 2018-2019
 */

/**
 * Classe 
 * @author FS
 */

class EAllPerson{
	
	/**
	 * 
	 * @param $InIdPerson L'identifiant de la personne
	 * @param $InFirstName Le prénom de la personne
	 * @param $InLastName Le nom de famille de la personne
	 * @param $InEmail L'email de la personne
	 * @param $InRole Le role de la personne
	 * @param $InIsActive Si la personne est active
	 * @param $InIsStudent Si la personne est un élève
	 * @param $InClass id classe de la personne
	 * @param $InClassName nom de la classe
	 * @param $InDegree	 année de la personne
	 * 
	 * 	 */
	
	public function __construct($InIdPerson = -1, $InFirstName = "",$InLastName = "",$InEmail = "",$InRole = -1,$InIsActive = false,$InIsStudent = false, $InClassName = "",$InClass= "",$InDegree = ""){
		
		$this->idPerson = $InIdPerson;
		$this->firstname = $InFirstName;
		$this->lastname = $InLastName;
		$this->email = $InEmail;
		$this->isActive = $InIsActive;
		$this->isStudent = $InIsStudent;
		$this->inRole = $InRole;
		$this->inClassName = $InClassName;
		$this->inDegree = $InDegree;
		$this->inClass = $InClass;

		switch ($this->inRole) {
			case ER_ADMINISTRATOR:
				$this->roleName = "Administration";
				$this->inClassName = "-";
				$this->inDegree = "-";
				break;
			case ER_DIRECTIONMEMBER:
				$this->roleName = "Direction";
				$this->inClassName = "-";
				$this->inDegree = "-";
				break;
			case ER_STUDENT:
				$this->roleName = "Élève";
				break;
		}


	}	
	
	
	public $idPerson;
	public $firstname;
	public $lastname;
	public $email;
	public $isStudent;
	public $isActive;
	public $roleName;
	public $inClassName;
	public $inDegree;
	public $inClass;
	public $inRole;

}