<?php
/**
 * @brief crée un nouvelle année et passe les utilisateur a l'année sup
 * @author 	quentin.fslr@eduge.ch
 */
require_once './inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');


if(EUtilitiesManager::getInstance()->NouvelleAnnee()){
    echo '{ "ReturnCode": 0, "Data": "Nouvelle année créez"}';
    exit();
}
else{
    echo '{ "ReturnCode": 6}';
    exit();
}
    

