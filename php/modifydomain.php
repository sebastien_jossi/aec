<?php
/**
 * @brief	Permet d'envoyer les variables à appManager
* 			pour modifier un domaine dans la base de données
* @author 	romain.ssr@eduge.ch
* 
* @date 09.02.2017
*/
require_once './inc.all.php';

$guidanceid = -1;

if (isset($_POST['guidanceId']))
	$guidanceid = $_POST['guidanceId'];

$domainid = -1;

if (isset($_POST['domainId']))
	$domainid = $_POST['domainId'];

$domaindescription = "";

if (isset($_POST['domaindescritpion']))
	$domaindescription = filter_var($_POST['domaindescritpion'], FILTER_SANITIZE_STRING);



	// J'envoie les informations nécessaires pour ajouter un domaine dans la base de donée.
	$modifiedDomain = EAppManager::getInstance()->modifyDomain($guidanceid,$domainid,$domaindescription);
	if ($modifiedDomain === false){
		echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de loadAllGuidance()"}';
		exit();
	}
	
	echo '{ "ReturnCode": 0}';
	