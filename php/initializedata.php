<?php
if (session_status() == PHP_SESSION_NONE) {     session_start(); }
/**
 * @brief @brief Initialise tout à 1 si c'est la première connexion de l'élève
 * @param $studentId L'identifiant de l'apprenti /!\ OBLIGATOIRE
 *
 * @author floran.stck@eduge.ch
 */
require_once './inc.all.php';


$studentId = $_POST['studentId'];
$guidanceId = $_POST['guidanceId'];

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');
if(($studentId != - 1) && ($guidanceId != - 1))
{
	// Je récupère toutes les orientations
	$data = EAppManager::getInstance()->Initialize($guidanceId, $studentId);
	if ($data === false){
		echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de Initialize()"}';
		exit();
	}
	exit();
}


// Si j'arrive ici, c'est pas bon
echo '{ "ReturnCode": 1, "Message": "Il manque les paramètres studentId ou guidanceId"}';

?>