<?php

/**
 * 
 * @brief Récupère les domaines d'une orientation
 * @param $guidanceid L'identifiant de l'orientation
 * @param $yearid l'dentifiant de l'année
 */

require_once './inc.all.php';

// Nécessaire lorsqu'on retourne du json
header('Content-Type: application/json');

// Je récupère l'id de guidance
$guidanceid = -1;
if (isset($_POST['guidanceId']))
	$guidanceid = $_POST['guidanceId'];

// Récupère l'id l'année
$yearid = "";
if(isset($_POST['yearId']))
	$yearid = $_POST['yearId'];

if ($guidanceid > 0){
	$domains = EAppManager::getInstance()->loadDomainsByGuidance($guidanceid, $yearid);
	if ($domains === false){
		echo '{ "ReturnCode": 2, "Message": "Un problème de récupération des données de loadDomainsByGuidance()"}';
		exit();
	}
	
	$jsn = json_encode($domains);
	if ($jsn == FALSE){
		$code = json_last_error();
		echo '{ "ReturnCode": 3, "Message": "Un problème de d\'encodage json ('.$code.'"}';
		exit();
	}
	echo '{ "ReturnCode": 0, "Data": '.utf8_encode($jsn).'}';
	exit();
	
}

// Si j'arrive ici, c'est pas bon
echo '{ "ReturnCode": 1, "Message": "Il manque le paramètre guidanceId"}';
