<?php

class EStates implements JsonSerializable
{

	/**
	 * @brief Class Constructor
	 */
	/**
	 * 
	 * @param $InStateId L'identifiant de l'état
	 * @param $InStateName Le nom de l'état
	 */
	public function __construct($InStateId = -1, $InStateName = "")
	{
		$this->stateid = $InStateId;
		$this->statename = $InStateName;
	}


	private function __clone()
	{}

	/***
	 * Transforme l'objet en json
	 * @return L'objet en json
	 */
	public function jsonSerialize()
	{
		return get_object_vars($this);
	}
	
	public function getStatesId(){

		return $this->statesid;
	}

	public function getYear(){

		return $this->states;
	}

	private $stateid;
	private $statename;
}