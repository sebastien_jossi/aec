/***
 * Les fonctions servent à redirigé l'utilisateur une fois que la connexion eduge.ch aient été valider	
 */

function updateSigninStatus(isSignedIn) {
    if (isSignedIn) {
      EELAuth.getUserInfo(onReceiveUserInfo);
    } else {
        EELAuth.signIn();
    }
  }
  function handleAuthClick(event) {
      updateSigninStatus(EELAuth.isSignedIn());
  }
   
 function onReceiveUserInfo(info) 
 {
	// Email de l'utilisateur
    var email = info.email;
          
    // Ici vous pouvez ajouter du code ajax afin de récupérer des informations
    // Et de le comparer à votre base de données
	$.ajax({
		method : 'POST',
		url : './php/getperson.php',
		data : {'email': email},
		dataType : 'json',
		success : function(data) {
			switch (data.ReturnCode) {
			case 0: // tout bon
				document.location.href= data.Message;
				break;
			case 1: // erreur param
				break;
			case 2: // erreur id
				break;
			case 3: // erreur encodage json
				break;
			}
		},
		error : function(jqXHR) {
			switch (jqXHR.status) {
			case 404:
				msg = "page not found. 404";
				break;
			case 200:
				msg = "problem avec json. 200";
				break;
			}
			$("#info").html(msg);
		}
	});  
 }