﻿/**
 * @copyright ronaldo.lrrpnt@eduge.ch 2016-2017
 * Contient des fonctions utile pour plusieurs page de la partie direction
 * @author RLP
 **/

/**
 * @copyright dario.gng@eduge.ch
 * Effectue un appel ajax et exécute une fonction en cas de succès
 *
 * La réponse JSON doit impérativement contenir les paramêtres :
 *      -ResponseCode -> code indiquant le status de la reponse
 *      -Data         -> Valeurs renvoyer en tant que reponse AJAX
 *
 *
 * @param string url        Le chemin vers le fichier qui va récupérer nos données
 * @param string callback   Le nom de la fonction à exécuter après avoir reçu les données. Il ne faut pas mettre de parenthèses
 * @param object params     Object qui contient tout les paramètres à envoyer
 * @param boolean async		Pour activer / désactiver l'asynchrone d'un appel ajax
 */
function get_data(url, callback, params = {}, async) {
    // Copier l'objet params dans une variable locale
    // qui sera simplement utilisées pour l'appel Ajax
    var dp = params;
    // On utilise le paramètre de la fonction qui est stocké sur le stack
    // pour créer un tableau qui contient les paramètres additionnels qui se
    // trouvent après params.
    // Si on stocke ces paramètres dans un tableau créé dans cette function,
    // il sera détruit après l'appel Ajax et on aura plus rien lorsqu'on sera
    // rappelé de manière asynchrone.
    params = Array();
    for (var i = 4; i < arguments.length; i++) {
        params.push(arguments[i]);
    }
    $.ajax({
        method: 'POST',
        url: url,
        data: dp,
        dataType: 'json',
        async: async,
        success: function (data) {
            var msg = '';
           // console.log(data);

            if (data != null) {
            // console.log(data.ReturnCode);
                switch (data.ReturnCode) {
                    case 0 : // tout bon
                        // On récupère par params, notre tableau des arguments.
                        params.unshift(data.Data);
                        callback.apply(this, params);
                        break;
                    case 1:
                        displayMessage("Veuillez remplir les champs marqué d'un *", 2); // erreur param
                        break;
                    case 2 : // problème récup données
                        alert("e");
                    case 3 : // problème encodage sur serveur
                    case 4 :
                        document.location.href="../../index.php"; //data de getpratices vide
                    case 5 :
                        displayMessage("Le commentaire n'est pas à vous.", 2);
                    case 6 :
                        displayMessage("Une erreur s\'est produite", 1);
                    case 7:
                        params.unshift(data.Data);
                        callback.apply(this, params);
                        break;
                    default:
                        msg = data.Message;
                        break;
                }
            }
        },
        error: function (jqXHR) {
            if (jqXHR.responseText != "")
                console.log(jqXHR);
            // Votre gestion de l'erreur
        }
    });
}

/**
 * Récupère les données nécessaire à la construction du tableau
 * @param guidanceId L'identifiant de l'orientation
 * @param yearId L'identifiant de l'année
 * @param degreeId L'identifiant de l'année
 * @param classroomId L'identifiant de la classe
 * @param stateId L'identifiant de l'état
 */
function completeArray(guidanceId, yearId, degreeId, classroomId, stateId)
{
    // Supression du tableau afin de le reconstruire
    $('.removeRefresh').empty();

    get_data('../../php/loadallhierarchybyguidance.php', process, {'guidanceId': guidanceId, 'yearId': yearId}, true, guidanceId, yearId, degreeId, classroomId, stateId);

    // Success de l'appel AJAX
    function process(data, guidanceId, yearId, degreeId, classroomId, stateId)
    {
        if (data[0]["domains"].length > 0)
        {
            var table = ('<table class="table table-bordered table-vcenter" id="skillsTable"><thead><tr id="domainsCodes"><th rowspan="2" id="thSkill">Domaine de compétence</th></tr><tr id="domainsDescriptions"></tr></thead><tbody id="bodySkill"><tr id="row0"><th id="thSkill" rowspan="1000">Compétences opérationelles</th></tr></tbody></table>');
            $('#tableData').append(table);

            displayDomainAndSkills(data, "RecapitulatifCompetence.php");
            callajaxdata(guidanceId, yearId, degreeId, classroomId, stateId);
        } else
        {
            var message = '<p class="alert alert-danger" style="margin-top:10px;"><span class="glyphicon glyphicon-exclamation-sign"></span> Cette orientation ne contient aucun domaine avec l\'année choisi !</p>';
            $('#tableData').append(message);
        }
    }
}

/**
 * Exécute les requêtes afin d'obtenir les données nécesaire à l'affiche des moyennes
 * @param guidanceId L'identifiant de l'orientation
 * @param yearId L'identifiant de l'année
 * @param degreeId L'identifiant de l'année
 * @param classroomId L'identifiant de la classe
 * @param stateId L'identifiant de l'état
 */
function callajaxdata(guidanceId, yearId, degreeId, classroomId, stateId) {

    // Supression du tableau afin de le vider et reconstruction
    $('.avgBottom').empty();

    // Ajoute des données au lien
    modifyURLRedirectionCase(degreeId, classroomId, yearId);

    // Récupère les données nécessaire à l'affichage de la moyenne
    get_data('../../php/loadalldatabyfilter.php', process, {'guidanceId': guidanceId, 'yearId': yearId, 'degreeId': degreeId, 'classroomId': classroomId}, true, guidanceId, yearId, degreeId, classroomId, stateId);

    // Success de l'appel AJAX
    function process(data, guidanceId, yearId, degreeId, classroomId, stateId)
    {
        if (typeof classroomId != 'undefined' && classroomId != -1)
            displayArrStudent(guidanceId, classroomId, yearId, data, stateId);
        else
            avgSkill(data, stateId);
    }
}


/**
 * Récupère les infos d'un skill spécifié et les mets sous forme de tableau (affichage)
 * @param guidanceid L'identifiant de l'orientation
 * @param domainid L'identifiant du domaine
 * @param skillid L'identifiant de la compétence
 * @param yearid L'année scolaire
 * @param degreeid L'identifiant du degré scolaire
 * @param classroomid L'identifiant de la classe
 */
function getInfoSkill(guidanceid, domainid, skillid, yearid, degreeid, classroomid, fakeid, withBtn, historic = false)
{
    var realid = domainid + "-" + skillid;

    // Récupère le skill spécifié
    if ($("#infoZone" + realid).length <= 0 || $("#tableZone" + realid).length <= 0)
        get_data('../../php/getskill.php', displaySkill, {'guidanceId': guidanceid, 'domainId': domainid, 'skillId': skillid}, true, fakeid, realid, withBtn);

    // Récupère les pratique du skill spécifié
    if ($("#practiceZone" + realid).length <= 0)
        get_data('../../php/getpractices.php', process, {'guidanceId': guidanceid, 'domainId': domainid, 'skillId': skillid}, true, guidanceid, domainid, skillid, yearid, degreeid, classroomid, fakeid, realid, withBtn);

    // Success de l'appel AJAX qui est au-dessus
    function process(data, guidanceid, domainid, skillid, yearid, degreeid, classroomid, fakeid, realid, withBtn)
    {
        displayPractices(data, domainid, fakeid, realid, withBtn);

        // Récupère les données nécessaire à l'affichage des statistiques
        get_data('../../php/loadalldatabyfilter.php', displayStatistic, {'guidanceId': guidanceid, 'domainId': domainid, 'skillId': skillid, 'yearId': yearid, 'degreeId': degreeid, 'classroomId': classroomid}, true, realid);
    }

    // Ne s'éxécute pas si on demande l'historique car il n'éxiste pas dans la base
    if (!historic && $("#commentZone" + realid).length <= 0)
        // Récupère les commentaire du skill spécifié
        get_data('../../php/getcommentbyskill.php', displayComments, {'guidanceId': guidanceid, 'domainId': domainid, 'skillId': skillid}, true, realid, withBtn);

}

/**
 * Si une classe à été choisi dans les filtres, cette fonction fabrique le tableau de chaque élève de la classe sélectionné.
 * @param guidanceId L'identifiant de l'orientation
 * @param classroomId L'identifiant de la classe
 */
function displayArrStudent(guidanceId, classroomId, yearId, dataAvg, stateId)
{
    get_data('../../php/loadalldatastudentbyclassroom.php', process, {'classroomId': classroomId, 'yearId': yearId}, true, guidanceId, dataAvg, stateId);

    function  process(data, guidanceId, dataAvg, stateId)
    {
        // Créer le tableau chaque élève
        data[0].forEach(function (student) {
            var section = $('<section class="ArrStudent removeRefresh" id="studentZone-' + student.studentid + '">');
            var arrayClone = $('#skillsTable').clone();
            section.append(arrayClone);

            $('#tableData').parent().append(section);

            arrayClone.find('#bodySkill').prop('id', "student" + student.studentid);
            arrayClone.find('#student' + student.studentid + ' > tr > td > a').addClass('disabled');
            arrayClone.css('margin-top', "0px");

            var label = $('<label>');
            label.append("Numéro de l'élève : " + student.studentid);
            arrayClone.before(label);
        });

        // Commençement à l'utilisation du système de couleur
        getPracticesForColorSystem(data, parseInt(guidanceId), yearId);

        // Appelle la fonction si elle existe pour mettre un bouton qui permet de supprimer le tableau (page d'impression)
        if (typeof addBtnRemoveInTableStudent == 'function')
            addBtnRemoveInTableStudent();

        console.log(dataAvg);

        // Affiche les moyennes maintenant pour éviter de cloner le tableau avec les couleurs du tableau principal
        avgSkill(dataAvg, stateId);
    }
}

/**
 * Récupère l'état pour un domaine et skill donné dans le tableau des practices
 * @param {array} arPractices Tableau de Practice
 * @param {integer} practiceId Le practice id pour lequel on veut le state
 * @param {string} s Représente le Domaine et le Skill (D-S)
 * @returns {integer} Le state. ou -1 si pas trouvée
 */
function getPracticeState4Practice(arPractices, practiceId, s){
    var retValue = -1;
    for ( let practice of arPractices)
    {
        var id = practice.domainid + "-" + practice.skillid;
        if (id == s && practice.practiceid == practiceId){
            retValue = parseInt(practice.stateid);
            break;
        }
    }
    // Done
    return retValue;
}

/**
 * Est-ce que le state existe pour un domaine-skill dans le tableau des practices
 * @param {array} arPractices Tableau de Practice
 * @param {integer} state Le state à tester dans la practice
 * @param {string} s Représente le Domaine et le Skill (D-S)
 * @returns {boolean} True si trouvé, autrement False.
*/
function hasPracticeState4DomainAndSkills(arPractices, state, s){
    var retValue = false;
    for ( let practice of arPractices)
    {
        var id = practice.domainid + "-" + practice.skillid;
        if (id == s && parseInt(practice.stateid) == state){
            retValue = true;
            break;
        }
    }
    // Done
    return retValue;
}

/**
 * Récupère le nombre de practice pour un domaine-skill dans le tableau des practices
 * @param {array} arPractices Tableau de Practice
 * @param {string} s Représente le Domaine et le Skill (D-S)
 * @returns {integer} Le nombre de practice. ou -1 si pas trouvé
 */
function getPracticeCount4DomainAndSkills(arPractices, s){
    var retValue = -1;
    for ( let practice of arPractices)
    {
        var id = practice.domainid + "-" + practice.skillid;
        if (id == s){
            retValue = parseInt(practice.TotalPractice);
            break;
        }
    }
    // Done
    return retValue;
}


/**
 * Mets le pourcentage en bas à droite de chaques skills et la couleur correspondante au pourcentage
 * @param arDataStudent Tableau contenant les évaluations des étudiants
 * @param stateId Identifiant de l'état
 */
function avgSkill(arDataStudent, stateId)
{
    $('#bodySkill > tr').find('td').each(function (tdcase, td) {
        var percent = 0;
        var nbStudent = 0; // Le nombre d'étudiant qui ont l'état de la compétence qui à été filtré
        /* @todo Vérifier si fonctionne avec la longueur du tableau
                 A supprimer
        var totalStudent = parseInt(arDataStudent[1]['TotalStudent']);
         */
        var totalStudent = arDataStudent[0].length;
        var tdId = $(td).attr('id');
        var totalPractice = getPracticeCount4DomainAndSkills(arDataStudent[3], tdId);
        /* @todo Remove
        arDataStudent[3].forEach(function (countpractice) {
            if (countpractice.domainid + '-' + countpractice.skillid == tdId){
                totalPractice = countpractice.TotalPractice;
                return false;
            }
        });
        */

        //console.log(arDataStudent);

        arDataStudent[0].forEach(function (student)
        {
            if (hasPracticeState4DomainAndSkills(student.datapractices, stateId, tdId))
                nbStudent++;

            /* @todo Remove
            student.datapractices.forEach(function (practice)
            {
                var id = practice.domainid + "-" + practice.skillid;
                if (id == tdId)
                    if (avg > parseInt(practice.stateid) || avg <= 0)
                        avg = parseInt(practice.stateid);
            });
            // Si l'etat choisi correspont à la moyenne
            if (state == stateId)
                nbStudent++;
            */
        });

        // Etablie un pourcentage des élèves ayant l'état filtré
        if (totalStudent > 0)
            percent = nbStudent / totalStudent * 100;

        // Arrondi le chiffre à deux décimal
        percent = percent.toFixed(2);
        var color = "white";

        if ($("#stateSelect option:selected").text() != "Pas vu")
        {
          if (percent < 25)
          color = "rgba(255, 0, 0, 0.5)!important";
          if (percent >= 25 && percent < 75)
          color = "rgba(255, 150, 0, 0.5)!important";
          if (percent >= 75)
          color = "rgba(0,255 , 0, 0.5)!important";

          if ($(td).attr('id') != null)
          {
            var avg = $('<div class="col-xs-12 avgBottom" style="background-color:' + color + ';">');
            avg.html('<b>' + percent + "%</b>");
            $(td).append(avg);
          }
        } else {
          if (percent < 25)
          color = "rgba(0, 255, 0, 0.5)!important";
          if (percent >= 25 && percent < 75)
          color = "rgba(255, 150, 0, 0.5)!important";
          if (percent >= 75)
          color = "rgba(255,0 , 0, 0.5)!important";

          if ($(td).attr('id') != null)
          {
            var avg = $('<div class="col-xs-12 avgBottom" style="background-color:' + color + ';">');
            avg.html('<b>' + percent + "%</b>");
            $(td).append(avg);
          }
        }
  });
}

/**
 * Affiche les informations à propos d'un skill
 * @param arSkill Le tableau contenant les informations du skill
 * @param fakeid Le faux id de la compétence
 * @param realid Le véritable id de la compétence
 * @param withBtn Affiche un bouton permettant de supprimer l'article (page Impression) si true | false n'affiche pas le bouton de supression
 */
function displaySkill(arSkill, fakeid, realid, withBtn = false)
{
    // Le tableau ne contient que un skill
    arSkill.forEach(function (skill)
    {
        //var fakeid = $('#bodySkill > tr > td#' + skill.owner + "-" + skill.skillid + "> a").find('b:first').text();
        //var realid = $('td#' + skill.owner + "-" + skill.skillid).attr("id");
        var skillZone = $('#skillZone' + realid);
        var bodyZone = $('.bodyZone');

        // Crée la zone pour le skill si elle n'éxiste pas
        if (skillZone.length <= 0)
        {
            skillZone = $('<section class="sectionZone nocut" id="skillZone' + realid + '">');
            bodyZone.append(skillZone);
        }

        // Définition de la compétence
        if ($("#infoZone" + realid).length <= 0)
        {
            var infoZone = $('<article class="nocut" id="infoZone' + realid + '">');

            if (withBtn)
            {
                var btn = $('<button class="btn btn-danger btnRemoveDiv noprint" id="btn/infoZone' + realid + '">');
                btn.append('<span class="glyphicon glyphicon-remove"></span></button>');
                createEventButtonRemoveDiv(btn);
                infoZone.append(btn);
            }

            infoZone.append('<p><b>' + fakeid + ': ' + skill.skillname + '</b>' + '<br/>' + skill.skilldefinition + '</p>');

            skillZone.prepend(infoZone);
        }

        // Tableau contenant les compétences social, methodologique, personnelle
        if ($("#tableZone" + realid).length <= 0)
        {
            var tableZone = $('<article class="nocut" id="tableZone' + realid + '">');
            var tab = $('<table class="table table-bordered table-vcenter">');
            var th = '<tr><th class="col-xs-4 text-center">Compétence méthodologique</th><th class="col-xs-4 text-center">Compétence sociale</th><th class="col-xs-4 text-center">Compétence personelle</th></tr>';
            var tr = $('<tr>');
            var m = $('<td class="text-center">'), s = $('<td class="text-center">'), p = $('<td class="text-center">');
            m.html(skill.skillmethodologic);
            s.html(skill.skillsocial);
            p.html(skill.skillpersonnal);
            tr.append(m, s, p);
            tab.append(th, tr);

            if (withBtn)
            {
                var btn = $('<button class="btn btn-danger btnRemoveDiv noprint" id="btn/tableZone' + realid + '">');
                btn.append('<span class="glyphicon glyphicon-remove"></span></button>');
                createEventButtonRemoveDiv(btn);
                tableZone.append(btn);
            }

            tableZone.append(tab);

            if ($('#practiceZone' + realid).length > 0)
                $('#practiceZone' + realid).before(tableZone);
            else
                skillZone.append(tableZone);
        }
    });
}

/**
 * Affiche les pratiques professionnelles d'un skill
 * @param arPractices Le tableau contenant les pratiques professionnelles
 * @param domainid L'identifiant du domaine
 * @param fakeid Le faux identifiant du skill (Ex: A1)
 * @param realid Le vrai identifiant de la compétence (Ex: A-10)
 * @param withBtn Affiche un bouton permettant de supprimer l'article (page Impression) si true | false n'affiche pas le bouton de supression
 *
 */
function displayPractices(arPractices, domainid, fakeid, realid, withBtn = false)
{
    var bodyZone = $('.bodyZone');
    var practices = $('<tbody>');
    var cptPractice = 1;
    var skillZone;
    var practiceZone;
    var btn;

    // Crée le tableau
    var tab = $('<table  class="table table-bordered table-vcenter">');
    tab.html('<thead><tr><th class="text-center col-xs-4" rowspan="2">Pratique professionnelle</th><th class="text-center" rowspan="2"><label>Pas vu</label></th><th class="text-center" rowspan="2">Expliqué</th><th class="text-center" rowspan="2">Exercé</th><th class="text-center" rowspan="2">Autonome</th><th class="text-center" rowspan="2">Fait dans les ateliers</th></tr></thead>');

    if ($('#practiceZone' + realid).length <= 0)
    {
        arPractices.forEach(function (practice)
        {
            realid = domainid + '-' + practice.owner;
            skillZone = $("#skillZone" + domainid + '-' + practice.owner);

            if ($('#practiceZone' + realid).length <= 0)
            {
                practiceZone = $('<article class="nocut" id="practiceZone' + realid + '">');

                if (withBtn)
                {
                    btn = $('<button class="btn btn-danger btnRemoveDiv noprint" id="btn/practiceZone' + realid + '">');
                    btn.append('<span class="glyphicon glyphicon-remove"></span></button>');
                    createEventButtonRemoveDiv(btn);
                }

                if (skillZone.length <= 0)
                {
                    skillZone = $('<section class="sectionZone nocut" id="skillZone' + realid + '">');
                    bodyZone.append(skillZone);
                }

                var tr = $('<tr id="practice-' + practice.practiceid + '">');
                tr.html('<td>' + domainid + '.' + fakeid.substr(1, fakeid.length) + "." + cptPractice + ": " + practice.practicedescription + ".</td>");
                practices.append(tr);
                cptPractice++;
            }
        });

        // Ajoute le tableau des pratiques avant le tableau des commentaires si celui-ci existe
        if ($("#commentZone" + realid).length && practiceZone != undefined)
        {
            tab.append(practices);
            if (withBtn)
                practiceZone.append(btn, tab);
            practiceZone.append(tab);
            $("#commentZone" + realid).before(practiceZone);
        } else
        {
            if (practiceZone != undefined)
            {
                tab.append(practices);
                if (withBtn)
                    practiceZone.append(btn, tab);
                practiceZone.append(tab);
                skillZone.append(practiceZone);
            }
        }
}
}

/**
 * Affiche les statistiques pour chaque pratique professionnelle
 * @param arStatistic Le tableau contenant les évaluations des apprentis
 * @param realid Le véritalbe identifiant de la compétence
 *
 */
function displayStatistic(arStatistic, realid)
{

    // Compte le nombre de td dans le tableau pour ne pas réajouter si l'utilisateur reclique sur la même case du tableau
    var nbCol = $('#skillZone' + realid + ' > #practiceZone' + realid + ' > table > tbody > tr:first > td').length;

    if (nbCol == 1)
    {
        $('#skillZone' + realid + ' > #practiceZone' + realid + ' > table > tbody').find('tr').each(function (trline, tr) {
            var practiceid = $(tr).attr('id').split("-")[1];
            var cptPasVu = 0;
            var cptExplique = 0;
            var cptExerce = 0;
            var cptAutonome = 0;
            arStatistic[0].forEach(function (student) {
                student.datapractices.forEach(function (statistic) {
                    if (statistic.practiceid == practiceid) {
                        // Compte le nombre de Pas Vu, Expliqué, Exercé, Autonome que les apprentis ont sélection pour la pratique professionnelle
                        switch (statistic.stateid)
                        {
                            case "1":
                                cptPasVu++;
                                break;
                            case "2":
                                cptExplique++;
                                break;
                            case "3":
                                cptExerce++;
                                break;
                            case "4":
                                cptAutonome++;
                                break;
                        }
                    }
                });
            });

            const workshops = arStatistic[2];
            const disWorkshop = getNbEtudiantByAtelier(arStatistic[0], practiceid)
                .map(workshopAndStates => {
                    const workshop = workshops.find(w => w.ID === workshopAndStates.workshopId);

                    const displayStates = workshopAndStates.states
                        .map(state => `${state.name}: ${state.studentsCount}`)
                        .join(', ');

                    const workshopName = workshop.NAME;
                    return `<strong>${workshopName}</strong> (${displayStates})<br>`;
                })
                .join('');

            var td = '<td class="text-center">' + cptPasVu + '</td><td class="text-center">' + cptExplique + '</td><td class="text-center">' + cptExerce + '</td><td class="text-center">' + cptAutonome + '</td><td>' + disWorkshop + '</td>';
            $(tr).append(td);
        });
    }
}

/**
 * Permet de savoir, pour une pratique professionnelle donnée, les ateliers dans lesquels
 * cette pratique a été exercée. Pour chaque atelier, cette fonction retourne le nombre
 * d’étudiants par niveau de compétence (voir l’exemple).
 * 
 * @param   {array}   arrEtudiant Le tableau contenant tous les étudiants ainsi que leurs compétences 
 * @param   {string}  practiceId  L’ID de la pratique professionnelle concernée
 * 
 * @returns {array}   Tableau contenant tous les ateliers, sous la forme suivante :
 */
function getNbEtudiantByAtelier(arrEtudiant, practiceId) {
    const stateNames = {
        1: 'Pas vu',
        2: 'Expliqué',
        3: 'Exercé',
        4: 'Autonome',
    };

    const workshops = [];

    // Récupérer toutes les pratiques pro. de tous les étudiants dans un tableau
    const practicesArrays = arrEtudiant.map(etudiant => etudiant.datapractices);
    const practices = [].concat(...practicesArrays)
        .filter(practice => practice.practiceid === practiceId);

    practices.forEach((practice) => {
        practice.workshops.forEach((workshopId) => {
            // Trouver l’atelier dans `workshops` ou le créer s’il n’y est pas déjà
            let workshop = workshops.find(w => w.workshopId === workshopId);
            if (workshop === undefined) {
                workshop = { workshopId, states: [] };
                workshops.push(workshop);
            }

            // Trouver l’état de maîtrise dans l’atelier ou le créer s’il n’existe pas déjà
            let state = workshop.states.find(state => state.id === practice.stateid);
            if (state === undefined) {
                state = {
                    id: practice.stateid,
                    name: stateNames[practice.stateid],
                    studentsCount: 0,
                };
                workshop.states.push(state);
            }

            // Incrémenter le nombre d’élèves pour cet état et cet atelier
            state.studentsCount += 1;

            // Trier les compétences par ID
            workshop.states = workshop.states.sort((a, b) => a.id - b.id);

    
            
        });
    });
   
    return workshops;
}

/**
 * Affiche les commentaires s'il y en a
 * @param arComment Le tableau contenant les commentaires
 * @param realid Le véritalbe identifiant de la compétence
 * @param withBtn Affiche un bouton permettant de supprimer l'article (page Impression) si true | false n'affiche pas le bouton de supression
 *
 */
function displayComments(arComment, realid, withBtn = false)
{
    if ($('#commentZone' + realid).length <= 0 && arComment.length > 0)
    {
        var commentZone = $('<article class="nocut table-responsive" id="commentZone' + realid + '">');
        var skillZone = skillZone = $("#skillZone" + realid);
        var table = $('<table class="table table-bordered table-vcenter" style="table-layout:fixed;"><thead><tr><th class="text-center" id="showComment">Les commentaires de la compétence<span class="pull-right glyphicon"></span></th></tr></thead>');
        var tbody = $('<tbody id="bodyComment"></tbody>');
        table.append(tbody);

        if (withBtn)
        {
            var btn = $('<button class="btn btn-danger btnRemoveDiv noprint" id="btn/commentZone' + realid + '">');
            btn.append('<span class="glyphicon glyphicon-remove"></span></button>');
            createEventButtonRemoveDiv(btn);
        }

        // Ajoute les commentaires dans le tableau
        arComment.forEach(function (commentary)
        {
            var comment = '<tr><td style="word-wrap:break-word;" >' + commentary.commentary + '</p><label class="pull-right">Posté le ' + commentary.dateComment + ' par ' + commentary.studentEmail + '</label></td></tr>';
            table.append(comment);
        });

        if (withBtn)
            commentZone.append(btn);
        commentZone.append(table);
        skillZone.append(commentZone);

}
}

/**
 * Remplie les selects (menu déroulant)
 * @param arrObject Contient les données pour remplir le filtre
 * @param idSelect Contient l'id du select HTML (mettre le #)
 * @param idObject Id de l'attribut de l'objet
 * @param nameObject Nom de l'attribut de l'objet
 * @param vSession Variable de session
 *
 */
function fillDropDown(arrObject, idSelect, idObject, nameObject, vSession)
{
    // Parti pour remplir le select
    arrObject.forEach(function (dataFilter)
    {
        var option;

        if (vSession == dataFilter[idObject])
            option = $('<option selected id="' + dataFilter[idObject] + '">');
        else
            option = $('<option id="' + dataFilter[idObject] + '">');

        var attribut = dataFilter[nameObject];
        option.html(attribut);
        $(idSelect).append(option);
    });
}
/**
 * Remplie les selects (menu déroulant)
 */
function displayWorkShop(arrObject)
{
    //console.log(arrObject);
    var cptrow = 1; // Numero de la ligne
    arrObject.forEach(function (workshop)
    {
        $('#bodyWork').append('<tr id="row' + cptrow + '"></tr>');

        // Fabrique une nouvelle ligne si elle n'éxiste pas
        if ($('#row' + cptrow).length == 0)
            $('#bodyWork').append('<tr id="row' + cptrow + '"></tr>');

        // Remplie les cases en colonne avec les données
        var line = $('#row' + cptrow);
        $.each(workshop, function (index, value)
        {
            var td = $('<td>');
            td.html(value);
            line.append(td);
        });
        var td = $('<td>');
        td.html('<button class="btn-primary btn btn"  data-toggle="modal" onclick="setDataEdit(\'' + workshop.workshopid + '\',\'' + workshop.workshopname + '\',\'' + workshop.workshopdescription.replace(/\n/, "<br>")+ '\',\'' + workshop.owner + '\',\'Edit\')" data-target="#modalWorkShopEdit">Modifier</button><button onclick="setDataDel(\'' + workshop.workshopid + '\')" class="btn-danger btn btn" data-toggle="modal" data-target="#modalWorkShopDelete">Supprimer</button>');
        line.append(td);
        cptrow++;

    });
}

/**
 * Crée un événement click pour les boutons de suppression des sections
 * @param btn Le bouton sur lequel il faut crée l'évenement click
 *
 */
function createEventButtonRemoveDiv(btn)
{
    $(btn).click(function (event) {
        event.preventDefault();
        var zone = $(btn).attr('id').split("/")[1];
        if (btn.parent().parent().children().length == 1)
            btn.parent().parent().remove();
        else
            $("#" + zone).remove();
    });
}
/**
 * Affiche les élèves dans la page GestionUtilisateur.php
 * @param arrObjet Le tableau contenant les utilisateur
 */
function DisplayPersonn(arrObjet){
    var cptrow = 1; // Numero de la ligne
    arrObjet.forEach(function (personne)
    {
        $('#bodyWork').append('<tr id="row' + cptrow + '"></tr>');

        // Fabrique une nouvelle ligne si elle n'éxiste pas
        if ($('#row' + cptrow).length == 0)
            $('#bodyWork').append('<tr id="row' + cptrow + '"></tr>');



        //Convertion des données pour affichage
        DatapersonneAffichage = personne;

        btn = '<td><button class="btn-primary btn btn" data-toggle="modal"  data-target="#ModalEditPersonn" onclick="setDataEditPerson(\'' + personne.idPerson + '\',\'' + personne.firstname + '\',\'' + personne.lastname + '\',\'' + personne.email + '\',\'' + personne.isActive + '\',\'' + personne.inRole + '\',\'' + personne.inClass + '\',\'' + personne.inDegree + '\',\'' + personne.isStudent + '\')"><i class="fas fa-user-edit"></i></button>';
        btn += '<button class="btn-danger btn btn" data-toggle="modal"  data-target="#ModalSuprPersonn" onclick="setDataSuprPerson(\'' + personne.idPerson + '\',\'' + personne.firstname + '\',\'' + personne.lastname + '\')"><i class="fas fa-trash-alt"></i></button></td>';

        //Suprimme info inutile
        delete DatapersonneAffichage.isStudent;
        delete DatapersonneAffichage.inRole;
        delete DatapersonneAffichage.inClass;

        if(DatapersonneAffichage.isActive==="1")
        DatapersonneAffichage.isActive = "Actif"
        else
        DatapersonneAffichage.isActive = "Désactiver"
        // Remplie les cases en colonne avec les données
        var line = $('#row' + cptrow);
        $.each(DatapersonneAffichage, function (index, value)
        {
            var td = $('<td>');
            td.html(value);
            line.append(td);
        });


        line.append(btn);

        cptrow++;



    });
    $(document).ready(function () {
        table = $('#tableWorkshop').DataTable({
            language: {
                url: "https://cdn.datatables.net/plug-ins/1.10.9/i18n/French.json"
            },
            search: {
                "caseInsensitive": false
            },
            iDisplayLength: 50,
            order: [order],
        });
        table.search($('input[name=RechercheInputValue]').val()).draw();


    $(".btnControlRole").click(function() {
        $("input[type=search]").focus();
        $("input[type=search]").val($(this).html());
        table.search($(this).html()).draw();
    });

});
}
/**
 * Permet d'affiche le bouton Nouvelle Année si année non créez
 */
 function DisplayButtonNewYear(data){
     now = new Date();
     label = now.getFullYear() +"-" +  (now.getFullYear()+1);
     if(label!=data[0]["yearname"]){
       $("#DivbtnNewYear").append('<button type="button" class="btn btn-warning"  data-toggle="modal"  data-target="#ModalNewYear">Nouvelle Année</button>');
     }

 }
