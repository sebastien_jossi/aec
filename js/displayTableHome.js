/**
 * @copyright ronaldo.lrrpnt@eduge.ch 2016-2017
 *
 * @brief Fonction qui crée le tableau des domaines et skills
 * @author RLP
 *
 * arGuidance Tableau contenant la hierarchy d'une orientation
 * pageRedirection Lien de la page où une case blanche doit rediriger quand elle est cliquée
 * 
 */
function displayDomainAndSkills(arGuidance, pageRedirection) 
{
	var code = $('#domainsCodes');
	var desc = $('#domainsDescriptions');
	var RowMax = 0;
	
	arGuidance.forEach(function(guidance) 
	{
		// Cherche le nombre maximum de ligne du tableau
		for (var i = 0; i < guidance.domains.length; i++)
		{
			var nb = guidance.domains[i]["skills"].length;
			if(nb > RowMax)
				RowMax = nb; 
		}
		
		guidance.domains.forEach(function(domain)
		{
			// Ajoute le domaine compétence
			var tdcode = $('<td class="skillDomainTd" id="domain' + domain.domainid +'">');
			var tddesc = $('<td class="skillDomainTd" id="domaindescription' + domain.domainid +'">');
			tdcode.html(domain.domainid);
			tddesc.html(domain.domaindescription);
			code.append(tdcode);
			desc.append(tddesc);
			
			var cptrow = 0; // Numero de la ligne
			var cptskill = 1; // Faux id
						
			// Ajoute les compétences opérationnelles
			domain.skills.forEach(function(skill)
			{	
				// Fabrique une nouvelle ligne si elle n'éxiste pas
				if($('#row' + cptrow).length == 0)
					$('#bodySkill').append('<tr id="row' + cptrow + '"></tr>');
				
				// Remplie les cases en colonne avec les données
				var line = $('#row' + cptrow);
				var td = $('<td id="'+ skill.owner + "-" + skill.skillid + '" style="position:relative;">'); 
				td.html('<a style="color: black; text-decoration: none;" onclick="appendFilterToURL(this)" href="'+ pageRedirection +'?guidanceid='+ guidance.guidanceid +'&domainid='+skill.owner+'&skillid=' + skill.skillid + '"><b>' + skill.owner + cptskill + '</b>: ' + skill.skillname + '</a>');
				line.append(td);
				cptskill++;			
				cptrow++;
					
			});			
			
			// Ajoute des cases vide pour aligner les colonnes suivantes
			for (var i = domain.skills.length; i < RowMax; i++)
			{
				if($('#row' + i).length == 0)
					$('#bodySkill').append('<tr id="row' + i + '"></tr>');
				
				$('#row' + i).append('<td></td>');
			}	
		});	
	});
}