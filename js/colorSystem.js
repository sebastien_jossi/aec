/**
 * @copyright ronaldo.lrrpnt@eduge.ch 2016-2017
 */

/**
 * Fonction qui colore les cases du tableau des apprentis (Système de couleur)
 * @author RLP
*/

/** Prend les pratiques professionnelles qui serviront à la création du tableau des élèves 
 * 	@param arDataStudent Contient les données de l'apprentis
 * 	@param guidanceId L'identifiant de l'orientation
 */
function getPracticesForColorSystem(arDataStudent, guidanceId, yearId = undefined)
{	
	get_data('../../php/loadallhierarchybyguidance.php', colorSystem, {'guidanceId' : guidanceId, 'yearId': yearId}, true, arDataStudent);
	
	/**
	 * Fait la moyenne des évaluations d'un apprentis et mets la couleur de la case en fonction du code couleur
	 * @param arDataStudent Tableau contenant les données de l'appentis
	 * @param arPractices Tableau contenant les données des pratiques professionnelles
	 * */
	function colorSystem(arPractices, arDataStudent)
	{	
		arDataStudent[0].forEach(function(student)
		{
			// Parcours chaque case du tableau de l'élève
			$('#student' + student.studentid + ' > tr').find('td').each(function(tdcase, td) 
			{
				var notevaluation = true;
				
				var cptPasVu = 0;	
				var cptExplique = 0;
				var cptExerce = 0;
				var cptAutonome = 0;
				
				// Parcours les domaines
				arPractices[0]['domains'].forEach(function(domain)
				{
					domain.skills.forEach(function(skill)
					{
						if($(td).attr('id') == skill.owner + "-" + skill.skillid)
						{
							student.datapractices.forEach(function(dpractice)
							{	
								skill.practices.forEach(function(practice)
								{
									var domainId = $(td).attr('id').split("-")[0];
									// Si c'est la bonne case les compteur vont être incrémenté
									if(practice.practiceid == dpractice.practiceid && practice.owner == dpractice.skillid && domainId == dpractice.domainid)
									{
										notevaluation = false;
										// Compte le nombre de Pas Vu, Expliqué, Exercé, Autonome que les apprentis ont sélection pour la pratique professionnelle
										switch (dpractice.stateid)
										{
											case "1":
												cptPasVu++;
												break;
											case "2":
												cptExplique++;
												break;
											case "3":
												cptExerce++;
												break;
											case "4":
												cptAutonome++;
												break;
										}	
									}													
								});	
							});
							// Système de couleur
							switch(true)
							{
								case notevaluation == true:
									$(td).css("background-color", "white");
									$(td).find('a').css("color", "black");
									break;
								case cptAutonome > 0:
									if (cptExerce > 0 || cptExplique > 0 || cptPasVu > 0)
										$(td).attr('class', 'td-1Aut');
									else
										$(td).attr('class', 'td-aut');
									$(td).find('a').css("color", "black");
									break;
								case cptExerce > 0:
									if (cptExplique > 0 || cptPasVu > 0)
										$(td).attr('class', 'td-1Exc');
									else
										$(td).attr('class', 'td-exc');
									$(td).find('a').attr("style", "color: black!important; text-decoration: none;");
									$(td).find('b').attr("style", "color: black!important;");
									break;
								case cptExplique > 0:
									if (cptPasVu > 0)
										$(td).attr('class', 'td-1Exp');
									else
										$(td).attr('class', 'td-exp');
									$(td).find('a').css("color", "black");
									break;
								case cptPasVu > 0:
									$(td).attr('class', 'td-pasVu');
									$(td).find('a').attr("style", "color: white!important; text-decoration: none;");
									$(td).find('b').attr("style", "color: white!important;");
									break;
							}	
						}
					});	
				});
			});
		});
	}
}